# MetHoS

MetHoS is a web-based platform for large-scale metabolomics. It is written in java that utilizes a set of software tools in order to achieve parallel processing, distributed storage and distributed statistical analysis of metabolomics data.