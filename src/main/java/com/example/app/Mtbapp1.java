package com.example.app;

import com.example.app.storage.StorageProperties;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.properties.EnableConfigurationProperties;


@SpringBootApplication
@EnableConfigurationProperties(StorageProperties.class)
public class Mtbapp1 {

	public static void main(String[] args) {
                        
		SpringApplication.run(Mtbapp1.class, args);
	}
        
     
}
