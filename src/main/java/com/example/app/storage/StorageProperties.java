/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.example.app.storage;

/**
 *
 * @author kostas
 */

import org.springframework.boot.context.properties.ConfigurationProperties;

@ConfigurationProperties("storage")
public class StorageProperties {

    private String location = "/home/kostas"; //for local
    private String locationserver = "/home/ubuntu"; //for server
    
    
    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }
    public String getLocationserver() {
        return locationserver;
    }

    public void setLocationserver(String locationserver) {
        this.locationserver = locationserver;
    }
}
