/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.example.app.converters;

import com.example.app.commands.UserProjectForm;
import com.example.app.domain.UserProject;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

/**
 *
 * @author kostas
 */
@Component
public class UserProjectToUserProjectForm implements Converter<UserProject, UserProjectForm>{
    
    @Override
    public UserProjectForm convert(UserProject userproject){
        UserProjectForm userproform =new UserProjectForm();
        userproform.setUserProjectKey(userproject.getUserProjectKey());
        userproform.setOwner(userproject.getOwner());
        userproform.setAccess(userproject.getAccess());
        userproform.setKnimeprocessing(userproject.getKnimeprocessing());
        userproform.setSparkanalysis(userproject.getSparkanalysis());
        return userproform;
    }

}
