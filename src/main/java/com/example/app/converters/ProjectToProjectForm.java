/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.example.app.converters;

import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

import com.example.app.domain.Project;
import com.example.app.commands.ProjectsForm;
/**
 *
 * @author kostas
 */
@Component
public class ProjectToProjectForm implements Converter<Project, ProjectsForm>{
    @Override
    public ProjectsForm convert(Project projects){
        ProjectsForm proform =new ProjectsForm();
        proform.setProjectid(projects.getProjectid());
        proform.setDatabasename(projects.getDatabasename());
        proform.setProname(projects.getProname());
        proform.setProdatecreated(projects.getProdatecreated());
        proform.setProdateedited(projects.getProdateedited());
        proform.setProcomments(projects.getProcomments());
        return proform;
    }
}
