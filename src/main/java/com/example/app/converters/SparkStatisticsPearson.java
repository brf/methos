/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.example.app.converters;

import com.datastax.spark.connector.japi.CassandraJavaUtil;
import com.datastax.spark.connector.japi.rdd.CassandraJavaPairRDD;
import com.example.app.domain.ExperimentFormExDes;
import com.example.app.domain.ExperimentFormExDesFl;
import com.example.app.domain.ExperimentFormExDesFlIn;
import com.example.app.domain.ExperimentFormExDesIn;
import com.example.app.domain.ExperimentFormOnlyParKey;
import com.example.app.commands.Explistrepliclistwithcorrpearsonspearman;
import com.example.app.domain.ExperimentFormExDesAd;
import com.example.app.domain.ExperimentFormExDesInAd;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import org.apache.spark.api.java.function.MapFunction;

import org.apache.spark.ml.feature.Imputer;
import org.apache.spark.ml.feature.ImputerModel;
import org.apache.spark.ml.feature.VectorAssembler;
import org.apache.spark.ml.linalg.Matrix;

import org.apache.spark.ml.stat.Correlation;
import org.apache.spark.sql.Dataset;
import org.apache.spark.sql.Encoder;
import org.apache.spark.sql.Encoders;
import org.apache.spark.sql.Row;
import org.apache.spark.sql.RowFactory;
import org.apache.spark.sql.SparkSession;
import org.apache.spark.sql.catalyst.encoders.RowEncoder;
import org.apache.spark.sql.expressions.Window;
import org.apache.spark.sql.functions;
import static org.apache.spark.sql.functions.asc;
import static org.apache.spark.sql.functions.col;

import static org.apache.spark.sql.functions.lit;
import static org.apache.spark.sql.functions.sum;
import org.apache.spark.sql.types.DataTypes;
import org.apache.spark.sql.types.Metadata;
import org.apache.spark.sql.types.StructField;
import org.apache.spark.sql.types.StructType;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 *
 * @author kostas
 */
@Component
public class SparkStatisticsPearson {
    @Autowired
    private SparkSession sp;
    
    public List<String> omitInExperiments(List<String> expids,String ionmode){
        List<String> metlist = new ArrayList<>();
        
        Dataset<Row> metlistinitial =sp.emptyDataFrame();
        if(ionmode==null||ionmode.equals("both")){
            Encoder<ExperimentFormOnlyParKey> ExpEncoder = Encoders.bean(ExperimentFormOnlyParKey.class);
            Encoder<ExperimentFormExDes> ExpEncoderoftwo = Encoders.bean(ExperimentFormExDes.class);
            Dataset<ExperimentFormOnlyParKey> dfexplistoriginal = sp.createDataset(expids, Encoders.STRING()).toDF("experimentid").as(ExpEncoder);
            CassandraJavaPairRDD<ExperimentFormOnlyParKey, ExperimentFormExDes> predf = CassandraJavaUtil.javaFunctions(CassandraJavaUtil.javaFunctions(dfexplistoriginal.javaRDD()).repartitionByCassandraReplica("mdb","experiment",100,CassandraJavaUtil.someColumns("experimentid"),CassandraJavaUtil.mapToRow(ExperimentFormOnlyParKey.class)))
                                                                                .joinWithCassandraTable("mdb","experiment", CassandraJavaUtil.someColumns("experimentid","description"), CassandraJavaUtil.someColumns("experimentid"), CassandraJavaUtil.mapRowTo(ExperimentFormExDes.class), CassandraJavaUtil.mapToRow(ExperimentFormOnlyParKey.class));
            metlistinitial =sp.createDataset(predf.map(x->x._2).rdd(), ExpEncoderoftwo).toDF( "description","experimentid").groupBy(col("experimentid"),col("description")).count();
        }else if(ionmode.equals("positive")){
            Encoder<ExperimentFormOnlyParKey> ExpEncoder = Encoders.bean(ExperimentFormOnlyParKey.class);
            Encoder<ExperimentFormExDesAd> ExpEncoderofthree = Encoders.bean(ExperimentFormExDesAd.class);
            Dataset<ExperimentFormOnlyParKey> dfexplistoriginal = sp.createDataset(expids, Encoders.STRING()).toDF("experimentid").as(ExpEncoder); 
            CassandraJavaPairRDD<ExperimentFormOnlyParKey, ExperimentFormExDesAd> predf = CassandraJavaUtil.javaFunctions(CassandraJavaUtil.javaFunctions(dfexplistoriginal.javaRDD()).repartitionByCassandraReplica("mdb","experiment",100,CassandraJavaUtil.someColumns("experimentid"),CassandraJavaUtil.mapToRow(ExperimentFormOnlyParKey.class)))
                                                                                .joinWithCassandraTable("mdb","experiment", CassandraJavaUtil.someColumns("experimentid","description","adduct"), CassandraJavaUtil.someColumns("experimentid"), CassandraJavaUtil.mapRowTo(ExperimentFormExDesAd.class), CassandraJavaUtil.mapToRow(ExperimentFormOnlyParKey.class));
            Dataset<Row> metlistinitialbef =sp.createDataset(predf.map(x->x._2).rdd(), ExpEncoderofthree).toDF( "adduct","description","experimentid");
            Dataset<Row> metlistinitialbe= metlistinitialbef.withColumn("ioniz", functions.substring(col("adduct"), -1, 1)).drop("adduct").filter(col("ioniz").equalTo("+"));
            metlistinitial = metlistinitialbe.drop(col("ioniz")).groupBy(col("experimentid"),col("description")).count();
        }
        else if(ionmode.equals("negative")){
            Encoder<ExperimentFormOnlyParKey> ExpEncoder = Encoders.bean(ExperimentFormOnlyParKey.class);
            Encoder<ExperimentFormExDesAd> ExpEncoderofthree = Encoders.bean(ExperimentFormExDesAd.class);
            Dataset<ExperimentFormOnlyParKey> dfexplistoriginal = sp.createDataset(expids, Encoders.STRING()).toDF("experimentid").as(ExpEncoder); 
            CassandraJavaPairRDD<ExperimentFormOnlyParKey, ExperimentFormExDesAd> predf = CassandraJavaUtil.javaFunctions(CassandraJavaUtil.javaFunctions(dfexplistoriginal.javaRDD()).repartitionByCassandraReplica("mdb","experiment",100,CassandraJavaUtil.someColumns("experimentid"),CassandraJavaUtil.mapToRow(ExperimentFormOnlyParKey.class)))
                                                                                .joinWithCassandraTable("mdb","experiment", CassandraJavaUtil.someColumns("experimentid","description","adduct"), CassandraJavaUtil.someColumns("experimentid"), CassandraJavaUtil.mapRowTo(ExperimentFormExDesAd.class), CassandraJavaUtil.mapToRow(ExperimentFormOnlyParKey.class));
            Dataset<Row> metlistinitialbef =sp.createDataset(predf.map(x->x._2).rdd(), ExpEncoderofthree).toDF( "adduct","description","experimentid");
            Dataset<Row> metlistinitialbe= metlistinitialbef.withColumn("ioniz", functions.substring(col("adduct"), -1, 1)).drop("adduct").filter(col("ioniz").equalTo("-"));
            metlistinitial = metlistinitialbe.drop(col("ioniz")).groupBy(col("experimentid"),col("description")).count();
        }
        
        Dataset<Row> metnamesandocur=metlistinitial.select(col("description")).groupBy(col("description")).count().orderBy( asc("description"));
        int expnum=expids.size();
        Dataset<Row> metnamesandocurinall =metnamesandocur.select(col("description"),col("count")).filter(col("description").notEqual("Unidentified")).filter(col("count").equalTo(expnum));
        metlist = metnamesandocurinall.select(col("description")).as(Encoders.STRING()).collectAsList();
        return metlist;
    }

    
    public List<String> omitInReplicates(List<String> expids){
        Dataset<Row> dfexplist = sp.createDataset(expids, Encoders.STRING()).toDF("experimentid");
        List<String> metlist = new ArrayList<>();
        Dataset<Row> expsidsandreplnum = sp.read().format("org.apache.spark.sql.cassandra")
                .options(new HashMap<String, String>() {
                    {
                        put("keyspace", "mdb");
                        put("table", "projectexperiment");
                    }
                })
                .load().select(col("experimentid"),col("replicatesnum")).join(dfexplist,"experimentid");

        List<Long> totalreplnumlist=expsidsandreplnum.select(col("replicatesnum")).agg(sum(col("replicatesnum"))).as(Encoders.LONG()).collectAsList();
        Long totalreplnum = totalreplnumlist.get(0);

        Encoder<ExperimentFormOnlyParKey> ExpEncoder = Encoders.bean(ExperimentFormOnlyParKey.class);
        Encoder<ExperimentFormExDesFl> ExpEncoderofthree = Encoders.bean(ExperimentFormExDesFl.class);
        Dataset<ExperimentFormOnlyParKey> dfexplistoriginal = sp.createDataset(expids, Encoders.STRING()).toDF("experimentid").as(ExpEncoder);
        CassandraJavaPairRDD<ExperimentFormOnlyParKey, ExperimentFormExDesFl> predf2 = CassandraJavaUtil.javaFunctions(CassandraJavaUtil.javaFunctions(dfexplistoriginal.javaRDD()).repartitionByCassandraReplica("mdb","experiment",100,CassandraJavaUtil.someColumns("experimentid"),CassandraJavaUtil.mapToRow(ExperimentFormOnlyParKey.class)))
                                                                            .joinWithCassandraTable("mdb","experiment", CassandraJavaUtil.someColumns("experimentid","description","filename"), CassandraJavaUtil.someColumns("experimentid"), CassandraJavaUtil.mapRowTo(ExperimentFormExDesFl.class), CassandraJavaUtil.mapToRow(ExperimentFormOnlyParKey.class));
        Dataset<Row> metlistinitial =sp.createDataset(predf2.map(x->x._2).rdd(), ExpEncoderofthree).toDF( "description","experimentid","filename").groupBy(col("experimentid"),col("filename"),col("description")).count();
       
        Dataset<Row> metnamesandocur=metlistinitial.select(col("description")).groupBy(col("description")).count().orderBy( asc("description"));
        Dataset<Row> metnamesandocurinall =metnamesandocur.select(col("description"),col("count")).filter(col("description").notEqual("Unidentified")).filter(col("count").equalTo(totalreplnum));
        metlist = metnamesandocurinall.select(col("description")).as(Encoders.STRING()).collectAsList();
        
        return metlist;
    }
    
    public List<String> replaceWithValueInExperiments (List<String> expids,String ionmode){
        List<String> metlist = new ArrayList<>();
       
        Dataset<Row> metlistinitial =sp.emptyDataFrame();
        if(ionmode==null||ionmode.equals("both")){
            Encoder<ExperimentFormOnlyParKey> ExpEncoder = Encoders.bean(ExperimentFormOnlyParKey.class);
            Encoder<ExperimentFormExDes> ExpEncoderoftwo = Encoders.bean(ExperimentFormExDes.class);
            Dataset<ExperimentFormOnlyParKey> dfexplistoriginal = sp.createDataset(expids, Encoders.STRING()).toDF("experimentid").as(ExpEncoder);
            CassandraJavaPairRDD<ExperimentFormOnlyParKey, ExperimentFormExDes> predf = CassandraJavaUtil.javaFunctions(CassandraJavaUtil.javaFunctions(dfexplistoriginal.javaRDD()).repartitionByCassandraReplica("mdb","experiment",100,CassandraJavaUtil.someColumns("experimentid"),CassandraJavaUtil.mapToRow(ExperimentFormOnlyParKey.class)))
                                                                                .joinWithCassandraTable("mdb","experiment", CassandraJavaUtil.someColumns("experimentid","description"), CassandraJavaUtil.someColumns("experimentid"), CassandraJavaUtil.mapRowTo(ExperimentFormExDes.class), CassandraJavaUtil.mapToRow(ExperimentFormOnlyParKey.class));
            metlistinitial =sp.createDataset(predf.map(x->x._2).rdd(), ExpEncoderoftwo).toDF( "description","experimentid").filter(col("description").notEqual("Unidentified"));
        }else if(ionmode.equals("positive")){
            Encoder<ExperimentFormOnlyParKey> ExpEncoder = Encoders.bean(ExperimentFormOnlyParKey.class);
            Encoder<ExperimentFormExDesAd> ExpEncoderofthree = Encoders.bean(ExperimentFormExDesAd.class);
            Dataset<ExperimentFormOnlyParKey> dfexplistoriginal = sp.createDataset(expids, Encoders.STRING()).toDF("experimentid").as(ExpEncoder); 
            CassandraJavaPairRDD<ExperimentFormOnlyParKey, ExperimentFormExDesAd> predf = CassandraJavaUtil.javaFunctions(CassandraJavaUtil.javaFunctions(dfexplistoriginal.javaRDD()).repartitionByCassandraReplica("mdb","experiment",100,CassandraJavaUtil.someColumns("experimentid"),CassandraJavaUtil.mapToRow(ExperimentFormOnlyParKey.class)))
                                                                                .joinWithCassandraTable("mdb","experiment", CassandraJavaUtil.someColumns("experimentid","description","adduct"), CassandraJavaUtil.someColumns("experimentid"), CassandraJavaUtil.mapRowTo(ExperimentFormExDesAd.class), CassandraJavaUtil.mapToRow(ExperimentFormOnlyParKey.class));
            Dataset<Row> metlistinitialbef =sp.createDataset(predf.map(x->x._2).rdd(), ExpEncoderofthree).toDF( "adduct","description","experimentid").filter(col("description").notEqual("Unidentified"));
            Dataset<Row> metlistinitialbe= metlistinitialbef.withColumn("ioniz", functions.substring(col("adduct"), -1, 1)).drop("adduct").filter(col("ioniz").equalTo("+"));
            metlistinitial = metlistinitialbe.drop(col("ioniz"));
        }
        else if(ionmode.equals("negative")){
            Encoder<ExperimentFormOnlyParKey> ExpEncoder = Encoders.bean(ExperimentFormOnlyParKey.class);
            Encoder<ExperimentFormExDesAd> ExpEncoderofthree = Encoders.bean(ExperimentFormExDesAd.class);
            Dataset<ExperimentFormOnlyParKey> dfexplistoriginal = sp.createDataset(expids, Encoders.STRING()).toDF("experimentid").as(ExpEncoder);
            CassandraJavaPairRDD<ExperimentFormOnlyParKey, ExperimentFormExDesAd> predf = CassandraJavaUtil.javaFunctions(CassandraJavaUtil.javaFunctions(dfexplistoriginal.javaRDD()).repartitionByCassandraReplica("mdb","experiment",100,CassandraJavaUtil.someColumns("experimentid"),CassandraJavaUtil.mapToRow(ExperimentFormOnlyParKey.class)))
                                                                                .joinWithCassandraTable("mdb","experiment", CassandraJavaUtil.someColumns("experimentid","description","adduct"), CassandraJavaUtil.someColumns("experimentid"), CassandraJavaUtil.mapRowTo(ExperimentFormExDesAd.class), CassandraJavaUtil.mapToRow(ExperimentFormOnlyParKey.class));
            Dataset<Row> metlistinitialbef =sp.createDataset(predf.map(x->x._2).rdd(), ExpEncoderofthree).toDF( "adduct","description","experimentid").filter(col("description").notEqual("Unidentified"));
            Dataset<Row> metlistinitialbe= metlistinitialbef.withColumn("ioniz", functions.substring(col("adduct"), -1, 1)).drop("adduct").filter(col("ioniz").equalTo("-"));
            metlistinitial = metlistinitialbe.drop(col("ioniz"));
        }
        
        Dataset<Row> mtbdistinct =metlistinitial.select(col("description")).distinct();
        metlist = mtbdistinct.as(Encoders.STRING()).collectAsList();
        return metlist;
    }
    
    public List<String> replaceWithValueInReplicates(List<String> expids){
        List<String> metlist = new ArrayList<>();
        
        Encoder<ExperimentFormOnlyParKey> ExpEncoder = Encoders.bean(ExperimentFormOnlyParKey.class);
        Encoder<ExperimentFormExDes> ExpEncoderoftwo = Encoders.bean(ExperimentFormExDes.class);
        Dataset<ExperimentFormOnlyParKey> dfexplistoriginal = sp.createDataset(expids, Encoders.STRING()).toDF("experimentid").as(ExpEncoder);
        CassandraJavaPairRDD<ExperimentFormOnlyParKey, ExperimentFormExDes> predf = CassandraJavaUtil.javaFunctions(CassandraJavaUtil.javaFunctions(dfexplistoriginal.javaRDD()).repartitionByCassandraReplica("mdb","experiment",100,CassandraJavaUtil.someColumns("experimentid"),CassandraJavaUtil.mapToRow(ExperimentFormOnlyParKey.class)))
                                                                            .joinWithCassandraTable("mdb","experiment", CassandraJavaUtil.someColumns("experimentid","description"), CassandraJavaUtil.someColumns("experimentid"), CassandraJavaUtil.mapRowTo(ExperimentFormExDes.class), CassandraJavaUtil.mapToRow(ExperimentFormOnlyParKey.class));
        Dataset<Row> metlistinitial =sp.createDataset(predf.map(x->x._2).rdd(), ExpEncoderoftwo).toDF( "description","experimentid").filter(col("description").notEqual("Unidentified"));
        
        Dataset<Row> mtbdistinct =metlistinitial.select(col("description")).distinct();
        metlist = mtbdistinct.as(Encoders.STRING()).collectAsList();

        return metlist;
    }
   
    public Explistrepliclistwithcorrpearsonspearman pearson(List<String> experimentlist,List<String>metabolitelist,String allorsome,String missval,String ionmode){
        Explistrepliclistwithcorrpearsonspearman exlistwithexpcorrandmetlistwithmetcorr = new Explistrepliclistwithcorrpearsonspearman();
        List<Double[][]> expsreplcorr= new ArrayList<>();
        List<String> missvalmtbsexp = new ArrayList<>();
        List<String> missvalmtbsrepl = new ArrayList<>();
        Integer firstrowflag=0;
        Integer secondrowflag=0;
        if(allorsome.equals("onallexprepl")){
            
            if(missval.equals("omit")){
                List<String>commonmtbsfromexps=omitInExperiments(experimentlist,ionmode);
                List<String>commonmtbsfromrepls=omitInReplicates(experimentlist);
                
                StructType schema = new StructType(new StructField[] {
                                new StructField("metabs",
                                                DataTypes.StringType, false,
                                                Metadata.empty()) });
                //for experiments
                if (commonmtbsfromexps != null && !commonmtbsfromexps.isEmpty() && commonmtbsfromexps.size()>1) {
                    
                    Encoder<ExperimentFormOnlyParKey> ExpEncoder = Encoders.bean(ExperimentFormOnlyParKey.class);
                    Encoder<ExperimentFormExDesIn> ExpEncoderofthree = Encoders.bean(ExperimentFormExDesIn.class);
                    Dataset<ExperimentFormOnlyParKey> dfexplistoriginal = sp.createDataset(experimentlist, Encoders.STRING()).toDF("experimentid").as(ExpEncoder);
                    CassandraJavaPairRDD<ExperimentFormOnlyParKey, ExperimentFormExDesIn> predf = CassandraJavaUtil.javaFunctions(CassandraJavaUtil.javaFunctions(dfexplistoriginal.javaRDD()).repartitionByCassandraReplica("mdb","experiment",100,CassandraJavaUtil.someColumns("experimentid"),CassandraJavaUtil.mapToRow(ExperimentFormOnlyParKey.class)))
                                                                                        .joinWithCassandraTable("mdb","experiment", CassandraJavaUtil.someColumns("experimentid","description","intensity"), CassandraJavaUtil.someColumns("experimentid"), CassandraJavaUtil.mapRowTo(ExperimentFormExDesIn.class), CassandraJavaUtil.mapToRow(ExperimentFormOnlyParKey.class));
                    Dataset<Row> explistonallmet =sp.createDataset(predf.map(x->x._2).rdd(), ExpEncoderofthree).toDF( "description","experimentid","intensity");

                    Dataset<Row> exponallmet = explistonallmet.filter(col("description").notEqual("Unidentified")).groupBy(explistonallmet.col("description"), explistonallmet.col("experimentid")).mean("intensity").orderBy(asc("experimentid"), asc("description"));
                    Dataset<Row> expavgonallmet = exponallmet.groupBy(exponallmet.col("description")).pivot("experimentid",new ArrayList<Object>(experimentlist)).mean("avg(intensity)").persist();
                    String[] expincols = experimentlist.parallelStream().toArray(String[]::new);
                    exlistwithexpcorrandmetlistwithmetcorr.setExplist(experimentlist);
                    
                    //finding missing metabolites in experiments
                    Dataset<Row> tempmissvalmtbsex=expavgonallmet.map((MapFunction<Row, Row>) originalrow -> {
                        if(originalrow.anyNull()){
                            String rowValues = new String();
                            rowValues=originalrow.get(0).toString();
                            Row newrow = RowFactory.create(rowValues);
                            return newrow;
                        }else{
                            String rowValues = "notmissing";
                            Row newrow = RowFactory.create(rowValues);
                            return newrow;
                        }
                    }, RowEncoder.apply(schema));
                    Dataset<Row> missvalmetabsex=tempmissvalmtbsex.select("metabs").filter(col("metabs").notEqual("notmissing"));
                    missvalmtbsexp=missvalmetabsex.as(Encoders.STRING()).collectAsList();
                    exlistwithexpcorrandmetlistwithmetcorr.setMissvalmtbsexp(missvalmtbsexp);
                    
                    Dataset<Row> newexpavgonallmet=expavgonallmet.na().drop();
                    
                    VectorAssembler assemblerexp1 = new VectorAssembler()
                            .setInputCols(expincols)
                            .setOutputCol("intensity");
                    Dataset<Row> expoutput1 = assemblerexp1.transform(newexpavgonallmet);

                    Row r1exp1 = Correlation.corr(expoutput1, "intensity").head();
                    Matrix r2exp1= r1exp1.getAs(0);
                    Double[][] r3exp1=new Double[r2exp1.numRows()][r2exp1.numCols()];
                    for (int i = 0; i < r2exp1.numRows(); i++) {
                        for (int j = 0; j < r2exp1.numCols(); j++) {
                            r2exp1.update(i, j, (long)(r2exp1.apply(i,j)* 1e2)/1e2);
                            r3exp1[i][j]=r2exp1.apply(i,j);
                        }
                    }
                    expsreplcorr.add(r3exp1);
                    exlistwithexpcorrandmetlistwithmetcorr.setExpcorrreplcorr(expsreplcorr);
                    expavgonallmet.unpersist();
                }else{
                    firstrowflag=1;
                }
                //for replicates
                if(commonmtbsfromrepls != null && !commonmtbsfromrepls.isEmpty() && commonmtbsfromrepls.size()>1){
                    
                    Encoder<ExperimentFormOnlyParKey> ExpEncoder = Encoders.bean(ExperimentFormOnlyParKey.class);
                    Encoder<ExperimentFormExDesFlIn> ExpEncoderoffour = Encoders.bean(ExperimentFormExDesFlIn.class);
                    Dataset<ExperimentFormOnlyParKey> dfexplistoriginal = sp.createDataset(experimentlist, Encoders.STRING()).toDF("experimentid").as(ExpEncoder);
                    CassandraJavaPairRDD<ExperimentFormOnlyParKey, ExperimentFormExDesFlIn> predf = CassandraJavaUtil.javaFunctions(CassandraJavaUtil.javaFunctions(dfexplistoriginal.javaRDD()).repartitionByCassandraReplica("mdb","experiment",100,CassandraJavaUtil.someColumns("experimentid"),CassandraJavaUtil.mapToRow(ExperimentFormOnlyParKey.class)))
                                                                                        .joinWithCassandraTable("mdb","experiment", CassandraJavaUtil.someColumns("experimentid","filename","description","intensity"), CassandraJavaUtil.someColumns("experimentid"), CassandraJavaUtil.mapRowTo(ExperimentFormExDesFlIn.class), CassandraJavaUtil.mapToRow(ExperimentFormOnlyParKey.class));
                    Dataset<Row> metlistinitial =sp.createDataset(predf.map(x->x._2).rdd(), ExpEncoderoffour).toDF( "description","experimentid","filename","intensity");
            
                    Dataset<Row> metfinalpeaks = metlistinitial.filter(col("description").notEqual("Unidentified")).groupBy(metlistinitial.col("experimentid"), metlistinitial.col("description"),metlistinitial.col("filename")).avg("intensity").orderBy(asc("experimentid"), asc("description"));

                    Dataset<Row> newmetfinalpeaks = metfinalpeaks.withColumn("expidfilename", functions.concat(col("experimentid"), lit("__"), col("filename"))).drop("experimentid","filename");
                    Dataset<Row> newmetfinalpeaksafterrename=newmetfinalpeaks.map((MapFunction<Row, Row>) originalrow -> {
                        String replicatewithdot = originalrow.get(2).toString();
                        String temp = replicatewithdot.replaceAll("\\.[mzmlxdatMZMLXDAT]{4,6}$","");
                        if(temp.contains(".")){
                                    temp=temp.replaceAll("[.]", "_");
                                }
                        Object[] rowValues = new Object[originalrow.length()];
                        rowValues[0]=originalrow.get(0);
                        rowValues[1]=originalrow.get(1);
                        rowValues[2]=temp;
                        Row newrow = RowFactory.create(rowValues);
                        return newrow;
                    }, RowEncoder.apply(newmetfinalpeaks.schema())).persist();
                    
                    Dataset<Row> filenameavgpeaks = newmetfinalpeaksafterrename.groupBy(newmetfinalpeaksafterrename.col("description")).pivot("expidfilename").mean("avg(intensity)").persist();

                    String[] temp1 = filenameavgpeaks.columns();
                    List<String>temprepl = new ArrayList<>();
                    for(String finame:temp1){
                        if(!finame.equals("description")){
                            temprepl.add(finame);
                        }
                    }

                    String[] replincols = temprepl.parallelStream().toArray(String[]::new);
                    exlistwithexpcorrandmetlistwithmetcorr.setReplilist(temprepl);

                    //metabolites with missing values in replicates
                    Dataset<Row> tempmissvalmtbsrepl=filenameavgpeaks.map((MapFunction<Row, Row>) originalrow -> {
                        if(originalrow.anyNull()){
                            String rowValues = new String();
                            rowValues=originalrow.get(0).toString();
                            Row newrow = RowFactory.create(rowValues);
                            return newrow;
                        }else{
                            String rowValues = "notmissing";
                            Row newrow = RowFactory.create(rowValues);
                            return newrow;
                        }
                    }, RowEncoder.apply(schema));
                    Dataset<Row> missvalmetabsrepl=tempmissvalmtbsrepl.select("metabs").filter(col("metabs").notEqual("notmissing"));
                    missvalmtbsrepl=missvalmetabsrepl.as(Encoders.STRING()).collectAsList();
                    exlistwithexpcorrandmetlistwithmetcorr.setMissvalmtbsrepl(missvalmtbsrepl);
                    
                    Dataset<Row> newfilenameavgpeaksrepl=filenameavgpeaks.na().drop();
                    VectorAssembler assemblerexp2 = new VectorAssembler()
                            .setInputCols(replincols)
                            .setOutputCol("intensity");
                    Dataset<Row> expoutput2 = assemblerexp2.transform(newfilenameavgpeaksrepl);

                    Row r1repl1 = Correlation.corr(expoutput2, "intensity").head();
                    Matrix r2repl1= r1repl1.getAs(0);
                    Double[][] r3repl1=new Double[r2repl1.numRows()][r2repl1.numCols()];
                    for (int i = 0; i < r2repl1.numRows(); i++) {
                        for (int j = 0; j < r2repl1.numCols(); j++) {
                            r2repl1.update(i, j, (long)(r2repl1.apply(i,j)* 1e2)/1e2);
                            r3repl1[i][j]=r2repl1.apply(i,j);
                        }
                    }
                    expsreplcorr.add(r3repl1);
                    exlistwithexpcorrandmetlistwithmetcorr.setExpcorrreplcorr(expsreplcorr);
                    newmetfinalpeaksafterrename.unpersist();
                    filenameavgpeaks.unpersist();
                }else{
                    secondrowflag=1;
                }
            }else{
                Dataset<Row> newfilenameavgpeaksexps = sp.emptyDataFrame();
                Dataset<Row> newfilenameavgpeaksrepl = sp.emptyDataFrame();
                List<String>commonmtbsfromexps=replaceWithValueInExperiments(experimentlist,ionmode);
                List<String>commonmtbsfromrepls=commonmtbsfromexps;
                //for experiments
                if (commonmtbsfromexps != null && !commonmtbsfromexps.isEmpty() ) {
                    
                    Encoder<ExperimentFormOnlyParKey> ExpEncoder = Encoders.bean(ExperimentFormOnlyParKey.class);
                    Encoder<ExperimentFormExDesIn> ExpEncoderofthree = Encoders.bean(ExperimentFormExDesIn.class);
                    Dataset<ExperimentFormOnlyParKey> dfexplistoriginal = sp.createDataset(experimentlist, Encoders.STRING()).toDF("experimentid").as(ExpEncoder);
                    CassandraJavaPairRDD<ExperimentFormOnlyParKey, ExperimentFormExDesIn> predf = CassandraJavaUtil.javaFunctions(CassandraJavaUtil.javaFunctions(dfexplistoriginal.javaRDD()).repartitionByCassandraReplica("mdb","experiment",100,CassandraJavaUtil.someColumns("experimentid"),CassandraJavaUtil.mapToRow(ExperimentFormOnlyParKey.class)))
                                                                                        .joinWithCassandraTable("mdb","experiment", CassandraJavaUtil.someColumns("experimentid","description","intensity"), CassandraJavaUtil.someColumns("experimentid"), CassandraJavaUtil.mapRowTo(ExperimentFormExDesIn.class), CassandraJavaUtil.mapToRow(ExperimentFormOnlyParKey.class));
                    Dataset<Row> explistonallmet =sp.createDataset(predf.map(x->x._2).rdd(), ExpEncoderofthree).toDF( "description","experimentid","intensity");
                    
                    Dataset<Row> exponallmet = explistonallmet.groupBy(explistonallmet.col("description"), explistonallmet.col("experimentid")).mean("intensity").orderBy(asc("experimentid"), asc("description"));
                    Dataset<Row> expavgonallmet = exponallmet.groupBy(exponallmet.col("description")).pivot("experimentid",new ArrayList<Object>(experimentlist)).mean("avg(intensity)").persist();
                    String[] expincols = experimentlist.parallelStream().toArray(String[]::new);
                    exlistwithexpcorrandmetlistwithmetcorr.setExplist(experimentlist);

                    StructType schema1 = new StructType(new StructField[] {
                            new StructField("metabs",
                                            DataTypes.StringType, false,
                                            Metadata.empty()) });

                    Dataset<Row> tempmissvalmtbs1=expavgonallmet.map((MapFunction<Row, Row>) originalrow -> {
                        if(originalrow.anyNull()){
                            String rowValues = new String();
                            rowValues=originalrow.get(0).toString();
                            Row newrow = RowFactory.create(rowValues);
                            return newrow;
                        }else{
                            String rowValues = "notmissing";
                            Row newrow = RowFactory.create(rowValues);
                            return newrow;
                        }
                    }, RowEncoder.apply(schema1));
                    Dataset<Row> missvalmetabs1=tempmissvalmtbs1.select("metabs").filter(col("metabs").notEqual("notmissing"));
                    missvalmtbsexp=missvalmetabs1.as(Encoders.STRING()).collectAsList();
                    exlistwithexpcorrandmetlistwithmetcorr.setMissvalmtbsexp(missvalmtbsexp);

                    if(missval.equals("replacewithzero")){
                        newfilenameavgpeaksexps=expavgonallmet.na().fill(0);
                    }else if(missval.equals("replacewithmean")){
                        newfilenameavgpeaksexps=expavgonallmet.map((MapFunction<Row, Row>) originalrow -> {
                            if(originalrow.anyNull()){
                                int originalrowsizeofdoubles=originalrow.length()-1;
                                Double sumrow = Double.valueOf(0);
                                Double meanrow = Double.valueOf(0);

                                Object[] rowValues = new Object[originalrow.length()];
                                rowValues[0]=originalrow.get(0).toString();

                                List<Integer>indextobereplaced = new ArrayList<>();
                                List<Double> avgintensities = new ArrayList<>();
                                int validvalues=0;
                                for(int k=1;k<=originalrowsizeofdoubles;k++){
                                    if(originalrow.isNullAt(k)){
                                        indextobereplaced.add(k);
                                        avgintensities.add(null);
                                    }else{
                                        sumrow = sumrow + originalrow.getDouble(k);
                                        avgintensities.add(originalrow.getDouble(k));
                                        validvalues++;
                                    }
                                }
                                meanrow=sumrow/(validvalues);

                                int indextobereplacedsize=indextobereplaced.size();
                                for(int i=0;i<indextobereplacedsize;i++){
                                    avgintensities.set(indextobereplaced.get(i)-1, meanrow);

                                }

                                for(int l=0;l<avgintensities.size();l++){
                                    rowValues[l+1]=avgintensities.get(l);
                                }
                                Row newrow = RowFactory.create(rowValues);

                                return newrow;
                            }else{
                                return originalrow;
                            }
                        }, RowEncoder.apply(expavgonallmet.schema()));

                    }else if(missval.equals("replacewithmedian")){
                        newfilenameavgpeaksexps=expavgonallmet.map((MapFunction<Row, Row>) originalrow -> {
                            if(originalrow.anyNull()){
                                int originalrowsizeofdoubles=originalrow.length()-1;
                                Double medianrow = Double.valueOf(0);

                                Object[] rowValues = new Object[originalrow.length()];
                                rowValues[0]=originalrow.get(0).toString();

                                List<Integer>indextobereplaced = new ArrayList<>();
                                List<Double> medianintensities = new ArrayList<>();
                                List<Double> rowmedian = new ArrayList<>();
                                int validvalues=0;
                                for(int k=1;k<=originalrowsizeofdoubles;k++){
                                    if(originalrow.isNullAt(k)){
                                        indextobereplaced.add(k);
                                        medianintensities.add(null);
                                    }else{
                                        medianintensities.add(originalrow.getDouble(k));
                                        rowmedian.add(originalrow.getDouble(k));
                                    }
                                }
                                int rowmediannum=rowmedian.size();
                                Double[] sortedmedianintensities= new Double[rowmediannum];

                                for(int i=0;i<rowmediannum;i++){
                                    sortedmedianintensities[i]=rowmedian.get(i);
                                }
                                Arrays.sort(sortedmedianintensities);
                                if (rowmediannum ==1){
                                    medianrow=sortedmedianintensities[0];
                                }else if(rowmediannum>1){
                                    if(rowmediannum % 2 == 0){
                                        int pos1=(rowmediannum-1)/2;
                                        int pos2=pos1+1;

                                        medianrow=(sortedmedianintensities[pos1]+sortedmedianintensities[pos2])/2;
                                    }else{
                                        int pos = (rowmediannum/2);
                                        medianrow=sortedmedianintensities[pos];
                                    }
                                }

                                int indextobereplacedsize=indextobereplaced.size();
                                for(int i=0;i<indextobereplacedsize;i++){
                                    medianintensities.set(indextobereplaced.get(i)-1, medianrow);
                                }
                                for(int l=0;l<medianintensities.size();l++){
                                    rowValues[l+1]=medianintensities.get(l);
                                }
                                Row newrow = RowFactory.create(rowValues);

                                return newrow;
                            }else{
                                return originalrow;
                            }
                        }, RowEncoder.apply(expavgonallmet.schema()));
                    }

                    VectorAssembler assemblerexp1 = new VectorAssembler()
                            .setInputCols(expincols)
                            .setOutputCol("intensity");
                    Dataset<Row> expoutput1 = assemblerexp1.transform(newfilenameavgpeaksexps);

                    Row r1exp1 = Correlation.corr(expoutput1, "intensity").head();
                    Matrix r2exp1= r1exp1.getAs(0);
                    Double[][] r3exp1=new Double[r2exp1.numRows()][r2exp1.numCols()];
                    for (int i = 0; i < r2exp1.numRows(); i++) {
                        for (int j = 0; j < r2exp1.numCols(); j++) {
                            r2exp1.update(i, j, (long)(r2exp1.apply(i,j)* 1e2)/1e2);
                            r3exp1[i][j]=r2exp1.apply(i,j);
                        }
                    }
                    expsreplcorr.add(r3exp1);
                    exlistwithexpcorrandmetlistwithmetcorr.setExpcorrreplcorr(expsreplcorr);
                    expavgonallmet.unpersist();
                }else{
                    firstrowflag=1;
                }
                
                //for replicates
                if(commonmtbsfromrepls != null && !commonmtbsfromrepls.isEmpty()){
                    Encoder<ExperimentFormOnlyParKey> ExpEncoder = Encoders.bean(ExperimentFormOnlyParKey.class);
                    Encoder<ExperimentFormExDesFlIn> ExpEncoderoffour = Encoders.bean(ExperimentFormExDesFlIn.class);
                    Dataset<ExperimentFormOnlyParKey> dfexplistoriginal = sp.createDataset(experimentlist, Encoders.STRING()).toDF("experimentid").as(ExpEncoder);
                    CassandraJavaPairRDD<ExperimentFormOnlyParKey, ExperimentFormExDesFlIn> predf = CassandraJavaUtil.javaFunctions(CassandraJavaUtil.javaFunctions(dfexplistoriginal.javaRDD()).repartitionByCassandraReplica("mdb","experiment",100,CassandraJavaUtil.someColumns("experimentid"),CassandraJavaUtil.mapToRow(ExperimentFormOnlyParKey.class)))
                                                                                        .joinWithCassandraTable("mdb","experiment", CassandraJavaUtil.someColumns("experimentid","filename","description","intensity"), CassandraJavaUtil.someColumns("experimentid"), CassandraJavaUtil.mapRowTo(ExperimentFormExDesFlIn.class), CassandraJavaUtil.mapToRow(ExperimentFormOnlyParKey.class));
                    Dataset<Row> metlistinitial =sp.createDataset(predf.map(x->x._2).rdd(), ExpEncoderoffour).toDF( "description","experimentid","filename","intensity");
                    Dataset<Row> metfinalpeaks = metlistinitial.groupBy(metlistinitial.col("experimentid"), metlistinitial.col("description"),metlistinitial.col("filename")).avg("intensity").orderBy(asc("experimentid"), asc("description"));
                    Dataset<Row> newmetfinalpeaks = metfinalpeaks.withColumn("expidfilename", functions.concat(col("experimentid"), lit("__"), col("filename"))).drop("experimentid","filename");
                    Dataset<Row> newmetfinalpeaksafterrename=newmetfinalpeaks.map((MapFunction<Row, Row>) originalrow -> {
                        String replicatewithdot = originalrow.get(2).toString();
                        String temp = replicatewithdot.replaceAll("\\.[mzmlxdatMZMLXDAT]{4,6}$","");
                        if(temp.contains(".")){
                                    temp=temp.replaceAll("[.]", "_");
                                }
                        Object[] rowValues = new Object[originalrow.length()];
                        rowValues[0]=originalrow.get(0);
                        rowValues[1]=originalrow.get(1);
                        rowValues[2]=temp;
                        Row newrow = RowFactory.create(rowValues);
                        return newrow;
                    }, RowEncoder.apply(newmetfinalpeaks.schema())).persist();
                    
                    Dataset<Row> filenameavgpeaks = newmetfinalpeaksafterrename.groupBy(newmetfinalpeaksafterrename.col("description")).pivot("expidfilename").mean("avg(intensity)");

                    String[] temp1 = filenameavgpeaks.columns();
                    List<String>temprepl = new ArrayList<>();
                    for(String finame:temp1){
                        if(!finame.equals("description")){
                            temprepl.add(finame);
                        }
                    }

                    String[] replincols = temprepl.parallelStream().toArray(String[]::new);
                    exlistwithexpcorrandmetlistwithmetcorr.setReplilist(temprepl);

                    //metabolites with missing values in replicates
                    StructType schema2 = new StructType(new StructField[] {
                                new StructField("metabs",
                                                DataTypes.StringType, false,
                                                Metadata.empty()) });

                    Dataset<Row> tempmissvalmtbs2=filenameavgpeaks.map((MapFunction<Row, Row>) originalrow -> {
                        if(originalrow.anyNull()){
                            String rowValues = new String();
                            rowValues=originalrow.get(0).toString();
                            Row newrow = RowFactory.create(rowValues);
                            return newrow;
                        }else{
                            String rowValues = "notmissing";
                            Row newrow = RowFactory.create(rowValues);
                            return newrow;
                        }
                    }, RowEncoder.apply(schema2));
                    Dataset<Row> missvalmetabs2=tempmissvalmtbs2.select("metabs").filter(col("metabs").notEqual("notmissing"));
                    missvalmtbsrepl=missvalmetabs2.as(Encoders.STRING()).collectAsList();
                    exlistwithexpcorrandmetlistwithmetcorr.setMissvalmtbsrepl(missvalmtbsrepl);

                    if(missval.equals("replacewithzero")){
                        newfilenameavgpeaksrepl=filenameavgpeaks.na().fill(0);
                    }else if(missval.equals("replacewithmean")){
                        newfilenameavgpeaksrepl=filenameavgpeaks.map((MapFunction<Row, Row>) originalrow -> {
                            if(originalrow.anyNull()){
                                int originalrowsizeofdoubles = originalrow.length()-1;
                                Double sumrow = Double.valueOf(0);
                                List<Double> meanrow = new ArrayList<>();

                                Object[] rowValues = new Object[originalrow.length()];
                                rowValues[0]=originalrow.get(0).toString();

                                String[] temporal = originalrow.schema().fieldNames();
                                String[] namesofrow = new String[originalrow.length()];
                                List<Integer> replnuminexp=new ArrayList<>();
                                int replnuminexpcounter=0;
                                int replnuminexpcounterwithvalidvalues=0;
                                List<Double> avgintensities = new ArrayList<>();

                                for(int k=0;k<originalrowsizeofdoubles;k++){
                                    String[] namesofrowtemp = temporal[k+1].split("__");
                                    namesofrow[k]=namesofrowtemp[0];
                                }
                                for(int i=0;i<originalrowsizeofdoubles;i++){
                                    replnuminexpcounter++;
                                    if(!namesofrow[i].equals(namesofrow[i+1])){
                                        replnuminexp.add(replnuminexpcounter);
                                        replnuminexpcounter=0;
                                    }
                                }
                                int experpointer=0;
                                int replpointer=1;
                                for(int k=1;k<=originalrowsizeofdoubles;k++){
                                    if( replpointer<=replnuminexp.get(experpointer)){
                                        if(originalrow.isNullAt(k)){
                                            avgintensities.add(null);
                                        }else{
                                            replnuminexpcounterwithvalidvalues++;
                                            sumrow = sumrow + originalrow.getDouble(k);
                                            avgintensities.add(originalrow.getDouble(k));
                                        }
                                        if(replpointer!=replnuminexp.get(experpointer)){
                                            replpointer++;
                                        }else if(replpointer==replnuminexp.get(experpointer)){
                                            if(replnuminexpcounterwithvalidvalues>0){
                                                meanrow.add(sumrow/replnuminexpcounterwithvalidvalues);
                                            }else{
                                                meanrow.add(Double.valueOf(0));
                                            }
                                            experpointer++;
                                            replpointer=1;
                                            replnuminexpcounterwithvalidvalues=0;
                                            sumrow = Double.valueOf(0);
                                        }
                                    }
                                }
                                int newexperpointer=0;
                                int newreplpointer=1;
                                for(int i=0;i<originalrowsizeofdoubles;i++){
                                    if(avgintensities.get(i)==null){
                                        if(newreplpointer<replnuminexp.get(newexperpointer)){
                                            avgintensities.set(i, meanrow.get(newexperpointer));
                                            newreplpointer++;
                                        }else if(newreplpointer==replnuminexp.get(newexperpointer)){
                                            avgintensities.set(i, meanrow.get(newexperpointer));
                                            newreplpointer=1;
                                            newexperpointer++;
                                        }
                                    }else{
                                        if(newreplpointer<replnuminexp.get(newexperpointer)){
                                            newreplpointer++;
                                        }else if(newreplpointer==replnuminexp.get(newexperpointer)){
                                            newreplpointer=1;
                                            newexperpointer++;
                                        }
                                    }

                                }
                                for(int l=0;l<avgintensities.size();l++){
                                    rowValues[l+1]=avgintensities.get(l);
                                }
                                Row newrow = RowFactory.create(rowValues);
                                return newrow;
                            }else{
                                return originalrow;
                            }
                        }, RowEncoder.apply(filenameavgpeaks.schema()));

                    }else if(missval.equals("replacewithmedian")){
                        newfilenameavgpeaksrepl=filenameavgpeaks.map((MapFunction<Row, Row>) originalrow -> {
                            if(originalrow.anyNull()){
                                int originalrowsizeofdoubles = originalrow.length()-1;
                                List<Double> medianlist = new ArrayList<>();

                                Object[] rowValues = new Object[originalrow.length()];
                                rowValues[0]=originalrow.get(0).toString();

                                String[] temporal = originalrow.schema().fieldNames();
                                String[] namesofrow = new String[originalrow.length()];
                                List<Integer> replnuminexp=new ArrayList<>();
                                int replnuminexpcounter=0;

                                List<Double> medianintensities = new ArrayList<>();
                                List<Double> rowmedian = new ArrayList<>();


                                for(int k=0;k<originalrowsizeofdoubles;k++){
                                    String[] namesofrowtemp = temporal[k+1].split("__");
                                    namesofrow[k]=namesofrowtemp[0];
                                }
                                for(int i=0;i<originalrowsizeofdoubles;i++){
                                    replnuminexpcounter++;
                                    if(!namesofrow[i].equals(namesofrow[i+1])){
                                        replnuminexp.add(replnuminexpcounter);
                                        replnuminexpcounter=0;
                                    }
                                }

                                int experpointer=0;
                                int replpointer=1;
                                for(int k=1;k<=originalrowsizeofdoubles;k++){
                                    if( replpointer<=replnuminexp.get(experpointer)){
                                        if(originalrow.isNullAt(k)){
                                            medianintensities.add(null);
                                        }else{
                                            rowmedian.add(originalrow.getDouble(k));
                                            medianintensities.add(originalrow.getDouble(k));
                                        }
                                        if(replpointer!=replnuminexp.get(experpointer)){
                                            replpointer++;
                                        }else if(replpointer==replnuminexp.get(experpointer)){

                                            int rowmediannum=rowmedian.size();
                                            Double[] sortedmedianintensities= new Double[rowmediannum];

                                            for(int i=0;i<rowmediannum;i++){
                                                sortedmedianintensities[i]=rowmedian.get(i);
                                            }
                                            Arrays.sort(sortedmedianintensities);
                                            if (rowmediannum ==1){
                                                medianlist.add(sortedmedianintensities[0]);
                                            }else if(rowmediannum>1){
                                                if(rowmediannum % 2 == 0){
                                                    int pos1=(rowmediannum-1)/2;
                                                    int pos2=pos1+1;

                                                    medianlist.add((sortedmedianintensities[pos1]+sortedmedianintensities[pos2])/2);
                                                }else{
                                                    int pos = (rowmediannum/2);
                                                    medianlist.add(sortedmedianintensities[pos]);
                                                }
                                            }else if(rowmediannum==0){
                                                    medianlist.add(Double.valueOf(0));
                                            }
                                            experpointer++;
                                            replpointer=1;
                                            rowmedian = new ArrayList<>();
                                        }
                                    }
                                }
                                int newexperpointer=0;
                                int newreplpointer=1;
                                for(int i=0;i<originalrowsizeofdoubles;i++){
                                    if(medianintensities.get(i)==null){
                                        if(newreplpointer<replnuminexp.get(newexperpointer)){
                                            medianintensities.set(i, medianlist.get(newexperpointer));
                                            newreplpointer++;
                                        }else if(newreplpointer==replnuminexp.get(newexperpointer)){
                                            medianintensities.set(i, medianlist.get(newexperpointer));
                                            newreplpointer=1;
                                            newexperpointer++;
                                        }
                                    }else{
                                        if(newreplpointer<replnuminexp.get(newexperpointer)){
                                            newreplpointer++;
                                        }else if(newreplpointer==replnuminexp.get(newexperpointer)){
                                            newreplpointer=1;
                                            newexperpointer++;
                                        }
                                    }

                                }

                                for(int l=0;l<medianintensities.size();l++){
                                    rowValues[l+1]=medianintensities.get(l);
                                }
                                Row newrow = RowFactory.create(rowValues);
                                return newrow;
                            }else{
                                return originalrow;
                            }
                        }, RowEncoder.apply(filenameavgpeaks.schema()));

                    }
                    VectorAssembler assemblerexp2 = new VectorAssembler()
                            .setInputCols(replincols)
                            .setOutputCol("intensity");
                    Dataset<Row> expoutput2 = assemblerexp2.transform(newfilenameavgpeaksrepl);

                    Row r1repl1 = Correlation.corr(expoutput2, "intensity").head();
                    Matrix r2repl1= r1repl1.getAs(0);
                    Double[][] r3repl1=new Double[r2repl1.numRows()][r2repl1.numCols()];
                    for (int i = 0; i < r2repl1.numRows(); i++) {
                        for (int j = 0; j < r2repl1.numCols(); j++) {
                            r2repl1.update(i, j, (long)(r2repl1.apply(i,j)* 1e2)/1e2);
                            r3repl1[i][j]=r2repl1.apply(i,j);
                        }
                    }
                    expsreplcorr.add(r3repl1);
                    exlistwithexpcorrandmetlistwithmetcorr.setExpcorrreplcorr(expsreplcorr);
                    newmetfinalpeaksafterrename.unpersist();
                }else{
                        secondrowflag=1;
                }
            }
            if(firstrowflag==1){
                exlistwithexpcorrandmetlistwithmetcorr.setRowflagexp(1);
            }
            if(secondrowflag==1){
                exlistwithexpcorrandmetlistwithmetcorr.setRowflagrepl(1);
            }
        }else if(allorsome.equals("onselectedexp")){
            Dataset<Row> dfmetabolitelist = sp.createDataset(metabolitelist, Encoders.STRING()).toDF("description");
            Dataset<Row> newfilenameavgpeaks=sp.emptyDataFrame();
            
            Encoder<ExperimentFormOnlyParKey> ExpEncoder = Encoders.bean(ExperimentFormOnlyParKey.class);
            Encoder<ExperimentFormExDesIn> ExpEncoderofthree = Encoders.bean(ExperimentFormExDesIn.class);
            Dataset<ExperimentFormOnlyParKey> dfexplistoriginal = sp.createDataset(experimentlist, Encoders.STRING()).toDF("experimentid").as(ExpEncoder);
            CassandraJavaPairRDD<ExperimentFormOnlyParKey, ExperimentFormExDesIn> predf = CassandraJavaUtil.javaFunctions(CassandraJavaUtil.javaFunctions(dfexplistoriginal.javaRDD()).repartitionByCassandraReplica("mdb","experiment",100,CassandraJavaUtil.someColumns("experimentid"),CassandraJavaUtil.mapToRow(ExperimentFormOnlyParKey.class)))
                                                                                .joinWithCassandraTable("mdb","experiment", CassandraJavaUtil.someColumns("experimentid","description","intensity"), CassandraJavaUtil.someColumns("experimentid"), CassandraJavaUtil.mapRowTo(ExperimentFormExDesIn.class), CassandraJavaUtil.mapToRow(ExperimentFormOnlyParKey.class));
            Dataset<Row> explistonallmet =sp.createDataset(predf.map(x->x._2).rdd(), ExpEncoderofthree).toDF( "description","experimentid","intensity");

            String[] expincols = experimentlist.parallelStream().toArray(String[]::new);
            exlistwithexpcorrandmetlistwithmetcorr.setExplist(experimentlist);
            Dataset<Row> exponallmet=sp.emptyDataFrame();
            Dataset<Row> tempmissvalmtbs=sp.emptyDataFrame();
            if(missval.equals("omit")){
                Dataset<Row> metlistinitial = explistonallmet.join(dfmetabolitelist,"description");
                exponallmet = metlistinitial.groupBy(metlistinitial.col("description"), metlistinitial.col("experimentid")).mean("intensity").orderBy(asc("experimentid"), asc("description"));
                Dataset<Row> expavgonallmet = exponallmet.groupBy(exponallmet.col("description")).pivot("experimentid",new ArrayList<Object>(experimentlist)).mean("avg(intensity)");
                newfilenameavgpeaks=expavgonallmet.na().drop(); 
            }else{
                exponallmet = explistonallmet.groupBy(explistonallmet.col("description"), explistonallmet.col("experimentid")).mean("intensity").orderBy(asc("experimentid"), asc("description")).persist();
                Dataset<Row> expavgonallmet = exponallmet.groupBy(exponallmet.col("description")).pivot("experimentid",new ArrayList<Object>(experimentlist)).mean("avg(intensity)");
                //finding missing metabolites
                StructType schema = new StructType(new StructField[] {
                            new StructField("description",DataTypes.StringType, false,Metadata.empty()),
                            new StructField("experimentid",DataTypes.createArrayType(DataTypes.StringType, true), false,Metadata.empty())});
                String[] columnnames=expavgonallmet.columns();
                int mtbswithallnullms=expavgonallmet.columns().length;
                
                tempmissvalmtbs=expavgonallmet.map((MapFunction<Row, Row>) originalrow -> {
                    if(originalrow.anyNull() && metabolitelist.contains(originalrow.get(0).toString())){
                        List<String>tmpexpidfilename=new ArrayList<>();
                        for(int i=1;i<mtbswithallnullms;i++){
                            if(originalrow.isNullAt(i)){
                                tmpexpidfilename.add(columnnames[i]);
                            }
                        }
                        String mtbname = originalrow.get(0).toString();
                        Row newrow = RowFactory.create(mtbname,tmpexpidfilename.toArray(new String[0]));
                        return newrow;
                    }else{
                        String mtbname = "notmissing";
                        List<String>tmpexpidfilename=new ArrayList<>();
                        Row newrow = RowFactory.create(mtbname,tmpexpidfilename.toArray(new String[0]));
                        return newrow;
                    }
                }, RowEncoder.apply(schema)).persist();
                
                Dataset<Row> missvalmetabs=tempmissvalmtbs.select("description").filter(col("description").notEqual("notmissing"));
                missvalmtbsexp=missvalmetabs.as(Encoders.STRING()).collectAsList();
                exlistwithexpcorrandmetlistwithmetcorr.setMissvalmtbsexp(missvalmtbsexp);
                
                Dataset<Row> nullmtbs = tempmissvalmtbs.withColumn("experimentid", functions.explode_outer(col("experimentid"))).withColumn("avg(intensity)", functions.lit(null).cast(DataTypes.DoubleType)).select(col("experimentid"),col("description"),col("avg(intensity)"));
                Dataset<Row> tmpsupernewmetfinalpeaksafterrename=exponallmet.union(nullmtbs);
                Dataset<Row> supernewmetfinalpeaksafterrename=tmpsupernewmetfinalpeaksafterrename.join(dfmetabolitelist,"description");
                    
                Dataset<Row> finalexpavgpeaks = supernewmetfinalpeaksafterrename.groupBy(supernewmetfinalpeaksafterrename.col("description")).pivot("experimentid",new ArrayList<Object>(experimentlist)).mean("avg(intensity)");
                
                if(missval.equals("replacewithzero")){
                    newfilenameavgpeaks=finalexpavgpeaks.na().fill(0);
                }else if(missval.equals("replacewithmean")){
                    newfilenameavgpeaks=finalexpavgpeaks.map((MapFunction<Row, Row>) originalrow -> {
                        if(originalrow.anyNull()){
                            int originalrowsizeofdoubles=originalrow.length()-1;
                            Double sumrow = Double.valueOf(0);
                            Double meanrow = Double.valueOf(0);

                            Object[] rowValues = new Object[originalrow.length()];
                            rowValues[0]=originalrow.get(0).toString();

                            List<Integer>indextobereplaced = new ArrayList<>();
                            List<Double> avgintensities = new ArrayList<>();
                            int validvalues=0;
                            for(int k=1;k<=originalrowsizeofdoubles;k++){
                                if(originalrow.isNullAt(k)){
                                    indextobereplaced.add(k);
                                    avgintensities.add(null);
                                }else{
                                    sumrow = sumrow + originalrow.getDouble(k);
                                    avgintensities.add(originalrow.getDouble(k));
                                    validvalues++;
                                }
                            }
                            meanrow=sumrow/(validvalues);

                            int indextobereplacedsize=indextobereplaced.size();
                            for(int i=0;i<indextobereplacedsize;i++){
                                avgintensities.set(indextobereplaced.get(i)-1, meanrow);

                            }

                            for(int l=0;l<avgintensities.size();l++){
                                rowValues[l+1]=avgintensities.get(l);
                            }
                            Row newrow = RowFactory.create(rowValues);

                            return newrow;
                        }else{
                            return originalrow;
                        }
                    }, RowEncoder.apply(finalexpavgpeaks.schema()));

                }else if(missval.equals("replacewithmedian")){
                    newfilenameavgpeaks=finalexpavgpeaks.map((MapFunction<Row, Row>) originalrow -> {
                        if(originalrow.anyNull()){
                            int originalrowsizeofdoubles=originalrow.length()-1;
                            Double medianrow = Double.valueOf(0);

                            Object[] rowValues = new Object[originalrow.length()];
                            rowValues[0]=originalrow.get(0).toString();

                            List<Integer>indextobereplaced = new ArrayList<>();
                            List<Double> medianintensities = new ArrayList<>();
                            List<Double> rowmedian = new ArrayList<>();
                            int validvalues=0;
                            for(int k=1;k<=originalrowsizeofdoubles;k++){
                                if(originalrow.isNullAt(k)){
                                    indextobereplaced.add(k);
                                    medianintensities.add(null);
                                }else{
                                    medianintensities.add(originalrow.getDouble(k));
                                    rowmedian.add(originalrow.getDouble(k));
                                }
                            }
                            int rowmediannum=rowmedian.size();
                            Double[] sortedmedianintensities= new Double[rowmediannum];

                            for(int i=0;i<rowmediannum;i++){
                                sortedmedianintensities[i]=rowmedian.get(i);
                            }
                            Arrays.sort(sortedmedianintensities);
                            if (rowmediannum ==1){
                                medianrow=sortedmedianintensities[0];
                            }else if(rowmediannum>1){
                                if(rowmediannum % 2 == 0){
                                    int pos1=(rowmediannum-1)/2;
                                    int pos2=pos1+1;

                                    medianrow=(sortedmedianintensities[pos1]+sortedmedianintensities[pos2])/2;
                                }else{
                                    int pos = (rowmediannum/2);
                                    medianrow=sortedmedianintensities[pos];
                                }
                            }

                            int indextobereplacedsize=indextobereplaced.size();
                            for(int i=0;i<indextobereplacedsize;i++){
                                medianintensities.set(indextobereplaced.get(i)-1, medianrow);
                            }
                            for(int l=0;l<medianintensities.size();l++){
                                rowValues[l+1]=medianintensities.get(l);
                            }
                            Row newrow = RowFactory.create(rowValues);

                            return newrow;
                        }else{
                            return originalrow;
                        }
                    }, RowEncoder.apply(finalexpavgpeaks.schema()));
                }
                
            }
            VectorAssembler assemblerexp1 = new VectorAssembler()
                    .setInputCols(expincols)
                    .setOutputCol("intensity");
            Dataset<Row> expoutput1 = assemblerexp1.transform(newfilenameavgpeaks);//.persist();

            Row r1exp1 = Correlation.corr(expoutput1, "intensity").head();
            Matrix r2exp1= r1exp1.getAs(0);
            Double[][] r3exp1=new Double[r2exp1.numRows()][r2exp1.numCols()];
            for (int i = 0; i < r2exp1.numRows(); i++) {
                for (int j = 0; j < r2exp1.numCols(); j++) {
                    r2exp1.update(i, j, (long)(r2exp1.apply(i,j)* 1e2)/1e2);
                    r3exp1[i][j]=r2exp1.apply(i,j);
                }
            }
            expsreplcorr.add(r3exp1);
            exlistwithexpcorrandmetlistwithmetcorr.setExpcorrreplcorr(expsreplcorr);
            exponallmet.unpersist();
            tempmissvalmtbs.unpersist();
        }else if(allorsome.equals("onselectedrepl")){
            Dataset<Row> dfmetabolitelist = sp.createDataset(metabolitelist, Encoders.STRING()).toDF("description");
            Dataset<Row> newfilenameavgpeaks=sp.emptyDataFrame();
            
            Encoder<ExperimentFormOnlyParKey> ExpEncoder = Encoders.bean(ExperimentFormOnlyParKey.class);
            Encoder<ExperimentFormExDesFlIn> ExpEncoderoffour = Encoders.bean(ExperimentFormExDesFlIn.class);
            Dataset<ExperimentFormOnlyParKey> dfexplistoriginal = sp.createDataset(experimentlist, Encoders.STRING()).toDF("experimentid").as(ExpEncoder);
            CassandraJavaPairRDD<ExperimentFormOnlyParKey, ExperimentFormExDesFlIn> predf = CassandraJavaUtil.javaFunctions(CassandraJavaUtil.javaFunctions(dfexplistoriginal.javaRDD()).repartitionByCassandraReplica("mdb","experiment",100,CassandraJavaUtil.someColumns("experimentid"),CassandraJavaUtil.mapToRow(ExperimentFormOnlyParKey.class)))
                                                                                .joinWithCassandraTable("mdb","experiment", CassandraJavaUtil.someColumns("experimentid","filename","description","intensity"), CassandraJavaUtil.someColumns("experimentid"), CassandraJavaUtil.mapRowTo(ExperimentFormExDesFlIn.class), CassandraJavaUtil.mapToRow(ExperimentFormOnlyParKey.class));
            Dataset<Row> metlistinitial =sp.createDataset(predf.map(x->x._2).rdd(), ExpEncoderoffour).toDF( "description","experimentid","filename","intensity");
            
            String[] replincols;
            Dataset<Row> newmetfinalpeaksafterrename=sp.emptyDataFrame();
            Dataset<Row> tempmissvalmtbs=sp.emptyDataFrame();
            if(missval.equals("omit")){
               Dataset<Row> metlistinitial1 = metlistinitial.join(dfmetabolitelist,"description");
               Dataset<Row> metfinalpeaks = metlistinitial1.groupBy(metlistinitial1.col("experimentid"), metlistinitial1.col("description"),metlistinitial1.col("filename")).avg("intensity").orderBy(asc("experimentid"), asc("description"));

                Dataset<Row> newmetfinalpeaks = metfinalpeaks.withColumn("expidfilename", functions.concat(col("experimentid"), lit("__"), col("filename"))).drop("experimentid","filename");
                Dataset<Row>filenameavgpeaks = newmetfinalpeaks.groupBy(newmetfinalpeaks.col("description")).pivot("expidfilename").mean("avg(intensity)");

                String[] temp1 = filenameavgpeaks.columns();
                List<String>temprepl = new ArrayList<>();

                for(String finame:temp1){
                    if(!finame.equals("description")){
                        String temp = finame.replaceAll("\\.[mzmlxdatMZMLXDAT]{4,6}$","");
                        if(temp.contains(".")){
                            temp=temp.replaceAll("[.]", "_");
                        }
                        temprepl.add(temp);
                        filenameavgpeaks=filenameavgpeaks.withColumnRenamed(finame, temp);
                    }
                }
                List<String>replinames=temprepl;
                replincols = replinames.parallelStream().toArray(String[]::new);
                exlistwithexpcorrandmetlistwithmetcorr.setReplilist(temprepl);
                newfilenameavgpeaks=filenameavgpeaks;
            }else{
                Dataset<Row> metfinalpeaks = metlistinitial.groupBy(metlistinitial.col("experimentid"), metlistinitial.col("description"),metlistinitial.col("filename")).avg("intensity").orderBy(asc("experimentid"), asc("description"));
                Dataset<Row> newmetfinalpeaks = metfinalpeaks.withColumn("expidfilename", functions.concat(col("experimentid"), lit("__"), col("filename"))).drop("experimentid","filename");
                newmetfinalpeaksafterrename=newmetfinalpeaks.map((MapFunction<Row, Row>) originalrow -> {
                        String replicatewithdot = originalrow.get(2).toString();
                        String temp = replicatewithdot.replaceAll("\\.[mzmlxdatMZMLXDAT]{4,6}$","");
                        if(temp.contains(".")){
                                    temp=temp.replaceAll("[.]", "_");
                                }
                        Object[] rowValues = new Object[originalrow.length()];
                        rowValues[0]=originalrow.get(0);
                        rowValues[1]=originalrow.get(1);
                        rowValues[2]=temp;
                        Row newrow = RowFactory.create(rowValues);
                        return newrow;
                    }, RowEncoder.apply(newmetfinalpeaks.schema())).persist();
                
                Dataset<Row> filenameavgpeaks = newmetfinalpeaksafterrename.groupBy(newmetfinalpeaksafterrename.col("description")).pivot("expidfilename").mean("avg(intensity)");

                String[] temp1 = filenameavgpeaks.columns();
                List<String>temprepl = new ArrayList<>();

                for(String finame:temp1){
                    if(!finame.equals("description")){
                        temprepl.add(finame);
                    }
                }
                List<String>replinames=temprepl;
                replincols = replinames.parallelStream().toArray(String[]::new);
                exlistwithexpcorrandmetlistwithmetcorr.setReplilist(temprepl);
                //finding missing metabolites
                StructType schema = new StructType(new StructField[] {
                            new StructField("description",DataTypes.StringType, false,Metadata.empty()),
                            new StructField("expidfilename",DataTypes.createArrayType(DataTypes.StringType, true), false,Metadata.empty())});
                String[] columnnames=temp1;
                int mtbswithallnullms=temp1.length;
                
                tempmissvalmtbs=filenameavgpeaks.map((MapFunction<Row, Row>) originalrow -> {
                    if(originalrow.anyNull() && metabolitelist.contains(originalrow.get(0).toString())){
                        List<String>tmpexpidfilename=new ArrayList<>();
                        for(int i=1;i<mtbswithallnullms;i++){
                            if(originalrow.isNullAt(i)){
                                tmpexpidfilename.add(columnnames[i]);
                            }
                        }
                        String mtbname = originalrow.get(0).toString();
                        Row newrow = RowFactory.create(mtbname,tmpexpidfilename.toArray(new String[0]));
                        return newrow;
                    }else{
                        String mtbname = "notmissing";
                        List<String>tmpexpidfilename=new ArrayList<>();
                        Row newrow = RowFactory.create(mtbname,tmpexpidfilename.toArray(new String[0]));
                        return newrow;
                    }
                }, RowEncoder.apply(schema)).persist(); 
                Dataset<Row> missvalmetabs=tempmissvalmtbs.select("description").filter(col("description").notEqual("notmissing"));
                missvalmtbsrepl=missvalmetabs.as(Encoders.STRING()).collectAsList();
                exlistwithexpcorrandmetlistwithmetcorr.setMissvalmtbsrepl(missvalmtbsrepl);
                
                Dataset<Row> nullmtbs = tempmissvalmtbs.withColumn("expidfilename", functions.explode_outer(col("expidfilename"))).withColumn("avg(intensity)", functions.lit(null).cast(DataTypes.DoubleType)).select(col("description"),col("avg(intensity)"),col("expidfilename"));
                Dataset<Row> tmpsupernewmetfinalpeaksafterrename=newmetfinalpeaksafterrename.union(nullmtbs);
                Dataset<Row> supernewmetfinalpeaksafterrename=tmpsupernewmetfinalpeaksafterrename.join(dfmetabolitelist,"description");
                Dataset<Row>expavgpeaks = supernewmetfinalpeaksafterrename.groupBy(supernewmetfinalpeaksafterrename.col("description")).pivot("expidfilename").mean("avg(intensity)");
                
                if(missval.equals("replacewithzero")){
                    newfilenameavgpeaks=expavgpeaks.na().fill(0);
                }else if(missval.equals("replacewithmean")){
                    newfilenameavgpeaks=expavgpeaks.map((MapFunction<Row, Row>) originalrow -> {
                        if(originalrow.anyNull()){
                            int originalrowsizeofdoubles = originalrow.length()-1;
                            Double sumrow = Double.valueOf(0);
                            List<Double> meanrow = new ArrayList<>();

                            Object[] rowValues = new Object[originalrow.length()];
                            rowValues[0]=originalrow.get(0).toString();

                            String[] temporal = originalrow.schema().fieldNames();
                            String[] namesofrow = new String[originalrow.length()];
                            List<Integer> replnuminexp=new ArrayList<>();
                            int replnuminexpcounter=0;
                            int replnuminexpcounterwithvalidvalues=0;
                            List<Double> avgintensities = new ArrayList<>();

                            for(int k=0;k<originalrowsizeofdoubles;k++){
                                String[] namesofrowtemp = temporal[k+1].split("__");
                                namesofrow[k]=namesofrowtemp[0];
                            }
                            for(int i=0;i<originalrowsizeofdoubles;i++){
                                replnuminexpcounter++;
                                if(!namesofrow[i].equals(namesofrow[i+1])){
                                    replnuminexp.add(replnuminexpcounter);
                                    replnuminexpcounter=0;
                                }
                            }
                            int experpointer=0;
                            int replpointer=1;
                            for(int k=1;k<=originalrowsizeofdoubles;k++){
                                if( replpointer<=replnuminexp.get(experpointer)){
                                    if(originalrow.isNullAt(k)){
                                        avgintensities.add(null);
                                    }else{
                                        replnuminexpcounterwithvalidvalues++;
                                        sumrow = sumrow + originalrow.getDouble(k);
                                        avgintensities.add(originalrow.getDouble(k));
                                    }
                                    if(replpointer!=replnuminexp.get(experpointer)){
                                        replpointer++;
                                    }else if(replpointer==replnuminexp.get(experpointer)){
                                        if(replnuminexpcounterwithvalidvalues>0){
                                            meanrow.add(sumrow/replnuminexpcounterwithvalidvalues);
                                        }else{
                                            meanrow.add(Double.valueOf(0));
                                        }
                                        experpointer++;
                                        replpointer=1;
                                        replnuminexpcounterwithvalidvalues=0;
                                        sumrow = Double.valueOf(0);
                                    }
                                }
                            }
                            int newexperpointer=0;
                            int newreplpointer=1;
                            for(int i=0;i<originalrowsizeofdoubles;i++){
                                if(avgintensities.get(i)==null){
                                    if(newreplpointer<replnuminexp.get(newexperpointer)){
                                        avgintensities.set(i, meanrow.get(newexperpointer));
                                        newreplpointer++;
                                    }else if(newreplpointer==replnuminexp.get(newexperpointer)){
                                        avgintensities.set(i, meanrow.get(newexperpointer));
                                        newreplpointer=1;
                                        newexperpointer++;
                                    }
                                }else{
                                    if(newreplpointer<replnuminexp.get(newexperpointer)){
                                        newreplpointer++;
                                    }else if(newreplpointer==replnuminexp.get(newexperpointer)){
                                        newreplpointer=1;
                                        newexperpointer++;
                                    }
                                }

                            }
                            for(int l=0;l<avgintensities.size();l++){
                                rowValues[l+1]=avgintensities.get(l);
                            }
                            Row newrow = RowFactory.create(rowValues);
                            return newrow;
                        }else{
                            return originalrow;
                        }
                    }, RowEncoder.apply(expavgpeaks.schema()));

                }else if(missval.equals("replacewithmedian")){
                    newfilenameavgpeaks=expavgpeaks.map((MapFunction<Row, Row>) originalrow -> {
                        if(originalrow.anyNull()){
                            int originalrowsizeofdoubles = originalrow.length()-1;
                            List<Double> medianlist = new ArrayList<>();

                            Object[] rowValues = new Object[originalrow.length()];
                            rowValues[0]=originalrow.get(0).toString();

                            String[] temporal = originalrow.schema().fieldNames();
                            String[] namesofrow = new String[originalrow.length()];
                            List<Integer> replnuminexp=new ArrayList<>();
                            int replnuminexpcounter=0;

                            List<Double> medianintensities = new ArrayList<>();
                            List<Double> rowmedian = new ArrayList<>();


                            for(int k=0;k<originalrowsizeofdoubles;k++){
                                String[] namesofrowtemp = temporal[k+1].split("__");
                                namesofrow[k]=namesofrowtemp[0];
                            }
                            for(int i=0;i<originalrowsizeofdoubles;i++){
                                replnuminexpcounter++;
                                if(!namesofrow[i].equals(namesofrow[i+1])){
                                    replnuminexp.add(replnuminexpcounter);
                                    replnuminexpcounter=0;
                                }
                            }

                            int experpointer=0;
                            int replpointer=1;
                            for(int k=1;k<=originalrowsizeofdoubles;k++){
                                if( replpointer<=replnuminexp.get(experpointer)){
                                    if(originalrow.isNullAt(k)){
                                        medianintensities.add(null);
                                    }else{
                                        rowmedian.add(originalrow.getDouble(k));
                                        medianintensities.add(originalrow.getDouble(k));
                                    }
                                    if(replpointer!=replnuminexp.get(experpointer)){
                                        replpointer++;
                                    }else if(replpointer==replnuminexp.get(experpointer)){

                                        int rowmediannum=rowmedian.size();
                                        Double[] sortedmedianintensities= new Double[rowmediannum];

                                        for(int i=0;i<rowmediannum;i++){
                                            sortedmedianintensities[i]=rowmedian.get(i);
                                        }
                                        Arrays.sort(sortedmedianintensities);
                                        if (rowmediannum ==1){
                                            medianlist.add(sortedmedianintensities[0]);
                                        }else if(rowmediannum>1){
                                            if(rowmediannum % 2 == 0){
                                                int pos1=(rowmediannum-1)/2;
                                                int pos2=pos1+1;

                                                medianlist.add((sortedmedianintensities[pos1]+sortedmedianintensities[pos2])/2);
                                            }else{
                                                int pos = (rowmediannum/2);
                                                medianlist.add(sortedmedianintensities[pos]);
                                            }
                                        }else if(rowmediannum==0){
                                                medianlist.add(Double.valueOf(0));
                                        }
                                        experpointer++;
                                        replpointer=1;
                                        rowmedian = new ArrayList<>();
                                    }
                                }
                            }
                            int newexperpointer=0;
                            int newreplpointer=1;
                            for(int i=0;i<originalrowsizeofdoubles;i++){
                                if(medianintensities.get(i)==null){
                                    if(newreplpointer<replnuminexp.get(newexperpointer)){
                                        medianintensities.set(i, medianlist.get(newexperpointer));
                                        newreplpointer++;
                                    }else if(newreplpointer==replnuminexp.get(newexperpointer)){
                                        medianintensities.set(i, medianlist.get(newexperpointer));
                                        newreplpointer=1;
                                        newexperpointer++;
                                    }
                                }else{
                                    if(newreplpointer<replnuminexp.get(newexperpointer)){
                                        newreplpointer++;
                                    }else if(newreplpointer==replnuminexp.get(newexperpointer)){
                                        newreplpointer=1;
                                        newexperpointer++;
                                    }
                                }

                            }

                            for(int l=0;l<medianintensities.size();l++){
                                rowValues[l+1]=medianintensities.get(l);
                            }
                            Row newrow = RowFactory.create(rowValues);
                            return newrow;
                        }else{
                            return originalrow;
                        }
                    }, RowEncoder.apply(expavgpeaks.schema()));
                }
                
            }
            VectorAssembler assemblerexp2 = new VectorAssembler()
                    .setInputCols(replincols)
                    .setOutputCol("intensity");
            Dataset<Row> expoutput2 = assemblerexp2.transform(newfilenameavgpeaks);

            Row r1repl1 = Correlation.corr(expoutput2, "intensity").head();
            Matrix r2repl1= r1repl1.getAs(0);
            Double[][] r3repl1=new Double[r2repl1.numRows()][r2repl1.numCols()];
            for (int i = 0; i < r2repl1.numRows(); i++) {
                for (int j = 0; j < r2repl1.numCols(); j++) {
                    r2repl1.update(i, j, (long)(r2repl1.apply(i,j)* 1e2)/1e2);
                    r3repl1[i][j]=r2repl1.apply(i,j);
                }
            }
            expsreplcorr.add(r3repl1);
            exlistwithexpcorrandmetlistwithmetcorr.setExpcorrreplcorr(expsreplcorr);
            tempmissvalmtbs.unpersist();
            newmetfinalpeaksafterrename.unpersist();
        }else if(allorsome.equals("onselectedmtb")){
            Dataset<Row> dfmetabolitelist = sp.createDataset(metabolitelist, Encoders.STRING()).toDF("description");
            Dataset<Row> newfilenameavgpeaks=sp.emptyDataFrame();
            
            Dataset<Row> explistonallmet =sp.emptyDataFrame();
            if(ionmode.equals("both")){
                Encoder<ExperimentFormOnlyParKey> ExpEncoder = Encoders.bean(ExperimentFormOnlyParKey.class);
                Encoder<ExperimentFormExDesIn> ExpEncoderofthree = Encoders.bean(ExperimentFormExDesIn.class);
                Dataset<ExperimentFormOnlyParKey> dfexplistoriginal = sp.createDataset(experimentlist, Encoders.STRING()).toDF("experimentid").as(ExpEncoder);
                CassandraJavaPairRDD<ExperimentFormOnlyParKey, ExperimentFormExDesIn> predf = CassandraJavaUtil.javaFunctions(CassandraJavaUtil.javaFunctions(dfexplistoriginal.javaRDD()).repartitionByCassandraReplica("mdb","experiment",100,CassandraJavaUtil.someColumns("experimentid"),CassandraJavaUtil.mapToRow(ExperimentFormOnlyParKey.class)))
                                                                                    .joinWithCassandraTable("mdb","experiment", CassandraJavaUtil.someColumns("experimentid","description","intensity"), CassandraJavaUtil.someColumns("experimentid"), CassandraJavaUtil.mapRowTo(ExperimentFormExDesIn.class), CassandraJavaUtil.mapToRow(ExperimentFormOnlyParKey.class));
                explistonallmet =sp.createDataset(predf.map(x->x._2).rdd(), ExpEncoderofthree).toDF( "description","experimentid","intensity").filter(col("description").notEqual("Unidentified"));
            }else if(ionmode.equals("positive")){
                Encoder<ExperimentFormOnlyParKey> ExpEncoder = Encoders.bean(ExperimentFormOnlyParKey.class);
                Encoder<ExperimentFormExDesInAd> ExpEncoderofthree = Encoders.bean(ExperimentFormExDesInAd.class);
                Dataset<ExperimentFormOnlyParKey> dfexplistoriginal = sp.createDataset(experimentlist, Encoders.STRING()).toDF("experimentid").as(ExpEncoder); 
                CassandraJavaPairRDD<ExperimentFormOnlyParKey, ExperimentFormExDesInAd> predf = CassandraJavaUtil.javaFunctions(CassandraJavaUtil.javaFunctions(dfexplistoriginal.javaRDD()).repartitionByCassandraReplica("mdb","experiment",100,CassandraJavaUtil.someColumns("experimentid"),CassandraJavaUtil.mapToRow(ExperimentFormOnlyParKey.class)))
                                                                                    .joinWithCassandraTable("mdb","experiment", CassandraJavaUtil.someColumns("experimentid","description","adduct","intensity"), CassandraJavaUtil.someColumns("experimentid"), CassandraJavaUtil.mapRowTo(ExperimentFormExDesInAd.class), CassandraJavaUtil.mapToRow(ExperimentFormOnlyParKey.class));
                Dataset<Row> metlistinitialbef =sp.createDataset(predf.map(x->x._2).rdd(), ExpEncoderofthree).toDF( "adduct","description","experimentid","intensity").filter(col("description").notEqual("Unidentified"));
                Dataset<Row> metlistinitialbe= metlistinitialbef.withColumn("ioniz", functions.substring(col("adduct"), -1, 1)).drop("adduct").filter(col("ioniz").equalTo("+"));
                explistonallmet = metlistinitialbe.drop(col("ioniz"));
            }else if(ionmode.equals("negative")){
                Encoder<ExperimentFormOnlyParKey> ExpEncoder = Encoders.bean(ExperimentFormOnlyParKey.class);
                Encoder<ExperimentFormExDesInAd> ExpEncoderofthree = Encoders.bean(ExperimentFormExDesInAd.class);
                Dataset<ExperimentFormOnlyParKey> dfexplistoriginal = sp.createDataset(experimentlist, Encoders.STRING()).toDF("experimentid").as(ExpEncoder);
                CassandraJavaPairRDD<ExperimentFormOnlyParKey, ExperimentFormExDesInAd> predf = CassandraJavaUtil.javaFunctions(CassandraJavaUtil.javaFunctions(dfexplistoriginal.javaRDD()).repartitionByCassandraReplica("mdb","experiment",100,CassandraJavaUtil.someColumns("experimentid"),CassandraJavaUtil.mapToRow(ExperimentFormOnlyParKey.class)))
                                                                                    .joinWithCassandraTable("mdb","experiment", CassandraJavaUtil.someColumns("experimentid","description","adduct","intensity"), CassandraJavaUtil.someColumns("experimentid"), CassandraJavaUtil.mapRowTo(ExperimentFormExDesInAd.class), CassandraJavaUtil.mapToRow(ExperimentFormOnlyParKey.class));
                Dataset<Row> metlistinitialbef =sp.createDataset(predf.map(x->x._2).rdd(), ExpEncoderofthree).toDF( "adduct","description","experimentid","intensity").filter(col("description").notEqual("Unidentified"));
                Dataset<Row> metlistinitialbe= metlistinitialbef.withColumn("ioniz", functions.substring(col("adduct"), -1, 1)).drop("adduct").filter(col("ioniz").equalTo("-"));
                explistonallmet = metlistinitialbe.drop(col("ioniz"));
            }
            
            String[] metincols = metabolitelist.parallelStream().toArray(String[]::new);
            exlistwithexpcorrandmetlistwithmetcorr.setMetlist(metabolitelist);
            Dataset<Row> exponallmet = sp.emptyDataFrame();
            Dataset<Row> tempmissvalmtbs = sp.emptyDataFrame();
            if(missval.equals("omit")){
                Dataset<Row> joinedexplistonallmet=explistonallmet.join(dfmetabolitelist,"description");

                exponallmet = joinedexplistonallmet.groupBy(joinedexplistonallmet.col("description"), joinedexplistonallmet.col("experimentid")).mean("intensity").orderBy(asc("experimentid"), asc("description"));
                Dataset<Row> expavgonallmet = exponallmet.groupBy(exponallmet.col("experimentid")).pivot("description",new ArrayList<Object>(metabolitelist)).mean("avg(intensity)");
                newfilenameavgpeaks=expavgonallmet;
            }else{
                exponallmet = explistonallmet.groupBy(explistonallmet.col("description"), explistonallmet.col("experimentid")).mean("intensity").orderBy(asc("experimentid"), asc("description")).persist();
                Dataset<Row> expavgpeaks = exponallmet.groupBy(col("description")).pivot("experimentid",new ArrayList<Object>(experimentlist)).mean("avg(intensity)");
                //finding missing metabolites
                StructType schema = new StructType(new StructField[] {
                            new StructField("description",DataTypes.StringType, false,Metadata.empty()),
                            new StructField("experimentid",DataTypes.createArrayType(DataTypes.StringType, true), false,Metadata.empty())});
                String[] columnnames=expavgpeaks.columns();
                int mtbswithallnullms=expavgpeaks.columns().length;
                
                tempmissvalmtbs=expavgpeaks.map((MapFunction<Row, Row>) originalrow -> {
                    if(originalrow.anyNull() && metabolitelist.contains(originalrow.get(0).toString())){
                        List<String>tmpexpidfilename=new ArrayList<>();
                        for(int i=1;i<mtbswithallnullms;i++){
                            if(originalrow.isNullAt(i)){
                                tmpexpidfilename.add(columnnames[i]);
                            }
                        }
                        String mtbname = originalrow.get(0).toString();
                        Row newrow = RowFactory.create(mtbname,tmpexpidfilename.toArray(new String[0]));
                        return newrow;
                    }else{
                        String mtbname = "notmissing";
                        List<String>tmpexpidfilename=new ArrayList<>();
                        Row newrow = RowFactory.create(mtbname,tmpexpidfilename.toArray(new String[0]));
                        return newrow;
                    }
                }, RowEncoder.apply(schema)).persist(); 
                Dataset<Row> missvalmetabs=tempmissvalmtbs.select("description").filter(col("description").notEqual("notmissing"));
                missvalmtbsexp=missvalmetabs.as(Encoders.STRING()).collectAsList();
                exlistwithexpcorrandmetlistwithmetcorr.setMissvalmtbsexp(missvalmtbsexp);
                
                Dataset<Row> nullmtbs = tempmissvalmtbs.withColumn("experimentid", functions.explode_outer(col("experimentid"))).withColumn("avg(intensity)", functions.lit(null).cast(DataTypes.DoubleType)).select(col("experimentid"),col("description"),col("avg(intensity)"));
                Dataset<Row> tmpsupernewmetfinalpeaksafterrename=exponallmet.union(nullmtbs);
                Dataset<Row> supernewmetfinalpeaksafterrename=tmpsupernewmetfinalpeaksafterrename.join(dfmetabolitelist,"description");
                    
                Dataset<Row> finalexpavgpeaks = supernewmetfinalpeaksafterrename.groupBy(supernewmetfinalpeaksafterrename.col("experimentid")).pivot("description",new ArrayList<Object>(metabolitelist)).mean("avg(intensity)");
                
                if(missval.equals("replacewithzero")){
                    newfilenameavgpeaks=finalexpavgpeaks.na().fill(0);
                }else if(missval.equals("replacewithmean")){
                    Imputer imputer = new Imputer()
                        .setInputCols(metincols)
                        .setOutputCols(metincols)
                        .setStrategy("mean");
                    ImputerModel model = imputer.fit(finalexpavgpeaks);
                    newfilenameavgpeaks=model.transform(finalexpavgpeaks);
                }else if(missval.equals("replacewithmedian")){
                    Imputer imputer = new Imputer()
                        .setInputCols(metincols)
                        .setOutputCols(metincols)
                        .setStrategy("median");
                    ImputerModel model = imputer.fit(finalexpavgpeaks);
                    newfilenameavgpeaks=model.transform(finalexpavgpeaks);
                }
                
            }
            
            VectorAssembler assemblerexp1 = new VectorAssembler()
                    .setInputCols(metincols)
                    .setOutputCol("intensity");
            Dataset<Row> expoutput1 = assemblerexp1.transform(newfilenameavgpeaks);
            Row r1exp1 = Correlation.corr(expoutput1, "intensity").head();
            Matrix r2exp1= r1exp1.getAs(0);
            Double[][] r3exp1=new Double[r2exp1.numRows()][r2exp1.numCols()];
            for (int i = 0; i < r2exp1.numRows(); i++) {
                for (int j = 0; j < r2exp1.numCols(); j++) {
                    r2exp1.update(i, j, (long)(r2exp1.apply(i,j)* 1e2)/1e2);
                    r3exp1[i][j]=r2exp1.apply(i,j);
                    
                }
            }
            expsreplcorr.add(r3exp1);
            exlistwithexpcorrandmetlistwithmetcorr.setExpcorrreplcorr(expsreplcorr);
            exponallmet.unpersist();
            tempmissvalmtbs.unpersist();
        }

        return exlistwithexpcorrandmetlistwithmetcorr;
    }
}
