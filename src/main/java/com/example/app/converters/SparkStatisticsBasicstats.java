/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.example.app.converters;

import com.datastax.spark.connector.japi.CassandraJavaUtil;
import com.datastax.spark.connector.japi.rdd.CassandraJavaPairRDD;
import com.example.app.commands.Basicstatsmtbs;
import com.example.app.domain.ExperimentFormExDes;
import com.example.app.domain.ExperimentFormExDesIn;
import com.example.app.domain.ExperimentFormOnlyParKey;
import com.example.app.commands.ProjectExperimentFormExNa;
import com.example.app.domain.ExperimentFormExDesAd;
import com.example.app.domain.ExperimentFormExDesInAd;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import org.apache.spark.ml.feature.VectorAssembler;
import org.apache.spark.ml.linalg.Matrix;
import org.apache.spark.ml.linalg.Vector;
import org.apache.spark.ml.stat.Summarizer;
import org.apache.spark.sql.Column;
import org.apache.spark.sql.Dataset;
import org.apache.spark.sql.Encoder;
import org.apache.spark.sql.Encoders;
import org.apache.spark.sql.Row;
import org.apache.spark.sql.SparkSession;
import org.apache.spark.sql.functions;
import static org.apache.spark.sql.functions.asc;
import static org.apache.spark.sql.functions.col;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 *
 * @author kostas
 */
@Component
public class SparkStatisticsBasicstats {
    @Autowired
    private SparkSession sp;
    
    public List<String> commonMetInExperiments(List<String> expids,String ionmode){
        List<String> metlist = new ArrayList<>();
        Dataset<Row> metlistinitial =sp.emptyDataFrame();
        if(ionmode.equals("both")){
            Encoder<ExperimentFormOnlyParKey> ExpEncoder = Encoders.bean(ExperimentFormOnlyParKey.class);
            Encoder<ExperimentFormExDes> ExpEncoderoftwo = Encoders.bean(ExperimentFormExDes.class);
            Dataset<ExperimentFormOnlyParKey> dfexplistoriginal = sp.createDataset(expids, Encoders.STRING()).toDF("experimentid").as(ExpEncoder);
            CassandraJavaPairRDD<ExperimentFormOnlyParKey, ExperimentFormExDes> predf = CassandraJavaUtil.javaFunctions(CassandraJavaUtil.javaFunctions(dfexplistoriginal.javaRDD()).repartitionByCassandraReplica("mdb","experiment",100,CassandraJavaUtil.someColumns("experimentid"),CassandraJavaUtil.mapToRow(ExperimentFormOnlyParKey.class)))
                                                                                .joinWithCassandraTable("mdb","experiment", CassandraJavaUtil.someColumns("experimentid","description"), CassandraJavaUtil.someColumns("experimentid"), CassandraJavaUtil.mapRowTo(ExperimentFormExDes.class), CassandraJavaUtil.mapToRow(ExperimentFormOnlyParKey.class));
            metlistinitial =sp.createDataset(predf.map(x->x._2).rdd(), ExpEncoderoftwo).toDF( "description","experimentid").groupBy(col("experimentid"),col("description")).count();
        }else if(ionmode.equals("positive")){
            Encoder<ExperimentFormOnlyParKey> ExpEncoder = Encoders.bean(ExperimentFormOnlyParKey.class);
            Encoder<ExperimentFormExDesAd> ExpEncoderofthree = Encoders.bean(ExperimentFormExDesAd.class);
            Dataset<ExperimentFormOnlyParKey> dfexplistoriginal = sp.createDataset(expids, Encoders.STRING()).toDF("experimentid").as(ExpEncoder); 
            CassandraJavaPairRDD<ExperimentFormOnlyParKey, ExperimentFormExDesAd> predf = CassandraJavaUtil.javaFunctions(CassandraJavaUtil.javaFunctions(dfexplistoriginal.javaRDD()).repartitionByCassandraReplica("mdb","experiment",100,CassandraJavaUtil.someColumns("experimentid"),CassandraJavaUtil.mapToRow(ExperimentFormOnlyParKey.class)))
                                                                                .joinWithCassandraTable("mdb","experiment", CassandraJavaUtil.someColumns("experimentid","description","adduct"), CassandraJavaUtil.someColumns("experimentid"), CassandraJavaUtil.mapRowTo(ExperimentFormExDesAd.class), CassandraJavaUtil.mapToRow(ExperimentFormOnlyParKey.class));
            Dataset<Row> metlistinitialbef =sp.createDataset(predf.map(x->x._2).rdd(), ExpEncoderofthree).toDF( "adduct","description","experimentid");
            Dataset<Row> metlistinitialbe= metlistinitialbef.withColumn("ioniz", functions.substring(col("adduct"), -1, 1)).drop("adduct").filter(col("ioniz").equalTo("+"));
            metlistinitial = metlistinitialbe.drop(col("ioniz")).groupBy(col("experimentid"),col("description")).count();
        }
        else if(ionmode.equals("negative")){
            Encoder<ExperimentFormOnlyParKey> ExpEncoder = Encoders.bean(ExperimentFormOnlyParKey.class);
            Encoder<ExperimentFormExDesAd> ExpEncoderofthree = Encoders.bean(ExperimentFormExDesAd.class);
            Dataset<ExperimentFormOnlyParKey> dfexplistoriginal = sp.createDataset(expids, Encoders.STRING()).toDF("experimentid").as(ExpEncoder); 
            CassandraJavaPairRDD<ExperimentFormOnlyParKey, ExperimentFormExDesAd> predf = CassandraJavaUtil.javaFunctions(CassandraJavaUtil.javaFunctions(dfexplistoriginal.javaRDD()).repartitionByCassandraReplica("mdb","experiment",100,CassandraJavaUtil.someColumns("experimentid"),CassandraJavaUtil.mapToRow(ExperimentFormOnlyParKey.class)))
                                                                                .joinWithCassandraTable("mdb","experiment", CassandraJavaUtil.someColumns("experimentid","description","adduct"), CassandraJavaUtil.someColumns("experimentid"), CassandraJavaUtil.mapRowTo(ExperimentFormExDesAd.class), CassandraJavaUtil.mapToRow(ExperimentFormOnlyParKey.class));
            Dataset<Row> metlistinitialbef =sp.createDataset(predf.map(x->x._2).rdd(), ExpEncoderofthree).toDF( "adduct","description","experimentid");
            Dataset<Row> metlistinitialbe= metlistinitialbef.withColumn("ioniz", functions.substring(col("adduct"), -1, 1)).drop("adduct").filter(col("ioniz").equalTo("-"));
            metlistinitial = metlistinitialbe.drop(col("ioniz")).groupBy(col("experimentid"),col("description")).count();
        }
        
        Dataset<Row> metnamesandocur=metlistinitial.select(col("description")).groupBy(col("description")).count().orderBy( asc("description"));
        int expnum=expids.size();
        Dataset<Row> metnamesandocurinall =metnamesandocur.select(col("description"),col("count")).filter(col("description").notEqual("Unidentified")).filter(col("count").equalTo(expnum));
        metlist = metnamesandocurinall.select(col("description")).as(Encoders.STRING()).collectAsList();
        
        return metlist;
    }
    
    public List<String> allMetInExperiments(List<String> expids,String ionmode){
        
        Dataset<Row> metlistinitial =sp.emptyDataFrame();
        if(ionmode.equals("both")){
            Encoder<ExperimentFormOnlyParKey> ExpEncoder = Encoders.bean(ExperimentFormOnlyParKey.class);
            Encoder<ExperimentFormExDes> ExpEncoderoftwo = Encoders.bean(ExperimentFormExDes.class);
            Dataset<ExperimentFormOnlyParKey> dfexplistoriginal = sp.createDataset(expids, Encoders.STRING()).toDF("experimentid").as(ExpEncoder);
            CassandraJavaPairRDD<ExperimentFormOnlyParKey, ExperimentFormExDes> predf = CassandraJavaUtil.javaFunctions(CassandraJavaUtil.javaFunctions(dfexplistoriginal.javaRDD()).repartitionByCassandraReplica("mdb","experiment",100,CassandraJavaUtil.someColumns("experimentid"),CassandraJavaUtil.mapToRow(ExperimentFormOnlyParKey.class)))
                                                                                .joinWithCassandraTable("mdb","experiment", CassandraJavaUtil.someColumns("experimentid","description"), CassandraJavaUtil.someColumns("experimentid"), CassandraJavaUtil.mapRowTo(ExperimentFormExDes.class), CassandraJavaUtil.mapToRow(ExperimentFormOnlyParKey.class));
            metlistinitial =sp.createDataset(predf.map(x->x._2).rdd(), ExpEncoderoftwo).toDF( "description","experimentid").filter(col("description").notEqual("Unidentified"));
        }else if(ionmode.equals("positive")){
            Encoder<ExperimentFormOnlyParKey> ExpEncoder = Encoders.bean(ExperimentFormOnlyParKey.class);
            Encoder<ExperimentFormExDesAd> ExpEncoderofthree = Encoders.bean(ExperimentFormExDesAd.class);
            Dataset<ExperimentFormOnlyParKey> dfexplistoriginal = sp.createDataset(expids, Encoders.STRING()).toDF("experimentid").as(ExpEncoder);
            CassandraJavaPairRDD<ExperimentFormOnlyParKey, ExperimentFormExDesAd> predf = CassandraJavaUtil.javaFunctions(CassandraJavaUtil.javaFunctions(dfexplistoriginal.javaRDD()).repartitionByCassandraReplica("mdb","experiment",100,CassandraJavaUtil.someColumns("experimentid"),CassandraJavaUtil.mapToRow(ExperimentFormOnlyParKey.class)))
                                                                                .joinWithCassandraTable("mdb","experiment", CassandraJavaUtil.someColumns("experimentid","description","adduct"), CassandraJavaUtil.someColumns("experimentid"), CassandraJavaUtil.mapRowTo(ExperimentFormExDesAd.class), CassandraJavaUtil.mapToRow(ExperimentFormOnlyParKey.class));
            Dataset<Row> metlistinitialbef =sp.createDataset(predf.map(x->x._2).rdd(), ExpEncoderofthree).toDF( "adduct","description","experimentid").filter(col("description").notEqual("Unidentified"));
            Dataset<Row> metlistinitialbe= metlistinitialbef.withColumn("ioniz", functions.substring(col("adduct"), -1, 1)).drop("adduct").filter(col("ioniz").equalTo("+"));
            metlistinitial = metlistinitialbe.drop(col("ioniz"));
        }
        else if(ionmode.equals("negative")){
            Encoder<ExperimentFormOnlyParKey> ExpEncoder = Encoders.bean(ExperimentFormOnlyParKey.class);
            Encoder<ExperimentFormExDesAd> ExpEncoderofthree = Encoders.bean(ExperimentFormExDesAd.class);
            Dataset<ExperimentFormOnlyParKey> dfexplistoriginal = sp.createDataset(expids, Encoders.STRING()).toDF("experimentid").as(ExpEncoder);
            CassandraJavaPairRDD<ExperimentFormOnlyParKey, ExperimentFormExDesAd> predf = CassandraJavaUtil.javaFunctions(CassandraJavaUtil.javaFunctions(dfexplistoriginal.javaRDD()).repartitionByCassandraReplica("mdb","experiment",100,CassandraJavaUtil.someColumns("experimentid"),CassandraJavaUtil.mapToRow(ExperimentFormOnlyParKey.class)))
                                                                                .joinWithCassandraTable("mdb","experiment", CassandraJavaUtil.someColumns("experimentid","description","adduct"), CassandraJavaUtil.someColumns("experimentid"), CassandraJavaUtil.mapRowTo(ExperimentFormExDesAd.class), CassandraJavaUtil.mapToRow(ExperimentFormOnlyParKey.class));
            Dataset<Row> metlistinitialbef =sp.createDataset(predf.map(x->x._2).rdd(), ExpEncoderofthree).toDF( "adduct","description","experimentid").filter(col("description").notEqual("Unidentified"));
            Dataset<Row> metlistinitialbe= metlistinitialbef.withColumn("ioniz", functions.substring(col("adduct"), -1, 1)).drop("adduct").filter(col("ioniz").equalTo("-"));
            metlistinitial = metlistinitialbe.drop(col("ioniz"));
        }
        List<String> metlist = metlistinitial.select(col("description")).distinct().orderBy(col("description")).as(Encoders.STRING()).collectAsList();
         
        return metlist;
    }
    
    public List<ProjectExperimentFormExNa> bsidname (List<String> expids){
        Dataset<Row> dfexplist = sp.createDataset(expids, Encoders.STRING()).toDF("experimentid");
        List<ProjectExperimentFormExNa> expidname=new ArrayList<>();
        Encoder<ProjectExperimentFormExNa> BoxplotmtbsEncoder1 = Encoders.bean(ProjectExperimentFormExNa.class);
        Dataset<Row> mtblistinitial = sp.read().format("org.apache.spark.sql.cassandra")
               .options(new HashMap<String, String>() {
                   {
                       put("keyspace", "mdb");
                       put("table", "projectexperiment");
                   }
               })
               .load().select(col("experimentid"), col("expname")).join(dfexplist,"experimentid");
        Dataset<ProjectExperimentFormExNa> finalboxplot = mtblistinitial.as(BoxplotmtbsEncoder1);
        expidname=finalboxplot.collectAsList();
       return expidname;
    }
    
    public List<Basicstatsmtbs> basicstats(List<String> experimentlist,List<String> metlist,String ionmode){
         
         Dataset<Row> dfmetabolitelist = sp.createDataset(metlist, Encoders.STRING()).toDF("description");
         Encoder<Basicstatsmtbs> BasicstatsmtbsEncoder1 = Encoders.bean(Basicstatsmtbs.class);

        Dataset<Row> mtblistinitial =sp.emptyDataFrame();
        if(ionmode.equals("both")){
            Encoder<ExperimentFormOnlyParKey> ExpEncoder = Encoders.bean(ExperimentFormOnlyParKey.class);
            Encoder<ExperimentFormExDesIn> ExpEncoderofthree = Encoders.bean(ExperimentFormExDesIn.class);
            Dataset<ExperimentFormOnlyParKey> dfexplistoriginal = sp.createDataset(experimentlist, Encoders.STRING()).toDF("experimentid").as(ExpEncoder);
            CassandraJavaPairRDD<ExperimentFormOnlyParKey, ExperimentFormExDesIn> predf = CassandraJavaUtil.javaFunctions(CassandraJavaUtil.javaFunctions(dfexplistoriginal.javaRDD()).repartitionByCassandraReplica("mdb","experiment",100,CassandraJavaUtil.someColumns("experimentid"),CassandraJavaUtil.mapToRow(ExperimentFormOnlyParKey.class)))
                                                                                .joinWithCassandraTable("mdb","experiment", CassandraJavaUtil.someColumns("experimentid","description","intensity"), CassandraJavaUtil.someColumns("experimentid"), CassandraJavaUtil.mapRowTo(ExperimentFormExDesIn.class), CassandraJavaUtil.mapToRow(ExperimentFormOnlyParKey.class));
            mtblistinitial =sp.createDataset(predf.map(x->x._2).rdd(), ExpEncoderofthree).toDF( "description","experimentid","intensity").filter(col("description").notEqual("Unidentified")).join(dfmetabolitelist,"description");
        }else if(ionmode.equals("positive")){
            Encoder<ExperimentFormOnlyParKey> ExpEncoder = Encoders.bean(ExperimentFormOnlyParKey.class);
            Encoder<ExperimentFormExDesInAd> ExpEncoderofthree = Encoders.bean(ExperimentFormExDesInAd.class);
            Dataset<ExperimentFormOnlyParKey> dfexplistoriginal = sp.createDataset(experimentlist, Encoders.STRING()).toDF("experimentid").as(ExpEncoder); 
            CassandraJavaPairRDD<ExperimentFormOnlyParKey, ExperimentFormExDesInAd> predf = CassandraJavaUtil.javaFunctions(CassandraJavaUtil.javaFunctions(dfexplistoriginal.javaRDD()).repartitionByCassandraReplica("mdb","experiment",100,CassandraJavaUtil.someColumns("experimentid"),CassandraJavaUtil.mapToRow(ExperimentFormOnlyParKey.class)))
                                                                                .joinWithCassandraTable("mdb","experiment", CassandraJavaUtil.someColumns("experimentid","description","adduct","intensity"), CassandraJavaUtil.someColumns("experimentid"), CassandraJavaUtil.mapRowTo(ExperimentFormExDesInAd.class), CassandraJavaUtil.mapToRow(ExperimentFormOnlyParKey.class));
            Dataset<Row> metlistinitialbef =sp.createDataset(predf.map(x->x._2).rdd(), ExpEncoderofthree).toDF( "adduct","description","experimentid","intensity").filter(col("description").notEqual("Unidentified"));
            Dataset<Row> metlistinitialbe= metlistinitialbef.withColumn("ioniz", functions.substring(col("adduct"), -1, 1)).drop("adduct").filter(col("ioniz").equalTo("+"));
            mtblistinitial = metlistinitialbe.drop(col("ioniz")).join(dfmetabolitelist,"description");
        }else if(ionmode.equals("negative")){
            Encoder<ExperimentFormOnlyParKey> ExpEncoder = Encoders.bean(ExperimentFormOnlyParKey.class);
            Encoder<ExperimentFormExDesInAd> ExpEncoderofthree = Encoders.bean(ExperimentFormExDesInAd.class);
            Dataset<ExperimentFormOnlyParKey> dfexplistoriginal = sp.createDataset(experimentlist, Encoders.STRING()).toDF("experimentid").as(ExpEncoder); 
            CassandraJavaPairRDD<ExperimentFormOnlyParKey, ExperimentFormExDesInAd> predf = CassandraJavaUtil.javaFunctions(CassandraJavaUtil.javaFunctions(dfexplistoriginal.javaRDD()).repartitionByCassandraReplica("mdb","experiment",100,CassandraJavaUtil.someColumns("experimentid"),CassandraJavaUtil.mapToRow(ExperimentFormOnlyParKey.class)))
                                                                                .joinWithCassandraTable("mdb","experiment", CassandraJavaUtil.someColumns("experimentid","description","adduct","intensity"), CassandraJavaUtil.someColumns("experimentid"), CassandraJavaUtil.mapRowTo(ExperimentFormExDesInAd.class), CassandraJavaUtil.mapToRow(ExperimentFormOnlyParKey.class));
            Dataset<Row> metlistinitialbef =sp.createDataset(predf.map(x->x._2).rdd(), ExpEncoderofthree).toDF( "adduct","description","experimentid","intensity").filter(col("description").notEqual("Unidentified"));
            Dataset<Row> metlistinitialbe= metlistinitialbef.withColumn("ioniz", functions.substring(col("adduct"), -1, 1)).drop("adduct").filter(col("ioniz").equalTo("-"));
            mtblistinitial = metlistinitialbe.drop(col("ioniz")).join(dfmetabolitelist,"description");
        }
         
         Dataset<Row> meanstdmtblistnew = mtblistinitial.groupBy(col("experimentid"),col("description")).agg(functions.mean("intensity").alias("mean"),functions.stddev("intensity").alias("std")).orderBy(col("experimentid"),col("description"));
         
         Dataset<Basicstatsmtbs> bstemp =  meanstdmtblistnew.as(BasicstatsmtbsEncoder1);
         List<Basicstatsmtbs> bs=bstemp.collectAsList();
         
           
         return bs;
     }
    
    public List<Basicstatsmtbs> basicstats2(List<String> experimentlist,List<String> metlist,String ionmode){
         
         Dataset<Row> dfmetabolitelist = sp.createDataset(metlist, Encoders.STRING()).toDF("description");
         Encoder<Basicstatsmtbs> BasicstatsmtbsEncoder1 = Encoders.bean(Basicstatsmtbs.class);

         Dataset<Row> mtblistinitial =sp.emptyDataFrame();
        if(ionmode.equals("both")){
            Encoder<ExperimentFormOnlyParKey> ExpEncoder = Encoders.bean(ExperimentFormOnlyParKey.class);
            Encoder<ExperimentFormExDesIn> ExpEncoderofthree = Encoders.bean(ExperimentFormExDesIn.class);
            Dataset<ExperimentFormOnlyParKey> dfexplistoriginal = sp.createDataset(experimentlist, Encoders.STRING()).toDF("experimentid").as(ExpEncoder);
            CassandraJavaPairRDD<ExperimentFormOnlyParKey, ExperimentFormExDesIn> predf = CassandraJavaUtil.javaFunctions(CassandraJavaUtil.javaFunctions(dfexplistoriginal.javaRDD()).repartitionByCassandraReplica("mdb","experiment",100,CassandraJavaUtil.someColumns("experimentid"),CassandraJavaUtil.mapToRow(ExperimentFormOnlyParKey.class)))
                                                                                .joinWithCassandraTable("mdb","experiment", CassandraJavaUtil.someColumns("experimentid","description","intensity"), CassandraJavaUtil.someColumns("experimentid"), CassandraJavaUtil.mapRowTo(ExperimentFormExDesIn.class), CassandraJavaUtil.mapToRow(ExperimentFormOnlyParKey.class));
            mtblistinitial =sp.createDataset(predf.map(x->x._2).rdd(), ExpEncoderofthree).toDF( "description","experimentid","intensity").filter(col("description").notEqual("Unidentified")).join(dfmetabolitelist,"description");
        }else if(ionmode.equals("positive")){
            Encoder<ExperimentFormOnlyParKey> ExpEncoder = Encoders.bean(ExperimentFormOnlyParKey.class);
            Encoder<ExperimentFormExDesInAd> ExpEncoderofthree = Encoders.bean(ExperimentFormExDesInAd.class);
            Dataset<ExperimentFormOnlyParKey> dfexplistoriginal = sp.createDataset(experimentlist, Encoders.STRING()).toDF("experimentid").as(ExpEncoder);
            CassandraJavaPairRDD<ExperimentFormOnlyParKey, ExperimentFormExDesInAd> predf = CassandraJavaUtil.javaFunctions(CassandraJavaUtil.javaFunctions(dfexplistoriginal.javaRDD()).repartitionByCassandraReplica("mdb","experiment",100,CassandraJavaUtil.someColumns("experimentid"),CassandraJavaUtil.mapToRow(ExperimentFormOnlyParKey.class)))
                                                                                .joinWithCassandraTable("mdb","experiment", CassandraJavaUtil.someColumns("experimentid","description","adduct","intensity"), CassandraJavaUtil.someColumns("experimentid"), CassandraJavaUtil.mapRowTo(ExperimentFormExDesInAd.class), CassandraJavaUtil.mapToRow(ExperimentFormOnlyParKey.class));
            Dataset<Row> metlistinitialbef =sp.createDataset(predf.map(x->x._2).rdd(), ExpEncoderofthree).toDF( "adduct","description","experimentid","intensity").filter(col("description").notEqual("Unidentified"));
            Dataset<Row> metlistinitialbe= metlistinitialbef.withColumn("ioniz", functions.substring(col("adduct"), -1, 1)).drop("adduct").filter(col("ioniz").equalTo("+"));
            mtblistinitial = metlistinitialbe.drop(col("ioniz")).join(dfmetabolitelist,"description");
        }else if(ionmode.equals("negative")){
            Encoder<ExperimentFormOnlyParKey> ExpEncoder = Encoders.bean(ExperimentFormOnlyParKey.class);
            Encoder<ExperimentFormExDesInAd> ExpEncoderofthree = Encoders.bean(ExperimentFormExDesInAd.class);
            Dataset<ExperimentFormOnlyParKey> dfexplistoriginal = sp.createDataset(experimentlist, Encoders.STRING()).toDF("experimentid").as(ExpEncoder); 
            CassandraJavaPairRDD<ExperimentFormOnlyParKey, ExperimentFormExDesInAd> predf = CassandraJavaUtil.javaFunctions(CassandraJavaUtil.javaFunctions(dfexplistoriginal.javaRDD()).repartitionByCassandraReplica("mdb","experiment",100,CassandraJavaUtil.someColumns("experimentid"),CassandraJavaUtil.mapToRow(ExperimentFormOnlyParKey.class)))
                                                                                .joinWithCassandraTable("mdb","experiment", CassandraJavaUtil.someColumns("experimentid","description","adduct","intensity"), CassandraJavaUtil.someColumns("experimentid"), CassandraJavaUtil.mapRowTo(ExperimentFormExDesInAd.class), CassandraJavaUtil.mapToRow(ExperimentFormOnlyParKey.class));
            Dataset<Row> metlistinitialbef =sp.createDataset(predf.map(x->x._2).rdd(), ExpEncoderofthree).toDF( "adduct","description","experimentid","intensity").filter(col("description").notEqual("Unidentified"));
            Dataset<Row> metlistinitialbe= metlistinitialbef.withColumn("ioniz", functions.substring(col("adduct"), -1, 1)).drop("adduct").filter(col("ioniz").equalTo("-"));
            mtblistinitial = metlistinitialbe.drop(col("ioniz")).join(dfmetabolitelist,"description");
        }
         
         Dataset<Row> meanstdmtblistnew = mtblistinitial.select(col("description"),col("intensity")).groupBy(col("description")).agg(functions.mean("intensity").alias("mean"),functions.stddev("intensity").alias("std")).orderBy(col("description")).withColumn("experimentid", functions.lit(null).cast("string"));
         Dataset<Basicstatsmtbs> bstemp =  meanstdmtblistnew.as(BasicstatsmtbsEncoder1);
         List<Basicstatsmtbs> bs=bstemp.collectAsList();
         
         return bs;
     }
    
}
