/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.example.app.converters;

import com.datastax.spark.connector.japi.CassandraJavaUtil;
import com.datastax.spark.connector.japi.rdd.CassandraJavaPairRDD;
import com.example.app.domain.ExperimentFormExDes;
import com.example.app.domain.ExperimentFormExDesFl;
import com.example.app.domain.ExperimentFormExDesFlIn;
import com.example.app.domain.ExperimentFormExDesIn;
import com.example.app.domain.ExperimentFormOnlyParKey;
import com.example.app.commands.Experimentslistclustering;
import com.example.app.commands.Metaboliteslistclustering;
import com.example.app.commands.Replicateslistclustering;
import com.example.app.domain.ExperimentFormExDesAd;
import com.example.app.domain.ExperimentFormExDesFlAd;
import com.example.app.domain.ExperimentFormExDesFlInAd;
import com.example.app.domain.ExperimentFormExDesInAd;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.TreeMap;
import org.apache.spark.api.java.function.MapFunction;
import org.apache.spark.ml.clustering.BisectingKMeans;
import org.apache.spark.ml.clustering.BisectingKMeansModel;
import org.apache.spark.ml.clustering.KMeans;
import org.apache.spark.ml.clustering.KMeansModel;
import org.apache.spark.ml.evaluation.ClusteringEvaluator;
import org.apache.spark.ml.feature.Imputer;
import org.apache.spark.ml.feature.ImputerModel;
import org.apache.spark.ml.feature.StandardScaler;
import org.apache.spark.ml.feature.StandardScalerModel;
import org.apache.spark.ml.feature.VectorAssembler;
import org.apache.spark.ml.linalg.Vector;
import org.apache.spark.ml.linalg.VectorUDT;
import org.apache.spark.ml.linalg.Vectors;
import org.apache.spark.sql.Dataset;
import org.apache.spark.sql.Encoder;
import org.apache.spark.sql.Encoders;
import org.apache.spark.sql.Row;
import org.apache.spark.sql.RowFactory;
import org.apache.spark.sql.SparkSession;
import org.apache.spark.sql.catalyst.encoders.RowEncoder;
import org.apache.spark.sql.functions;
import static org.apache.spark.sql.functions.asc;
import static org.apache.spark.sql.functions.callUDF;
import static org.apache.spark.sql.functions.col;
import static org.apache.spark.sql.functions.lit;
import static org.apache.spark.sql.functions.sum;
import org.apache.spark.sql.types.DataTypes;
import org.apache.spark.sql.types.Metadata;
import org.apache.spark.sql.types.StructField;
import org.apache.spark.sql.types.StructType;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import scala.collection.JavaConversions;
/**
 *
 * @author kostas
 */
@Component
public class SparkStatisticsClustering {
    
    @Autowired
    private SparkSession sp;
    
    public List<String> omitInExperiments(List<String> expids,String ionmode){
        
        List<String> metlist = new ArrayList<>();
        
        Dataset<Row> metlistinitial =sp.emptyDataFrame();
        if(ionmode.equals("both")){
            Encoder<ExperimentFormOnlyParKey> ExpEncoder = Encoders.bean(ExperimentFormOnlyParKey.class);
            Encoder<ExperimentFormExDes> ExpEncoderoftwo = Encoders.bean(ExperimentFormExDes.class);
            Dataset<ExperimentFormOnlyParKey> dfexplistoriginal = sp.createDataset(expids, Encoders.STRING()).toDF("experimentid").as(ExpEncoder);
            CassandraJavaPairRDD<ExperimentFormOnlyParKey, ExperimentFormExDes> predf = CassandraJavaUtil.javaFunctions(CassandraJavaUtil.javaFunctions(dfexplistoriginal.javaRDD()).repartitionByCassandraReplica("mdb","experiment",100,CassandraJavaUtil.someColumns("experimentid"),CassandraJavaUtil.mapToRow(ExperimentFormOnlyParKey.class)))
                                                                                .joinWithCassandraTable("mdb","experiment", CassandraJavaUtil.someColumns("experimentid","description"), CassandraJavaUtil.someColumns("experimentid"), CassandraJavaUtil.mapRowTo(ExperimentFormExDes.class), CassandraJavaUtil.mapToRow(ExperimentFormOnlyParKey.class));
            metlistinitial =sp.createDataset(predf.map(x->x._2).rdd(), ExpEncoderoftwo).toDF( "description","experimentid").groupBy(col("experimentid"),col("description")).count();
        }else if(ionmode.equals("positive")){
            Encoder<ExperimentFormOnlyParKey> ExpEncoder = Encoders.bean(ExperimentFormOnlyParKey.class);
            Encoder<ExperimentFormExDesAd> ExpEncoderofthree = Encoders.bean(ExperimentFormExDesAd.class);
            Dataset<ExperimentFormOnlyParKey> dfexplistoriginal = sp.createDataset(expids, Encoders.STRING()).toDF("experimentid").as(ExpEncoder);
            CassandraJavaPairRDD<ExperimentFormOnlyParKey, ExperimentFormExDesAd> predf = CassandraJavaUtil.javaFunctions(CassandraJavaUtil.javaFunctions(dfexplistoriginal.javaRDD()).repartitionByCassandraReplica("mdb","experiment",100,CassandraJavaUtil.someColumns("experimentid"),CassandraJavaUtil.mapToRow(ExperimentFormOnlyParKey.class)))
                                                                                .joinWithCassandraTable("mdb","experiment", CassandraJavaUtil.someColumns("experimentid","description","adduct"), CassandraJavaUtil.someColumns("experimentid"), CassandraJavaUtil.mapRowTo(ExperimentFormExDesAd.class), CassandraJavaUtil.mapToRow(ExperimentFormOnlyParKey.class));
            Dataset<Row> metlistinitialbef =sp.createDataset(predf.map(x->x._2).rdd(), ExpEncoderofthree).toDF( "adduct","description","experimentid");
            Dataset<Row> metlistinitialbe= metlistinitialbef.withColumn("ioniz", functions.substring(col("adduct"), -1, 1)).drop("adduct").filter(col("ioniz").equalTo("+"));
            metlistinitial = metlistinitialbe.drop(col("ioniz")).groupBy(col("experimentid"),col("description")).count();
        }
        else if(ionmode.equals("negative")){
            Encoder<ExperimentFormOnlyParKey> ExpEncoder = Encoders.bean(ExperimentFormOnlyParKey.class);
            Encoder<ExperimentFormExDesAd> ExpEncoderofthree = Encoders.bean(ExperimentFormExDesAd.class);
            Dataset<ExperimentFormOnlyParKey> dfexplistoriginal = sp.createDataset(expids, Encoders.STRING()).toDF("experimentid").as(ExpEncoder);
            CassandraJavaPairRDD<ExperimentFormOnlyParKey, ExperimentFormExDesAd> predf = CassandraJavaUtil.javaFunctions(CassandraJavaUtil.javaFunctions(dfexplistoriginal.javaRDD()).repartitionByCassandraReplica("mdb","experiment",100,CassandraJavaUtil.someColumns("experimentid"),CassandraJavaUtil.mapToRow(ExperimentFormOnlyParKey.class)))
                                                                                .joinWithCassandraTable("mdb","experiment", CassandraJavaUtil.someColumns("experimentid","description","adduct"), CassandraJavaUtil.someColumns("experimentid"), CassandraJavaUtil.mapRowTo(ExperimentFormExDesAd.class), CassandraJavaUtil.mapToRow(ExperimentFormOnlyParKey.class));
            Dataset<Row> metlistinitialbef =sp.createDataset(predf.map(x->x._2).rdd(), ExpEncoderofthree).toDF( "adduct","description","experimentid");
            Dataset<Row> metlistinitialbe= metlistinitialbef.withColumn("ioniz", functions.substring(col("adduct"), -1, 1)).drop("adduct").filter(col("ioniz").equalTo("-"));
            metlistinitial = metlistinitialbe.drop(col("ioniz")).groupBy(col("experimentid"),col("description")).count();
        }
        
        Dataset<Row> metnamesandocur=metlistinitial.select(col("description")).groupBy(col("description")).count().orderBy( asc("description"));
        int expnum=expids.size();
        Dataset<Row> metnamesandocurinall =metnamesandocur.select(col("description"),col("count")).filter(col("description").notEqual("Unidentified")).filter(col("count").equalTo(expnum));
        metlist = metnamesandocurinall.select(col("description")).as(Encoders.STRING()).collectAsList();
        return metlist;
    }
    
    public List<String> replaceWithValueInExperiments (List<String> expids,String ionmode){
        List<String> metlist = new ArrayList<>();
        
        Dataset<Row> metlistinitial =sp.emptyDataFrame();
        if(ionmode.equals("both")){
            Encoder<ExperimentFormOnlyParKey> ExpEncoder = Encoders.bean(ExperimentFormOnlyParKey.class);
            Encoder<ExperimentFormExDes> ExpEncoderoftwo = Encoders.bean(ExperimentFormExDes.class);
            Dataset<ExperimentFormOnlyParKey> dfexplistoriginal = sp.createDataset(expids, Encoders.STRING()).toDF("experimentid").as(ExpEncoder);
            CassandraJavaPairRDD<ExperimentFormOnlyParKey, ExperimentFormExDes> predf = CassandraJavaUtil.javaFunctions(CassandraJavaUtil.javaFunctions(dfexplistoriginal.javaRDD()).repartitionByCassandraReplica("mdb","experiment",100,CassandraJavaUtil.someColumns("experimentid"),CassandraJavaUtil.mapToRow(ExperimentFormOnlyParKey.class)))
                                                                                .joinWithCassandraTable("mdb","experiment", CassandraJavaUtil.someColumns("experimentid","description"), CassandraJavaUtil.someColumns("experimentid"), CassandraJavaUtil.mapRowTo(ExperimentFormExDes.class), CassandraJavaUtil.mapToRow(ExperimentFormOnlyParKey.class));
            metlistinitial =sp.createDataset(predf.map(x->x._2).rdd(), ExpEncoderoftwo).toDF( "description","experimentid").filter(col("description").notEqual("Unidentified"));
        }else if(ionmode.equals("positive")){
            Encoder<ExperimentFormOnlyParKey> ExpEncoder = Encoders.bean(ExperimentFormOnlyParKey.class);
            Encoder<ExperimentFormExDesAd> ExpEncoderofthree = Encoders.bean(ExperimentFormExDesAd.class);
            Dataset<ExperimentFormOnlyParKey> dfexplistoriginal = sp.createDataset(expids, Encoders.STRING()).toDF("experimentid").as(ExpEncoder);
            CassandraJavaPairRDD<ExperimentFormOnlyParKey, ExperimentFormExDesAd> predf = CassandraJavaUtil.javaFunctions(CassandraJavaUtil.javaFunctions(dfexplistoriginal.javaRDD()).repartitionByCassandraReplica("mdb","experiment",100,CassandraJavaUtil.someColumns("experimentid"),CassandraJavaUtil.mapToRow(ExperimentFormOnlyParKey.class)))
                                                                                .joinWithCassandraTable("mdb","experiment", CassandraJavaUtil.someColumns("experimentid","description","adduct"), CassandraJavaUtil.someColumns("experimentid"), CassandraJavaUtil.mapRowTo(ExperimentFormExDesAd.class), CassandraJavaUtil.mapToRow(ExperimentFormOnlyParKey.class));
            Dataset<Row> metlistinitialbef =sp.createDataset(predf.map(x->x._2).rdd(), ExpEncoderofthree).toDF( "adduct","description","experimentid").filter(col("description").notEqual("Unidentified"));
            Dataset<Row> metlistinitialbe= metlistinitialbef.withColumn("ioniz", functions.substring(col("adduct"), -1, 1)).drop("adduct").filter(col("ioniz").equalTo("+"));
            metlistinitial = metlistinitialbe.drop(col("ioniz"));
        }
        else if(ionmode.equals("negative")){
            Encoder<ExperimentFormOnlyParKey> ExpEncoder = Encoders.bean(ExperimentFormOnlyParKey.class);
            Encoder<ExperimentFormExDesAd> ExpEncoderofthree = Encoders.bean(ExperimentFormExDesAd.class);
            Dataset<ExperimentFormOnlyParKey> dfexplistoriginal = sp.createDataset(expids, Encoders.STRING()).toDF("experimentid").as(ExpEncoder);
            CassandraJavaPairRDD<ExperimentFormOnlyParKey, ExperimentFormExDesAd> predf = CassandraJavaUtil.javaFunctions(CassandraJavaUtil.javaFunctions(dfexplistoriginal.javaRDD()).repartitionByCassandraReplica("mdb","experiment",100,CassandraJavaUtil.someColumns("experimentid"),CassandraJavaUtil.mapToRow(ExperimentFormOnlyParKey.class)))
                                                                                .joinWithCassandraTable("mdb","experiment", CassandraJavaUtil.someColumns("experimentid","description","adduct"), CassandraJavaUtil.someColumns("experimentid"), CassandraJavaUtil.mapRowTo(ExperimentFormExDesAd.class), CassandraJavaUtil.mapToRow(ExperimentFormOnlyParKey.class));
            Dataset<Row> metlistinitialbef =sp.createDataset(predf.map(x->x._2).rdd(), ExpEncoderofthree).toDF( "adduct","description","experimentid").filter(col("description").notEqual("Unidentified"));
            Dataset<Row> metlistinitialbe= metlistinitialbef.withColumn("ioniz", functions.substring(col("adduct"), -1, 1)).drop("adduct").filter(col("ioniz").equalTo("-"));
            metlistinitial = metlistinitialbe.drop(col("ioniz"));
        }
        
        Dataset<Row> mtbdistinct =metlistinitial.select(col("description")).distinct();
        metlist = mtbdistinct.as(Encoders.STRING()).collectAsList();
        return metlist;
    }
    
    public List<String> omitInReplicates(List<String> expids,String ionmode){
        List<String> metlist = new ArrayList<>();
        Dataset<Row> dfexplist = sp.createDataset(expids, Encoders.STRING()).toDF("experimentid");
        
        Dataset<Row> expsidsandreplnum = sp.read().format("org.apache.spark.sql.cassandra")
                .options(new HashMap<String, String>() {
                    {
                        put("keyspace", "mdb");
                        put("table", "projectexperiment");
                    }
                })
                .load().select(col("experimentid"),col("replicatesnum")).join(dfexplist,"experimentid");

        List<Long> totalreplnumlist=expsidsandreplnum.select(col("replicatesnum")).agg(sum(col("replicatesnum"))).as(Encoders.LONG()).collectAsList();
        Long totalreplnum = totalreplnumlist.get(0);

        Dataset<Row> metlistinitial =sp.emptyDataFrame();
        if(ionmode.equals("both")){
            Encoder<ExperimentFormOnlyParKey> ExpEncoder = Encoders.bean(ExperimentFormOnlyParKey.class);
            Encoder<ExperimentFormExDesFl> ExpEncoderofthree = Encoders.bean(ExperimentFormExDesFl.class);
            Dataset<ExperimentFormOnlyParKey> dfexplistoriginal = sp.createDataset(expids, Encoders.STRING()).toDF("experimentid").as(ExpEncoder);
            CassandraJavaPairRDD<ExperimentFormOnlyParKey, ExperimentFormExDesFl> predf2 = CassandraJavaUtil.javaFunctions(CassandraJavaUtil.javaFunctions(dfexplistoriginal.javaRDD()).repartitionByCassandraReplica("mdb","experiment",100,CassandraJavaUtil.someColumns("experimentid"),CassandraJavaUtil.mapToRow(ExperimentFormOnlyParKey.class)))
                                                                                .joinWithCassandraTable("mdb","experiment", CassandraJavaUtil.someColumns("experimentid","description","filename"), CassandraJavaUtil.someColumns("experimentid"), CassandraJavaUtil.mapRowTo(ExperimentFormExDesFl.class), CassandraJavaUtil.mapToRow(ExperimentFormOnlyParKey.class));
            metlistinitial =sp.createDataset(predf2.map(x->x._2).rdd(), ExpEncoderofthree).toDF( "description","experimentid","filename").groupBy(col("experimentid"),col("filename"),col("description")).count();

        }else if(ionmode.equals("positive")){
            Encoder<ExperimentFormOnlyParKey> ExpEncoder = Encoders.bean(ExperimentFormOnlyParKey.class);
            Encoder<ExperimentFormExDesFlAd> ExpEncoderofthree = Encoders.bean(ExperimentFormExDesFlAd.class);
            Dataset<ExperimentFormOnlyParKey> dfexplistoriginal = sp.createDataset(expids, Encoders.STRING()).toDF("experimentid").as(ExpEncoder);
            CassandraJavaPairRDD<ExperimentFormOnlyParKey, ExperimentFormExDesFlAd> predf = CassandraJavaUtil.javaFunctions(CassandraJavaUtil.javaFunctions(dfexplistoriginal.javaRDD()).repartitionByCassandraReplica("mdb","experiment",100,CassandraJavaUtil.someColumns("experimentid"),CassandraJavaUtil.mapToRow(ExperimentFormOnlyParKey.class)))
                                                                                .joinWithCassandraTable("mdb","experiment", CassandraJavaUtil.someColumns("experimentid","filename","description","adduct"), CassandraJavaUtil.someColumns("experimentid"), CassandraJavaUtil.mapRowTo(ExperimentFormExDesFlAd.class), CassandraJavaUtil.mapToRow(ExperimentFormOnlyParKey.class));
            Dataset<Row> metlistinitialbef =sp.createDataset(predf.map(x->x._2).rdd(), ExpEncoderofthree).toDF( "adduct","description","experimentid","filename").filter(col("description").notEqual("Unidentified"));
            Dataset<Row> metlistinitialbe= metlistinitialbef.withColumn("ioniz", functions.substring(col("adduct"), -1, 1)).drop("adduct").filter(col("ioniz").equalTo("+"));
            metlistinitial = metlistinitialbe.drop(col("ioniz")).groupBy(col("experimentid"),col("filename"),col("description")).count();
        }else if(ionmode.equals("negative")){
            Encoder<ExperimentFormOnlyParKey> ExpEncoder = Encoders.bean(ExperimentFormOnlyParKey.class);
            Encoder<ExperimentFormExDesFlAd> ExpEncoderofthree = Encoders.bean(ExperimentFormExDesFlAd.class);
            Dataset<ExperimentFormOnlyParKey> dfexplistoriginal = sp.createDataset(expids, Encoders.STRING()).toDF("experimentid").as(ExpEncoder);
            CassandraJavaPairRDD<ExperimentFormOnlyParKey, ExperimentFormExDesFlAd> predf = CassandraJavaUtil.javaFunctions(CassandraJavaUtil.javaFunctions(dfexplistoriginal.javaRDD()).repartitionByCassandraReplica("mdb","experiment",100,CassandraJavaUtil.someColumns("experimentid"),CassandraJavaUtil.mapToRow(ExperimentFormOnlyParKey.class)))
                                                                                .joinWithCassandraTable("mdb","experiment", CassandraJavaUtil.someColumns("experimentid","filename","description","adduct"), CassandraJavaUtil.someColumns("experimentid"), CassandraJavaUtil.mapRowTo(ExperimentFormExDesFlAd.class), CassandraJavaUtil.mapToRow(ExperimentFormOnlyParKey.class));
            Dataset<Row> metlistinitialbef =sp.createDataset(predf.map(x->x._2).rdd(), ExpEncoderofthree).toDF( "adduct","description","experimentid","filename").filter(col("description").notEqual("Unidentified"));
            Dataset<Row> metlistinitialbe= metlistinitialbef.withColumn("ioniz", functions.substring(col("adduct"), -1, 1)).drop("adduct").filter(col("ioniz").equalTo("-"));
            metlistinitial = metlistinitialbe.drop(col("ioniz")).groupBy(col("experimentid"),col("filename"),col("description")).count();
        }
        
        Dataset<Row> metnamesandocur=metlistinitial.select(col("description")).groupBy(col("description")).count().orderBy( asc("description"));
        Dataset<Row> metnamesandocurinall =metnamesandocur.select(col("description"),col("count")).filter(col("description").notEqual("Unidentified")).filter(col("count").equalTo(totalreplnum));
        metlist = metnamesandocurinall.select(col("description")).as(Encoders.STRING()).collectAsList();
        
        return metlist;
    }
    
    
    public List<String> replaceWithValueInReplicates(List<String> expids,String ionmode){
        List<String> metlist = new ArrayList<>();
        
        Dataset<Row> metlistinitial =sp.emptyDataFrame();
        if(ionmode.equals("both")){
                Encoder<ExperimentFormOnlyParKey> ExpEncoder = Encoders.bean(ExperimentFormOnlyParKey.class);
                Encoder<ExperimentFormExDesFl> ExpEncoderofthree = Encoders.bean(ExperimentFormExDesFl.class);
                Dataset<ExperimentFormOnlyParKey> dfexplistoriginal = sp.createDataset(expids, Encoders.STRING()).toDF("experimentid").as(ExpEncoder);
                CassandraJavaPairRDD<ExperimentFormOnlyParKey, ExperimentFormExDesFl> predf2 = CassandraJavaUtil.javaFunctions(CassandraJavaUtil.javaFunctions(dfexplistoriginal.javaRDD()).repartitionByCassandraReplica("mdb","experiment",100,CassandraJavaUtil.someColumns("experimentid"),CassandraJavaUtil.mapToRow(ExperimentFormOnlyParKey.class)))
                                                                                    .joinWithCassandraTable("mdb","experiment", CassandraJavaUtil.someColumns("experimentid","description","filename"), CassandraJavaUtil.someColumns("experimentid"), CassandraJavaUtil.mapRowTo(ExperimentFormExDesFl.class), CassandraJavaUtil.mapToRow(ExperimentFormOnlyParKey.class));
                metlistinitial =sp.createDataset(predf2.map(x->x._2).rdd(), ExpEncoderofthree).toDF( "description","experimentid","filename").filter(col("description").notEqual("Unidentified"));
        
            }else if(ionmode.equals("positive")){
                Encoder<ExperimentFormOnlyParKey> ExpEncoder = Encoders.bean(ExperimentFormOnlyParKey.class);
                Encoder<ExperimentFormExDesFlAd> ExpEncoderofthree = Encoders.bean(ExperimentFormExDesFlAd.class);
                Dataset<ExperimentFormOnlyParKey> dfexplistoriginal = sp.createDataset(expids, Encoders.STRING()).toDF("experimentid").as(ExpEncoder);
                CassandraJavaPairRDD<ExperimentFormOnlyParKey, ExperimentFormExDesFlAd> predf = CassandraJavaUtil.javaFunctions(CassandraJavaUtil.javaFunctions(dfexplistoriginal.javaRDD()).repartitionByCassandraReplica("mdb","experiment",100,CassandraJavaUtil.someColumns("experimentid"),CassandraJavaUtil.mapToRow(ExperimentFormOnlyParKey.class)))
                                                                                    .joinWithCassandraTable("mdb","experiment", CassandraJavaUtil.someColumns("experimentid","filename","description","adduct"), CassandraJavaUtil.someColumns("experimentid"), CassandraJavaUtil.mapRowTo(ExperimentFormExDesFlAd.class), CassandraJavaUtil.mapToRow(ExperimentFormOnlyParKey.class));
                Dataset<Row> metlistinitialbef =sp.createDataset(predf.map(x->x._2).rdd(), ExpEncoderofthree).toDF( "adduct","description","experimentid","filename").filter(col("description").notEqual("Unidentified"));
                Dataset<Row> metlistinitialbe= metlistinitialbef.withColumn("ioniz", functions.substring(col("adduct"), -1, 1)).drop("adduct").filter(col("ioniz").equalTo("+"));
                metlistinitial = metlistinitialbe.drop(col("ioniz"));
            }else if(ionmode.equals("negative")){
                Encoder<ExperimentFormOnlyParKey> ExpEncoder = Encoders.bean(ExperimentFormOnlyParKey.class);
                Encoder<ExperimentFormExDesFlAd> ExpEncoderofthree = Encoders.bean(ExperimentFormExDesFlAd.class);
                Dataset<ExperimentFormOnlyParKey> dfexplistoriginal = sp.createDataset(expids, Encoders.STRING()).toDF("experimentid").as(ExpEncoder);
                CassandraJavaPairRDD<ExperimentFormOnlyParKey, ExperimentFormExDesFlAd> predf = CassandraJavaUtil.javaFunctions(CassandraJavaUtil.javaFunctions(dfexplistoriginal.javaRDD()).repartitionByCassandraReplica("mdb","experiment",100,CassandraJavaUtil.someColumns("experimentid"),CassandraJavaUtil.mapToRow(ExperimentFormOnlyParKey.class)))
                                                                                    .joinWithCassandraTable("mdb","experiment", CassandraJavaUtil.someColumns("experimentid","filename","description","adduct"), CassandraJavaUtil.someColumns("experimentid"), CassandraJavaUtil.mapRowTo(ExperimentFormExDesFlAd.class), CassandraJavaUtil.mapToRow(ExperimentFormOnlyParKey.class));
                Dataset<Row> metlistinitialbef =sp.createDataset(predf.map(x->x._2).rdd(), ExpEncoderofthree).toDF( "adduct","description","experimentid","filename").filter(col("description").notEqual("Unidentified"));
                Dataset<Row> metlistinitialbe= metlistinitialbef.withColumn("ioniz", functions.substring(col("adduct"), -1, 1)).drop("adduct").filter(col("ioniz").equalTo("-"));
                metlistinitial = metlistinitialbe.drop(col("ioniz"));
            }
        
        Dataset<Row> mtbdistinct =metlistinitial.select(col("description")).distinct();
        metlist = mtbdistinct.as(Encoders.STRING()).collectAsList();

        return metlist;
    }
    
    
    public Experimentslistclustering expsclustering(List<String> experimentlist,List<String> metabolitelist,String allorsome,String missval, String clusteringmethod,Integer initialval,String ionmode,String normval) {
        
        Experimentslistclustering experimentslistclustering = new Experimentslistclustering();
        List<String> metlist = new ArrayList<>();
        List<String> missvalmtbs=new ArrayList<>();
        List<Double[]> clustcenters = new ArrayList<>();
        Integer rowflag = 0;
        
        if(allorsome.equals("onall")){
            Dataset<Row> newfilenameavgpeaks=sp.emptyDataFrame();
            if(missval.equals("omit")){
                metlist=omitInExperiments(experimentlist,ionmode);
            }else{
                metlist=replaceWithValueInExperiments(experimentlist,ionmode);
            }
            
            if (metlist != null && !metlist.isEmpty()) {
                
                Dataset<Row> metlistinitial2 =sp.emptyDataFrame();
                if(ionmode.equals("both")){
                    Encoder<ExperimentFormOnlyParKey> ExpEncoder = Encoders.bean(ExperimentFormOnlyParKey.class);
                    Encoder<ExperimentFormExDesIn> ExpEncoderofthree = Encoders.bean(ExperimentFormExDesIn.class);
                    Dataset<ExperimentFormOnlyParKey> dfexplistoriginal = sp.createDataset(experimentlist, Encoders.STRING()).toDF("experimentid").as(ExpEncoder);
                    CassandraJavaPairRDD<ExperimentFormOnlyParKey, ExperimentFormExDesIn> predf = CassandraJavaUtil.javaFunctions(CassandraJavaUtil.javaFunctions(dfexplistoriginal.javaRDD()).repartitionByCassandraReplica("mdb","experiment",100,CassandraJavaUtil.someColumns("experimentid"),CassandraJavaUtil.mapToRow(ExperimentFormOnlyParKey.class)))
                                                                                        .joinWithCassandraTable("mdb","experiment", CassandraJavaUtil.someColumns("experimentid","description","intensity"), CassandraJavaUtil.someColumns("experimentid"), CassandraJavaUtil.mapRowTo(ExperimentFormExDesIn.class), CassandraJavaUtil.mapToRow(ExperimentFormOnlyParKey.class));
                    metlistinitial2 =sp.createDataset(predf.map(x->x._2).rdd(), ExpEncoderofthree).toDF( "description","experimentid","intensity").filter(col("description").notEqual("Unidentified"));
                }else if(ionmode.equals("positive")){
                    Encoder<ExperimentFormOnlyParKey> ExpEncoder = Encoders.bean(ExperimentFormOnlyParKey.class);
                    Encoder<ExperimentFormExDesInAd> ExpEncoderofthree = Encoders.bean(ExperimentFormExDesInAd.class);
                    Dataset<ExperimentFormOnlyParKey> dfexplistoriginal = sp.createDataset(experimentlist, Encoders.STRING()).toDF("experimentid").as(ExpEncoder);
                    CassandraJavaPairRDD<ExperimentFormOnlyParKey, ExperimentFormExDesInAd> predf = CassandraJavaUtil.javaFunctions(CassandraJavaUtil.javaFunctions(dfexplistoriginal.javaRDD()).repartitionByCassandraReplica("mdb","experiment",100,CassandraJavaUtil.someColumns("experimentid"),CassandraJavaUtil.mapToRow(ExperimentFormOnlyParKey.class)))
                                                                                        .joinWithCassandraTable("mdb","experiment", CassandraJavaUtil.someColumns("experimentid","description","adduct","intensity"), CassandraJavaUtil.someColumns("experimentid"), CassandraJavaUtil.mapRowTo(ExperimentFormExDesInAd.class), CassandraJavaUtil.mapToRow(ExperimentFormOnlyParKey.class));
                    Dataset<Row> metlistinitialbef =sp.createDataset(predf.map(x->x._2).rdd(), ExpEncoderofthree).toDF( "adduct","description","experimentid","intensity").filter(col("description").notEqual("Unidentified"));
                    Dataset<Row> metlistinitialbe= metlistinitialbef.withColumn("ioniz", functions.substring(col("adduct"), -1, 1)).drop("adduct").filter(col("ioniz").equalTo("+"));
                    metlistinitial2 = metlistinitialbe.drop(col("ioniz"));
                }else if(ionmode.equals("negative")){
                    Encoder<ExperimentFormOnlyParKey> ExpEncoder = Encoders.bean(ExperimentFormOnlyParKey.class);
                    Encoder<ExperimentFormExDesInAd> ExpEncoderofthree = Encoders.bean(ExperimentFormExDesInAd.class);
                    Dataset<ExperimentFormOnlyParKey> dfexplistoriginal = sp.createDataset(experimentlist, Encoders.STRING()).toDF("experimentid").as(ExpEncoder);
                    CassandraJavaPairRDD<ExperimentFormOnlyParKey, ExperimentFormExDesInAd> predf = CassandraJavaUtil.javaFunctions(CassandraJavaUtil.javaFunctions(dfexplistoriginal.javaRDD()).repartitionByCassandraReplica("mdb","experiment",100,CassandraJavaUtil.someColumns("experimentid"),CassandraJavaUtil.mapToRow(ExperimentFormOnlyParKey.class)))
                                                                                        .joinWithCassandraTable("mdb","experiment", CassandraJavaUtil.someColumns("experimentid","description","adduct","intensity"), CassandraJavaUtil.someColumns("experimentid"), CassandraJavaUtil.mapRowTo(ExperimentFormExDesInAd.class), CassandraJavaUtil.mapToRow(ExperimentFormOnlyParKey.class));
                    Dataset<Row> metlistinitialbef =sp.createDataset(predf.map(x->x._2).rdd(), ExpEncoderofthree).toDF( "adduct","description","experimentid","intensity").filter(col("description").notEqual("Unidentified"));
                    Dataset<Row> metlistinitialbe= metlistinitialbef.withColumn("ioniz", functions.substring(col("adduct"), -1, 1)).drop("adduct").filter(col("ioniz").equalTo("-"));
                    metlistinitial2 = metlistinitialbe.drop(col("ioniz"));
                }
                
                String[] metincols=metlist.parallelStream().toArray(String[]::new); 
                Dataset<Row> tempmissvalmtbs=sp.emptyDataFrame();
                Dataset<Row> expavgpeaks=sp.emptyDataFrame();
                Dataset<Row> metfinalpeaks =sp.emptyDataFrame();
                
                if(normval.equals("normvalmean")|| normval.equals("no")|| normval.equals("normvalmeanmtb")){
                    metfinalpeaks = metlistinitial2.groupBy(metlistinitial2.col("experimentid"),metlistinitial2.col("description")).avg("intensity").persist();
                    expavgpeaks = metfinalpeaks.groupBy(metfinalpeaks.col("description")).pivot("experimentid",new ArrayList<Object>(experimentlist)).mean("avg(intensity)");
                }else if(normval.equals("normvalorig")){//or       
                //this is z-score on description and experiment
                    Dataset<Row> tmp1 = metlistinitial2.groupBy(col("experimentid"),col("description")).agg(functions.mean("intensity"),functions.stddev("intensity")).withColumnRenamed("experimentid", "expid").withColumnRenamed("description", "descri");
                    Dataset<Row> tmpexpgroups2 = metlistinitial2.join(tmp1,metlistinitial2.col("experimentid").equalTo(tmp1.col("expid")).and(metlistinitial2.col("description").equalTo(tmp1.col("descri")))).withColumn("zscore",((col("intensity").$minus(col("avg(intensity)"))).$div(col("stddev_samp(intensity)")))).na().fill(0);
                    Dataset<Row>  finalexp = tmpexpgroups2.drop("intensity","avg(intensity)","stddev_samp(intensity)","expid","descri").withColumnRenamed("zscore", "intensity");
                    metfinalpeaks = finalexp.groupBy(finalexp.col("experimentid"),finalexp.col("description")).avg("intensity").persist();
                    expavgpeaks = metfinalpeaks.groupBy(metfinalpeaks.col("description")).pivot("experimentid",new ArrayList<Object>(experimentlist)).mean("avg(intensity)"); 
                }

                if(missval.equals("omit")){
                    //finding missing metabolites
                    StructType schema = new StructType(new StructField[] {
                                new StructField("metabs",
                                                DataTypes.StringType, false,
                                                Metadata.empty()) });

                    tempmissvalmtbs=expavgpeaks.map((MapFunction<Row, Row>) originalrow -> {
                        if(originalrow.anyNull()){
                            String rowValues = new String();
                            rowValues=originalrow.get(0).toString();
                            Row newrow = RowFactory.create(rowValues);
                            return newrow;
                        }else{
                            String rowValues = "notmissing";
                            Row newrow = RowFactory.create(rowValues);
                            return newrow;
                        }
                    }, RowEncoder.apply(schema)).persist();
                    Dataset<Row> missvalmetabs=tempmissvalmtbs.select("metabs").filter(col("metabs").notEqual("notmissing"));
                    missvalmtbs=missvalmetabs.as(Encoders.STRING()).collectAsList();
                    experimentslistclustering.setMissvalmtbs(missvalmtbs);
                    experimentslistclustering.setMtbslist(metlist);
                
                    Dataset<Row> dfmetabolitelist = sp.createDataset(metlist, Encoders.STRING()).toDF("description");
                    Dataset<Row> tempexpavgpeaks = metfinalpeaks.join(dfmetabolitelist,"description");
                    
                    Dataset<Row> scaledData = sp.emptyDataFrame();
                    if(normval.equals("normvalmean")){
                        Dataset<Row> tmp1 = tempexpavgpeaks.groupBy(col("experimentid")).agg(functions.mean("avg(intensity)"),functions.stddev("avg(intensity)")).withColumnRenamed("experimentid", "expid");
                        Dataset<Row> tmpexpgroups2 = tempexpavgpeaks.join(tmp1,tempexpavgpeaks.col("experimentid").equalTo(tmp1.col("expid"))).withColumn("zscore",((col("avg(intensity)").$minus(col("avg(avg(intensity))"))).$div(col("stddev_samp(avg(intensity))")))).na().fill(0);
                        scaledData = tmpexpgroups2.drop("avg(intensity)","avg(avg(intensity))","stddev_samp(avg(intensity))","expid").withColumnRenamed("zscore", "avg(intensity)");
                        
                    }else if(normval.equals("normvalmeanmtb")){
                        Dataset<Row> sthto = tempexpavgpeaks.groupBy(col("description")).agg(functions.mean("avg(intensity)"),functions.stddev("avg(intensity)")).withColumnRenamed("description", "descri");
                        Dataset<Row>sthbefore = tempexpavgpeaks.join(sthto,tempexpavgpeaks.col("description").equalTo(sthto.col("descri"))).withColumn("zscore",((col("avg(intensity)").$minus(col("avg(avg(intensity))"))).$div(col("stddev_samp(avg(intensity))")))).na().fill(0);
                        scaledData=sthbefore.drop("avg(intensity)","avg(avg(intensity))","stddev_samp(avg(intensity))","descri").withColumnRenamed("zscore", "intens").groupBy(col("experimentid"),col("description")).mean("intens").withColumnRenamed("avg(intens)", "avg(intensity)");
                    }else{
                        scaledData=tempexpavgpeaks;
                        
                    }
                    
                    Dataset<Row> dfwithnulls = scaledData.groupBy(scaledData.col("experimentid")).agg(functions.collect_list(functions.map(col("description"),col("avg(intensity)")))).toDF("experimentid","descinten");
                    StructType schema2 = new StructType(new StructField[] {
                                    new StructField("expids",DataTypes.StringType, false,Metadata.empty()),
                                    new StructField("features",new VectorUDT(),false,Metadata.empty())
                        });
                    Dataset<Row> expsandvectorsdf=dfwithnulls.map((MapFunction<Row, Row>) originalrow -> {
                            String firstpos = new String();
                            firstpos=originalrow.get(0).toString();
                            List<scala.collection.Map<String,Double>>mplist=originalrow.getList(1);
                            int s = mplist.size();
                            Map<String,Double>treemp=new TreeMap<>();
                            for(int k=0;k<s;k++){
                                Object[] desc = JavaConversions.mapAsJavaMap(mplist.get(k)).keySet().toArray();
                                Object[] kvlist= JavaConversions.mapAsJavaMap(mplist.get(k)).values().toArray();
                                treemp.put(desc[0].toString(),Double.parseDouble(kvlist[0].toString()));
                            }
                            Object[] yo1 = treemp.values().toArray();
                            double[] tmplist= new double[s];
                            for(int i=0;i<s;i++){
                                tmplist[i]=Double.parseDouble(yo1[i].toString());
                            }
                            Row newrow = RowFactory.create(firstpos,Vectors.dense(tmplist));
                            return newrow;

                    }, RowEncoder.apply(schema2)).persist();

                    
                    if(clusteringmethod.equals("kmeans")){

                        // Trains a k-means model.
                        long randomSeed = new Random().nextLong();
                        KMeans kmeans = new KMeans().setK(initialval).setInitMode("random").setSeed(randomSeed);

                        KMeansModel model = kmeans.fit(expsandvectorsdf);

                        // Make predictions
                        Dataset<Row> predictions = model.transform(expsandvectorsdf);
                        Dataset<Row> clustinfotemp = predictions.select(col("expids"),col("prediction")).groupBy(col("prediction")).count().orderBy(asc("prediction"));

                        experimentslistclustering.setClustinfo(clustinfotemp.select(col("count")).as(Encoders.LONG()).collectAsList());
                        experimentslistclustering.setExpslist(predictions.select(col("expids")).as(Encoders.STRING()).collectAsList());
                        experimentslistclustering.setClusts(predictions.select(col("prediction")).as(Encoders.INT()).collectAsList());
                        
                        if(clustinfotemp.count()>1){
                            // Evaluate clustering by computing Silhouette score
                            ClusteringEvaluator evaluator = new ClusteringEvaluator();
                            double silhouette = evaluator.evaluate(predictions);
                            experimentslistclustering.setSilhouette(silhouette);
                        }else{
                            rowflag = 3;
                        }
                        Vector[] centers = model.clusterCenters();
                        int numrows=centers.length;
                        int numcols=metincols.length;

                        for (int i=0;i<numrows;i++) {
                          double[] temparray1 = centers[i].toArray();
                          Double[] temparray2 = new Double[temparray1.length];
                          for(int j=0;j<numcols;j++){
                              temparray2[j]=temparray1[j];
                          }
                          clustcenters.add(temparray2);
                        }
                        experimentslistclustering.setExpsclustscenters(clustcenters);
                        expsandvectorsdf.unpersist(); 
                    }else if(clusteringmethod.equals("bikmeans")){
                        
                        BisectingKMeans bkm = new BisectingKMeans().setK(initialval);
                        BisectingKMeansModel model = bkm.fit(expsandvectorsdf);

                        // Make predictions
                        Dataset<Row> predictions = model.transform(expsandvectorsdf);
                        Dataset<Row> clustinfotemp = predictions.select(col("expids"),col("prediction")).groupBy(col("prediction")).count().orderBy(asc("prediction"));

                        experimentslistclustering.setClustinfo(clustinfotemp.select(col("count")).as(Encoders.LONG()).collectAsList());
                        experimentslistclustering.setExpslist(predictions.select(col("expids")).as(Encoders.STRING()).collectAsList());
                        experimentslistclustering.setClusts(predictions.select(col("prediction")).as(Encoders.INT()).collectAsList());
                        
                        if(clustinfotemp.count()>1){
                            // Evaluate clustering.
                            ClusteringEvaluator evaluator = new ClusteringEvaluator();
                            double silhouette = evaluator.evaluate(predictions);
                            experimentslistclustering.setSilhouette(silhouette);
                        }else{
                            rowflag = 3;
                        }
                        Vector[] centers = model.clusterCenters();
                        int numrows=centers.length;
                        int numcols=metincols.length;

                        for (int i=0;i<numrows;i++) {
                          double[] temparray1 = centers[i].toArray();
                          Double[] temparray2 = new Double[temparray1.length];
                          for(int j=0;j<numcols;j++){
                              temparray2[j]=temparray1[j];
                          }
                          clustcenters.add(temparray2);
                        }
                        experimentslistclustering.setExpsclustscenters(clustcenters);
                        expsandvectorsdf.unpersist();
                    }
                }else{
                   
                    //finding missing metabolites
                    StructType schema = new StructType(new StructField[] {
                                new StructField("description",DataTypes.StringType, false,Metadata.empty()),
                                new StructField("experimentid",DataTypes.createArrayType(DataTypes.StringType, true), false,Metadata.empty())});
                    String[] columnnames=expavgpeaks.columns();
                    int mtbswithallnullms=columnnames.length;

                    tempmissvalmtbs=expavgpeaks.map((MapFunction<Row, Row>) originalrow -> {
                        if(originalrow.anyNull()){
                            List<String>tmpexpidfilename=new ArrayList<>();
                            for(int i=1;i<mtbswithallnullms;i++){
                                if(originalrow.isNullAt(i)){
                                    tmpexpidfilename.add(columnnames[i]);
                                }
                            }
                            String mtbname = originalrow.get(0).toString();
                            Row newrow = RowFactory.create(mtbname,tmpexpidfilename.toArray(new String[0]));
                            return newrow;
                        }else{
                            String mtbname = "notmissing";
                            List<String>tmpexpidfilename=new ArrayList<>();
                            Row newrow = RowFactory.create(mtbname,tmpexpidfilename.toArray(new String[0]));
                            return newrow;
                        }
                    }, RowEncoder.apply(schema)).persist();
                    Dataset<Row> missvalmetabs=tempmissvalmtbs.select("description").filter(col("description").notEqual("notmissing"));
                    missvalmtbs=missvalmetabs.as(Encoders.STRING()).collectAsList();
                    experimentslistclustering.setMissvalmtbs(missvalmtbs);
                    experimentslistclustering.setMtbslist(metlist);
                    
                    Dataset<Row> nullmtbs = tempmissvalmtbs.filter(col("description").notEqual("notmissing")).withColumn("experimentid", functions.explode_outer(col("experimentid"))).withColumn("avg(intensity)", functions.lit(null).cast(DataTypes.DoubleType)).select(col("experimentid"),col("description"),col("avg(intensity)"));
                    Dataset<Row> tmpsupernewmetfinalpeaksafterrename=metfinalpeaks.union(nullmtbs);
                    if(missval.equals("replacewithzero")){
                        newfilenameavgpeaks=tmpsupernewmetfinalpeaksafterrename.na().fill(0);
                    }else if(missval.equals("replacewithmean")){
                        Dataset<Row>tmp1newfilenameavgpeaks = tmpsupernewmetfinalpeaksafterrename.groupBy(tmpsupernewmetfinalpeaksafterrename.col("description")).mean("avg(intensity)");
                        Dataset<Row>tmp2newfilenameavgpeaks = tmpsupernewmetfinalpeaksafterrename.join(tmp1newfilenameavgpeaks,"description");
                        newfilenameavgpeaks=tmp2newfilenameavgpeaks.map((MapFunction<Row, Row>) originalrow -> {
                            if(originalrow.anyNull()){
                                String mtbname = originalrow.get(0).toString();
                                String expid = originalrow.get(1).toString();
                                double inten = Double.parseDouble(originalrow.get(3).toString());
                                Row newrow = RowFactory.create(mtbname,expid,inten);
                                return newrow;
                            }else{
                                String mtbname = originalrow.get(0).toString();
                                String expid = originalrow.get(1).toString();
                                double inten = Double.parseDouble(originalrow.get(2).toString());
                                Row newrow = RowFactory.create(mtbname,expid,inten);
                                return newrow;
                            }
                        }, RowEncoder.apply(tmp2newfilenameavgpeaks.schema())).drop("avg(avg(intensity))");
                        
                    }else if(missval.equals("replacewithmedian")){
                        Dataset<Row>tmp1newfilenameavgpeaks = tmpsupernewmetfinalpeaksafterrename.groupBy(tmpsupernewmetfinalpeaksafterrename.col("description")).agg(callUDF("percentile_approx", col("avg(intensity)"), lit(0.5)).as("median"));
                        Dataset<Row>tmp2newfilenameavgpeaks = tmpsupernewmetfinalpeaksafterrename.join(tmp1newfilenameavgpeaks,"description");
                        newfilenameavgpeaks=tmp2newfilenameavgpeaks.map((MapFunction<Row, Row>) originalrow -> {
                            if(originalrow.anyNull()){
                                String mtbname = originalrow.get(0).toString();
                                String expid = originalrow.get(1).toString();
                                double inten = Double.parseDouble(originalrow.get(3).toString());
                                Row newrow = RowFactory.create(mtbname,expid,inten);
                                return newrow;
                            }else{
                                String mtbname = originalrow.get(0).toString();
                                String expid = originalrow.get(1).toString();
                                double inten = Double.parseDouble(originalrow.get(2).toString());
                                Row newrow = RowFactory.create(mtbname,expid,inten);
                                return newrow;
                            }
                        }, RowEncoder.apply(tmp2newfilenameavgpeaks.schema())).drop("median");
                        
                    }
                    
                    Dataset<Row> scaledData = sp.emptyDataFrame();
                    if(normval.equals("normvalmean")){
                        Dataset<Row> tmp1 = newfilenameavgpeaks.groupBy(col("experimentid")).agg(functions.mean("avg(intensity)"),functions.stddev("avg(intensity)")).withColumnRenamed("experimentid", "expid");
                        Dataset<Row> tmpexpgroups2 = newfilenameavgpeaks.join(tmp1,newfilenameavgpeaks.col("experimentid").equalTo(tmp1.col("expid"))).withColumn("zscore",((col("avg(intensity)").$minus(col("avg(avg(intensity))"))).$div(col("stddev_samp(avg(intensity))")))).na().fill(0);
                        scaledData = tmpexpgroups2.drop("avg(intensity)","avg(avg(intensity))","stddev_samp(avg(intensity))","expid").withColumnRenamed("zscore", "avg(intensity)");
                        
                    }else if(normval.equals("normvalmeanmtb")){
                        Dataset<Row> sthto = newfilenameavgpeaks.groupBy(col("description")).agg(functions.mean("avg(intensity)"),functions.stddev("avg(intensity)")).withColumnRenamed("description", "descri");
                        Dataset<Row>sthbefore = newfilenameavgpeaks.join(sthto,newfilenameavgpeaks.col("description").equalTo(sthto.col("descri"))).withColumn("zscore",((col("avg(intensity)").$minus(col("avg(avg(intensity))"))).$div(col("stddev_samp(avg(intensity))")))).na().fill(0);
                        scaledData=sthbefore.drop("avg(intensity)","avg(avg(intensity))","stddev_samp(avg(intensity))","descri").withColumnRenamed("zscore", "intens").groupBy(col("experimentid"),col("description")).mean("intens").withColumnRenamed("avg(intens)", "avg(intensity)");
                    }else{
                        scaledData=newfilenameavgpeaks;
                    }
                    
                    Dataset<Row> dfwithnulls = scaledData.groupBy(scaledData.col("experimentid")).agg(functions.collect_list(functions.map(col("description"),col("avg(intensity)")))).toDF("experimentid","descinten");
                    StructType schema2 = new StructType(new StructField[] {
                                    new StructField("expids",DataTypes.StringType, false,Metadata.empty()),
                                    new StructField("features",new VectorUDT(),false,Metadata.empty())
                        });
                    Dataset<Row> expsandvectorsdf=dfwithnulls.map((MapFunction<Row, Row>) originalrow -> {
                            String firstpos = new String();
                            firstpos=originalrow.get(0).toString();
                            List<scala.collection.Map<String,Double>>mplist=originalrow.getList(1);
                            int s = mplist.size();
                            Map<String,Double>treemp=new TreeMap<>();
                            for(int k=0;k<s;k++){
                                Object[] desc = JavaConversions.mapAsJavaMap(mplist.get(k)).keySet().toArray();
                                Object[] kvlist= JavaConversions.mapAsJavaMap(mplist.get(k)).values().toArray();
                                treemp.put(desc[0].toString(),Double.parseDouble(kvlist[0].toString()));
                            }
                            Object[] yo1 = treemp.values().toArray();
                            double[] tmplist= new double[s];
                            for(int i=0;i<s;i++){
                                tmplist[i]=Double.parseDouble(yo1[i].toString());
                            }
                            Row newrow = RowFactory.create(firstpos,Vectors.dense(tmplist));
                            return newrow;

                    }, RowEncoder.apply(schema2)).persist();
                
                    if(clusteringmethod.equals("kmeans")){

                         // Trains a k-means model.
                        long randomSeed = new Random().nextLong();
                        KMeans kmeans = new KMeans().setK(initialval).setInitMode("random").setSeed(randomSeed);
                        KMeansModel model = kmeans.fit(expsandvectorsdf);
                        // Make predictions
                        Dataset<Row> predictions = model.transform(expsandvectorsdf);
                        Dataset<Row> clustinfotemp = predictions.select(col("expids"),col("prediction")).groupBy(col("prediction")).count().orderBy(asc("prediction"));

                        experimentslistclustering.setClustinfo(clustinfotemp.select(col("count")).as(Encoders.LONG()).collectAsList());
                        experimentslistclustering.setExpslist(predictions.select(col("expids")).as(Encoders.STRING()).collectAsList());
                        experimentslistclustering.setClusts(predictions.select(col("prediction")).as(Encoders.INT()).collectAsList());
                        
                        if(clustinfotemp.count()>1){
                            // Evaluate clustering by computing Silhouette score
                            ClusteringEvaluator evaluator = new ClusteringEvaluator();
                            double silhouette = evaluator.evaluate(predictions);
                            experimentslistclustering.setSilhouette(silhouette);
                        }else{
                            rowflag = 3;
                        }  
                        Vector[] centers = model.clusterCenters();
                        int numrows=centers.length;
                        int numcols=metincols.length;

                        for (int i=0;i<numrows;i++) {
                          double[] temparray1 = centers[i].toArray();
                          Double[] temparray2 = new Double[temparray1.length];
                          for(int j=0;j<numcols;j++){
                              temparray2[j]=temparray1[j];
                          }
                          clustcenters.add(temparray2);
                        }
                        experimentslistclustering.setExpsclustscenters(clustcenters);
                        expsandvectorsdf.unpersist(); 
                    }else if(clusteringmethod.equals("bikmeans")){

                        BisectingKMeans bkm = new BisectingKMeans().setK(initialval);
                        BisectingKMeansModel model = bkm.fit(expsandvectorsdf);

                        // Make predictions
                        Dataset<Row> predictions = model.transform(expsandvectorsdf);
                        Dataset<Row> clustinfotemp = predictions.select(col("expids"),col("prediction")).groupBy(col("prediction")).count().orderBy(asc("prediction"));

                        experimentslistclustering.setClustinfo(clustinfotemp.select(col("count")).as(Encoders.LONG()).collectAsList());
                        experimentslistclustering.setExpslist(predictions.select(col("expids")).as(Encoders.STRING()).collectAsList());
                        experimentslistclustering.setClusts(predictions.select(col("prediction")).as(Encoders.INT()).collectAsList());
                        if(clustinfotemp.count()>1){
                            // Evaluate clustering.
                            ClusteringEvaluator evaluator = new ClusteringEvaluator();
                            double silhouette = evaluator.evaluate(predictions);
                            experimentslistclustering.setSilhouette(silhouette);
                        }else{
                            rowflag = 3;
                        }
                        Vector[] centers = model.clusterCenters();
                        int numrows=centers.length;
                        int numcols=metincols.length;

                        for (int i=0;i<numrows;i++) {
                          double[] temparray1 = centers[i].toArray();
                          Double[] temparray2 = new Double[temparray1.length];
                          for(int j=0;j<numcols;j++){
                              temparray2[j]=temparray1[j];
                          }
                          clustcenters.add(temparray2);
                        }
                        experimentslistclustering.setExpsclustscenters(clustcenters);
                        expsandvectorsdf.unpersist();
                    }
                }
                tempmissvalmtbs.unpersist();
                metfinalpeaks.unpersist();
            }else{
                rowflag=1;
            }
        }else if(allorsome.equals("onselected")){
            Collections.sort(metabolitelist);
            metlist=metabolitelist;
            Dataset<Row> newfilenameavgpeaks=sp.emptyDataFrame();
            Dataset<Row> dfmetabolitelist = sp.createDataset(metlist, Encoders.STRING()).toDF("description");
            
            
            Dataset<Row> metlistinitial =sp.emptyDataFrame();
            if(ionmode.equals("both")){
                Encoder<ExperimentFormOnlyParKey> ExpEncoder = Encoders.bean(ExperimentFormOnlyParKey.class);
                Encoder<ExperimentFormExDesIn> ExpEncoderofthree = Encoders.bean(ExperimentFormExDesIn.class);
                Dataset<ExperimentFormOnlyParKey> dfexplistoriginal = sp.createDataset(experimentlist, Encoders.STRING()).toDF("experimentid").as(ExpEncoder);
                CassandraJavaPairRDD<ExperimentFormOnlyParKey, ExperimentFormExDesIn> predf = CassandraJavaUtil.javaFunctions(CassandraJavaUtil.javaFunctions(dfexplistoriginal.javaRDD()).repartitionByCassandraReplica("mdb","experiment",100,CassandraJavaUtil.someColumns("experimentid"),CassandraJavaUtil.mapToRow(ExperimentFormOnlyParKey.class)))
                                                                                    .joinWithCassandraTable("mdb","experiment", CassandraJavaUtil.someColumns("experimentid","description","intensity"), CassandraJavaUtil.someColumns("experimentid"), CassandraJavaUtil.mapRowTo(ExperimentFormExDesIn.class), CassandraJavaUtil.mapToRow(ExperimentFormOnlyParKey.class));
                metlistinitial =sp.createDataset(predf.map(x->x._2).rdd(), ExpEncoderofthree).toDF( "description","experimentid","intensity");
            }else if(ionmode.equals("positive")){
                Encoder<ExperimentFormOnlyParKey> ExpEncoder = Encoders.bean(ExperimentFormOnlyParKey.class);
                Encoder<ExperimentFormExDesInAd> ExpEncoderofthree = Encoders.bean(ExperimentFormExDesInAd.class);
                Dataset<ExperimentFormOnlyParKey> dfexplistoriginal = sp.createDataset(experimentlist, Encoders.STRING()).toDF("experimentid").as(ExpEncoder); 
                CassandraJavaPairRDD<ExperimentFormOnlyParKey, ExperimentFormExDesInAd> predf = CassandraJavaUtil.javaFunctions(CassandraJavaUtil.javaFunctions(dfexplistoriginal.javaRDD()).repartitionByCassandraReplica("mdb","experiment",100,CassandraJavaUtil.someColumns("experimentid"),CassandraJavaUtil.mapToRow(ExperimentFormOnlyParKey.class)))
                                                                                    .joinWithCassandraTable("mdb","experiment", CassandraJavaUtil.someColumns("experimentid","description","adduct","intensity"), CassandraJavaUtil.someColumns("experimentid"), CassandraJavaUtil.mapRowTo(ExperimentFormExDesInAd.class), CassandraJavaUtil.mapToRow(ExperimentFormOnlyParKey.class));
                Dataset<Row> metlistinitialbef =sp.createDataset(predf.map(x->x._2).rdd(), ExpEncoderofthree).toDF( "adduct","description","experimentid","intensity");
                Dataset<Row> metlistinitialbe= metlistinitialbef.withColumn("ioniz", functions.substring(col("adduct"), -1, 1)).drop("adduct").filter(col("ioniz").equalTo("+"));
                metlistinitial = metlistinitialbe.drop(col("ioniz"));
            }else if(ionmode.equals("negative")){
                Encoder<ExperimentFormOnlyParKey> ExpEncoder = Encoders.bean(ExperimentFormOnlyParKey.class);
                Encoder<ExperimentFormExDesInAd> ExpEncoderofthree = Encoders.bean(ExperimentFormExDesInAd.class);
                Dataset<ExperimentFormOnlyParKey> dfexplistoriginal = sp.createDataset(experimentlist, Encoders.STRING()).toDF("experimentid").as(ExpEncoder);
                CassandraJavaPairRDD<ExperimentFormOnlyParKey, ExperimentFormExDesInAd> predf = CassandraJavaUtil.javaFunctions(CassandraJavaUtil.javaFunctions(dfexplistoriginal.javaRDD()).repartitionByCassandraReplica("mdb","experiment",100,CassandraJavaUtil.someColumns("experimentid"),CassandraJavaUtil.mapToRow(ExperimentFormOnlyParKey.class)))
                                                                                    .joinWithCassandraTable("mdb","experiment", CassandraJavaUtil.someColumns("experimentid","description","adduct","intensity"), CassandraJavaUtil.someColumns("experimentid"), CassandraJavaUtil.mapRowTo(ExperimentFormExDesInAd.class), CassandraJavaUtil.mapToRow(ExperimentFormOnlyParKey.class));
                Dataset<Row> metlistinitialbef =sp.createDataset(predf.map(x->x._2).rdd(), ExpEncoderofthree).toDF( "adduct","description","experimentid","intensity");
                Dataset<Row> metlistinitialbe= metlistinitialbef.withColumn("ioniz", functions.substring(col("adduct"), -1, 1)).drop("adduct").filter(col("ioniz").equalTo("-"));
                metlistinitial = metlistinitialbe.drop(col("ioniz"));
            }
            
            
            String[] metincols = metlist.parallelStream().toArray(String[]::new);
            experimentslistclustering.setMtbslist(metabolitelist);
            if(missval.equals("omit")){
                Dataset<Row> metlistinitial1 = metlistinitial.join(dfmetabolitelist,"description");
                
                Dataset<Row> metfinalpeaks =sp.emptyDataFrame();
                if(normval.equals("normvalmean")|| normval.equals("no")|| normval.equals("normvalmeanmtb")){
                    metfinalpeaks = metlistinitial1.groupBy(metlistinitial1.col("experimentid"),metlistinitial1.col("description")).avg("intensity");
                }else if(normval.equals("normvalorig")){//or       
                //this is z-score on description and experiment
                    Dataset<Row> tmp1 = metlistinitial1.groupBy(col("experimentid"),col("description")).agg(functions.mean("intensity"),functions.stddev("intensity")).withColumnRenamed("experimentid", "expid").withColumnRenamed("description", "descri");
                    Dataset<Row> tmpexpgroups2 = metlistinitial1.join(tmp1,metlistinitial1.col("experimentid").equalTo(tmp1.col("expid")).and(metlistinitial1.col("description").equalTo(tmp1.col("descri")))).withColumn("zscore",((col("intensity").$minus(col("avg(intensity)"))).$div(col("stddev_samp(intensity)")))).na().fill(0);
                    Dataset<Row>  finalexp = tmpexpgroups2.drop("intensity","avg(intensity)","stddev_samp(intensity)","expid","descri").withColumnRenamed("zscore", "intensity");
                    metfinalpeaks = finalexp.groupBy(finalexp.col("experimentid"),finalexp.col("description")).avg("intensity");
                }
                
                Dataset<Row> scaledData = sp.emptyDataFrame();
                if(normval.equals("normvalmean")){
                    Dataset<Row> tmp1 = metfinalpeaks.groupBy(col("experimentid")).agg(functions.mean("avg(intensity)"),functions.stddev("avg(intensity)")).withColumnRenamed("experimentid", "expid");
                    Dataset<Row> tmpexpgroups2 = metfinalpeaks.join(tmp1,metfinalpeaks.col("experimentid").equalTo(tmp1.col("expid"))).withColumn("zscore",((col("avg(intensity)").$minus(col("avg(avg(intensity))"))).$div(col("stddev_samp(avg(intensity))")))).na().fill(0);
                    scaledData = tmpexpgroups2.drop("avg(intensity)","avg(avg(intensity))","stddev_samp(avg(intensity))","expid").withColumnRenamed("zscore", "avg(intensity)");
                   
                }else if(normval.equals("normvalmeanmtb")){
                        Dataset<Row> sthto = metfinalpeaks.groupBy(col("description")).agg(functions.mean("avg(intensity)"),functions.stddev("avg(intensity)")).withColumnRenamed("description", "descri");
                        Dataset<Row>sthbefore = metfinalpeaks.join(sthto,metfinalpeaks.col("description").equalTo(sthto.col("descri"))).withColumn("zscore",((col("avg(intensity)").$minus(col("avg(avg(intensity))"))).$div(col("stddev_samp(avg(intensity))")))).na().fill(0);
                        scaledData=sthbefore.drop("avg(intensity)","avg(avg(intensity))","stddev_samp(avg(intensity))","descri").withColumnRenamed("zscore", "intens").groupBy(col("experimentid"),col("description")).mean("intens").withColumnRenamed("avg(intens)", "avg(intensity)");
                }else{
                    scaledData=metfinalpeaks;
                }
                
                
                Dataset<Row> dfwithnulls = scaledData.groupBy(scaledData.col("experimentid")).agg(functions.collect_list(functions.map(col("description"),col("avg(intensity)")))).toDF("experimentid","descinten");
                StructType schema = new StructType(new StructField[] {
                                    new StructField("expids",DataTypes.StringType, false,Metadata.empty()),
                                    new StructField("features",new VectorUDT(),false,Metadata.empty())
                        });
                Dataset<Row> expsandvectorsdf=dfwithnulls.map((MapFunction<Row, Row>) originalrow -> {
                        String firstpos = new String();
                        firstpos=originalrow.get(0).toString();
                        List<scala.collection.Map<String,Double>>mplist=originalrow.getList(1);
                        int s = mplist.size();
                        Map<String,Double>treemp=new TreeMap<>();
                        for(int k=0;k<s;k++){
                            Object[] desc = JavaConversions.mapAsJavaMap(mplist.get(k)).keySet().toArray();
                            Object[] kvlist= JavaConversions.mapAsJavaMap(mplist.get(k)).values().toArray();
                            treemp.put(desc[0].toString(),Double.parseDouble(kvlist[0].toString()));
                        }
                        Object[] yo1 = treemp.values().toArray();
                        double[] tmplist= new double[s];
                        for(int i=0;i<s;i++){
                            tmplist[i]=Double.parseDouble(yo1[i].toString());
                        }
                        Row newrow = RowFactory.create(firstpos,Vectors.dense(tmplist));
                        return newrow;

                }, RowEncoder.apply(schema)).persist();
                
                
                if(clusteringmethod.equals("kmeans")){
                    long randomSeed = new Random().nextLong();
                    KMeans kmeans = new KMeans().setK(initialval).setInitMode("random").setSeed(randomSeed);

                    KMeansModel model = kmeans.fit(expsandvectorsdf);

                    // Make predictions
                    Dataset<Row> predictions = model.transform(expsandvectorsdf);
                    Dataset<Row> clustinfotemp = predictions.select(col("expids"),col("prediction")).groupBy(col("prediction")).count().orderBy(asc("prediction"));

                    experimentslistclustering.setClustinfo(clustinfotemp.select(col("count")).as(Encoders.LONG()).collectAsList());
                    experimentslistclustering.setExpslist(predictions.select(col("expids")).as(Encoders.STRING()).collectAsList());
                    experimentslistclustering.setClusts(predictions.select(col("prediction")).as(Encoders.INT()).collectAsList());
                    experimentslistclustering.setMtbslist(metabolitelist);
                    
                    if(clustinfotemp.count()>1){
                        // Evaluate clustering by computing Silhouette score
                        ClusteringEvaluator evaluator = new ClusteringEvaluator();
                        double silhouette = evaluator.evaluate(predictions);
                        experimentslistclustering.setSilhouette(silhouette);
                    }else{
                        rowflag=3;
                    }
                    Vector[] centers = model.clusterCenters();
                    int numrows=centers.length;
                    int numcols=metincols.length;

                    for (int i=0;i<numrows;i++) {
                      double[] temparray1 = centers[i].toArray();
                      Double[] temparray2 = new Double[temparray1.length];
                      for(int j=0;j<numcols;j++){
                          temparray2[j]=temparray1[j];
                      }
                      clustcenters.add(temparray2);
                    }
                    experimentslistclustering.setExpsclustscenters(clustcenters);
                }else if(clusteringmethod.equals("bikmeans")){

                    BisectingKMeans bkm = new BisectingKMeans().setK(initialval);
                    BisectingKMeansModel model = bkm.fit(expsandvectorsdf);

                    // Make predictions
                    Dataset<Row> predictions = model.transform(expsandvectorsdf);
                    Dataset<Row> clustinfotemp = predictions.select(col("expids"),col("prediction")).groupBy(col("prediction")).count().orderBy(asc("prediction"));

                    experimentslistclustering.setClustinfo(clustinfotemp.select(col("count")).as(Encoders.LONG()).collectAsList());
                    experimentslistclustering.setExpslist(predictions.select(col("expids")).as(Encoders.STRING()).collectAsList());
                    experimentslistclustering.setClusts(predictions.select(col("prediction")).as(Encoders.INT()).collectAsList());
                    if(clustinfotemp.count()>1){
                        // Evaluate clustering.
                        ClusteringEvaluator evaluator = new ClusteringEvaluator();
                        double silhouette = evaluator.evaluate(predictions);
                        experimentslistclustering.setSilhouette(silhouette);
                    }else{
                        rowflag=3;
                    }
                    Vector[] centers = model.clusterCenters();
                    int numrows=centers.length;
                    int numcols=metincols.length;

                    for (int i=0;i<numrows;i++) {
                      double[] temparray1 = centers[i].toArray();
                      Double[] temparray2 = new Double[temparray1.length];
                      for(int j=0;j<numcols;j++){
                          temparray2[j]=temparray1[j];
                      }
                      clustcenters.add(temparray2);
                    }
                    experimentslistclustering.setExpsclustscenters(clustcenters);
                }
                expsandvectorsdf.unpersist();
            }else{
                Dataset<Row> metfinalpeaks =sp.emptyDataFrame();
                if(normval.equals("normvalmean")|| normval.equals("no")|| normval.equals("normvalmeanmtb")){
                    metfinalpeaks = metlistinitial.groupBy(metlistinitial.col("experimentid"),metlistinitial.col("description")).avg("intensity").persist();
                }else if(normval.equals("normvalorig")){//or       
                //this is z-score on description and experiment
                    Dataset<Row> tmp1 = metlistinitial.groupBy(col("experimentid"),col("description")).agg(functions.mean("intensity"),functions.stddev("intensity")).withColumnRenamed("experimentid", "expid").withColumnRenamed("description", "descri");
                    Dataset<Row> tmpexpgroups2 = metlistinitial.join(tmp1,metlistinitial.col("experimentid").equalTo(tmp1.col("expid")).and(metlistinitial.col("description").equalTo(tmp1.col("descri")))).withColumn("zscore",((col("intensity").$minus(col("avg(intensity)"))).$div(col("stddev_samp(intensity)")))).na().fill(0);
                    Dataset<Row>  finalexp = tmpexpgroups2.drop("intensity","avg(intensity)","stddev_samp(intensity)","expid","descri").withColumnRenamed("zscore", "intensity");
                    metfinalpeaks = finalexp.groupBy(finalexp.col("experimentid"),finalexp.col("description")).avg("intensity").persist();
                }
                
                
                Dataset<Row> expavgpeaks = metfinalpeaks.groupBy(col("description")).pivot("experimentid",new ArrayList<Object>(experimentlist)).mean("avg(intensity)");
                //finding missing metabolites
                StructType schema = new StructType(new StructField[] {
                            new StructField("description",DataTypes.StringType, false,Metadata.empty()),
                            new StructField("experimentid",DataTypes.createArrayType(DataTypes.StringType, true), false,Metadata.empty())});
                String[] columnnames=expavgpeaks.columns();
                int mtbswithallnullms=columnnames.length;
                
                Dataset<Row> tempmissvalmtbs=expavgpeaks.map((MapFunction<Row, Row>) originalrow -> {
                    if(originalrow.anyNull() && metabolitelist.contains(originalrow.get(0).toString())){
                        List<String>tmpexpidfilename=new ArrayList<>();
                        for(int i=1;i<mtbswithallnullms;i++){
                            if(originalrow.isNullAt(i)){
                                tmpexpidfilename.add(columnnames[i]);
                            }
                        }
                        String mtbname = originalrow.get(0).toString();
                        Row newrow = RowFactory.create(mtbname,tmpexpidfilename.toArray(new String[0]));
                        return newrow;
                    }else{
                        String mtbname = "notmissing";
                        List<String>tmpexpidfilename=new ArrayList<>();
                        Row newrow = RowFactory.create(mtbname,tmpexpidfilename.toArray(new String[0]));
                        return newrow;
                    }
                }, RowEncoder.apply(schema)).persist(); 
                Dataset<Row> missvalmetabs=tempmissvalmtbs.select("description").filter(col("description").notEqual("notmissing"));
                missvalmtbs=missvalmetabs.as(Encoders.STRING()).collectAsList();
                experimentslistclustering.setMissvalmtbs(missvalmtbs);
                experimentslistclustering.setMtbslist(metabolitelist);
                
                Dataset<Row> nullmtbs = tempmissvalmtbs.filter(col("description").notEqual("notmissing")).withColumn("experimentid", functions.explode_outer(col("experimentid"))).withColumn("avg(intensity)", functions.lit(null).cast(DataTypes.DoubleType)).select(col("experimentid"),col("description"),col("avg(intensity)"));
                Dataset<Row> tmpsupernewmetfinalpeaksafterrename=metfinalpeaks.union(nullmtbs);
                Dataset<Row> supernewmetfinalpeaksafterrename=tmpsupernewmetfinalpeaksafterrename.join(dfmetabolitelist,"description");
                
                if(missval.equals("replacewithzero")){
                    newfilenameavgpeaks=supernewmetfinalpeaksafterrename.na().fill(0);
                }else if(missval.equals("replacewithmean")){
                    
                    Dataset<Row>tmp1newfilenameavgpeaks = supernewmetfinalpeaksafterrename.groupBy(supernewmetfinalpeaksafterrename.col("description")).mean("avg(intensity)");
                    Dataset<Row>tmp2newfilenameavgpeaks = supernewmetfinalpeaksafterrename.join(tmp1newfilenameavgpeaks,"description");
                    newfilenameavgpeaks=tmp2newfilenameavgpeaks.map((MapFunction<Row, Row>) originalrow -> {
                        if(originalrow.anyNull()){
                            String mtbname = originalrow.get(0).toString();
                            String expid = originalrow.get(1).toString();
                            double inten = Double.parseDouble(originalrow.get(3).toString());
                            Row newrow = RowFactory.create(mtbname,expid,inten);
                            return newrow;
                        }else{
                            String mtbname = originalrow.get(0).toString();
                            String expid = originalrow.get(1).toString();
                            double inten = Double.parseDouble(originalrow.get(2).toString());
                            Row newrow = RowFactory.create(mtbname,expid,inten);
                            return newrow;
                        }
                    }, RowEncoder.apply(tmp2newfilenameavgpeaks.schema())).drop("avg(avg(intensity))");
                }else if(missval.equals("replacewithmedian")){
                    
                    Dataset<Row>tmp1newfilenameavgpeaks = supernewmetfinalpeaksafterrename.groupBy(supernewmetfinalpeaksafterrename.col("description")).agg(callUDF("percentile_approx", col("avg(intensity)"), lit(0.5)).as("median"));
                    Dataset<Row>tmp2newfilenameavgpeaks = supernewmetfinalpeaksafterrename.join(tmp1newfilenameavgpeaks,"description");
                    newfilenameavgpeaks=tmp2newfilenameavgpeaks.map((MapFunction<Row, Row>) originalrow -> {
                        if(originalrow.anyNull()){
                            String mtbname = originalrow.get(0).toString();
                            String expid = originalrow.get(1).toString();
                            double inten = Double.parseDouble(originalrow.get(3).toString());
                            Row newrow = RowFactory.create(mtbname,expid,inten);
                            return newrow;
                        }else{
                            String mtbname = originalrow.get(0).toString();
                            String expid = originalrow.get(1).toString();
                            double inten = Double.parseDouble(originalrow.get(2).toString());
                            Row newrow = RowFactory.create(mtbname,expid,inten);
                            return newrow;
                        }
                    }, RowEncoder.apply(tmp2newfilenameavgpeaks.schema())).drop("median");
                }
                
                Dataset<Row> scaledData = sp.emptyDataFrame();
                if(normval.equals("normvalmean")){
                    Dataset<Row> tmp1 = newfilenameavgpeaks.groupBy(col("experimentid")).agg(functions.mean("avg(intensity)"),functions.stddev("avg(intensity)")).withColumnRenamed("experimentid", "expid");
                    Dataset<Row> tmpexpgroups2 = newfilenameavgpeaks.join(tmp1,newfilenameavgpeaks.col("experimentid").equalTo(tmp1.col("expid"))).withColumn("zscore",((col("avg(intensity)").$minus(col("avg(avg(intensity))"))).$div(col("stddev_samp(avg(intensity))")))).na().fill(0);
                    scaledData = tmpexpgroups2.drop("avg(intensity)","avg(avg(intensity))","stddev_samp(avg(intensity))","expid").withColumnRenamed("zscore", "avg(intensity)");
                    
                }else if(normval.equals("normvalmeanmtb")){
                        Dataset<Row> sthto = newfilenameavgpeaks.groupBy(col("description")).agg(functions.mean("avg(intensity)"),functions.stddev("avg(intensity)")).withColumnRenamed("description", "descri");
                        Dataset<Row>sthbefore = newfilenameavgpeaks.join(sthto,newfilenameavgpeaks.col("description").equalTo(sthto.col("descri"))).withColumn("zscore",((col("avg(intensity)").$minus(col("avg(avg(intensity))"))).$div(col("stddev_samp(avg(intensity))")))).na().fill(0);
                        scaledData=sthbefore.drop("avg(intensity)","avg(avg(intensity))","stddev_samp(avg(intensity))","descri").withColumnRenamed("zscore", "intens").groupBy(col("experimentid"),col("description")).mean("intens").withColumnRenamed("avg(intens)", "avg(intensity)");
                }else{
                    scaledData=newfilenameavgpeaks;
                }
                
                Dataset<Row> dfwithnulls = scaledData.groupBy(scaledData.col("experimentid")).agg(functions.collect_list(functions.map(col("description"),col("avg(intensity)")))).toDF("experimentid","descinten");
                StructType schema2 = new StructType(new StructField[] {
                                new StructField("expids",DataTypes.StringType, false,Metadata.empty()),
                                new StructField("features",new VectorUDT(),false,Metadata.empty())
                    });
                Dataset<Row> expsandvectorsdf=dfwithnulls.map((MapFunction<Row, Row>) originalrow -> {
                        String firstpos = new String();
                        firstpos=originalrow.get(0).toString();
                        List<scala.collection.Map<String,Double>>mplist=originalrow.getList(1);
                        int s = mplist.size();
                        Map<String,Double>treemp=new TreeMap<>();
                        for(int k=0;k<s;k++){
                            Object[] desc = JavaConversions.mapAsJavaMap(mplist.get(k)).keySet().toArray();
                            Object[] kvlist= JavaConversions.mapAsJavaMap(mplist.get(k)).values().toArray();
                            treemp.put(desc[0].toString(),Double.parseDouble(kvlist[0].toString()));
                        }
                        Object[] yo1 = treemp.values().toArray();
                        double[] tmplist= new double[s];
                        for(int i=0;i<s;i++){
                            tmplist[i]=Double.parseDouble(yo1[i].toString());
                        }
                        Row newrow = RowFactory.create(firstpos,Vectors.dense(tmplist));
                        return newrow;

                }, RowEncoder.apply(schema2)).persist();
                
                
                if(clusteringmethod.equals("kmeans")){

                    long randomSeed = new Random().nextLong();
                    KMeans kmeans = new KMeans().setK(initialval).setInitMode("random").setSeed(randomSeed);

                    KMeansModel model = kmeans.fit(expsandvectorsdf);

                    // Make predictions
                    Dataset<Row> predictions = model.transform(expsandvectorsdf);
                    Dataset<Row> clustinfotemp = predictions.select(col("expids"),col("prediction")).groupBy(col("prediction")).count().orderBy(asc("prediction"));

                    experimentslistclustering.setClustinfo(clustinfotemp.select(col("count")).as(Encoders.LONG()).collectAsList());
                    experimentslistclustering.setExpslist(predictions.select(col("expids")).as(Encoders.STRING()).collectAsList());
                    experimentslistclustering.setClusts(predictions.select(col("prediction")).as(Encoders.INT()).collectAsList());
                    experimentslistclustering.setMtbslist(metabolitelist);
                    
                    if(clustinfotemp.count()>1){
                        // Evaluate clustering by computing Silhouette score
                        ClusteringEvaluator evaluator = new ClusteringEvaluator();
                        double silhouette = evaluator.evaluate(predictions);
                        experimentslistclustering.setSilhouette(silhouette);
                    }else{
                        rowflag=3;
                    }
                    Vector[] centers = model.clusterCenters();
                    int numrows=centers.length;
                    int numcols=metincols.length;

                    for (int i=0;i<numrows;i++) {
                      double[] temparray1 = centers[i].toArray();
                      Double[] temparray2 = new Double[temparray1.length];
                      for(int j=0;j<numcols;j++){
                          temparray2[j]=temparray1[j];
                      }
                      clustcenters.add(temparray2);
                    }
                    experimentslistclustering.setExpsclustscenters(clustcenters);
                    expsandvectorsdf.unpersist();
                }else if(clusteringmethod.equals("bikmeans")){
                    
                    BisectingKMeans bkm = new BisectingKMeans().setK(initialval);
                    BisectingKMeansModel model = bkm.fit(expsandvectorsdf);

                    // Make predictions
                    Dataset<Row> predictions = model.transform(expsandvectorsdf);
                    Dataset<Row> clustinfotemp = predictions.select(col("expids"),col("prediction")).groupBy(col("prediction")).count().orderBy(asc("prediction"));

                    experimentslistclustering.setClustinfo(clustinfotemp.select(col("count")).as(Encoders.LONG()).collectAsList());
                    experimentslistclustering.setExpslist(predictions.select(col("expids")).as(Encoders.STRING()).collectAsList());
                    experimentslistclustering.setClusts(predictions.select(col("prediction")).as(Encoders.INT()).collectAsList());
                    
                    if(clustinfotemp.count()>1){
                        // Evaluate clustering.
                        ClusteringEvaluator evaluator = new ClusteringEvaluator();
                        double silhouette = evaluator.evaluate(predictions);
                        experimentslistclustering.setSilhouette(silhouette);
                    }else{
                        rowflag=3;
                    }
                    Vector[] centers = model.clusterCenters();
                    int numrows=centers.length;
                    int numcols=metincols.length;

                    for (int i=0;i<numrows;i++) {
                      double[] temparray1 = centers[i].toArray();
                      Double[] temparray2 = new Double[temparray1.length];
                      for(int j=0;j<numcols;j++){
                          temparray2[j]=temparray1[j];
                      }
                      clustcenters.add(temparray2);
                    }
                    experimentslistclustering.setExpsclustscenters(clustcenters);
                    expsandvectorsdf.unpersist();
                }
                metfinalpeaks.unpersist();
                tempmissvalmtbs.unpersist();
            }
        }
        if(rowflag==1){
            experimentslistclustering.setRowflag(1);
        }else if(rowflag==3){
            experimentslistclustering.setRowflag(3);
        }
        return experimentslistclustering;
    }
    
    
    
    public Replicateslistclustering repliclustering(List<String> experimentlist,List<String> metabolitelist,String allorsome,String missval, String clusteringmethod,Integer initialval,String ionmode,String normval) {
        Replicateslistclustering replicateslistclustering = new Replicateslistclustering();
        List<String> metlist = new ArrayList<>();
        List<String> missvalmtbs=new ArrayList<>();
        List<Double[]> clustcenters = new ArrayList<>();
        Integer rowflag = 0;
         
        if(allorsome.equals("onall")){
            
            if(missval.equals("omit")){
                metlist=omitInReplicates(experimentlist,ionmode);
            }else{
                metlist=replaceWithValueInReplicates(experimentlist,ionmode);
            }
            
            if (metlist != null && !metlist.isEmpty()){
                Dataset<Row> newfilenameavgpeaks=sp.emptyDataFrame();
               
                Dataset<Row> metlistinitial =sp.emptyDataFrame();
                if(ionmode.equals("both")){
                    Encoder<ExperimentFormOnlyParKey> ExpEncoder = Encoders.bean(ExperimentFormOnlyParKey.class);
                    Encoder<ExperimentFormExDesFlIn> ExpEncoderoffour = Encoders.bean(ExperimentFormExDesFlIn.class);
                    Dataset<ExperimentFormOnlyParKey> dfexplistoriginal = sp.createDataset(experimentlist, Encoders.STRING()).toDF("experimentid").as(ExpEncoder);
                    CassandraJavaPairRDD<ExperimentFormOnlyParKey, ExperimentFormExDesFlIn> predf = CassandraJavaUtil.javaFunctions(CassandraJavaUtil.javaFunctions(dfexplistoriginal.javaRDD()).repartitionByCassandraReplica("mdb","experiment",100,CassandraJavaUtil.someColumns("experimentid"),CassandraJavaUtil.mapToRow(ExperimentFormOnlyParKey.class)))
                                                                                        .joinWithCassandraTable("mdb","experiment", CassandraJavaUtil.someColumns("experimentid","filename","description","intensity"), CassandraJavaUtil.someColumns("experimentid"), CassandraJavaUtil.mapRowTo(ExperimentFormExDesFlIn.class), CassandraJavaUtil.mapToRow(ExperimentFormOnlyParKey.class));
                    metlistinitial =sp.createDataset(predf.map(x->x._2).rdd(), ExpEncoderoffour).toDF( "description","experimentid","filename","intensity").filter(col("description").notEqual("Unidentified"));
                }else if(ionmode.equals("positive")){
                    Encoder<ExperimentFormOnlyParKey> ExpEncoder = Encoders.bean(ExperimentFormOnlyParKey.class);
                    Encoder<ExperimentFormExDesFlInAd> ExpEncoderofthree = Encoders.bean(ExperimentFormExDesFlInAd.class);
                    Dataset<ExperimentFormOnlyParKey> dfexplistoriginal = sp.createDataset(experimentlist, Encoders.STRING()).toDF("experimentid").as(ExpEncoder); //description, adduct, experimentid
                    CassandraJavaPairRDD<ExperimentFormOnlyParKey, ExperimentFormExDesFlInAd> predf = CassandraJavaUtil.javaFunctions(CassandraJavaUtil.javaFunctions(dfexplistoriginal.javaRDD()).repartitionByCassandraReplica("mdb","experiment",100,CassandraJavaUtil.someColumns("experimentid"),CassandraJavaUtil.mapToRow(ExperimentFormOnlyParKey.class)))
                                                                                        .joinWithCassandraTable("mdb","experiment", CassandraJavaUtil.someColumns("experimentid","filename","description","adduct","intensity"), CassandraJavaUtil.someColumns("experimentid"), CassandraJavaUtil.mapRowTo(ExperimentFormExDesFlInAd.class), CassandraJavaUtil.mapToRow(ExperimentFormOnlyParKey.class));
                    Dataset<Row> metlistinitialbef =sp.createDataset(predf.map(x->x._2).rdd(), ExpEncoderofthree).toDF( "adduct","description","experimentid","filename","intensity").filter(col("description").notEqual("Unidentified"));
                    Dataset<Row> metlistinitialbe= metlistinitialbef.withColumn("ioniz", functions.substring(col("adduct"), -1, 1)).drop("adduct").filter(col("ioniz").equalTo("+"));
                    metlistinitial = metlistinitialbe.drop(col("ioniz"));
                }else if(ionmode.equals("negative")){
                    Encoder<ExperimentFormOnlyParKey> ExpEncoder = Encoders.bean(ExperimentFormOnlyParKey.class);
                    Encoder<ExperimentFormExDesFlInAd> ExpEncoderofthree = Encoders.bean(ExperimentFormExDesFlInAd.class);
                    Dataset<ExperimentFormOnlyParKey> dfexplistoriginal = sp.createDataset(experimentlist, Encoders.STRING()).toDF("experimentid").as(ExpEncoder); //description, adduct, experimentid
                    CassandraJavaPairRDD<ExperimentFormOnlyParKey, ExperimentFormExDesFlInAd> predf = CassandraJavaUtil.javaFunctions(CassandraJavaUtil.javaFunctions(dfexplistoriginal.javaRDD()).repartitionByCassandraReplica("mdb","experiment",100,CassandraJavaUtil.someColumns("experimentid"),CassandraJavaUtil.mapToRow(ExperimentFormOnlyParKey.class)))
                                                                                        .joinWithCassandraTable("mdb","experiment", CassandraJavaUtil.someColumns("experimentid","filename","description","adduct","intensity"), CassandraJavaUtil.someColumns("experimentid"), CassandraJavaUtil.mapRowTo(ExperimentFormExDesFlInAd.class), CassandraJavaUtil.mapToRow(ExperimentFormOnlyParKey.class));
                    Dataset<Row> metlistinitialbef =sp.createDataset(predf.map(x->x._2).rdd(), ExpEncoderofthree).toDF( "adduct","description","experimentid","filename","intensity").filter(col("description").notEqual("Unidentified"));
                    Dataset<Row> metlistinitialbe= metlistinitialbef.withColumn("ioniz", functions.substring(col("adduct"), -1, 1)).drop("adduct").filter(col("ioniz").equalTo("-"));
                    metlistinitial = metlistinitialbe.drop(col("ioniz"));
                }
                
                Dataset<Row> newmetfinalpeaks = sp.emptyDataFrame();
                if(normval.equals("normvalmean")|| normval.equals("no")|| normval.equals("normvalmeanmtb")){
                    Dataset<Row> metfinalpeaks = metlistinitial.groupBy(metlistinitial.col("experimentid"),metlistinitial.col("description"),metlistinitial.col("filename")).avg("intensity").orderBy(asc("experimentid"), asc("description"));
                    newmetfinalpeaks = metfinalpeaks.withColumn("expidfilename", functions.concat(col("experimentid"), lit("__"), col("filename"))).drop("experimentid","filename").select(col("description"),col("avg(intensity)"),col("expidfilename"));
                }else if(normval.equals("normvalorig")){//or       
                //this is z-score on description and experiment
                    Dataset<Row> tmp1 = metlistinitial.groupBy(col("experimentid"),col("description"),col("filename")).agg(functions.mean("intensity"),functions.stddev("intensity")).withColumnRenamed("experimentid", "expid").withColumnRenamed("filename", "flname").withColumnRenamed("description", "descri");
                    Dataset<Row> tmpexpgroups2 = metlistinitial.join(tmp1,metlistinitial.col("experimentid").equalTo(tmp1.col("expid")).and(metlistinitial.col("filename").equalTo(tmp1.col("flname"))).and(metlistinitial.col("description").equalTo(tmp1.col("descri")))).withColumn("zscore",((col("intensity").$minus(col("avg(intensity)"))).$div(col("stddev_samp(intensity)")))).na().fill(0);
                    Dataset<Row> finalexp = tmpexpgroups2.drop("intensity","avg(intensity)","stddev_samp(intensity)","expid","descri","flname").withColumnRenamed("zscore", "intensity");
                    newmetfinalpeaks = finalexp.withColumn("expidfilename", functions.concat(col("experimentid"), lit("__"), col("filename"))).drop("experimentid","filename").groupBy(col("expidfilename"), col("description")).mean("intensity").select(col("description"),col("avg(intensity)"),col("expidfilename"));
                }
               
                Dataset<Row> newmetfinalpeaksafterrename=newmetfinalpeaks.map((MapFunction<Row, Row>) originalrow -> {
                    String replicatewithdot = originalrow.get(2).toString();
                    String temp = replicatewithdot.replaceAll("\\.[mzmlxdatMZMLXDAT]{4,6}$","");
                    if(temp.contains(".")){
                                temp=temp.replaceAll("[.]", "_");
                            }
                    Object[] rowValues = new Object[originalrow.length()];
                    rowValues[0]=originalrow.get(0);
                    rowValues[1]=originalrow.get(1);
                    rowValues[2]=temp;
                    Row newrow = RowFactory.create(rowValues);
                    return newrow;
                }, RowEncoder.apply(newmetfinalpeaks.schema())).persist();
                

                Dataset<Row> expavgpeaksmissval = newmetfinalpeaksafterrename.groupBy(col("description")).pivot("expidfilename").mean("avg(intensity)");//.persist();
                String[] metincols = metlist.parallelStream().toArray(String[]::new);
               
                if(missval.equals("omit")){
                    //finding missing metabolites
                    StructType schema = new StructType(new StructField[] {
                                new StructField("metabs",
                                                DataTypes.StringType, false,
                                                Metadata.empty()) });

                    Dataset<Row> tempmissvalmtbs=expavgpeaksmissval.map((MapFunction<Row, Row>) originalrow -> {
                        if(originalrow.anyNull()){
                            String rowValues = new String();
                            rowValues=originalrow.get(0).toString();
                            Row newrow = RowFactory.create(rowValues);
                            return newrow;
                        }else{
                            String rowValues = "notmissing";
                            Row newrow = RowFactory.create(rowValues);
                            return newrow;
                        }
                    }, RowEncoder.apply(schema));
                    Dataset<Row> missvalmetabs=tempmissvalmtbs.select("metabs").filter(col("metabs").notEqual("notmissing"));
                    missvalmtbs=missvalmetabs.as(Encoders.STRING()).collectAsList();
                    replicateslistclustering.setMissvalmtbs(missvalmtbs);
                    replicateslistclustering.setMtbslist(metlist);
                    
                    Dataset<Row> dfmetabolitelist = sp.createDataset(metlist, Encoders.STRING()).toDF("description");
                    Dataset<Row> tempexpavgpeaks = newmetfinalpeaksafterrename.join(dfmetabolitelist,"description");
                    
                    Dataset<Row> scaledData = sp.emptyDataFrame();
                    if(normval.equals("normvalmean")){
                        Dataset<Row> tmp1 = tempexpavgpeaks.groupBy(col("expidfilename")).agg(functions.mean("avg(intensity)"),functions.stddev("avg(intensity)")).withColumnRenamed("expidfilename", "expidflname");
                        Dataset<Row> tmpexpgroups2 = tempexpavgpeaks.join(tmp1,tempexpavgpeaks.col("expidfilename").equalTo(tmp1.col("expidflname"))).withColumn("zscore",((col("avg(intensity)").$minus(col("avg(avg(intensity))"))).$div(col("stddev_samp(avg(intensity))")))).na().fill(0);
                        scaledData = tmpexpgroups2.drop("avg(intensity)","avg(avg(intensity))","stddev_samp(avg(intensity))","expidflname").withColumnRenamed("zscore", "avg(intensity)");
                        
                    }else if(normval.equals("normvalmeanmtb")){
                        Dataset<Row> sthto = tempexpavgpeaks.groupBy(col("description")).agg(functions.mean("avg(intensity)"),functions.stddev("avg(intensity)")).withColumnRenamed("description", "descri");
                        Dataset<Row>sthbefore = tempexpavgpeaks.join(sthto,tempexpavgpeaks.col("description").equalTo(sthto.col("descri"))).withColumn("zscore",((col("avg(intensity)").$minus(col("avg(avg(intensity))"))).$div(col("stddev_samp(avg(intensity))")))).na().fill(0);
                        scaledData=sthbefore.drop("avg(intensity)","avg(avg(intensity))","stddev_samp(avg(intensity))","descri").withColumnRenamed("zscore", "avg(intensity)");
                    }else{
                        scaledData=tempexpavgpeaks;
                    }
                    
                    newfilenameavgpeaks=scaledData;
                    Dataset<Row> dfwithnulls = newfilenameavgpeaks.groupBy(newfilenameavgpeaks.col("expidfilename")).agg(functions.collect_list(functions.map(col("description"),col("avg(intensity)")))).toDF("expidfilename","descinten");
                    StructType schema2 = new StructType(new StructField[] {
                                new StructField("expidsrepl",DataTypes.StringType, false,Metadata.empty()),
                                new StructField("features",new VectorUDT(),false,Metadata.empty())
                    });
                    Dataset<Row> expsandvectorsdf=dfwithnulls.map((MapFunction<Row, Row>) originalrow -> {
                            String firstpos = new String();
                            firstpos=originalrow.get(0).toString();
                            List<scala.collection.Map<String,Double>>mplist=originalrow.getList(1);
                            int s = mplist.size();
                            Map<String,Double>treemp=new TreeMap<>();
                            for(int k=0;k<s;k++){
                                Object[] desc = JavaConversions.mapAsJavaMap(mplist.get(k)).keySet().toArray();
                                Object[] kvlist= JavaConversions.mapAsJavaMap(mplist.get(k)).values().toArray();
                                treemp.put(desc[0].toString(),Double.parseDouble(kvlist[0].toString()));
                            }
                            Object[] yo1 = treemp.values().toArray();
                            double[] tmplist= new double[s];
                            for(int i=0;i<s;i++){
                                tmplist[i]=Double.parseDouble(yo1[i].toString());
                            }
                            Row newrow = RowFactory.create(firstpos,Vectors.dense(tmplist));
                            return newrow;

                    }, RowEncoder.apply(schema2)).persist();
                    
                    if(clusteringmethod.equals("kmeans")){

                        long randomSeed = new Random().nextLong();
                        KMeans kmeans = new KMeans().setK(initialval).setInitMode("random").setSeed(randomSeed);

                        KMeansModel model = kmeans.fit(expsandvectorsdf);

                        // Make predictions
                        Dataset<Row> predictions = model.transform(expsandvectorsdf);
                        Dataset<Row> clustinfotemp = predictions.select(col("expidsrepl"),col("prediction")).groupBy(col("prediction")).count().orderBy(asc("prediction"));

                        replicateslistclustering.setClustinfo(clustinfotemp.select(col("count")).as(Encoders.LONG()).collectAsList());
                        replicateslistclustering.setReplilist(predictions.select(col("expidsrepl")).as(Encoders.STRING()).collectAsList());
                        replicateslistclustering.setClusts(predictions.select(col("prediction")).as(Encoders.INT()).collectAsList());
                        
                        if(clustinfotemp.count()>1){
                            // Evaluate clustering by computing Silhouette score
                            ClusteringEvaluator evaluator = new ClusteringEvaluator();
                            double silhouette = evaluator.evaluate(predictions);
                            replicateslistclustering.setSilhouette(silhouette);
                        }else{
                            rowflag=3;
                        }
                        Vector[] centers = model.clusterCenters();
                        int numrows=centers.length;
                        int numcols=metincols.length;

                        for (int i=0;i<numrows;i++) {                      
                          double[] temparray1 = centers[i].toArray();
                          Double[] temparray2 = new Double[temparray1.length];
                          for(int j=0;j<numcols;j++){
                              temparray2[j]=temparray1[j];
                          }
                          clustcenters.add(temparray2);
                        }
                        replicateslistclustering.setRepliclustscenters(clustcenters);
                        expsandvectorsdf.unpersist();
                    }else if(clusteringmethod.equals("bikmeans")){
                        
                        BisectingKMeans bkm = new BisectingKMeans().setK(initialval);
                        BisectingKMeansModel model = bkm.fit(expsandvectorsdf);

                        // Make predictions
                        Dataset<Row> predictions = model.transform(expsandvectorsdf);
                        Dataset<Row> clustinfotemp = predictions.select(col("expidsrepl"),col("prediction")).groupBy(col("prediction")).count().orderBy(asc("prediction"));

                        replicateslistclustering.setClustinfo(clustinfotemp.select(col("count")).as(Encoders.LONG()).collectAsList());
                        replicateslistclustering.setReplilist(predictions.select(col("expidsrepl")).as(Encoders.STRING()).collectAsList());
                        replicateslistclustering.setClusts(predictions.select(col("prediction")).as(Encoders.INT()).collectAsList());
                        
                        if(clustinfotemp.count()>1){
                            // Evaluate clustering.
                            ClusteringEvaluator evaluator = new ClusteringEvaluator();
                            double silhouette = evaluator.evaluate(predictions);
                            replicateslistclustering.setSilhouette(silhouette);
                        }else{
                            rowflag=3;
                        }
                        // Shows the result.
                        Vector[] centers = model.clusterCenters();
                        int numrows=centers.length;
                        int numcols=metincols.length;

                        for (int i=0;i<numrows;i++) {
                          double[] temparray1 = centers[i].toArray();
                          Double[] temparray2 = new Double[temparray1.length];
                          for(int j=0;j<numcols;j++){
                              temparray2[j]=temparray1[j];
                          }
                          clustcenters.add(temparray2);
                        }
                        replicateslistclustering.setRepliclustscenters(clustcenters);
                        expsandvectorsdf.unpersist();
                    }
                }else{
                    //finding missing metabolites
                    StructType schema = new StructType(new StructField[] {
                                new StructField("description",DataTypes.StringType, false,Metadata.empty()),
                                new StructField("expidfilename",DataTypes.createArrayType(DataTypes.StringType, true), false,Metadata.empty())});
                    String[] columnnames=expavgpeaksmissval.columns();
                    int mtbswithallnullms=columnnames.length;

                    Dataset<Row> tempmissvalmtbs=expavgpeaksmissval.map((MapFunction<Row, Row>) originalrow -> {
                        if(originalrow.anyNull()){
                            List<String>tmpexpidfilename=new ArrayList<>();
                            for(int i=1;i<mtbswithallnullms;i++){
                                if(originalrow.isNullAt(i)){
                                    tmpexpidfilename.add(columnnames[i]);
                                }
                            }
                            String mtbname = originalrow.get(0).toString();
                            Row newrow = RowFactory.create(mtbname,tmpexpidfilename.toArray(new String[0]));
                            return newrow;
                        }else{
                            String mtbname = "notmissing";
                            List<String>tmpexpidfilename=new ArrayList<>();
                            Row newrow = RowFactory.create(mtbname,tmpexpidfilename.toArray(new String[0]));
                            return newrow;
                        }
                    }, RowEncoder.apply(schema)).persist();
                    Dataset<Row> missvalmetabs=tempmissvalmtbs.select("description").filter(col("description").notEqual("notmissing"));
                    missvalmtbs=missvalmetabs.as(Encoders.STRING()).collectAsList();
                    replicateslistclustering.setMissvalmtbs(missvalmtbs);
                    replicateslistclustering.setMtbslist(metlist);
                    
                    Dataset<Row> nullmtbs = tempmissvalmtbs.filter(col("description").notEqual("notmissing")).withColumn("expidfilename", functions.explode_outer(col("expidfilename"))).withColumn("avg(intensity)", functions.lit(null).cast(DataTypes.DoubleType)).select(col("description"),col("avg(intensity)"),col("expidfilename"));

                    Dataset<Row> tmpsupernewmetfinalpeaksafterrename=newmetfinalpeaksafterrename.union(nullmtbs);

                    if(missval.equals("replacewithzero")){
                        newfilenameavgpeaks=tmpsupernewmetfinalpeaksafterrename.na().fill(0);
                    }else if(missval.equals("replacewithmean")){
                        Dataset<Row>tmp1newfilenameavgpeaks = tmpsupernewmetfinalpeaksafterrename.groupBy(tmpsupernewmetfinalpeaksafterrename.col("description")).mean("avg(intensity)");
                        Dataset<Row>tmp2newfilenameavgpeaks = tmpsupernewmetfinalpeaksafterrename.join(tmp1newfilenameavgpeaks,"description");
                        
                        newfilenameavgpeaks=tmp2newfilenameavgpeaks.map((MapFunction<Row, Row>) originalrow -> {
                            if(originalrow.anyNull()){
                                String mtbname = originalrow.get(0).toString();
                                String expid = originalrow.get(2).toString();
                                double inten = Double.parseDouble(originalrow.get(3).toString());
                                Row newrow = RowFactory.create(mtbname,inten,expid);
                                return newrow;
                            }else{
                                String mtbname = originalrow.get(0).toString();
                                String expid = originalrow.get(2).toString();
                                double inten = Double.parseDouble(originalrow.get(1).toString());
                                Row newrow = RowFactory.create(mtbname,inten,expid);
                                return newrow;
                            }
                        }, RowEncoder.apply(tmpsupernewmetfinalpeaksafterrename.schema())).drop("avg(avg(intensity))");
                        
                    }else if(missval.equals("replacewithmedian")){
                        Dataset<Row>tmp1newfilenameavgpeaks = tmpsupernewmetfinalpeaksafterrename.groupBy(tmpsupernewmetfinalpeaksafterrename.col("description")).agg(callUDF("percentile_approx", col("avg(intensity)"), lit(0.5)).as("median"));
                        Dataset<Row>tmp2newfilenameavgpeaks = tmpsupernewmetfinalpeaksafterrename.join(tmp1newfilenameavgpeaks,"description");
                        newfilenameavgpeaks=tmp2newfilenameavgpeaks.map((MapFunction<Row, Row>) originalrow -> {
                            if(originalrow.anyNull()){
                                String mtbname = originalrow.get(0).toString();
                                String expid = originalrow.get(2).toString();
                                double inten = Double.parseDouble(originalrow.get(3).toString());
                                Row newrow = RowFactory.create(mtbname,inten,expid);
                                return newrow;
                            }else{
                                String mtbname = originalrow.get(0).toString();
                                String expid = originalrow.get(2).toString();
                                double inten = Double.parseDouble(originalrow.get(1).toString());
                                Row newrow = RowFactory.create(mtbname,inten,expid);
                                return newrow;
                            }
                        }, RowEncoder.apply(tmpsupernewmetfinalpeaksafterrename.schema())).drop("median");
                       
                    }
                    
                    Dataset<Row> scaledData = sp.emptyDataFrame();
                    if(normval.equals("normvalmean")){
                        Dataset<Row> tmp1 = newfilenameavgpeaks.groupBy(col("expidfilename")).agg(functions.mean("avg(intensity)"),functions.stddev("avg(intensity)")).withColumnRenamed("expidfilename", "expidflname");
                        Dataset<Row> tmpexpgroups2 = newfilenameavgpeaks.join(tmp1,newfilenameavgpeaks.col("expidfilename").equalTo(tmp1.col("expidflname"))).withColumn("zscore",((col("avg(intensity)").$minus(col("avg(avg(intensity))"))).$div(col("stddev_samp(avg(intensity))")))).na().fill(0);
                        scaledData = tmpexpgroups2.drop("avg(intensity)","avg(avg(intensity))","stddev_samp(avg(intensity))","expidflname").withColumnRenamed("zscore", "avg(intensity)");
                        
                    }else if(normval.equals("normvalmeanmtb")){
                        Dataset<Row> sthto = newfilenameavgpeaks.groupBy(col("description")).agg(functions.mean("avg(intensity)"),functions.stddev("avg(intensity)")).withColumnRenamed("description", "descri");
                        Dataset<Row>sthbefore = newfilenameavgpeaks.join(sthto,newfilenameavgpeaks.col("description").equalTo(sthto.col("descri"))).withColumn("zscore",((col("avg(intensity)").$minus(col("avg(avg(intensity))"))).$div(col("stddev_samp(avg(intensity))")))).na().fill(0);
                        scaledData=sthbefore.drop("avg(intensity)","avg(avg(intensity))","stddev_samp(avg(intensity))","descri").withColumnRenamed("zscore", "avg(intensity)");
                    }else{
                        scaledData=newfilenameavgpeaks;
                    }
                    
                    Dataset<Row> dfwithnulls = scaledData.groupBy(scaledData.col("expidfilename")).agg(functions.collect_list(functions.map(col("description"),col("avg(intensity)")))).toDF("expidfilename","descinten");
                    StructType schema2 = new StructType(new StructField[] {
                                new StructField("expidsrepl",DataTypes.StringType, false,Metadata.empty()),
                                new StructField("features",new VectorUDT(),false,Metadata.empty())
                    });
                    Dataset<Row> expsandvectorsdf=dfwithnulls.map((MapFunction<Row, Row>) originalrow -> {
                            String firstpos = originalrow.get(0).toString();
                            List<scala.collection.Map<String,Double>>mplist=originalrow.getList(1);
                            int s = mplist.size();
                            Map<String,Double>treemp=new TreeMap<>();
                            for(int k=0;k<s;k++){
                                Object[] desc = JavaConversions.mapAsJavaMap(mplist.get(k)).keySet().toArray();
                                Object[] kvlist= JavaConversions.mapAsJavaMap(mplist.get(k)).values().toArray();
                                treemp.put(desc[0].toString(),Double.parseDouble(kvlist[0].toString()));
                            }
                            Object[] yo1 = treemp.values().toArray();
                            double[] tmplist= new double[s];
                            for(int i=0;i<s;i++){
                                tmplist[i]=Double.parseDouble(yo1[i].toString());
                            }
                            Row newrow = RowFactory.create(firstpos,Vectors.dense(tmplist));
                            return newrow;

                    }, RowEncoder.apply(schema2)).persist();
                    if(clusteringmethod.equals("kmeans")){
                        
                        long randomSeed = new Random().nextLong();
                        KMeans kmeans = new KMeans().setK(initialval).setInitMode("random").setSeed(randomSeed);

                        KMeansModel model = kmeans.fit(expsandvectorsdf);

                        // Make predictions
                        Dataset<Row> predictions = model.transform(expsandvectorsdf);
                        Dataset<Row> clustinfotemp = predictions.select(col("expidsrepl"),col("prediction")).groupBy(col("prediction")).count().orderBy(asc("prediction"));

                        replicateslistclustering.setClustinfo(clustinfotemp.select(col("count")).as(Encoders.LONG()).collectAsList());
                        replicateslistclustering.setReplilist(predictions.select(col("expidsrepl")).as(Encoders.STRING()).collectAsList());
                        replicateslistclustering.setClusts(predictions.select(col("prediction")).as(Encoders.INT()).collectAsList());
                        if(clustinfotemp.count()>1){
                            // Evaluate clustering by computing Silhouette score
                            ClusteringEvaluator evaluator = new ClusteringEvaluator();
                            double silhouette = evaluator.evaluate(predictions);
                            replicateslistclustering.setSilhouette(silhouette);
                        }else{
                            rowflag=3;
                        }
                        Vector[] centers = model.clusterCenters();
                        int numrows=centers.length;
                        int numcols=metincols.length;

                        for (int i=0;i<numrows;i++) {                      
                          double[] temparray1 = centers[i].toArray();
                          Double[] temparray2 = new Double[temparray1.length];
                          for(int j=0;j<numcols;j++){
                              temparray2[j]=temparray1[j];

                          }
                          clustcenters.add(temparray2);
                        }
                        replicateslistclustering.setRepliclustscenters(clustcenters);
                        expsandvectorsdf.unpersist();
                    }else if(clusteringmethod.equals("bikmeans")){

                        BisectingKMeans bkm = new BisectingKMeans().setK(initialval);
                        BisectingKMeansModel model = bkm.fit(expsandvectorsdf);

                        // Make predictions
                        Dataset<Row> predictions = model.transform(expsandvectorsdf);
                        Dataset<Row> clustinfotemp = predictions.select(col("expidsrepl"),col("prediction")).groupBy(col("prediction")).count().orderBy(asc("prediction"));

                        replicateslistclustering.setClustinfo(clustinfotemp.select(col("count")).as(Encoders.LONG()).collectAsList());
                        replicateslistclustering.setReplilist(predictions.select(col("expidsrepl")).as(Encoders.STRING()).collectAsList());
                        replicateslistclustering.setClusts(predictions.select(col("prediction")).as(Encoders.INT()).collectAsList());
                        if(clustinfotemp.count()>1){
                            // Evaluate clustering.
                            ClusteringEvaluator evaluator = new ClusteringEvaluator();
                            double silhouette = evaluator.evaluate(predictions);
                            replicateslistclustering.setSilhouette(silhouette);
                        }else{
                            rowflag=3;
                        }
                        Vector[] centers = model.clusterCenters();
                        int numrows=centers.length;
                        int numcols=metincols.length;

                        for (int i=0;i<numrows;i++) {
                          double[] temparray1 = centers[i].toArray();
                          Double[] temparray2 = new Double[temparray1.length];
                          for(int j=0;j<numcols;j++){
                              temparray2[j]=temparray1[j];
                              
                          }
                          clustcenters.add(temparray2);
                        }
                        replicateslistclustering.setRepliclustscenters(clustcenters);
                        expsandvectorsdf.unpersist();
                    }
                    dfwithnulls.unpersist();
                    tempmissvalmtbs.unpersist();
                }
                newmetfinalpeaksafterrename.unpersist();
                
            }else{
                rowflag=2;
            }
        }else if(allorsome.equals("onselected")){
            Collections.sort(metabolitelist);
            metlist=metabolitelist;
            Dataset<Row> dfmetabolitelist = sp.createDataset(metlist, Encoders.STRING()).toDF("description");
            
            Dataset<Row> metlistinitial =sp.emptyDataFrame();
            if(ionmode.equals("both")){
                Encoder<ExperimentFormOnlyParKey> ExpEncoder = Encoders.bean(ExperimentFormOnlyParKey.class);
                Encoder<ExperimentFormExDesFlIn> ExpEncoderoffour = Encoders.bean(ExperimentFormExDesFlIn.class);
                Dataset<ExperimentFormOnlyParKey> dfexplistoriginal = sp.createDataset(experimentlist, Encoders.STRING()).toDF("experimentid").as(ExpEncoder);
                CassandraJavaPairRDD<ExperimentFormOnlyParKey, ExperimentFormExDesFlIn> predf = CassandraJavaUtil.javaFunctions(CassandraJavaUtil.javaFunctions(dfexplistoriginal.javaRDD()).repartitionByCassandraReplica("mdb","experiment",100,CassandraJavaUtil.someColumns("experimentid"),CassandraJavaUtil.mapToRow(ExperimentFormOnlyParKey.class)))
                                                                                    .joinWithCassandraTable("mdb","experiment", CassandraJavaUtil.someColumns("experimentid","filename","description","intensity"), CassandraJavaUtil.someColumns("experimentid"), CassandraJavaUtil.mapRowTo(ExperimentFormExDesFlIn.class), CassandraJavaUtil.mapToRow(ExperimentFormOnlyParKey.class));
                metlistinitial =sp.createDataset(predf.map(x->x._2).rdd(), ExpEncoderoffour).toDF( "description","experimentid","filename","intensity").filter(col("description").notEqual("Unidentified"));
            }else if(ionmode.equals("positive")){
                Encoder<ExperimentFormOnlyParKey> ExpEncoder = Encoders.bean(ExperimentFormOnlyParKey.class);
                Encoder<ExperimentFormExDesFlInAd> ExpEncoderofthree = Encoders.bean(ExperimentFormExDesFlInAd.class);
                Dataset<ExperimentFormOnlyParKey> dfexplistoriginal = sp.createDataset(experimentlist, Encoders.STRING()).toDF("experimentid").as(ExpEncoder); 
                CassandraJavaPairRDD<ExperimentFormOnlyParKey, ExperimentFormExDesFlInAd> predf = CassandraJavaUtil.javaFunctions(CassandraJavaUtil.javaFunctions(dfexplistoriginal.javaRDD()).repartitionByCassandraReplica("mdb","experiment",100,CassandraJavaUtil.someColumns("experimentid"),CassandraJavaUtil.mapToRow(ExperimentFormOnlyParKey.class)))
                                                                                    .joinWithCassandraTable("mdb","experiment", CassandraJavaUtil.someColumns("experimentid","filename","description","adduct","intensity"), CassandraJavaUtil.someColumns("experimentid"), CassandraJavaUtil.mapRowTo(ExperimentFormExDesFlInAd.class), CassandraJavaUtil.mapToRow(ExperimentFormOnlyParKey.class));
                Dataset<Row> metlistinitialbef =sp.createDataset(predf.map(x->x._2).rdd(), ExpEncoderofthree).toDF( "adduct","description","experimentid","filename","intensity").filter(col("description").notEqual("Unidentified"));
                Dataset<Row> metlistinitialbe= metlistinitialbef.withColumn("ioniz", functions.substring(col("adduct"), -1, 1)).drop("adduct").filter(col("ioniz").equalTo("+"));
                metlistinitial = metlistinitialbe.drop(col("ioniz"));
            }else if(ionmode.equals("negative")){
                Encoder<ExperimentFormOnlyParKey> ExpEncoder = Encoders.bean(ExperimentFormOnlyParKey.class);
                Encoder<ExperimentFormExDesFlInAd> ExpEncoderofthree = Encoders.bean(ExperimentFormExDesFlInAd.class);
                Dataset<ExperimentFormOnlyParKey> dfexplistoriginal = sp.createDataset(experimentlist, Encoders.STRING()).toDF("experimentid").as(ExpEncoder);
                CassandraJavaPairRDD<ExperimentFormOnlyParKey, ExperimentFormExDesFlInAd> predf = CassandraJavaUtil.javaFunctions(CassandraJavaUtil.javaFunctions(dfexplistoriginal.javaRDD()).repartitionByCassandraReplica("mdb","experiment",100,CassandraJavaUtil.someColumns("experimentid"),CassandraJavaUtil.mapToRow(ExperimentFormOnlyParKey.class)))
                                                                                    .joinWithCassandraTable("mdb","experiment", CassandraJavaUtil.someColumns("experimentid","filename","description","adduct","intensity"), CassandraJavaUtil.someColumns("experimentid"), CassandraJavaUtil.mapRowTo(ExperimentFormExDesFlInAd.class), CassandraJavaUtil.mapToRow(ExperimentFormOnlyParKey.class));
                Dataset<Row> metlistinitialbef =sp.createDataset(predf.map(x->x._2).rdd(), ExpEncoderofthree).toDF( "adduct","description","experimentid","filename","intensity").filter(col("description").notEqual("Unidentified"));
                Dataset<Row> metlistinitialbe= metlistinitialbef.withColumn("ioniz", functions.substring(col("adduct"), -1, 1)).drop("adduct").filter(col("ioniz").equalTo("-"));
                metlistinitial = metlistinitialbe.drop(col("ioniz"));
            }
            
            
            Dataset<Row> newfilenameavgpeaks=sp.emptyDataFrame();
            replicateslistclustering.setMtbslist(metabolitelist);
            String[] metincols = metlist.parallelStream().toArray(String[]::new);
            if(missval.equals("omit")){
                Dataset<Row> metlistinitial1 = metlistinitial.join(dfmetabolitelist,"description");
                
                Dataset<Row>filenameavgpeaks = sp.emptyDataFrame();
                if(normval.equals("normvalmean")|| normval.equals("no")|| normval.equals("normvalmeanmtb")){
                    Dataset<Row>metfinalpeaks = metlistinitial1.groupBy(metlistinitial1.col("experimentid"),metlistinitial1.col("description"),metlistinitial1.col("filename")).avg("intensity").orderBy(asc("experimentid"), asc("description"));
                    filenameavgpeaks = metfinalpeaks.withColumn("expidfilename", functions.concat(col("experimentid"), lit("__"), col("filename"))).drop("experimentid","filename");
                }else if(normval.equals("normvalorig")){//or       
                    //this is z-score on description and experiment
                    Dataset<Row> tmp1 = metlistinitial1.groupBy(col("experimentid"),metlistinitial1.col("filename"),col("description")).agg(functions.mean("intensity"),functions.stddev("intensity")).withColumnRenamed("experimentid", "expid").withColumnRenamed("filename", "flname").withColumnRenamed("description", "descri");
                    Dataset<Row> tmpexpgroups2 = metlistinitial1.join(tmp1,metlistinitial1.col("experimentid").equalTo(tmp1.col("expid")).and(metlistinitial1.col("filename").equalTo(tmp1.col("flname"))).and(metlistinitial.col("description").equalTo(tmp1.col("descri")))).withColumn("zscore",((col("intensity").$minus(col("avg(intensity)"))).$div(col("stddev_samp(intensity)")))).na().fill(0);
                    Dataset<Row> finalexp = tmpexpgroups2.drop("intensity","avg(intensity)","stddev_samp(intensity)","expid","descri","flname").withColumnRenamed("zscore", "intensity");
                    filenameavgpeaks = finalexp.withColumn("expidfilename", functions.concat(col("experimentid"), lit("__"), col("filename"))).drop("experimentid","filename").groupBy(col("expidfilename"), col("description")).mean("intensity");//.persist();
                }
                
                Dataset<Row> scaledData = sp.emptyDataFrame();
                if(normval.equals("normvalmean")){
                    Dataset<Row> tmp1 = filenameavgpeaks.groupBy(col("expidfilename")).agg(functions.mean("avg(intensity)"),functions.stddev("avg(intensity)")).withColumnRenamed("expidfilename", "expidflname");
                    Dataset<Row> tmpexpgroups2 = filenameavgpeaks.join(tmp1,filenameavgpeaks.col("expidfilename").equalTo(tmp1.col("expidflname"))).withColumn("zscore",((col("avg(intensity)").$minus(col("avg(avg(intensity))"))).$div(col("stddev_samp(avg(intensity))")))).na().fill(0);
                    scaledData = tmpexpgroups2.drop("avg(intensity)","avg(avg(intensity))","stddev_samp(avg(intensity))","expidflname").withColumnRenamed("zscore", "avg(intensity)").select(col("description"),col("avg(intensity)"),col("expidfilename"));
                   
                }else if(normval.equals("normvalmeanmtb")){
                        Dataset<Row> sthto = filenameavgpeaks.groupBy(col("description")).agg(functions.mean("avg(intensity)"),functions.stddev("avg(intensity)")).withColumnRenamed("description", "descri");
                        Dataset<Row>sthbefore = filenameavgpeaks.join(sthto,filenameavgpeaks.col("description").equalTo(sthto.col("descri"))).withColumn("zscore",((col("avg(intensity)").$minus(col("avg(avg(intensity))"))).$div(col("stddev_samp(avg(intensity))")))).na().fill(0);
                        scaledData=sthbefore.drop("avg(intensity)","avg(avg(intensity))","stddev_samp(avg(intensity))","descri").withColumnRenamed("zscore", "avg(intensity)").select(col("description"),col("avg(intensity)"),col("expidfilename"));
                }else{
                    scaledData=filenameavgpeaks.select(col("description"),col("avg(intensity)"),col("expidfilename"));
                }
                
                Dataset<Row> newmetfinalpeaksafterrename=scaledData.map((MapFunction<Row, Row>) originalrow -> {
                        String replicatewithdot = originalrow.get(2).toString();
                        String temp = replicatewithdot.replaceAll("\\.[mzmlxdatMZMLXDAT]{4,6}$","");
                        if(temp.contains(".")){
                                    temp=temp.replaceAll("[.]", "_");
                                }
                        Object[] rowValues = new Object[originalrow.length()];
                        rowValues[0]=originalrow.get(0);
                        rowValues[1]=originalrow.get(1);
                        rowValues[2]=temp;
                        Row newrow = RowFactory.create(rowValues);
                        return newrow;
                    }, RowEncoder.apply(scaledData.schema()));
                

                newfilenameavgpeaks=newmetfinalpeaksafterrename;
                Dataset<Row> dfwithnulls = newfilenameavgpeaks.groupBy(newfilenameavgpeaks.col("expidfilename")).agg(functions.collect_list(functions.map(col("description"),col("avg(intensity)")))).toDF("expidfilename","descinten");
                StructType schema2 = new StructType(new StructField[] {
                            new StructField("expidsrepl",DataTypes.StringType, false,Metadata.empty()),
                            new StructField("features",new VectorUDT(),false,Metadata.empty())
                });
                Dataset<Row> expsandvectorsdf=dfwithnulls.map((MapFunction<Row, Row>) originalrow -> {
                        String firstpos = new String();
                        firstpos=originalrow.get(0).toString();
                        List<scala.collection.Map<String,Double>>mplist=originalrow.getList(1);
                        int s = mplist.size();
                        Map<String,Double>treemp=new TreeMap<>();
                        for(int k=0;k<s;k++){
                            Object[] desc = JavaConversions.mapAsJavaMap(mplist.get(k)).keySet().toArray();
                            Object[] kvlist= JavaConversions.mapAsJavaMap(mplist.get(k)).values().toArray();
                            treemp.put(desc[0].toString(),Double.parseDouble(kvlist[0].toString()));
                        }
                        Object[] yo1 = treemp.values().toArray();
                        double[] tmplist= new double[s];
                        for(int i=0;i<s;i++){
                            tmplist[i]=Double.parseDouble(yo1[i].toString());
                        }
                        Row newrow = RowFactory.create(firstpos,Vectors.dense(tmplist));
                        return newrow;

                }, RowEncoder.apply(schema2)).persist();
                
                if(clusteringmethod.equals("kmeans")){
                    
                    long randomSeed = new Random().nextLong();
                    KMeans kmeans = new KMeans().setK(initialval).setInitMode("random").setSeed(randomSeed);

                    KMeansModel model = kmeans.fit(expsandvectorsdf);

                    // Make predictions
                    Dataset<Row> predictions = model.transform(expsandvectorsdf);
                    Dataset<Row> clustinfotemp = predictions.select(col("expidsrepl"),col("prediction")).groupBy(col("prediction")).count().orderBy(asc("prediction"));

                    replicateslistclustering.setClustinfo(clustinfotemp.select(col("count")).as(Encoders.LONG()).collectAsList());
                    replicateslistclustering.setReplilist(predictions.select(col("expidsrepl")).as(Encoders.STRING()).collectAsList());
                    replicateslistclustering.setClusts(predictions.select(col("prediction")).as(Encoders.INT()).collectAsList());
                    
                    if(clustinfotemp.count()>1){
                        // Evaluate clustering by computing Silhouette score
                        ClusteringEvaluator evaluator = new ClusteringEvaluator();
                        double silhouette = evaluator.evaluate(predictions);
                        replicateslistclustering.setSilhouette(silhouette);
                    }else{
                        rowflag=3;
                    }
                    Vector[] centers = model.clusterCenters();
                    int numrows=centers.length;
                    int numcols=metincols.length;

                    for (int i=0;i<numrows;i++) {
                      double[] temparray1 = centers[i].toArray();
                      Double[] temparray2 = new Double[temparray1.length];
                      for(int j=0;j<numcols;j++){
                          temparray2[j]=temparray1[j];
                      }
                      clustcenters.add(temparray2);
                    }
                    replicateslistclustering.setRepliclustscenters(clustcenters);
                    expsandvectorsdf.unpersist();
                }else if(clusteringmethod.equals("bikmeans")){
                    
                    BisectingKMeans bkm = new BisectingKMeans().setK(initialval);
                    BisectingKMeansModel model = bkm.fit(expsandvectorsdf);

                    // Make predictions
                    Dataset<Row> predictions = model.transform(expsandvectorsdf);
                    Dataset<Row> clustinfotemp = predictions.select(col("expidsrepl"),col("prediction")).groupBy(col("prediction")).count().orderBy(asc("prediction"));

                    replicateslistclustering.setClustinfo(clustinfotemp.select(col("count")).as(Encoders.LONG()).collectAsList());
                    replicateslistclustering.setReplilist(predictions.select(col("expidsrepl")).as(Encoders.STRING()).collectAsList());
                    replicateslistclustering.setClusts(predictions.select(col("prediction")).as(Encoders.INT()).collectAsList());
                    
                    if(clustinfotemp.count()>1){
                        // Evaluate clustering.
                        ClusteringEvaluator evaluator = new ClusteringEvaluator();
                        double silhouette = evaluator.evaluate(predictions);
                        replicateslistclustering.setSilhouette(silhouette);
                    }else{
                        rowflag=3;
                    }
                    Vector[] centers = model.clusterCenters();
                    int numrows=centers.length;
                    int numcols=metincols.length;

                    for (int i=0;i<numrows;i++) {
                      double[] temparray1 = centers[i].toArray();
                      Double[] temparray2 = new Double[temparray1.length];
                      for(int j=0;j<numcols;j++){
                          temparray2[j]=temparray1[j];
                      }
                      clustcenters.add(temparray2);
                    }
                    replicateslistclustering.setRepliclustscenters(clustcenters);
                    expsandvectorsdf.unpersist();
                }
            }else{
                Dataset<Row>filenameavgpeaks = sp.emptyDataFrame();
                if(normval.equals("normvalmean")|| normval.equals("no")|| normval.equals("normvalmeanmtb")){
                    Dataset<Row>metfinalpeaks = metlistinitial.groupBy(metlistinitial.col("experimentid"),metlistinitial.col("description"),metlistinitial.col("filename")).avg("intensity").orderBy(asc("experimentid"), asc("description"));
                    filenameavgpeaks = metfinalpeaks.withColumn("expidfilename", functions.concat(col("experimentid"), lit("__"), col("filename"))).drop("experimentid","filename").select(col("description"),col("avg(intensity)"),col("expidfilename"));
                }else if(normval.equals("normvalorig")){//or       
                    //this is z-score on description and experiment
                    Dataset<Row> tmp1 = metlistinitial.groupBy(col("experimentid"),metlistinitial.col("filename"),col("description")).agg(functions.mean("intensity"),functions.stddev("intensity")).withColumnRenamed("experimentid", "expid").withColumnRenamed("filename", "flname").withColumnRenamed("description", "descri");
                    Dataset<Row> tmpexpgroups2 = metlistinitial.join(tmp1,metlistinitial.col("experimentid").equalTo(tmp1.col("expid")).and(metlistinitial.col("filename").equalTo(tmp1.col("flname"))).and(metlistinitial.col("description").equalTo(tmp1.col("descri")))).withColumn("zscore",((col("intensity").$minus(col("avg(intensity)"))).$div(col("stddev_samp(intensity)")))).na().fill(0);
                    Dataset<Row> finalexp = tmpexpgroups2.drop("intensity","avg(intensity)","stddev_samp(intensity)","expid","descri","flname").withColumnRenamed("zscore", "intensity");
                    filenameavgpeaks = finalexp.withColumn("expidfilename", functions.concat(col("experimentid"), lit("__"), col("filename"))).drop("experimentid","filename").groupBy(col("expidfilename"), col("description")).mean("intensity").select(col("description"),col("avg(intensity)"),col("expidfilename"));//.groupBy(finalexp.col("description")).pivot("expidfilename").mean("avg(intensity)");//.persist();
                }
                
                Dataset<Row> newmetfinalpeaksafterrename=filenameavgpeaks.map((MapFunction<Row, Row>) originalrow -> {
                        String replicatewithdot = originalrow.get(2).toString();
                        String temp = replicatewithdot.replaceAll("\\.[mzmlxdatMZMLXDAT]{4,6}$","");
                        if(temp.contains(".")){
                                    temp=temp.replaceAll("[.]", "_");
                                }
                        Object[] rowValues = new Object[originalrow.length()];
                        rowValues[0]=originalrow.get(0);
                        rowValues[1]=originalrow.get(1);
                        rowValues[2]=temp;
                        Row newrow = RowFactory.create(rowValues);
                        return newrow;
                    }, RowEncoder.apply(filenameavgpeaks.schema())).persist();
                
                //finding missing metabolites
                Dataset<Row> expavgpeaksmissval = newmetfinalpeaksafterrename.groupBy(newmetfinalpeaksafterrename.col("description")).pivot("expidfilename").mean("avg(intensity)");
                StructType schema = new StructType(new StructField[] {
                            new StructField("description",DataTypes.StringType, false,Metadata.empty()),
                            new StructField("expidfilename",DataTypes.createArrayType(DataTypes.StringType, true), false,Metadata.empty())});
                String[] columnnames=expavgpeaksmissval.columns();
                int mtbswithallnullms=columnnames.length;
                
                Dataset<Row> tempmissvalmtbs=expavgpeaksmissval.map((MapFunction<Row, Row>) originalrow -> {
                    if(originalrow.anyNull() && metabolitelist.contains(originalrow.get(0).toString())){
                        List<String>tmpexpidfilename=new ArrayList<>();
                        for(int i=1;i<mtbswithallnullms;i++){
                            if(originalrow.isNullAt(i)){
                                tmpexpidfilename.add(columnnames[i]);
                            }
                        }
                        String mtbname = originalrow.get(0).toString();
                        Row newrow = RowFactory.create(mtbname,tmpexpidfilename.toArray(new String[0]));
                        return newrow;
                    }else{
                        String mtbname = "notmissing";
                        List<String>tmpexpidfilename=new ArrayList<>();
                        Row newrow = RowFactory.create(mtbname,tmpexpidfilename.toArray(new String[0]));
                        return newrow;
                    }
                }, RowEncoder.apply(schema)).persist(); 
                Dataset<Row> missvalmetabs=tempmissvalmtbs.select("description").filter(col("description").notEqual("notmissing"));
                missvalmtbs=missvalmetabs.as(Encoders.STRING()).collectAsList();
                replicateslistclustering.setMissvalmtbs(missvalmtbs);
                replicateslistclustering.setMtbslist(metabolitelist);
                
                Dataset<Row> nullmtbs = tempmissvalmtbs.filter(col("description").notEqual("notmissing")).withColumn("expidfilename", functions.explode_outer(col("expidfilename"))).withColumn("avg(intensity)", functions.lit(null).cast(DataTypes.DoubleType)).select(col("description"),col("avg(intensity)"),col("expidfilename"));
                Dataset<Row> tmpsupernewmetfinalpeaksafterrename=newmetfinalpeaksafterrename.union(nullmtbs);
                Dataset<Row> supernewmetfinalpeaksafterrename=tmpsupernewmetfinalpeaksafterrename.join(dfmetabolitelist,"description");

                
                if(missval.equals("replacewithzero")){
                    newfilenameavgpeaks=supernewmetfinalpeaksafterrename.na().fill(0);
                }else if(missval.equals("replacewithmean")){
                    Dataset<Row>tmp1newfilenameavgpeaks = supernewmetfinalpeaksafterrename.groupBy(supernewmetfinalpeaksafterrename.col("description")).mean("avg(intensity)");
                    Dataset<Row>tmp2newfilenameavgpeaks = supernewmetfinalpeaksafterrename.join(tmp1newfilenameavgpeaks,"description");
                    newfilenameavgpeaks=tmp2newfilenameavgpeaks.map((MapFunction<Row, Row>) originalrow -> {
                        if(originalrow.anyNull()){
                            String mtbname = originalrow.get(0).toString();
                            String expid = originalrow.get(2).toString();
                            double inten = Double.parseDouble(originalrow.get(3).toString());
                            Row newrow = RowFactory.create(mtbname,inten,expid);
                            return newrow;
                        }else{
                            String mtbname = originalrow.get(0).toString();
                            String expid = originalrow.get(2).toString();
                            double inten = Double.parseDouble(originalrow.get(1).toString());
                            Row newrow = RowFactory.create(mtbname,inten,expid);
                            return newrow;
                        }
                    }, RowEncoder.apply(tmpsupernewmetfinalpeaksafterrename.schema())).drop("avg(avg(intensity))");
                    
                }else if(missval.equals("replacewithmedian")){
                    Dataset<Row>tmp1newfilenameavgpeaks = supernewmetfinalpeaksafterrename.groupBy(supernewmetfinalpeaksafterrename.col("description")).agg(callUDF("percentile_approx", col("avg(intensity)"), lit(0.5)).as("median"));
                    Dataset<Row>tmp2newfilenameavgpeaks = supernewmetfinalpeaksafterrename.join(tmp1newfilenameavgpeaks,"description");
                    newfilenameavgpeaks=tmp2newfilenameavgpeaks.map((MapFunction<Row, Row>) originalrow -> {
                        if(originalrow.anyNull()){
                            String mtbname = originalrow.get(0).toString();
                            String expid = originalrow.get(2).toString();
                            double inten = Double.parseDouble(originalrow.get(3).toString());
                            Row newrow = RowFactory.create(mtbname,inten,expid);
                            return newrow;
                        }else{
                            String mtbname = originalrow.get(0).toString();
                            String expid = originalrow.get(2).toString();
                            double inten = Double.parseDouble(originalrow.get(1).toString());
                            Row newrow = RowFactory.create(mtbname,inten,expid);
                            return newrow;
                        }
                    }, RowEncoder.apply(tmpsupernewmetfinalpeaksafterrename.schema())).drop("median");
                    
                }
                
                Dataset<Row> scaledData = sp.emptyDataFrame();
                if(normval.equals("normvalmean")){
                    Dataset<Row> tmp1 = newfilenameavgpeaks.groupBy(col("expidfilename")).agg(functions.mean("avg(intensity)"),functions.stddev("avg(intensity)")).withColumnRenamed("expidfilename", "expidflname");
                    Dataset<Row> tmpexpgroups2 = newfilenameavgpeaks.join(tmp1,newfilenameavgpeaks.col("expidfilename").equalTo(tmp1.col("expidflname"))).withColumn("zscore",((col("avg(intensity)").$minus(col("avg(avg(intensity))"))).$div(col("stddev_samp(avg(intensity))")))).na().fill(0);
                    scaledData = tmpexpgroups2.drop("avg(intensity)","avg(avg(intensity))","stddev_samp(avg(intensity))","expidflname").withColumnRenamed("zscore", "avg(intensity)");
                    
                }else if(normval.equals("normvalmeanmtb")){
                        Dataset<Row> sthto = newfilenameavgpeaks.groupBy(col("description")).agg(functions.mean("avg(intensity)"),functions.stddev("avg(intensity)")).withColumnRenamed("description", "descri");
                        Dataset<Row>sthbefore = newfilenameavgpeaks.join(sthto,newfilenameavgpeaks.col("description").equalTo(sthto.col("descri"))).withColumn("zscore",((col("avg(intensity)").$minus(col("avg(avg(intensity))"))).$div(col("stddev_samp(avg(intensity))")))).na().fill(0);
                        scaledData=sthbefore.drop("avg(intensity)","avg(avg(intensity))","stddev_samp(avg(intensity))","descri").withColumnRenamed("zscore", "avg(intensity)");
                }else{
                    scaledData=newfilenameavgpeaks;
                }
                
                Dataset<Row> dfwithnulls = scaledData.groupBy(scaledData.col("expidfilename")).agg(functions.collect_list(functions.map(col("description"),col("avg(intensity)")))).toDF("expidfilename","descinten");
                StructType schema2 = new StructType(new StructField[] {
                            new StructField("expidsrepl",DataTypes.StringType, false,Metadata.empty()),
                            new StructField("features",new VectorUDT(),false,Metadata.empty())
                });
                Dataset<Row> expsandvectorsdf=dfwithnulls.map((MapFunction<Row, Row>) originalrow -> {
                        String firstpos = new String();
                        firstpos=originalrow.get(0).toString();
                        List<scala.collection.Map<String,Double>>mplist=originalrow.getList(1);
                        int s = mplist.size();
                        Map<String,Double>treemp=new TreeMap<>();
                        for(int k=0;k<s;k++){
                            Object[] desc = JavaConversions.mapAsJavaMap(mplist.get(k)).keySet().toArray();
                            Object[] kvlist= JavaConversions.mapAsJavaMap(mplist.get(k)).values().toArray();
                            treemp.put(desc[0].toString(),Double.parseDouble(kvlist[0].toString()));
                        }
                        Object[] yo1 = treemp.values().toArray();
                        double[] tmplist= new double[s];
                        for(int i=0;i<s;i++){
                            tmplist[i]=Double.parseDouble(yo1[i].toString());
                        }
                        Row newrow = RowFactory.create(firstpos,Vectors.dense(tmplist));
                        return newrow;

                }, RowEncoder.apply(schema2)).persist();
                if(clusteringmethod.equals("kmeans")){
                    
                    long randomSeed = new Random().nextLong();
                    KMeans kmeans = new KMeans().setK(initialval).setInitMode("random").setSeed(randomSeed);

                    KMeansModel model = kmeans.fit(expsandvectorsdf);

                    // Make predictions
                    Dataset<Row> predictions = model.transform(expsandvectorsdf);
                    Dataset<Row> clustinfotemp = predictions.select(col("expidsrepl"),col("prediction")).groupBy(col("prediction")).count().orderBy(asc("prediction"));

                    replicateslistclustering.setClustinfo(clustinfotemp.select(col("count")).as(Encoders.LONG()).collectAsList());
                    replicateslistclustering.setReplilist(predictions.select(col("expidsrepl")).as(Encoders.STRING()).collectAsList());
                    replicateslistclustering.setClusts(predictions.select(col("prediction")).as(Encoders.INT()).collectAsList());
                    if(clustinfotemp.count()>1){
                        // Evaluate clustering by computing Silhouette score
                        ClusteringEvaluator evaluator = new ClusteringEvaluator();
                        double silhouette = evaluator.evaluate(predictions);
                        replicateslistclustering.setSilhouette(silhouette);
                    }else{
                        rowflag=3;
                    }
                    Vector[] centers = model.clusterCenters();
                    int numrows=centers.length;
                    int numcols=metincols.length;

                    for (int i=0;i<numrows;i++) {                      
                      double[] temparray1 = centers[i].toArray();
                      Double[] temparray2 = new Double[temparray1.length];
                      for(int j=0;j<numcols;j++){
                          temparray2[j]=temparray1[j];
                      }
                      clustcenters.add(temparray2);
                    }
                    replicateslistclustering.setRepliclustscenters(clustcenters);
                    expsandvectorsdf.unpersist();
                }else if(clusteringmethod.equals("bikmeans")){
                    
                    BisectingKMeans bkm = new BisectingKMeans().setK(initialval);
                    BisectingKMeansModel model = bkm.fit(expsandvectorsdf);

                    // Make predictions
                    Dataset<Row> predictions = model.transform(expsandvectorsdf);
                    Dataset<Row> clustinfotemp = predictions.select(col("expidsrepl"),col("prediction")).groupBy(col("prediction")).count().orderBy(asc("prediction"));

                    replicateslistclustering.setClustinfo(clustinfotemp.select(col("count")).as(Encoders.LONG()).collectAsList());
                    replicateslistclustering.setReplilist(predictions.select(col("expidsrepl")).as(Encoders.STRING()).collectAsList());
                    replicateslistclustering.setClusts(predictions.select(col("prediction")).as(Encoders.INT()).collectAsList());
                    if(clustinfotemp.count()>1){
                        // Evaluate clustering.
                        ClusteringEvaluator evaluator = new ClusteringEvaluator();
                        double silhouette = evaluator.evaluate(predictions);
                        replicateslistclustering.setSilhouette(silhouette);
                    }else{
                        rowflag=3;
                    }
                    Vector[] centers = model.clusterCenters();
                    int numrows=centers.length;
                    int numcols=metincols.length;

                    for (int i=0;i<numrows;i++) {
                      double[] temparray1 = centers[i].toArray();
                      Double[] temparray2 = new Double[temparray1.length];
                      for(int j=0;j<numcols;j++){
                          temparray2[j]=temparray1[j];
                      }
                      clustcenters.add(temparray2);
                    }
                    replicateslistclustering.setRepliclustscenters(clustcenters);
                    expsandvectorsdf.unpersist();
                }
                newmetfinalpeaksafterrename.unpersist();                
                tempmissvalmtbs.unpersist();
            }
        }
        if(rowflag==2){
            replicateslistclustering.setRowflag(2);
        }else if(rowflag==3){
            replicateslistclustering.setRowflag(3);
        }
        return replicateslistclustering;
    }
    
    
    public Metaboliteslistclustering metaboliteclustering(List<String> experimentlist,List<String> metabolitelist,String allorsome,String missval, String clusteringmethod,Integer initialval,String ionmode,String normval) {
        Metaboliteslistclustering metaboliteslistclustering = new Metaboliteslistclustering();
        List<String> missvalmtbs=new ArrayList<>();
        List<Double[]> clustcenters = new ArrayList<>();
        metaboliteslistclustering.setExpslist(experimentlist);
        List<String> metlist = new ArrayList<>();
        Integer rowflag = 0;
        if(allorsome.equals("onall")){
            if(missval.equals("omit")){
                metlist=omitInExperiments(experimentlist,ionmode);
            }else{
                metlist=replaceWithValueInExperiments(experimentlist,ionmode);
            }
            
            if (metlist != null && !metlist.isEmpty()) {
                Dataset<Row> newfilenameavgpeaks=sp.emptyDataFrame();
                
                Dataset<Row> metlistinitial =sp.emptyDataFrame();
                if(ionmode.equals("both")){
                    Encoder<ExperimentFormOnlyParKey> ExpEncoder = Encoders.bean(ExperimentFormOnlyParKey.class);
                    Encoder<ExperimentFormExDesIn> ExpEncoderofthree = Encoders.bean(ExperimentFormExDesIn.class);
                    Dataset<ExperimentFormOnlyParKey> dfexplistoriginal = sp.createDataset(experimentlist, Encoders.STRING()).toDF("experimentid").as(ExpEncoder);
                    CassandraJavaPairRDD<ExperimentFormOnlyParKey, ExperimentFormExDesIn> predf = CassandraJavaUtil.javaFunctions(CassandraJavaUtil.javaFunctions(dfexplistoriginal.javaRDD()).repartitionByCassandraReplica("mdb","experiment",100,CassandraJavaUtil.someColumns("experimentid"),CassandraJavaUtil.mapToRow(ExperimentFormOnlyParKey.class)))
                                                                                        .joinWithCassandraTable("mdb","experiment", CassandraJavaUtil.someColumns("experimentid","description","intensity"), CassandraJavaUtil.someColumns("experimentid"), CassandraJavaUtil.mapRowTo(ExperimentFormExDesIn.class), CassandraJavaUtil.mapToRow(ExperimentFormOnlyParKey.class));
                    metlistinitial =sp.createDataset(predf.map(x->x._2).rdd(), ExpEncoderofthree).toDF( "description","experimentid","intensity").filter(col("description").notEqual("Unidentified"));
                }else if(ionmode.equals("positive")){
                    Encoder<ExperimentFormOnlyParKey> ExpEncoder = Encoders.bean(ExperimentFormOnlyParKey.class);
                    Encoder<ExperimentFormExDesInAd> ExpEncoderofthree = Encoders.bean(ExperimentFormExDesInAd.class);
                    Dataset<ExperimentFormOnlyParKey> dfexplistoriginal = sp.createDataset(experimentlist, Encoders.STRING()).toDF("experimentid").as(ExpEncoder);
                    CassandraJavaPairRDD<ExperimentFormOnlyParKey, ExperimentFormExDesInAd> predf = CassandraJavaUtil.javaFunctions(CassandraJavaUtil.javaFunctions(dfexplistoriginal.javaRDD()).repartitionByCassandraReplica("mdb","experiment",100,CassandraJavaUtil.someColumns("experimentid"),CassandraJavaUtil.mapToRow(ExperimentFormOnlyParKey.class)))
                                                                                        .joinWithCassandraTable("mdb","experiment", CassandraJavaUtil.someColumns("experimentid","description","adduct","intensity"), CassandraJavaUtil.someColumns("experimentid"), CassandraJavaUtil.mapRowTo(ExperimentFormExDesInAd.class), CassandraJavaUtil.mapToRow(ExperimentFormOnlyParKey.class));
                    Dataset<Row> metlistinitialbef =sp.createDataset(predf.map(x->x._2).rdd(), ExpEncoderofthree).toDF( "adduct","description","experimentid","intensity").filter(col("description").notEqual("Unidentified"));
                    Dataset<Row> metlistinitialbe= metlistinitialbef.withColumn("ioniz", functions.substring(col("adduct"), -1, 1)).drop("adduct").filter(col("ioniz").equalTo("+"));
                    metlistinitial = metlistinitialbe.drop(col("ioniz"));
                }else if(ionmode.equals("negative")){
                    Encoder<ExperimentFormOnlyParKey> ExpEncoder = Encoders.bean(ExperimentFormOnlyParKey.class);
                    Encoder<ExperimentFormExDesInAd> ExpEncoderofthree = Encoders.bean(ExperimentFormExDesInAd.class);
                    Dataset<ExperimentFormOnlyParKey> dfexplistoriginal = sp.createDataset(experimentlist, Encoders.STRING()).toDF("experimentid").as(ExpEncoder); 
                    CassandraJavaPairRDD<ExperimentFormOnlyParKey, ExperimentFormExDesInAd> predf = CassandraJavaUtil.javaFunctions(CassandraJavaUtil.javaFunctions(dfexplistoriginal.javaRDD()).repartitionByCassandraReplica("mdb","experiment",100,CassandraJavaUtil.someColumns("experimentid"),CassandraJavaUtil.mapToRow(ExperimentFormOnlyParKey.class)))
                                                                                        .joinWithCassandraTable("mdb","experiment", CassandraJavaUtil.someColumns("experimentid","description","adduct","intensity"), CassandraJavaUtil.someColumns("experimentid"), CassandraJavaUtil.mapRowTo(ExperimentFormExDesInAd.class), CassandraJavaUtil.mapToRow(ExperimentFormOnlyParKey.class));
                    Dataset<Row> metlistinitialbef =sp.createDataset(predf.map(x->x._2).rdd(), ExpEncoderofthree).toDF( "adduct","description","experimentid","intensity").filter(col("description").notEqual("Unidentified"));
                    Dataset<Row> metlistinitialbe= metlistinitialbef.withColumn("ioniz", functions.substring(col("adduct"), -1, 1)).drop("adduct").filter(col("ioniz").equalTo("-"));
                    metlistinitial = metlistinitialbe.drop(col("ioniz"));
                }
                
                String[] expincols = experimentlist.parallelStream().toArray(String[]::new);
                Dataset<Row> expavgpeaks=sp.emptyDataFrame();
                Dataset<Row> metfinalpeaks =sp.emptyDataFrame();
                
                if(normval.equals("normvalmean")|| normval.equals("no")|| normval.equals("normvalmeanmtb")){
                    metfinalpeaks = metlistinitial.groupBy(metlistinitial.col("experimentid"),metlistinitial.col("description")).avg("intensity");
                    expavgpeaks = metfinalpeaks.groupBy(metfinalpeaks.col("description")).pivot("experimentid",new ArrayList<Object>(experimentlist)).mean("avg(intensity)").persist();
                }else if(normval.equals("normvalorig")){//or       
                //this is z-score on description and experiment
                    Dataset<Row> tmp1 = metlistinitial.groupBy(col("experimentid"),col("description")).agg(functions.mean("intensity"),functions.stddev("intensity")).withColumnRenamed("experimentid", "expid").withColumnRenamed("description", "descri");
                    Dataset<Row> tmpexpgroups2 = metlistinitial.join(tmp1,metlistinitial.col("experimentid").equalTo(tmp1.col("expid")).and(metlistinitial.col("description").equalTo(tmp1.col("descri")))).withColumn("zscore",((col("intensity").$minus(col("avg(intensity)"))).$div(col("stddev_samp(intensity)")))).na().fill(0);
                    Dataset<Row>  finalexp = tmpexpgroups2.drop("intensity","avg(intensity)","stddev_samp(intensity)","expid","descri").withColumnRenamed("zscore", "intensity");
                    metfinalpeaks = finalexp.groupBy(finalexp.col("experimentid"),finalexp.col("description")).avg("intensity");
                    expavgpeaks = metfinalpeaks.groupBy(metfinalpeaks.col("description")).pivot("experimentid",new ArrayList<Object>(experimentlist)).mean("avg(intensity)").persist(); 
                }
                

                StructType schema = new StructType(new StructField[] {
                            new StructField("metabs",
                                            DataTypes.StringType, false,
                                            Metadata.empty()) });

                Dataset<Row> tempmissvalmtbs=expavgpeaks.map((MapFunction<Row, Row>) originalrow -> {
                    if(originalrow.anyNull()){
                        String rowValues = new String();
                        rowValues=originalrow.get(0).toString();
                        Row newrow = RowFactory.create(rowValues);
                        return newrow;
                    }else{
                        String rowValues = "notmissing";
                        Row newrow = RowFactory.create(rowValues);
                        return newrow;
                    }
                }, RowEncoder.apply(schema));
                Dataset<Row> missvalmetabs=tempmissvalmtbs.select("metabs").filter(col("metabs").notEqual("notmissing"));
                missvalmtbs=missvalmetabs.as(Encoders.STRING()).collectAsList();
                metaboliteslistclustering.setMissvalmtbs(missvalmtbs);
                if(missval.equals("omit")){
                    newfilenameavgpeaks=expavgpeaks.na().drop();
                }else if(missval.equals("replacewithzero")){
                    newfilenameavgpeaks=expavgpeaks.na().fill(0);
                }else if(missval.equals("replacewithmean")){
                    newfilenameavgpeaks=expavgpeaks.map((MapFunction<Row, Row>) originalrow -> {
                        if(originalrow.anyNull()){
                            int originalrowsizeofdoubles=originalrow.length()-1;
                            Double sumrow = Double.valueOf(0);
                            Double meanrow = Double.valueOf(0);

                            Object[] rowValues = new Object[originalrow.length()];
                            rowValues[0]=originalrow.get(0).toString();

                            List<Integer>indextobereplaced = new ArrayList<>();
                            List<Double> avgintensities = new ArrayList<>();
                            int validvalues=0;
                            for(int k=1;k<=originalrowsizeofdoubles;k++){
                                if(originalrow.isNullAt(k)){
                                    indextobereplaced.add(k);
                                    avgintensities.add(null);
                                }else{
                                    sumrow = sumrow + originalrow.getDouble(k);
                                    avgintensities.add(originalrow.getDouble(k));
                                    validvalues++;
                                }
                            }
                            meanrow=sumrow/(validvalues);

                            int indextobereplacedsize=indextobereplaced.size();
                            for(int i=0;i<indextobereplacedsize;i++){
                                avgintensities.set(indextobereplaced.get(i)-1, meanrow);

                            }

                            for(int l=0;l<avgintensities.size();l++){
                                rowValues[l+1]=avgintensities.get(l);
                            }
                            Row newrow = RowFactory.create(rowValues);

                            return newrow;
                        }else{
                            return originalrow;
                        }
                    }, RowEncoder.apply(expavgpeaks.schema()));
                }else if(missval.equals("replacewithmedian")){
                    newfilenameavgpeaks=expavgpeaks.map((MapFunction<Row, Row>) originalrow -> {
                        if(originalrow.anyNull()){
                            int originalrowsizeofdoubles=originalrow.length()-1;
                            Double medianrow = Double.valueOf(0);

                            Object[] rowValues = new Object[originalrow.length()];
                            rowValues[0]=originalrow.get(0).toString();

                            List<Integer>indextobereplaced = new ArrayList<>();
                            List<Double> medianintensities = new ArrayList<>();
                            List<Double> rowmedian = new ArrayList<>();
                            int validvalues=0;
                            for(int k=1;k<=originalrowsizeofdoubles;k++){
                                if(originalrow.isNullAt(k)){
                                    indextobereplaced.add(k);
                                    medianintensities.add(null);
                                }else{
                                    medianintensities.add(originalrow.getDouble(k));
                                    rowmedian.add(originalrow.getDouble(k));
                                }
                            }
                            int rowmediannum=rowmedian.size();
                            Double[] sortedmedianintensities= new Double[rowmediannum];

                            for(int i=0;i<rowmediannum;i++){
                                sortedmedianintensities[i]=rowmedian.get(i);
                            }
                            Arrays.sort(sortedmedianintensities);
                            if (rowmediannum ==1){
                                medianrow=sortedmedianintensities[0];
                            }else if(rowmediannum>1){
                                if(rowmediannum % 2 == 0){
                                    int pos1=(rowmediannum-1)/2;
                                    int pos2=pos1+1;
                                    medianrow=(sortedmedianintensities[pos1]+sortedmedianintensities[pos2])/2;
                                }else{
                                    int pos = (rowmediannum/2);
                                    medianrow=sortedmedianintensities[pos];
                                }
                            }

                            int indextobereplacedsize=indextobereplaced.size();
                            for(int i=0;i<indextobereplacedsize;i++){
                                medianintensities.set(indextobereplaced.get(i)-1, medianrow);
                            }
                            for(int l=0;l<medianintensities.size();l++){
                                rowValues[l+1]=medianintensities.get(l);
                            }
                            Row newrow = RowFactory.create(rowValues);

                            return newrow;
                        }else{
                            return originalrow;
                        }
                    }, RowEncoder.apply(expavgpeaks.schema()));
                }
                
                Dataset<Row>normalonmtbmeans =sp.emptyDataFrame();
                if(normval.equals("normvalmeanmtb")){
                    normalonmtbmeans = newfilenameavgpeaks.map((MapFunction<Row, Row>) originalrow -> {
                        Object[] rowValues = new Object[originalrow.length()];
                        rowValues[0]=originalrow.get(0).toString();
                        int originalrowsizeofdoubles=originalrow.length()-1;
                        Double tomean = Double.valueOf(0);
                        Double stdofrow = Double.valueOf(0);

                        for(int k=1;k<=originalrowsizeofdoubles;k++){
                                tomean=tomean+originalrow.getDouble(k);
                        }
                        Double meanofrow = tomean/originalrowsizeofdoubles;

                        Double sqrdis = Double.valueOf(0);
                        for(int k=1;k<=originalrowsizeofdoubles;k++){
                            Double tmpdis=originalrow.getDouble(k)-meanofrow;
                            sqrdis=sqrdis+(tmpdis*tmpdis);
                        }
                        stdofrow=Math.sqrt(sqrdis/originalrowsizeofdoubles);

                        for(int k=1;k<=originalrowsizeofdoubles;k++){
                            rowValues[k]=(originalrow.getDouble(k)-meanofrow)/stdofrow;
                        }
                        Row newrow = RowFactory.create(rowValues);
                        return newrow;
                    }, RowEncoder.apply(newfilenameavgpeaks.schema()));
                }else{
                    normalonmtbmeans=newfilenameavgpeaks;
                }
                    
                if(clusteringmethod.equals("kmeans")){

                    VectorAssembler assemblerexp1 = new VectorAssembler()
                        .setInputCols(expincols)
                        .setOutputCol("intensity");
                    Dataset<Row> expoutput = assemblerexp1.transform(normalonmtbmeans);
                    
                    Dataset<Row> scaledData = sp.emptyDataFrame();
                    if(normval.equals("normvalmean")){
                        StandardScaler scaler = new StandardScaler()
                                .setInputCol("intensity")
                                .setOutputCol("features")
                                .setWithStd(true)
                                .setWithMean(true);

                        StandardScalerModel scalerModel = scaler.fit(expoutput);

                        scaledData = scalerModel.transform(expoutput).persist();
                    }else{
                        scaledData=expoutput.withColumnRenamed("intensity", "features").persist();
                    }
                    
                    
                    long randomSeed = new Random().nextLong();
                    KMeans kmeans = new KMeans().setK(initialval).setInitMode("random").setSeed(randomSeed);

                    KMeansModel model = kmeans.fit(scaledData);

                    // Make predictions
                    Dataset<Row> predictions = model.transform(scaledData);
                    Dataset<Row> clustinfotemp = predictions.select(col("description"),col("prediction")).groupBy(col("prediction")).count().orderBy(asc("prediction"));

                    metaboliteslistclustering.setClustinfo(clustinfotemp.select(col("count")).as(Encoders.LONG()).collectAsList());
                    metaboliteslistclustering.setMtbslist(predictions.select(col("description")).as(Encoders.STRING()).collectAsList());
                    metaboliteslistclustering.setClusts(predictions.select(col("prediction")).as(Encoders.INT()).collectAsList());
                    
                    if(clustinfotemp.count()>1){                    
                        ClusteringEvaluator evaluator = new ClusteringEvaluator();
                        double silhouette = evaluator.evaluate(predictions);
                        metaboliteslistclustering.setSilhouette(silhouette);
                    }else{
                        rowflag=3;
                    }
                    Vector[] centers = model.clusterCenters();
                    int numrows=centers.length;
                    int numcols=expincols.length;

                    for (int i=0;i<numrows;i++) {
                      double[] temparray1 = centers[i].toArray();
                      Double[] temparray2 = new Double[temparray1.length];
                      for(int j=0;j<numcols;j++){
                          temparray2[j]=temparray1[j];
                      }
                      clustcenters.add(temparray2);
                    }
                    metaboliteslistclustering.setMtbsclustscenters(clustcenters);
                    scaledData.unpersist();
                }else if(clusteringmethod.equals("bikmeans")){
                    
                    VectorAssembler assemblerexp1 = new VectorAssembler()
                        .setInputCols(expincols)
                        .setOutputCol("intensity");
                    Dataset<Row> expoutput = assemblerexp1.transform(normalonmtbmeans).persist();
                    
                    Dataset<Row> scaledData = sp.emptyDataFrame();
                    if(normval.equals("normvalmean")){
                        StandardScaler scaler = new StandardScaler()
                                .setInputCol("intensity")
                                .setOutputCol("features")
                                .setWithStd(true)
                                .setWithMean(true);

                        StandardScalerModel scalerModel = scaler.fit(expoutput);
                        scaledData = scalerModel.transform(expoutput).persist();
                    }else{
                        scaledData=expoutput.withColumnRenamed("intensity", "features").persist();
                    }
                    
                    BisectingKMeans bkm = new BisectingKMeans().setK(initialval);
                    BisectingKMeansModel model = bkm.fit(scaledData);

                    // Make predictions
                    Dataset<Row> predictions = model.transform(scaledData);
                    Dataset<Row> clustinfotemp = predictions.select(col("description"),col("prediction")).groupBy(col("prediction")).count().orderBy(asc("prediction"));

                    metaboliteslistclustering.setClustinfo(clustinfotemp.select(col("count")).as(Encoders.LONG()).collectAsList());
                    metaboliteslistclustering.setMtbslist(predictions.select(col("description")).as(Encoders.STRING()).collectAsList());
                    metaboliteslistclustering.setClusts(predictions.select(col("prediction")).as(Encoders.INT()).collectAsList());

                    if(clustinfotemp.count()>1){
                        // Evaluate clustering.
                        ClusteringEvaluator evaluator = new ClusteringEvaluator();
                        double silhouette = evaluator.evaluate(predictions);
                        metaboliteslistclustering.setSilhouette(silhouette);
                    }else{
                        rowflag=3;
                    }
                    Vector[] centers = model.clusterCenters();
                    int numrows=centers.length;
                    int numcols=expincols.length;

                    for (int i=0;i<numrows;i++) {
                      double[] temparray1 = centers[i].toArray();
                      Double[] temparray2 = new Double[temparray1.length];
                      for(int j=0;j<numcols;j++){
                          temparray2[j]=temparray1[j];
                      }
                      clustcenters.add(temparray2);
                    }
                    metaboliteslistclustering.setMtbsclustscenters(clustcenters);
                    scaledData.unpersist();
                }
                expavgpeaks.unpersist();
            }else{
                rowflag=1;
            }
        }else if(allorsome.equals("onselected")){
            Dataset<Row> dfmetabolitelist = sp.createDataset(metabolitelist, Encoders.STRING()).toDF("description");
             
            Dataset<Row> metlistinitial =sp.emptyDataFrame();
            if(ionmode.equals("both")){
                Encoder<ExperimentFormOnlyParKey> ExpEncoder = Encoders.bean(ExperimentFormOnlyParKey.class);
                Encoder<ExperimentFormExDesIn> ExpEncoderofthree = Encoders.bean(ExperimentFormExDesIn.class);
                Dataset<ExperimentFormOnlyParKey> dfexplistoriginal = sp.createDataset(experimentlist, Encoders.STRING()).toDF("experimentid").as(ExpEncoder);
                CassandraJavaPairRDD<ExperimentFormOnlyParKey, ExperimentFormExDesIn> predf = CassandraJavaUtil.javaFunctions(CassandraJavaUtil.javaFunctions(dfexplistoriginal.javaRDD()).repartitionByCassandraReplica("mdb","experiment",100,CassandraJavaUtil.someColumns("experimentid"),CassandraJavaUtil.mapToRow(ExperimentFormOnlyParKey.class)))
                                                                                    .joinWithCassandraTable("mdb","experiment", CassandraJavaUtil.someColumns("experimentid","description","intensity"), CassandraJavaUtil.someColumns("experimentid"), CassandraJavaUtil.mapRowTo(ExperimentFormExDesIn.class), CassandraJavaUtil.mapToRow(ExperimentFormOnlyParKey.class));
                metlistinitial =sp.createDataset(predf.map(x->x._2).rdd(), ExpEncoderofthree).toDF( "description","experimentid","intensity");
            }else if(ionmode.equals("positive")){
                Encoder<ExperimentFormOnlyParKey> ExpEncoder = Encoders.bean(ExperimentFormOnlyParKey.class);
                Encoder<ExperimentFormExDesInAd> ExpEncoderofthree = Encoders.bean(ExperimentFormExDesInAd.class);
                Dataset<ExperimentFormOnlyParKey> dfexplistoriginal = sp.createDataset(experimentlist, Encoders.STRING()).toDF("experimentid").as(ExpEncoder); //description, adduct, experimentid
                CassandraJavaPairRDD<ExperimentFormOnlyParKey, ExperimentFormExDesInAd> predf = CassandraJavaUtil.javaFunctions(CassandraJavaUtil.javaFunctions(dfexplistoriginal.javaRDD()).repartitionByCassandraReplica("mdb","experiment",100,CassandraJavaUtil.someColumns("experimentid"),CassandraJavaUtil.mapToRow(ExperimentFormOnlyParKey.class)))
                                                                                    .joinWithCassandraTable("mdb","experiment", CassandraJavaUtil.someColumns("experimentid","description","adduct","intensity"), CassandraJavaUtil.someColumns("experimentid"), CassandraJavaUtil.mapRowTo(ExperimentFormExDesInAd.class), CassandraJavaUtil.mapToRow(ExperimentFormOnlyParKey.class));
                Dataset<Row> metlistinitialbef =sp.createDataset(predf.map(x->x._2).rdd(), ExpEncoderofthree).toDF( "adduct","description","experimentid","intensity");
                Dataset<Row> metlistinitialbe= metlistinitialbef.withColumn("ioniz", functions.substring(col("adduct"), -1, 1)).drop("adduct").filter(col("ioniz").equalTo("+"));
                metlistinitial = metlistinitialbe.drop(col("ioniz"));
            }else if(ionmode.equals("negative")){
                Encoder<ExperimentFormOnlyParKey> ExpEncoder = Encoders.bean(ExperimentFormOnlyParKey.class);
                Encoder<ExperimentFormExDesInAd> ExpEncoderofthree = Encoders.bean(ExperimentFormExDesInAd.class);
                Dataset<ExperimentFormOnlyParKey> dfexplistoriginal = sp.createDataset(experimentlist, Encoders.STRING()).toDF("experimentid").as(ExpEncoder); //description, adduct, experimentid
                CassandraJavaPairRDD<ExperimentFormOnlyParKey, ExperimentFormExDesInAd> predf = CassandraJavaUtil.javaFunctions(CassandraJavaUtil.javaFunctions(dfexplistoriginal.javaRDD()).repartitionByCassandraReplica("mdb","experiment",100,CassandraJavaUtil.someColumns("experimentid"),CassandraJavaUtil.mapToRow(ExperimentFormOnlyParKey.class)))
                                                                                    .joinWithCassandraTable("mdb","experiment", CassandraJavaUtil.someColumns("experimentid","description","adduct","intensity"), CassandraJavaUtil.someColumns("experimentid"), CassandraJavaUtil.mapRowTo(ExperimentFormExDesInAd.class), CassandraJavaUtil.mapToRow(ExperimentFormOnlyParKey.class));
                Dataset<Row> metlistinitialbef =sp.createDataset(predf.map(x->x._2).rdd(), ExpEncoderofthree).toDF( "adduct","description","experimentid","intensity");
                Dataset<Row> metlistinitialbe= metlistinitialbef.withColumn("ioniz", functions.substring(col("adduct"), -1, 1)).drop("adduct").filter(col("ioniz").equalTo("-"));
                metlistinitial = metlistinitialbe.drop(col("ioniz"));
            }
            
            Dataset<Row> newfilenameavgpeaks=sp.emptyDataFrame();
            String[] expincols = experimentlist.parallelStream().toArray(String[]::new);
            if(missval.equals("omit")){
                Dataset<Row> metlistinitial1 = metlistinitial.join(dfmetabolitelist,"description");
                
                Dataset<Row> metfinalpeaks =sp.emptyDataFrame();
                if(normval.equals("normvalmean")|| normval.equals("no")|| normval.equals("normvalmeanmtb")){
                    metfinalpeaks = metlistinitial1.groupBy(metlistinitial1.col("experimentid"),metlistinitial1.col("description")).avg("intensity").persist();
                }else if(normval.equals("normvalorig")){//or       
                //this is z-score on description and experiment
                    Dataset<Row> tmp1 = metlistinitial1.groupBy(col("experimentid"),col("description")).agg(functions.mean("intensity"),functions.stddev("intensity")).withColumnRenamed("experimentid", "expid").withColumnRenamed("description", "descri");
                    Dataset<Row> tmpexpgroups2 = metlistinitial1.join(tmp1,metlistinitial1.col("experimentid").equalTo(tmp1.col("expid")).and(metlistinitial1.col("description").equalTo(tmp1.col("descri")))).withColumn("zscore",((col("intensity").$minus(col("avg(intensity)"))).$div(col("stddev_samp(intensity)")))).na().fill(0);
                    Dataset<Row>  finalexp = tmpexpgroups2.drop("intensity","avg(intensity)","stddev_samp(intensity)","expid","descri").withColumnRenamed("zscore", "intensity");
                    metfinalpeaks = finalexp.groupBy(finalexp.col("experimentid"),finalexp.col("description")).avg("intensity").persist();
                }
                
                Dataset<Row> scaledData = sp.emptyDataFrame();
                if(normval.equals("normvalmean")){
                    Dataset<Row> tmp1 = metfinalpeaks.groupBy(col("experimentid")).agg(functions.mean("avg(intensity)"),functions.stddev("avg(intensity)")).withColumnRenamed("experimentid", "expid");
                    Dataset<Row> tmpexpgroups2 = metfinalpeaks.join(tmp1,metfinalpeaks.col("experimentid").equalTo(tmp1.col("expid"))).withColumn("zscore",((col("avg(intensity)").$minus(col("avg(avg(intensity))"))).$div(col("stddev_samp(avg(intensity))")))).na().fill(0);
                    scaledData = tmpexpgroups2.drop("avg(intensity)","avg(avg(intensity))","stddev_samp(avg(intensity))","expid").withColumnRenamed("zscore", "avg(intensity)");
                    
                }else if(normval.equals("normvalmeanmtb")){
                        Dataset<Row> sthto = metfinalpeaks.groupBy(col("description")).agg(functions.mean("avg(intensity)"),functions.stddev("avg(intensity)")).withColumnRenamed("description", "descri");
                        Dataset<Row>sthbefore = metfinalpeaks.join(sthto,metfinalpeaks.col("description").equalTo(sthto.col("descri"))).withColumn("zscore",((col("avg(intensity)").$minus(col("avg(avg(intensity))"))).$div(col("stddev_samp(avg(intensity))")))).na().fill(0);
                        scaledData=sthbefore.drop("avg(intensity)","avg(avg(intensity))","stddev_samp(avg(intensity))","descri").withColumnRenamed("zscore", "avg(intensity)");
                }else{
                    scaledData=metfinalpeaks;
                }
                
                newfilenameavgpeaks = scaledData.groupBy(scaledData.col("description")).pivot("experimentid",new ArrayList<Object>(experimentlist)).mean("avg(intensity)");
                
                if(clusteringmethod.equals("kmeans")){
                    
                    VectorAssembler assemblerexp1 = new VectorAssembler()
                        .setInputCols(expincols)
                        .setOutputCol("features");
                    Dataset<Row> expoutput = assemblerexp1.transform(newfilenameavgpeaks).persist();
                    
                    long randomSeed = new Random().nextLong();
                    KMeans kmeans = new KMeans().setK(initialval).setInitMode("random").setSeed(randomSeed);
                    KMeansModel model = kmeans.fit(expoutput);

                    // Make predictions
                    Dataset<Row> predictions = model.transform(expoutput);
                    Dataset<Row> clustinfotemp = predictions.select(col("description"),col("prediction")).groupBy(col("prediction")).count().orderBy(asc("prediction"));

                    metaboliteslistclustering.setClustinfo(clustinfotemp.select(col("count")).as(Encoders.LONG()).collectAsList());
                    metaboliteslistclustering.setMtbslist(predictions.select(col("description")).as(Encoders.STRING()).collectAsList());
                    metaboliteslistclustering.setClusts(predictions.select(col("prediction")).as(Encoders.INT()).collectAsList());
                    
                    if(clustinfotemp.count()>1){
                        // Evaluate clustering by computing Silhouette score
                        ClusteringEvaluator evaluator = new ClusteringEvaluator();
                        double silhouette = evaluator.evaluate(predictions);
                        metaboliteslistclustering.setSilhouette(silhouette);
                    }else{
                        rowflag=3;
                    }
                    Vector[] centers = model.clusterCenters();
                    int numrows=centers.length;
                    int numcols=expincols.length;

                    for (int i=0;i<numrows;i++) {
                      double[] temparray1 = centers[i].toArray();
                      Double[] temparray2 = new Double[temparray1.length];
                      for(int j=0;j<numcols;j++){
                          temparray2[j]=temparray1[j];
                      }
                      clustcenters.add(temparray2);
                    }
                    metaboliteslistclustering.setMtbsclustscenters(clustcenters);
                    expoutput.unpersist();
                }else if(clusteringmethod.equals("bikmeans")){
                    
                    VectorAssembler assemblerexp1 = new VectorAssembler()
                        .setInputCols(expincols)
                        .setOutputCol("features");
                    Dataset<Row> expoutput = assemblerexp1.transform(newfilenameavgpeaks).persist();

                    BisectingKMeans bkm = new BisectingKMeans().setK(initialval);
                    BisectingKMeansModel model = bkm.fit(expoutput);

                    // Make predictions
                    Dataset<Row> predictions = model.transform(expoutput);
                    Dataset<Row> clustinfotemp = predictions.select(col("description"),col("prediction")).groupBy(col("prediction")).count().orderBy(asc("prediction"));

                    metaboliteslistclustering.setClustinfo(clustinfotemp.select(col("count")).as(Encoders.LONG()).collectAsList());
                    metaboliteslistclustering.setMtbslist(predictions.select(col("description")).as(Encoders.STRING()).collectAsList());
                    metaboliteslistclustering.setClusts(predictions.select(col("prediction")).as(Encoders.INT()).collectAsList());
                    
                    if(clustinfotemp.count()>1){
                        // Evaluate clustering.
                        ClusteringEvaluator evaluator = new ClusteringEvaluator();
                        double silhouette = evaluator.evaluate(predictions);
                        metaboliteslistclustering.setSilhouette(silhouette);
                    }else{
                        rowflag=3;
                    }
                    Vector[] centers = model.clusterCenters();
                    int numrows=centers.length;
                    int numcols=expincols.length;

                    for (int i=0;i<numrows;i++) {
                      double[] temparray1 = centers[i].toArray();
                      Double[] temparray2 = new Double[temparray1.length];
                      for(int j=0;j<numcols;j++){
                          temparray2[j]=temparray1[j];
                      }
                      clustcenters.add(temparray2);
                    }
                    metaboliteslistclustering.setMtbsclustscenters(clustcenters);
                    expoutput.unpersist();
                }
            }else{
                
                Dataset<Row> metfinalpeaks =sp.emptyDataFrame();
                Dataset<Row> expavgpeaks=sp.emptyDataFrame();
                if(normval.equals("normvalmean")|| normval.equals("no")|| normval.equals("normvalmeanmtb")){
                    metfinalpeaks = metlistinitial.groupBy(metlistinitial.col("experimentid"),metlistinitial.col("description")).avg("intensity").persist();
                    expavgpeaks = metfinalpeaks.groupBy(metfinalpeaks.col("description")).pivot("experimentid",new ArrayList<Object>(experimentlist)).mean("avg(intensity)");
                }else if(normval.equals("normvalorig")){//or       
                //this is z-score on description and experiment
                    Dataset<Row> tmp1 = metlistinitial.groupBy(col("experimentid"),col("description")).agg(functions.mean("intensity"),functions.stddev("intensity")).withColumnRenamed("experimentid", "expid").withColumnRenamed("description", "descri");
                    Dataset<Row> tmpexpgroups2 = metlistinitial.join(tmp1,metlistinitial.col("experimentid").equalTo(tmp1.col("expid")).and(metlistinitial.col("description").equalTo(tmp1.col("descri")))).withColumn("zscore",((col("intensity").$minus(col("avg(intensity)"))).$div(col("stddev_samp(intensity)")))).na().fill(0);
                    Dataset<Row>  finalexp = tmpexpgroups2.drop("intensity","avg(intensity)","stddev_samp(intensity)","expid","descri").withColumnRenamed("zscore", "intensity");
                    metfinalpeaks = finalexp.groupBy(finalexp.col("experimentid"),finalexp.col("description")).avg("intensity").persist();
                    expavgpeaks = metfinalpeaks.groupBy(metfinalpeaks.col("description")).pivot("experimentid",new ArrayList<Object>(experimentlist)).mean("avg(intensity)"); 
                }
                
                //finding missing metabolites
                StructType schema = new StructType(new StructField[] {
                            new StructField("description",DataTypes.StringType, false,Metadata.empty()),
                            new StructField("experimentid",DataTypes.createArrayType(DataTypes.StringType, true), false,Metadata.empty())});
                String[] columnnames=expavgpeaks.columns();
                int mtbswithallnullms=columnnames.length;
                
                Dataset<Row> tempmissvalmtbs=expavgpeaks.map((MapFunction<Row, Row>) originalrow -> {
                    if(originalrow.anyNull() && metabolitelist.contains(originalrow.get(0).toString())){
                        List<String>tmpexpidfilename=new ArrayList<>();
                        for(int i=1;i<mtbswithallnullms;i++){
                            if(originalrow.isNullAt(i)){
                                tmpexpidfilename.add(columnnames[i]);
                            }
                        }
                        String mtbname = originalrow.get(0).toString();
                        Row newrow = RowFactory.create(mtbname,tmpexpidfilename.toArray(new String[0]));
                        return newrow;
                    }else{
                        String mtbname = "notmissing";
                        List<String>tmpexpidfilename=new ArrayList<>();
                        Row newrow = RowFactory.create(mtbname,tmpexpidfilename.toArray(new String[0]));
                        return newrow;
                    }
                }, RowEncoder.apply(schema)).persist(); 
                Dataset<Row> missvalmetabs=tempmissvalmtbs.select("description").filter(col("description").notEqual("notmissing"));
                missvalmtbs=missvalmetabs.as(Encoders.STRING()).collectAsList();
                metaboliteslistclustering.setMissvalmtbs(missvalmtbs);
                
                Dataset<Row> nullmtbs = tempmissvalmtbs.filter(col("description").notEqual("notmissing")).withColumn("experimentid", functions.explode_outer(col("experimentid"))).withColumn("avg(intensity)", functions.lit(null).cast(DataTypes.DoubleType)).select(col("experimentid"),col("description"),col("avg(intensity)"));
                Dataset<Row> tmpsupernewmetfinalpeaksafterrename=metfinalpeaks.union(nullmtbs);
                Dataset<Row> supernewmetfinalpeaksafterrename=tmpsupernewmetfinalpeaksafterrename.join(dfmetabolitelist,"description");
                
                Dataset<Row> finalexpavgpeaks = supernewmetfinalpeaksafterrename.groupBy(supernewmetfinalpeaksafterrename.col("description")).pivot("experimentid",new ArrayList<Object>(experimentlist)).mean("avg(intensity)");
                
                if(missval.equals("replacewithzero")){
                    newfilenameavgpeaks=finalexpavgpeaks.na().fill(0);
                }else if(missval.equals("replacewithmean")){
                    newfilenameavgpeaks=finalexpavgpeaks.map((MapFunction<Row, Row>) originalrow -> {
                        if(originalrow.anyNull() ){
                            int originalrowsizeofdoubles=originalrow.length()-1;
                            Double sumrow = Double.valueOf(0);
                            Double meanrow = Double.valueOf(0);

                            Object[] rowValues = new Object[originalrow.length()];
                            rowValues[0]=originalrow.get(0).toString();

                            List<Integer>indextobereplaced = new ArrayList<>();
                            List<Double> avgintensities = new ArrayList<>();
                            int validvalues=0;
                            for(int k=1;k<=originalrowsizeofdoubles;k++){
                                if(originalrow.isNullAt(k)){
                                    indextobereplaced.add(k);
                                    avgintensities.add(null);
                                }else{
                                    sumrow = sumrow + originalrow.getDouble(k);
                                    avgintensities.add(originalrow.getDouble(k));
                                    validvalues++;
                                }
                            }
                            meanrow=sumrow/(validvalues);

                            int indextobereplacedsize=indextobereplaced.size();
                            for(int i=0;i<indextobereplacedsize;i++){
                                avgintensities.set(indextobereplaced.get(i)-1, meanrow);

                            }
                            for(int l=0;l<avgintensities.size();l++){
                                rowValues[l+1]=avgintensities.get(l);
                            }
                            Row newrow = RowFactory.create(rowValues);

                            return newrow;
                        }else{
                            return originalrow;
                        }
                    }, RowEncoder.apply(finalexpavgpeaks.schema()));
                }else if(missval.equals("replacewithmedian")){
                    newfilenameavgpeaks=finalexpavgpeaks.map((MapFunction<Row, Row>) originalrow -> {
                        if(originalrow.anyNull() ){
                            int originalrowsizeofdoubles=originalrow.length()-1;
                            Double medianrow = Double.valueOf(0);

                            Object[] rowValues = new Object[originalrow.length()];
                            rowValues[0]=originalrow.get(0).toString();

                            List<Integer>indextobereplaced = new ArrayList<>();
                            List<Double> medianintensities = new ArrayList<>();
                            List<Double> rowmedian = new ArrayList<>();
                            int validvalues=0;
                            for(int k=1;k<=originalrowsizeofdoubles;k++){
                                if(originalrow.isNullAt(k)){
                                    indextobereplaced.add(k);
                                    medianintensities.add(null);
                                }else{
                                    medianintensities.add(originalrow.getDouble(k));
                                    rowmedian.add(originalrow.getDouble(k));
                                }
                            }

                            int rowmediannum=rowmedian.size();
                            Double[] sortedmedianintensities= new Double[rowmediannum];
                            for(int i=0;i<rowmediannum;i++){
                                sortedmedianintensities[i]=rowmedian.get(i);
                            }
                            Arrays.sort(sortedmedianintensities);
                            if (rowmediannum ==1){
                                medianrow=sortedmedianintensities[0];
                            }else if(rowmediannum>1){
                                if(rowmediannum % 2 == 0){
                                    int pos1=(rowmediannum-1)/2;
                                    int pos2=pos1+1;
                                    medianrow=(sortedmedianintensities[pos1]+sortedmedianintensities[pos2])/2;
                                }else{
                                    int pos = (rowmediannum/2);
                                    medianrow=sortedmedianintensities[pos];
                                }
                            }

                            int indextobereplacedsize=indextobereplaced.size();
                            for(int i=0;i<indextobereplacedsize;i++){
                                medianintensities.set(indextobereplaced.get(i)-1, medianrow);
                            }
                            for(int l=0;l<medianintensities.size();l++){
                                rowValues[l+1]=medianintensities.get(l);
                            }
                            Row newrow = RowFactory.create(rowValues);

                            return newrow;
                        }else{
                            return originalrow;
                        }
                    }, RowEncoder.apply(finalexpavgpeaks.schema()));
                }
                
                Dataset<Row>normalonmtbmeans =sp.emptyDataFrame();
                if(normval.equals("normvalmeanmtb")){
                    normalonmtbmeans = newfilenameavgpeaks.map((MapFunction<Row, Row>) originalrow -> {
                        Object[] rowValues = new Object[originalrow.length()];
                        rowValues[0]=originalrow.get(0).toString();
                        int originalrowsizeofdoubles=originalrow.length()-1;
                        Double tomean = Double.valueOf(0);
                        Double stdofrow = Double.valueOf(0);

                        for(int k=1;k<=originalrowsizeofdoubles;k++){
                                tomean=tomean+originalrow.getDouble(k);
                        }
                        Double meanofrow = tomean/originalrowsizeofdoubles;

                        Double sqrdis = Double.valueOf(0);
                        for(int k=1;k<=originalrowsizeofdoubles;k++){
                            Double tmpdis=originalrow.getDouble(k)-meanofrow;
                            sqrdis=sqrdis+(tmpdis*tmpdis);
                        }
                        stdofrow=Math.sqrt(sqrdis/originalrowsizeofdoubles);

                        for(int k=1;k<=originalrowsizeofdoubles;k++){
                            rowValues[k]=(originalrow.getDouble(k)-meanofrow)/stdofrow;
                        }
                        Row newrow = RowFactory.create(rowValues);
                        return newrow;
                    }, RowEncoder.apply(newfilenameavgpeaks.schema()));
                }else{
                    normalonmtbmeans=newfilenameavgpeaks;
                }
                
                if(clusteringmethod.equals("kmeans")){
                    
                    VectorAssembler assemblerexp1 = new VectorAssembler()
                        .setInputCols(expincols)
                        .setOutputCol("intensity");
                    Dataset<Row> expoutput = assemblerexp1.transform(normalonmtbmeans);
                    
                    Dataset<Row> scaledData = sp.emptyDataFrame();
                    if(normval.equals("normvalmean")){
                        StandardScaler scaler = new StandardScaler()
                                .setInputCol("intensity")
                                .setOutputCol("features")
                                .setWithStd(true)
                                .setWithMean(true);

                        StandardScalerModel scalerModel = scaler.fit(expoutput);

                        scaledData = scalerModel.transform(expoutput).persist();
                    }else{
                        scaledData=expoutput.withColumnRenamed("intensity", "features").persist();
                    }
                    
                    long randomSeed = new Random().nextLong();
                    KMeans kmeans = new KMeans().setK(initialval).setInitMode("random").setSeed(randomSeed);

                    KMeansModel model = kmeans.fit(scaledData);

                    // Make predictions
                    Dataset<Row> predictions = model.transform(scaledData);
                    Dataset<Row> clustinfotemp = predictions.select(col("description"),col("prediction")).groupBy(col("prediction")).count().orderBy(asc("prediction"));

                    metaboliteslistclustering.setClustinfo(clustinfotemp.select(col("count")).as(Encoders.LONG()).collectAsList());
                    metaboliteslistclustering.setMtbslist(predictions.select(col("description")).as(Encoders.STRING()).collectAsList());
                    metaboliteslistclustering.setClusts(predictions.select(col("prediction")).as(Encoders.INT()).collectAsList());
                    
                    if(clustinfotemp.count()>1){
                        //Evaluate clustering by computing Silhouette score
                        ClusteringEvaluator evaluator = new ClusteringEvaluator();
                        double silhouette = evaluator.evaluate(predictions);
                        metaboliteslistclustering.setSilhouette(silhouette);
                    }else{
                        rowflag=3;
                    }
                    Vector[] centers = model.clusterCenters();
                    int numrows=centers.length;
                    int numcols=expincols.length;

                    for (int i=0;i<numrows;i++) {
                      double[] temparray1 = centers[i].toArray();
                      Double[] temparray2 = new Double[temparray1.length];
                      for(int j=0;j<numcols;j++){
                          temparray2[j]=temparray1[j];
                      }
                      clustcenters.add(temparray2);
                    }
                    metaboliteslistclustering.setMtbsclustscenters(clustcenters);
                    scaledData.unpersist();
                }else if(clusteringmethod.equals("bikmeans")){
                    
                    VectorAssembler assemblerexp1 = new VectorAssembler()
                        .setInputCols(expincols)
                        .setOutputCol("intensity");
                    Dataset<Row> expoutput = assemblerexp1.transform(normalonmtbmeans);
                    
                    Dataset<Row> scaledData = sp.emptyDataFrame();
                    if(normval.equals("normvalmean")){
                        StandardScaler scaler = new StandardScaler()
                                .setInputCol("intensity")
                                .setOutputCol("features")
                                .setWithStd(true)
                                .setWithMean(true);

                        StandardScalerModel scalerModel = scaler.fit(expoutput);

                        scaledData = scalerModel.transform(expoutput).persist();
                    }else{
                        scaledData=expoutput.withColumnRenamed("intensity", "features").persist();
                    }
   
                    BisectingKMeans bkm = new BisectingKMeans().setK(initialval);
                    BisectingKMeansModel model = bkm.fit(scaledData);

                    // Make predictions
                    Dataset<Row> predictions = model.transform(scaledData);
                    Dataset<Row> clustinfotemp = predictions.select(col("description"),col("prediction")).groupBy(col("prediction")).count().orderBy(asc("prediction"));

                    metaboliteslistclustering.setClustinfo(clustinfotemp.select(col("count")).as(Encoders.LONG()).collectAsList());
                    metaboliteslistclustering.setMtbslist(predictions.select(col("description")).as(Encoders.STRING()).collectAsList());
                    metaboliteslistclustering.setClusts(predictions.select(col("prediction")).as(Encoders.INT()).collectAsList());

                    if(clustinfotemp.count()>1){
                        // Evaluate clustering.
                        ClusteringEvaluator evaluator = new ClusteringEvaluator();
                        double silhouette = evaluator.evaluate(predictions);
                        metaboliteslistclustering.setSilhouette(silhouette);
                    }else{
                        rowflag=3;
                    }
                    Vector[] centers = model.clusterCenters();
                    int numrows=centers.length;
                    int numcols=expincols.length;

                    for (int i=0;i<numrows;i++) {
                      double[] temparray1 = centers[i].toArray();
                      Double[] temparray2 = new Double[temparray1.length];
                      for(int j=0;j<numcols;j++){
                          temparray2[j]=temparray1[j];
                      }
                      clustcenters.add(temparray2);
                    }
                    metaboliteslistclustering.setMtbsclustscenters(clustcenters);
                    scaledData.unpersist();
                }
                metfinalpeaks.unpersist();
                tempmissvalmtbs.unpersist();
            }
        }
        if(rowflag==1){
            metaboliteslistclustering.setRowflag(1);
        }else if(rowflag==3){
            metaboliteslistclustering.setRowflag(3);
        }
        return metaboliteslistclustering;
    }
}
