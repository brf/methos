/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.example.app.converters;

import com.example.app.commands.UserProjectForm;
import com.example.app.domain.UserProject;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;

/**
 *
 * @author kostas
 */
@Component
public class UserProjectFormToUserProject implements Converter<UserProjectForm, UserProject> {
    @Override 
    public UserProject convert(UserProjectForm userproform) {
        UserProject userproject = new UserProject();
        if (userproform.getUserProjectKey() != null && !StringUtils.isEmpty(userproform.getUserProjectKey())) {
            //System.out.println("yolo1");
            userproject.setUserProjectKey(userproform.getUserProjectKey());
        }
        userproject.setAccess(userproform.getAccess());
        userproject.setOwner(userproform.getOwner());
        userproject.setKnimeprocessing(userproform.getKnimeprocessing());
        userproject.setSparkanalysis(userproform.getSparkanalysis());
        return userproject;
    }
}
