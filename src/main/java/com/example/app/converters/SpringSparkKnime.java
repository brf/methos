/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.example.app.converters;


import com.datastax.spark.connector.japi.CassandraJavaUtil;
import com.datastax.spark.connector.japi.rdd.CassandraJavaPairRDD;
import com.example.app.domain.ExperimentFormExDes;
import com.example.app.domain.ExperimentFormOnlyParKey;
import com.example.app.domain.Identifiedunidentified;
import com.example.app.storage.StorageProperties;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;

import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;

import java.util.HashMap;
import java.util.List;
import java.util.concurrent.TimeUnit;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.apache.spark.SparkFiles;

import org.apache.spark.api.java.JavaRDD;
import org.apache.spark.api.java.JavaSparkContext;
import org.apache.spark.sql.Dataset;
import org.apache.spark.sql.Encoder;
import org.apache.spark.sql.Encoders;
import org.apache.spark.sql.Row;
import org.apache.spark.sql.SparkSession;
import org.apache.spark.sql.catalyst.encoders.RowEncoder;
import org.apache.spark.sql.functions;
import static org.apache.spark.sql.functions.col;
import org.apache.spark.sql.types.DataTypes;
import org.apache.spark.sql.types.Metadata;
import org.apache.spark.sql.types.StructField;
import org.apache.spark.sql.types.StructType;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;


/**
 *
 * @author kostas
 */
@Component
public class SpringSparkKnime {
    
    private Path rootLocation;
    private Path rootLocationserver;
    
    @Autowired
    private SparkSession sp;
    
    @Autowired
    private JavaSparkContext sc;
    
    

    @Autowired
    public SpringSparkKnime(StorageProperties properties) {
        this.rootLocation = Paths.get(properties.getLocation());
        this.rootLocationserver = Paths.get(properties.getLocationserver());
    }
    
    public String listselectedexp(String projectidtoprocess,List<String> expnamefolder) {
        if (!Files.exists(rootLocation)) {
            rootLocation=rootLocationserver;
        }
        
        File listselected = new File(this.rootLocation.toString()+"/listselected_ProjectList_"+projectidtoprocess+".txt");
        File directory = new File("/mnt/msfiles");

        try {
            FileWriter fw = new FileWriter(listselected.getAbsoluteFile());
            BufferedWriter bw = new BufferedWriter(fw);
            for (String expname : expnamefolder) {
               bw.write(directory+"/"+"ProjectList"+"/"+projectidtoprocess+ "/" +expname + "\n");
            }
            bw.close();
            fw.close();
        } catch (IOException e) {
            e.printStackTrace();
        }

        return this.rootLocation.toString()+"/listselected_ProjectList_"+projectidtoprocess+".txt";
    }        
   
    public void preprocess(String pathoffilewithexpspaths,String projectid, Integer partit,String dbname, String wrk){
            
            try {    
                ProcessBuilder processbuilder = new ProcessBuilder("/home/ubuntu/distributing_listselected.sh","listselected_ProjectList_"+projectid+".txt","ProjectList",projectid);
                Process p1 = processbuilder.start();
                try {
                    p1.waitFor();
                } catch (InterruptedException ex) {
                    Logger.getLogger(SpringSparkKnime.class.getName()).log(Level.SEVERE, null, ex);
                }
            } catch (IOException ex) {
                Logger.getLogger(SpringSparkKnime.class.getName()).log(Level.SEVERE, null, ex);
            }        
        
        JavaRDD <String> rddfilepaths = sc.textFile(pathoffilewithexpspaths,partit); 
        if(dbname.equals("MoNA: Fiehn")){
            if(wrk.equals("Lenient")){
                System.out.println(rddfilepaths.pipe("/home/ubuntu/download_process_delete_lenient_fiehn.sh").collect());
            }else if(wrk.equals("SmallMass")){
                System.out.println(rddfilepaths.pipe("/home/ubuntu/download_process_delete_smallmass_fiehn.sh").collect());
            }else if(wrk.equals("Default")){
                System.out.println(rddfilepaths.pipe("/home/ubuntu/download_process_delete_default_fiehn.sh").collect());
            }else if(wrk.equals("MediumMass")){
                System.out.println(rddfilepaths.pipe("/home/ubuntu/download_process_delete_mediummass_fiehn.sh").collect());
            }else if(wrk.equals("Strict")){
                System.out.println(rddfilepaths.pipe("/home/ubuntu/download_process_delete_strict_fiehn.sh").collect());
            }
        }else if(dbname.equals("MassBank")){
            if(wrk.equals("Lenient")){
                System.out.println(rddfilepaths.pipe("/home/ubuntu/download_process_delete_lenient_massbank.sh").collect());
            }else if(wrk.equals("SmallMass")){
                System.out.println(rddfilepaths.pipe("/home/ubuntu/download_process_delete_smallmass_massbank.sh").collect());
            }else if(wrk.equals("Default")){
                System.out.println(rddfilepaths.pipe("/home/ubuntu/download_process_delete_default_massbank.sh").collect());
            }else if(wrk.equals("MediumMass")){
                System.out.println(rddfilepaths.pipe("/home/ubuntu/download_process_delete_mediummass_massbank.sh").collect());
            }else if(wrk.equals("Strict")){
                System.out.println(rddfilepaths.pipe("/home/ubuntu/download_process_delete_strict_massbank.sh").collect());
            }else if(wrk.equals("MS2 spectral matching")){
                System.out.println(rddfilepaths.pipe("/home/ubuntu/download_process_delete_default_ms2_massbank.sh").collect());
            }
        }else if(dbname.equals("HMDB")){
            if(wrk.equals("Lenient")){
                System.out.println(rddfilepaths.pipe("/home/ubuntu/download_process_delete_lenient_hmdb.sh").collect());
            }else if(wrk.equals("SmallMass")){
                System.out.println(rddfilepaths.pipe("/home/ubuntu/download_process_delete_smallmass_hmdb.sh").collect());
            }else if(wrk.equals("Default")){
                System.out.println(rddfilepaths.pipe("/home/ubuntu/download_process_delete_default_hmdb.sh").collect());
            }else if(wrk.equals("MediumMass")){
                System.out.println(rddfilepaths.pipe("/home/ubuntu/download_process_delete_mediummass_hmdb.sh").collect());
            }else if(wrk.equals("Strict")){
                System.out.println(rddfilepaths.pipe("/home/ubuntu/download_process_delete_strict_hmdb.sh").collect());
            }
        }
        
        try {    
            ProcessBuilder processbuilder = new ProcessBuilder("/home/ubuntu/deleting_listselected.sh","listselected_ProjectList_"+projectid+".txt","ProjectList",projectid);

            Process p2 = processbuilder.start();
            try {
                p2.waitFor();
            } catch (InterruptedException ex) {
                Logger.getLogger(SpringSparkKnime.class.getName()).log(Level.SEVERE, null, ex);
            }
        } catch (IOException ex) {
            Logger.getLogger(SpringSparkKnime.class.getName()).log(Level.SEVERE, null, ex);
        }
        
    }
    
    public List<Identifiedunidentified> countIdentifiedUnidentified(List<String> experimentids,String wrk){
        Dataset<Row> dfexplist = sp.createDataset(experimentids, Encoders.STRING()).toDF("experimentid");        
        Encoder<ExperimentFormOnlyParKey> OnlyparkeyEncoder = Encoders.bean(ExperimentFormOnlyParKey.class);
        Encoder<ExperimentFormExDes> ExpEncoderoftwo = Encoders.bean(ExperimentFormExDes.class);
        Dataset<ExperimentFormOnlyParKey> dfexplistoriginal = sp.createDataset(experimentids, Encoders.STRING()).toDF("experimentid").as(OnlyparkeyEncoder);

        CassandraJavaPairRDD<ExperimentFormOnlyParKey, ExperimentFormExDes> predf = CassandraJavaUtil.javaFunctions(CassandraJavaUtil.javaFunctions(dfexplistoriginal.javaRDD()).repartitionByCassandraReplica("mdb","experiment",100,CassandraJavaUtil.someColumns("experimentid"),CassandraJavaUtil.mapToRow(ExperimentFormOnlyParKey.class)))
                                                                    .joinWithCassandraTable("mdb","experiment", CassandraJavaUtil.someColumns("experimentid","description"), CassandraJavaUtil.someColumns("experimentid"), CassandraJavaUtil.mapRowTo(ExperimentFormExDes.class), CassandraJavaUtil.mapToRow(ExperimentFormOnlyParKey.class));
        Dataset<Row> peakset =sp.createDataset(predf.map(x->x._2).rdd(), ExpEncoderoftwo).toDF( "description","experimentid").persist();
        
        Dataset<Row> unidentified =sp.emptyDataFrame();
        if(wrk.equals("MS2 spectral matching")){
            unidentified=dfexplist.withColumn("unidentified", functions.lit(0).cast(DataTypes.LongType));
        }else{
            Dataset<Row> allunidentified = peakset.filter(col("description").equalTo("Unidentified"));
            unidentified = allunidentified.groupBy(col("experimentid"),col("description")).count().withColumnRenamed("count", "unidentified").drop(col("description"));
        }
        Dataset<Row> allidentified = peakset.filter(col("description").notEqual("Unidentified")).groupBy(col("experimentid"),col("description")).count();
        Dataset<Row> lastidentified = allidentified.groupBy(col("experimentid")).count().withColumnRenamed("count", "identified");
        
        Encoder<Identifiedunidentified> ExpEncoder = Encoders.bean(Identifiedunidentified.class);
        Dataset<Identifiedunidentified> alltogether = unidentified.join(lastidentified, "experimentid").as(ExpEncoder);
        List<Identifiedunidentified> unin = alltogether.collectAsList();
        peakset.unpersist();
        return unin;

    }
    
}
