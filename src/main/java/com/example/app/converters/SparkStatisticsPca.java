/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.example.app.converters;

import com.datastax.driver.core.Cluster;
import com.datastax.driver.core.Session;
import com.example.app.commands.Experimentslistpca;
import com.example.app.commands.Metaboliteslistpca;
import com.example.app.commands.Replicateslistpca;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;
import org.apache.commons.lang3.ArrayUtils;
import org.apache.spark.api.java.function.MapFunction;
import org.apache.spark.ml.feature.CountVectorizer;
import org.apache.spark.ml.feature.CountVectorizerModel;
import org.apache.spark.ml.feature.PCA;
import org.apache.spark.ml.feature.PCAModel;
import org.apache.spark.ml.feature.StandardScaler;
import org.apache.spark.ml.feature.StandardScalerModel;
import org.apache.spark.ml.feature.VectorAssembler;
import org.apache.spark.ml.linalg.VectorUDT;
import org.apache.spark.ml.linalg.Vectors;
import org.apache.spark.sql.Dataset;
import org.apache.spark.sql.Encoders;
import org.apache.spark.sql.Row;
import org.apache.spark.sql.RowFactory;
import org.apache.spark.sql.SparkSession;
import org.apache.spark.sql.catalyst.encoders.RowEncoder;
import org.apache.spark.sql.functions;
import static org.apache.spark.sql.functions.asc;
import static org.apache.spark.sql.functions.col;
import static org.apache.spark.sql.functions.lit;
import org.apache.spark.sql.types.DataTypes;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import static org.apache.spark.sql.functions.*;
import org.apache.spark.sql.types.Metadata;
import org.apache.spark.sql.types.StructField;
import org.apache.spark.sql.types.StructType;
import scala.collection.JavaConversions;
import com.datastax.spark.connector.japi.CassandraJavaUtil;
import com.datastax.spark.connector.japi.rdd.CassandraJavaPairRDD;
import com.example.app.commands.Findmissvalforgroupexps;
import com.example.app.domain.ExperimentFormExDes;
import com.example.app.domain.ExperimentFormExDesAd;
import com.example.app.domain.ExperimentFormExDesFl;
import com.example.app.domain.ExperimentFormExDesFlAd;
import com.example.app.domain.ExperimentFormExDesFlIn;
import com.example.app.domain.ExperimentFormExDesFlInAd;
import com.example.app.domain.ExperimentFormOnlyParKey;
import com.example.app.domain.ExperimentFormExDesIn;
import com.example.app.domain.ExperimentFormExDesInAd;

import org.apache.spark.api.java.JavaSparkContext;
import org.apache.spark.sql.Encoder;
import org.apache.spark.util.SizeEstimator;

/**
 *
 * @author kostas
 */
@Component
public class SparkStatisticsPca{
    
    @Autowired
    private SparkSession sp;
    
    @Autowired
    private JavaSparkContext sc;
   
    
    public List<String> omitInExperiments(List<String> expids,String ionmode){
        
        List<String> metlist = new ArrayList<>();
        Dataset<Row> metlistinitial =sp.emptyDataFrame();
        if(ionmode.equals("both")){
            Encoder<ExperimentFormOnlyParKey> ExpEncoder = Encoders.bean(ExperimentFormOnlyParKey.class);
            Encoder<ExperimentFormExDes> ExpEncoderoftwo = Encoders.bean(ExperimentFormExDes.class);
            Dataset<ExperimentFormOnlyParKey> dfexplistoriginal = sp.createDataset(expids, Encoders.STRING()).toDF("experimentid").as(ExpEncoder);
            CassandraJavaPairRDD<ExperimentFormOnlyParKey, ExperimentFormExDes> predf = CassandraJavaUtil.javaFunctions(CassandraJavaUtil.javaFunctions(dfexplistoriginal.javaRDD()).repartitionByCassandraReplica("mdb","experiment",100,CassandraJavaUtil.someColumns("experimentid"),CassandraJavaUtil.mapToRow(ExperimentFormOnlyParKey.class)))
                                                                                .joinWithCassandraTable("mdb","experiment", CassandraJavaUtil.someColumns("experimentid","description"), CassandraJavaUtil.someColumns("experimentid"), CassandraJavaUtil.mapRowTo(ExperimentFormExDes.class), CassandraJavaUtil.mapToRow(ExperimentFormOnlyParKey.class));
            metlistinitial =sp.createDataset(predf.map(x->x._2).rdd(), ExpEncoderoftwo).toDF( "description","experimentid").groupBy(col("experimentid"),col("description")).count();
        }else if(ionmode.equals("positive")){
            Encoder<ExperimentFormOnlyParKey> ExpEncoder = Encoders.bean(ExperimentFormOnlyParKey.class);
            Encoder<ExperimentFormExDesAd> ExpEncoderofthree = Encoders.bean(ExperimentFormExDesAd.class);
            Dataset<ExperimentFormOnlyParKey> dfexplistoriginal = sp.createDataset(expids, Encoders.STRING()).toDF("experimentid").as(ExpEncoder);
            CassandraJavaPairRDD<ExperimentFormOnlyParKey, ExperimentFormExDesAd> predf = CassandraJavaUtil.javaFunctions(CassandraJavaUtil.javaFunctions(dfexplistoriginal.javaRDD()).repartitionByCassandraReplica("mdb","experiment",100,CassandraJavaUtil.someColumns("experimentid"),CassandraJavaUtil.mapToRow(ExperimentFormOnlyParKey.class)))
                                                                                .joinWithCassandraTable("mdb","experiment", CassandraJavaUtil.someColumns("experimentid","description","adduct"), CassandraJavaUtil.someColumns("experimentid"), CassandraJavaUtil.mapRowTo(ExperimentFormExDesAd.class), CassandraJavaUtil.mapToRow(ExperimentFormOnlyParKey.class));
            Dataset<Row> metlistinitialbef =sp.createDataset(predf.map(x->x._2).rdd(), ExpEncoderofthree).toDF( "adduct","description","experimentid");
            Dataset<Row> metlistinitialbe= metlistinitialbef.withColumn("ioniz", functions.substring(col("adduct"), -1, 1)).drop("adduct").filter(col("ioniz").equalTo("+"));
            metlistinitial = metlistinitialbe.drop(col("ioniz")).groupBy(col("experimentid"),col("description")).count();
        }
        else if(ionmode.equals("negative")){
            Encoder<ExperimentFormOnlyParKey> ExpEncoder = Encoders.bean(ExperimentFormOnlyParKey.class);
            Encoder<ExperimentFormExDesAd> ExpEncoderofthree = Encoders.bean(ExperimentFormExDesAd.class);
            Dataset<ExperimentFormOnlyParKey> dfexplistoriginal = sp.createDataset(expids, Encoders.STRING()).toDF("experimentid").as(ExpEncoder); 
            CassandraJavaPairRDD<ExperimentFormOnlyParKey, ExperimentFormExDesAd> predf = CassandraJavaUtil.javaFunctions(CassandraJavaUtil.javaFunctions(dfexplistoriginal.javaRDD()).repartitionByCassandraReplica("mdb","experiment",100,CassandraJavaUtil.someColumns("experimentid"),CassandraJavaUtil.mapToRow(ExperimentFormOnlyParKey.class)))
                                                                                .joinWithCassandraTable("mdb","experiment", CassandraJavaUtil.someColumns("experimentid","description","adduct"), CassandraJavaUtil.someColumns("experimentid"), CassandraJavaUtil.mapRowTo(ExperimentFormExDesAd.class), CassandraJavaUtil.mapToRow(ExperimentFormOnlyParKey.class));
            Dataset<Row> metlistinitialbef =sp.createDataset(predf.map(x->x._2).rdd(), ExpEncoderofthree).toDF( "adduct","description","experimentid");
            Dataset<Row> metlistinitialbe= metlistinitialbef.withColumn("ioniz", functions.substring(col("adduct"), -1, 1)).drop("adduct").filter(col("ioniz").equalTo("-"));
            metlistinitial = metlistinitialbe.drop(col("ioniz")).groupBy(col("experimentid"),col("description")).count();
        }
        Dataset<Row> metnamesandocur=metlistinitial.select(col("description")).groupBy(col("description")).count().orderBy( asc("description"));
        int expnum=expids.size();
        Dataset<Row> metnamesandocurinall =metnamesandocur.select(col("description"),col("count")).filter(col("description").notEqual("Unidentified")).filter(col("count").equalTo(expnum));
        metlist = metnamesandocurinall.select(col("description")).as(Encoders.STRING()).collectAsList();
        return metlist;
    }
    
    public List<String> replaceWithValueInExperiments (List<String> expids,String ionmode){
        List<String> metlist = new ArrayList<>();
        Dataset<Row> metlistinitial =sp.emptyDataFrame();
        if(ionmode.equals("both")){
            Encoder<ExperimentFormOnlyParKey> ExpEncoder = Encoders.bean(ExperimentFormOnlyParKey.class);
            Encoder<ExperimentFormExDes> ExpEncoderoftwo = Encoders.bean(ExperimentFormExDes.class);
            Dataset<ExperimentFormOnlyParKey> dfexplistoriginal = sp.createDataset(expids, Encoders.STRING()).toDF("experimentid").as(ExpEncoder);
            CassandraJavaPairRDD<ExperimentFormOnlyParKey, ExperimentFormExDes> predf = CassandraJavaUtil.javaFunctions(CassandraJavaUtil.javaFunctions(dfexplistoriginal.javaRDD()).repartitionByCassandraReplica("mdb","experiment",100,CassandraJavaUtil.someColumns("experimentid"),CassandraJavaUtil.mapToRow(ExperimentFormOnlyParKey.class)))
                                                                                .joinWithCassandraTable("mdb","experiment", CassandraJavaUtil.someColumns("experimentid","description"), CassandraJavaUtil.someColumns("experimentid"), CassandraJavaUtil.mapRowTo(ExperimentFormExDes.class), CassandraJavaUtil.mapToRow(ExperimentFormOnlyParKey.class));
            metlistinitial =sp.createDataset(predf.map(x->x._2).rdd(), ExpEncoderoftwo).toDF( "description","experimentid").filter(col("description").notEqual("Unidentified"));
        }else if(ionmode.equals("positive")){
            Encoder<ExperimentFormOnlyParKey> ExpEncoder = Encoders.bean(ExperimentFormOnlyParKey.class);
            Encoder<ExperimentFormExDesAd> ExpEncoderofthree = Encoders.bean(ExperimentFormExDesAd.class);
            Dataset<ExperimentFormOnlyParKey> dfexplistoriginal = sp.createDataset(expids, Encoders.STRING()).toDF("experimentid").as(ExpEncoder); 
            CassandraJavaPairRDD<ExperimentFormOnlyParKey, ExperimentFormExDesAd> predf = CassandraJavaUtil.javaFunctions(CassandraJavaUtil.javaFunctions(dfexplistoriginal.javaRDD()).repartitionByCassandraReplica("mdb","experiment",100,CassandraJavaUtil.someColumns("experimentid"),CassandraJavaUtil.mapToRow(ExperimentFormOnlyParKey.class)))
                                                                                .joinWithCassandraTable("mdb","experiment", CassandraJavaUtil.someColumns("experimentid","description","adduct"), CassandraJavaUtil.someColumns("experimentid"), CassandraJavaUtil.mapRowTo(ExperimentFormExDesAd.class), CassandraJavaUtil.mapToRow(ExperimentFormOnlyParKey.class));
            Dataset<Row> metlistinitialbef =sp.createDataset(predf.map(x->x._2).rdd(), ExpEncoderofthree).toDF( "adduct","description","experimentid").filter(col("description").notEqual("Unidentified"));
            Dataset<Row> metlistinitialbe= metlistinitialbef.withColumn("ioniz", functions.substring(col("adduct"), -1, 1)).drop("adduct").filter(col("ioniz").equalTo("+"));
            metlistinitial = metlistinitialbe.drop(col("ioniz"));
        }
        else if(ionmode.equals("negative")){
            Encoder<ExperimentFormOnlyParKey> ExpEncoder = Encoders.bean(ExperimentFormOnlyParKey.class);
            Encoder<ExperimentFormExDesAd> ExpEncoderofthree = Encoders.bean(ExperimentFormExDesAd.class);
            Dataset<ExperimentFormOnlyParKey> dfexplistoriginal = sp.createDataset(expids, Encoders.STRING()).toDF("experimentid").as(ExpEncoder);
            CassandraJavaPairRDD<ExperimentFormOnlyParKey, ExperimentFormExDesAd> predf = CassandraJavaUtil.javaFunctions(CassandraJavaUtil.javaFunctions(dfexplistoriginal.javaRDD()).repartitionByCassandraReplica("mdb","experiment",100,CassandraJavaUtil.someColumns("experimentid"),CassandraJavaUtil.mapToRow(ExperimentFormOnlyParKey.class)))
                                                                                .joinWithCassandraTable("mdb","experiment", CassandraJavaUtil.someColumns("experimentid","description","adduct"), CassandraJavaUtil.someColumns("experimentid"), CassandraJavaUtil.mapRowTo(ExperimentFormExDesAd.class), CassandraJavaUtil.mapToRow(ExperimentFormOnlyParKey.class));
            Dataset<Row> metlistinitialbef =sp.createDataset(predf.map(x->x._2).rdd(), ExpEncoderofthree).toDF( "adduct","description","experimentid").filter(col("description").notEqual("Unidentified"));
            Dataset<Row> metlistinitialbe= metlistinitialbef.withColumn("ioniz", functions.substring(col("adduct"), -1, 1)).drop("adduct").filter(col("ioniz").equalTo("-"));
            metlistinitial = metlistinitialbe.drop(col("ioniz"));
        }
        Dataset<Row> mtbdistinct =metlistinitial.select(col("description")).distinct();
        metlist = mtbdistinct.as(Encoders.STRING()).collectAsList();
        return metlist;
    }
    
    public List<String> omitInReplicates(List<String> expids,String ionmode){
        List<String> metlist = new ArrayList<>();
        Dataset<Row> dfexplist = sp.createDataset(expids, Encoders.STRING()).toDF("experimentid");
        Dataset<Row> expsidsandreplnum = sp.read().format("org.apache.spark.sql.cassandra")
                .options(new HashMap<String, String>() {
                    {
                        put("keyspace", "mdb");
                        put("table", "projectexperiment");
                    }
                })
                .load().select(col("experimentid"),col("replicatesnum")).join(dfexplist,"experimentid");

        List<Long> totalreplnumlist=expsidsandreplnum.select(col("replicatesnum")).agg(sum(col("replicatesnum")).cast(DataTypes.LongType)).as(Encoders.LONG()).collectAsList();
        Long totalreplnum = totalreplnumlist.get(0);
        
        Dataset<Row> metlistinitial =sp.emptyDataFrame();
        if(ionmode.equals("both")){
            Encoder<ExperimentFormOnlyParKey> ExpEncoder = Encoders.bean(ExperimentFormOnlyParKey.class);
            Encoder<ExperimentFormExDesFl> ExpEncoderofthree = Encoders.bean(ExperimentFormExDesFl.class);
            Dataset<ExperimentFormOnlyParKey> dfexplistoriginal = sp.createDataset(expids, Encoders.STRING()).toDF("experimentid").as(ExpEncoder);
            CassandraJavaPairRDD<ExperimentFormOnlyParKey, ExperimentFormExDesFl> predf2 = CassandraJavaUtil.javaFunctions(CassandraJavaUtil.javaFunctions(dfexplistoriginal.javaRDD()).repartitionByCassandraReplica("mdb","experiment",100,CassandraJavaUtil.someColumns("experimentid"),CassandraJavaUtil.mapToRow(ExperimentFormOnlyParKey.class)))
                                                                                .joinWithCassandraTable("mdb","experiment", CassandraJavaUtil.someColumns("experimentid","description","filename"), CassandraJavaUtil.someColumns("experimentid"), CassandraJavaUtil.mapRowTo(ExperimentFormExDesFl.class), CassandraJavaUtil.mapToRow(ExperimentFormOnlyParKey.class));
            metlistinitial =sp.createDataset(predf2.map(x->x._2).rdd(), ExpEncoderofthree).toDF( "description","experimentid","filename").groupBy(col("experimentid"),col("filename"),col("description")).count();

        }else if(ionmode.equals("positive")){
            Encoder<ExperimentFormOnlyParKey> ExpEncoder = Encoders.bean(ExperimentFormOnlyParKey.class);
            Encoder<ExperimentFormExDesFlAd> ExpEncoderofthree = Encoders.bean(ExperimentFormExDesFlAd.class);
            Dataset<ExperimentFormOnlyParKey> dfexplistoriginal = sp.createDataset(expids, Encoders.STRING()).toDF("experimentid").as(ExpEncoder);
            CassandraJavaPairRDD<ExperimentFormOnlyParKey, ExperimentFormExDesFlAd> predf = CassandraJavaUtil.javaFunctions(CassandraJavaUtil.javaFunctions(dfexplistoriginal.javaRDD()).repartitionByCassandraReplica("mdb","experiment",100,CassandraJavaUtil.someColumns("experimentid"),CassandraJavaUtil.mapToRow(ExperimentFormOnlyParKey.class)))
                                                                                .joinWithCassandraTable("mdb","experiment", CassandraJavaUtil.someColumns("experimentid","filename","description","adduct"), CassandraJavaUtil.someColumns("experimentid"), CassandraJavaUtil.mapRowTo(ExperimentFormExDesFlAd.class), CassandraJavaUtil.mapToRow(ExperimentFormOnlyParKey.class));
            Dataset<Row> metlistinitialbef =sp.createDataset(predf.map(x->x._2).rdd(), ExpEncoderofthree).toDF( "adduct","description","experimentid","filename").filter(col("description").notEqual("Unidentified"));
            Dataset<Row> metlistinitialbe= metlistinitialbef.withColumn("ioniz", functions.substring(col("adduct"), -1, 1)).drop("adduct").filter(col("ioniz").equalTo("+"));
            metlistinitial = metlistinitialbe.drop(col("ioniz")).groupBy(col("experimentid"),col("filename"),col("description")).count();
        }else if(ionmode.equals("negative")){
            Encoder<ExperimentFormOnlyParKey> ExpEncoder = Encoders.bean(ExperimentFormOnlyParKey.class);
            Encoder<ExperimentFormExDesFlAd> ExpEncoderofthree = Encoders.bean(ExperimentFormExDesFlAd.class);
            Dataset<ExperimentFormOnlyParKey> dfexplistoriginal = sp.createDataset(expids, Encoders.STRING()).toDF("experimentid").as(ExpEncoder); 
            CassandraJavaPairRDD<ExperimentFormOnlyParKey, ExperimentFormExDesFlAd> predf = CassandraJavaUtil.javaFunctions(CassandraJavaUtil.javaFunctions(dfexplistoriginal.javaRDD()).repartitionByCassandraReplica("mdb","experiment",100,CassandraJavaUtil.someColumns("experimentid"),CassandraJavaUtil.mapToRow(ExperimentFormOnlyParKey.class)))
                                                                                .joinWithCassandraTable("mdb","experiment", CassandraJavaUtil.someColumns("experimentid","filename","description","adduct"), CassandraJavaUtil.someColumns("experimentid"), CassandraJavaUtil.mapRowTo(ExperimentFormExDesFlAd.class), CassandraJavaUtil.mapToRow(ExperimentFormOnlyParKey.class));
            Dataset<Row> metlistinitialbef =sp.createDataset(predf.map(x->x._2).rdd(), ExpEncoderofthree).toDF( "adduct","description","experimentid","filename").filter(col("description").notEqual("Unidentified"));
            Dataset<Row> metlistinitialbe= metlistinitialbef.withColumn("ioniz", functions.substring(col("adduct"), -1, 1)).drop("adduct").filter(col("ioniz").equalTo("-"));
            metlistinitial = metlistinitialbe.drop(col("ioniz")).groupBy(col("experimentid"),col("filename"),col("description")).count();
        }
        
        
        Dataset<Row> metnamesandocur=metlistinitial.select(col("description")).groupBy(col("description")).count().orderBy( asc("description"));
        Dataset<Row> metnamesandocurinall =metnamesandocur.select(col("description"),col("count")).filter(col("description").notEqual("Unidentified")).filter(col("count").equalTo(totalreplnum));
        metlist = metnamesandocurinall.select(col("description")).as(Encoders.STRING()).collectAsList();
        
        return metlist;
    }
    
    
    public List<String> replaceWithValueInReplicates(List<String> expids,String ionmode){
        List<String> metlist = new ArrayList<>();
        
        Dataset<Row> metlistinitial =sp.emptyDataFrame();
        if(ionmode.equals("both")){
                Encoder<ExperimentFormOnlyParKey> ExpEncoder = Encoders.bean(ExperimentFormOnlyParKey.class);
                Encoder<ExperimentFormExDesFl> ExpEncoderofthree = Encoders.bean(ExperimentFormExDesFl.class);
                Dataset<ExperimentFormOnlyParKey> dfexplistoriginal = sp.createDataset(expids, Encoders.STRING()).toDF("experimentid").as(ExpEncoder);
                CassandraJavaPairRDD<ExperimentFormOnlyParKey, ExperimentFormExDesFl> predf2 = CassandraJavaUtil.javaFunctions(CassandraJavaUtil.javaFunctions(dfexplistoriginal.javaRDD()).repartitionByCassandraReplica("mdb","experiment",100,CassandraJavaUtil.someColumns("experimentid"),CassandraJavaUtil.mapToRow(ExperimentFormOnlyParKey.class)))
                                                                                    .joinWithCassandraTable("mdb","experiment", CassandraJavaUtil.someColumns("experimentid","description","filename"), CassandraJavaUtil.someColumns("experimentid"), CassandraJavaUtil.mapRowTo(ExperimentFormExDesFl.class), CassandraJavaUtil.mapToRow(ExperimentFormOnlyParKey.class));
                metlistinitial =sp.createDataset(predf2.map(x->x._2).rdd(), ExpEncoderofthree).toDF( "description","experimentid","filename").filter(col("description").notEqual("Unidentified"));
        
            }else if(ionmode.equals("positive")){
                Encoder<ExperimentFormOnlyParKey> ExpEncoder = Encoders.bean(ExperimentFormOnlyParKey.class);
                Encoder<ExperimentFormExDesFlAd> ExpEncoderofthree = Encoders.bean(ExperimentFormExDesFlAd.class);
                Dataset<ExperimentFormOnlyParKey> dfexplistoriginal = sp.createDataset(expids, Encoders.STRING()).toDF("experimentid").as(ExpEncoder);
                CassandraJavaPairRDD<ExperimentFormOnlyParKey, ExperimentFormExDesFlAd> predf = CassandraJavaUtil.javaFunctions(CassandraJavaUtil.javaFunctions(dfexplistoriginal.javaRDD()).repartitionByCassandraReplica("mdb","experiment",100,CassandraJavaUtil.someColumns("experimentid"),CassandraJavaUtil.mapToRow(ExperimentFormOnlyParKey.class)))
                                                                                    .joinWithCassandraTable("mdb","experiment", CassandraJavaUtil.someColumns("experimentid","filename","description","adduct"), CassandraJavaUtil.someColumns("experimentid"), CassandraJavaUtil.mapRowTo(ExperimentFormExDesFlAd.class), CassandraJavaUtil.mapToRow(ExperimentFormOnlyParKey.class));
                Dataset<Row> metlistinitialbef =sp.createDataset(predf.map(x->x._2).rdd(), ExpEncoderofthree).toDF( "adduct","description","experimentid","filename").filter(col("description").notEqual("Unidentified"));
                Dataset<Row> metlistinitialbe= metlistinitialbef.withColumn("ioniz", functions.substring(col("adduct"), -1, 1)).drop("adduct").filter(col("ioniz").equalTo("+"));
                metlistinitial = metlistinitialbe.drop(col("ioniz"));
            }else if(ionmode.equals("negative")){
                Encoder<ExperimentFormOnlyParKey> ExpEncoder = Encoders.bean(ExperimentFormOnlyParKey.class);
                Encoder<ExperimentFormExDesFlAd> ExpEncoderofthree = Encoders.bean(ExperimentFormExDesFlAd.class);
                Dataset<ExperimentFormOnlyParKey> dfexplistoriginal = sp.createDataset(expids, Encoders.STRING()).toDF("experimentid").as(ExpEncoder);
                CassandraJavaPairRDD<ExperimentFormOnlyParKey, ExperimentFormExDesFlAd> predf = CassandraJavaUtil.javaFunctions(CassandraJavaUtil.javaFunctions(dfexplistoriginal.javaRDD()).repartitionByCassandraReplica("mdb","experiment",100,CassandraJavaUtil.someColumns("experimentid"),CassandraJavaUtil.mapToRow(ExperimentFormOnlyParKey.class)))
                                                                                    .joinWithCassandraTable("mdb","experiment", CassandraJavaUtil.someColumns("experimentid","filename","description","adduct"), CassandraJavaUtil.someColumns("experimentid"), CassandraJavaUtil.mapRowTo(ExperimentFormExDesFlAd.class), CassandraJavaUtil.mapToRow(ExperimentFormOnlyParKey.class));
                Dataset<Row> metlistinitialbef =sp.createDataset(predf.map(x->x._2).rdd(), ExpEncoderofthree).toDF( "adduct","description","experimentid","filename").filter(col("description").notEqual("Unidentified"));
                Dataset<Row> metlistinitialbe= metlistinitialbef.withColumn("ioniz", functions.substring(col("adduct"), -1, 1)).drop("adduct").filter(col("ioniz").equalTo("-"));
                metlistinitial = metlistinitialbe.drop(col("ioniz"));
            }
        Dataset<Row> mtbdistinct =metlistinitial.select(col("description")).distinct();
        metlist = mtbdistinct.as(Encoders.STRING()).collectAsList();

        return metlist;
    }
    
    public List<String> findtopmtbs(List<String> expids,List<String> mtbs,Integer nrmtbs,String ionmode){
        List<String> metlist = new ArrayList<>();
        Dataset<Row> dfmtblist = sp.createDataset(mtbs, Encoders.STRING()).toDF("description");
        Dataset<Row> metlistinitial2 = sp.emptyDataFrame();
        if(ionmode.equals("both")){
            Encoder<ExperimentFormOnlyParKey> ExpEncoder = Encoders.bean(ExperimentFormOnlyParKey.class);
            Encoder<ExperimentFormExDesIn> ExpEncoderofthree = Encoders.bean(ExperimentFormExDesIn.class);
            Dataset<ExperimentFormOnlyParKey> dfexplistoriginal = sp.createDataset(expids, Encoders.STRING()).toDF("experimentid").as(ExpEncoder);
            CassandraJavaPairRDD<ExperimentFormOnlyParKey, ExperimentFormExDesIn> predf = CassandraJavaUtil.javaFunctions(CassandraJavaUtil.javaFunctions(dfexplistoriginal.javaRDD()).repartitionByCassandraReplica("mdb","experiment",100,CassandraJavaUtil.someColumns("experimentid"),CassandraJavaUtil.mapToRow(ExperimentFormOnlyParKey.class)))
                                                                        .joinWithCassandraTable("mdb","experiment", CassandraJavaUtil.someColumns("experimentid","description","intensity"), CassandraJavaUtil.someColumns("experimentid"), CassandraJavaUtil.mapRowTo(ExperimentFormExDesIn.class), CassandraJavaUtil.mapToRow(ExperimentFormOnlyParKey.class));
            metlistinitial2 =sp.createDataset(predf.map(x->x._2).rdd(), ExpEncoderofthree).toDF( "description","experimentid","intensity").filter(col("description").notEqual("Unidentified")).join(dfmtblist,"description");
        }else if(ionmode.equals("positive")){
            Encoder<ExperimentFormOnlyParKey> ExpEncoder = Encoders.bean(ExperimentFormOnlyParKey.class);
            Encoder<ExperimentFormExDesInAd> ExpEncoderofthree = Encoders.bean(ExperimentFormExDesInAd.class);
            Dataset<ExperimentFormOnlyParKey> dfexplistoriginal = sp.createDataset(expids, Encoders.STRING()).toDF("experimentid").as(ExpEncoder); 
            CassandraJavaPairRDD<ExperimentFormOnlyParKey, ExperimentFormExDesInAd> predf = CassandraJavaUtil.javaFunctions(CassandraJavaUtil.javaFunctions(dfexplistoriginal.javaRDD()).repartitionByCassandraReplica("mdb","experiment",100,CassandraJavaUtil.someColumns("experimentid"),CassandraJavaUtil.mapToRow(ExperimentFormOnlyParKey.class)))
                                                                                .joinWithCassandraTable("mdb","experiment", CassandraJavaUtil.someColumns("experimentid","description","adduct","intensity"), CassandraJavaUtil.someColumns("experimentid"), CassandraJavaUtil.mapRowTo(ExperimentFormExDesInAd.class), CassandraJavaUtil.mapToRow(ExperimentFormOnlyParKey.class));
            Dataset<Row> metlistinitialbef =sp.createDataset(predf.map(x->x._2).rdd(), ExpEncoderofthree).toDF( "adduct","description","experimentid","intensity").filter(col("description").notEqual("Unidentified"));
            Dataset<Row> metlistinitialbe= metlistinitialbef.withColumn("ioniz", functions.substring(col("adduct"), -1, 1)).drop("adduct").filter(col("ioniz").equalTo("+"));
            metlistinitial2 = metlistinitialbe.drop(col("ioniz")).join(dfmtblist,"description");
        }
        else if(ionmode.equals("negative")){
            Encoder<ExperimentFormOnlyParKey> ExpEncoder = Encoders.bean(ExperimentFormOnlyParKey.class);
            Encoder<ExperimentFormExDesInAd> ExpEncoderofthree = Encoders.bean(ExperimentFormExDesInAd.class);
            Dataset<ExperimentFormOnlyParKey> dfexplistoriginal = sp.createDataset(expids, Encoders.STRING()).toDF("experimentid").as(ExpEncoder); 
            CassandraJavaPairRDD<ExperimentFormOnlyParKey, ExperimentFormExDesInAd> predf = CassandraJavaUtil.javaFunctions(CassandraJavaUtil.javaFunctions(dfexplistoriginal.javaRDD()).repartitionByCassandraReplica("mdb","experiment",100,CassandraJavaUtil.someColumns("experimentid"),CassandraJavaUtil.mapToRow(ExperimentFormOnlyParKey.class)))
                                                                                .joinWithCassandraTable("mdb","experiment", CassandraJavaUtil.someColumns("experimentid","description","adduct","intensity"), CassandraJavaUtil.someColumns("experimentid"), CassandraJavaUtil.mapRowTo(ExperimentFormExDesInAd.class), CassandraJavaUtil.mapToRow(ExperimentFormOnlyParKey.class));
            Dataset<Row> metlistinitialbef =sp.createDataset(predf.map(x->x._2).rdd(), ExpEncoderofthree).toDF( "adduct","description","experimentid","intensity").filter(col("description").notEqual("Unidentified"));
            Dataset<Row> metlistinitialbe= metlistinitialbef.withColumn("ioniz", functions.substring(col("adduct"), -1, 1)).drop("adduct").filter(col("ioniz").equalTo("-"));
            metlistinitial2 = metlistinitialbe.drop(col("ioniz")).join(dfmtblist,"description");
        }
        
        //take the 16000 most frequent metabolites
        Dataset<Row>desclist = metlistinitial2.groupBy(col("experimentid")).agg(functions.collect_list(col("description")));
        CountVectorizerModel cvModel = new CountVectorizer()
            .setInputCol("collect_list(description)")
            .setOutputCol("rawFeatures")
            .setVocabSize(nrmtbs)
            .setMinDF(1)
            .fit(desclist);

        cvModel.transform(desclist);
        String[] topmtbs = cvModel.vocabulary();
        metlist=Arrays.asList(topmtbs);
        return metlist;
    }

    public Experimentslistpca expspca(List<String> experimentlist,List<String> metabolitelist,String allorsome,String missval,String normval,String ionmode) {

        Experimentslistpca experimentslistpca = new Experimentslistpca();
        List<String> missvalmtbs=new ArrayList<>();
        List<Double[][]> pcsarray= new ArrayList<>();
        List<Double[]> variancesarray = new ArrayList<>();
        String nrmtbsexps = new String();
        Integer rowflag = 0;
        
        if(allorsome.equals("onall")){
            Dataset<Row> metlistinitial =sp.emptyDataFrame();
            if(ionmode.equals("both")){
                Encoder<ExperimentFormOnlyParKey> ExpEncoder = Encoders.bean(ExperimentFormOnlyParKey.class);
                Encoder<ExperimentFormExDesIn> ExpEncoderofthree = Encoders.bean(ExperimentFormExDesIn.class);
                Dataset<ExperimentFormOnlyParKey> dfexplistoriginal = sp.createDataset(experimentlist, Encoders.STRING()).toDF("experimentid").as(ExpEncoder);
                CassandraJavaPairRDD<ExperimentFormOnlyParKey, ExperimentFormExDesIn> predf = CassandraJavaUtil.javaFunctions(CassandraJavaUtil.javaFunctions(dfexplistoriginal.javaRDD()).repartitionByCassandraReplica("mdb","experiment",100,CassandraJavaUtil.someColumns("experimentid"),CassandraJavaUtil.mapToRow(ExperimentFormOnlyParKey.class)))
                                                                                    .joinWithCassandraTable("mdb","experiment", CassandraJavaUtil.someColumns("experimentid","description","intensity"), CassandraJavaUtil.someColumns("experimentid"), CassandraJavaUtil.mapRowTo(ExperimentFormExDesIn.class), CassandraJavaUtil.mapToRow(ExperimentFormOnlyParKey.class));
                metlistinitial =sp.createDataset(predf.map(x->x._2).rdd(), ExpEncoderofthree).toDF( "description","experimentid","intensity").filter(col("description").notEqual("Unidentified"));
            }else if(ionmode.equals("positive")){
                Encoder<ExperimentFormOnlyParKey> ExpEncoder = Encoders.bean(ExperimentFormOnlyParKey.class);
                Encoder<ExperimentFormExDesInAd> ExpEncoderofthree = Encoders.bean(ExperimentFormExDesInAd.class);
                Dataset<ExperimentFormOnlyParKey> dfexplistoriginal = sp.createDataset(experimentlist, Encoders.STRING()).toDF("experimentid").as(ExpEncoder); 
                CassandraJavaPairRDD<ExperimentFormOnlyParKey, ExperimentFormExDesInAd> predf = CassandraJavaUtil.javaFunctions(CassandraJavaUtil.javaFunctions(dfexplistoriginal.javaRDD()).repartitionByCassandraReplica("mdb","experiment",100,CassandraJavaUtil.someColumns("experimentid"),CassandraJavaUtil.mapToRow(ExperimentFormOnlyParKey.class)))
                                                                                    .joinWithCassandraTable("mdb","experiment", CassandraJavaUtil.someColumns("experimentid","description","adduct","intensity"), CassandraJavaUtil.someColumns("experimentid"), CassandraJavaUtil.mapRowTo(ExperimentFormExDesInAd.class), CassandraJavaUtil.mapToRow(ExperimentFormOnlyParKey.class));
                Dataset<Row> metlistinitialbef =sp.createDataset(predf.map(x->x._2).rdd(), ExpEncoderofthree).toDF( "adduct","description","experimentid","intensity").filter(col("description").notEqual("Unidentified"));
                Dataset<Row> metlistinitialbe= metlistinitialbef.withColumn("ioniz", functions.substring(col("adduct"), -1, 1)).drop("adduct").filter(col("ioniz").equalTo("+"));
                metlistinitial = metlistinitialbe.drop(col("ioniz"));
            }else if(ionmode.equals("negative")){
                Encoder<ExperimentFormOnlyParKey> ExpEncoder = Encoders.bean(ExperimentFormOnlyParKey.class);
                Encoder<ExperimentFormExDesInAd> ExpEncoderofthree = Encoders.bean(ExperimentFormExDesInAd.class);
                Dataset<ExperimentFormOnlyParKey> dfexplistoriginal = sp.createDataset(experimentlist, Encoders.STRING()).toDF("experimentid").as(ExpEncoder); 
                CassandraJavaPairRDD<ExperimentFormOnlyParKey, ExperimentFormExDesInAd> predf = CassandraJavaUtil.javaFunctions(CassandraJavaUtil.javaFunctions(dfexplistoriginal.javaRDD()).repartitionByCassandraReplica("mdb","experiment",100,CassandraJavaUtil.someColumns("experimentid"),CassandraJavaUtil.mapToRow(ExperimentFormOnlyParKey.class)))
                                                                                    .joinWithCassandraTable("mdb","experiment", CassandraJavaUtil.someColumns("experimentid","description","adduct","intensity"), CassandraJavaUtil.someColumns("experimentid"), CassandraJavaUtil.mapRowTo(ExperimentFormExDesInAd.class), CassandraJavaUtil.mapToRow(ExperimentFormOnlyParKey.class));
                Dataset<Row> metlistinitialbef =sp.createDataset(predf.map(x->x._2).rdd(), ExpEncoderofthree).toDF( "adduct","description","experimentid","intensity").filter(col("description").notEqual("Unidentified"));
                Dataset<Row> metlistinitialbe= metlistinitialbef.withColumn("ioniz", functions.substring(col("adduct"), -1, 1)).drop("adduct").filter(col("ioniz").equalTo("-"));
                metlistinitial = metlistinitialbe.drop(col("ioniz"));
            }
            Dataset<Row> expoutput = null;
            Dataset<Row> expavgpeaks = sp.emptyDataFrame();
            if(normval.equals("normvalmean")|| normval.equals("no")|| normval.equals("normvalmeanmtb")){
                Dataset<Row> metfinalpeaks = metlistinitial.groupBy(metlistinitial.col("experimentid"),metlistinitial.col("description")).avg("intensity").orderBy(asc("experimentid"), asc("description"));
                expavgpeaks = metfinalpeaks.groupBy(metfinalpeaks.col("description")).pivot("experimentid",new ArrayList<Object>(experimentlist)).mean("avg(intensity)").persist();
            }else if(normval.equals("normvalorig")){//or       
            //this is z-score on description and experiment
                Dataset<Row> tmp1 = metlistinitial.groupBy(col("experimentid"),col("description")).agg(functions.mean("intensity"),functions.stddev("intensity")).withColumnRenamed("experimentid", "expid").withColumnRenamed("description", "descri");
                Dataset<Row> tmpexpgroups2 = metlistinitial.join(tmp1,metlistinitial.col("experimentid").equalTo(tmp1.col("expid")).and(metlistinitial.col("description").equalTo(tmp1.col("descri")))).withColumn("zscore",((col("intensity").$minus(col("avg(intensity)"))).$div(col("stddev_samp(intensity)")))).na().fill(0);
                Dataset<Row> finalexp = tmpexpgroups2.drop("intensity","avg(intensity)","stddev_samp(intensity)","expid","descri").withColumnRenamed("zscore", "avg(intensity)");
                expavgpeaks = finalexp.groupBy(finalexp.col("description")).pivot("experimentid",new ArrayList<Object>(experimentlist)).mean("avg(intensity)").persist();
            }
            
            String[] expincols = experimentlist.parallelStream().toArray(String[]::new);
            //finding missing metabolites
            StructType schema = new StructType(new StructField[] {
			new StructField("metabs",
					DataTypes.StringType, false,
					Metadata.empty()) });
                
            Dataset<Row> tempmissvalmtbs=expavgpeaks.map((MapFunction<Row, Row>) originalrow -> {
                if(originalrow.anyNull()){
                    String rowValues = new String();
                    rowValues=originalrow.get(0).toString();
                    Row newrow = RowFactory.create(rowValues);
                    return newrow;
                }else{
                    String rowValues = "notmissing";
                    Row newrow = RowFactory.create(rowValues);
                    return newrow;
                }
            }, RowEncoder.apply(schema)).filter(col("metabs").notEqual("notmissing"));
            missvalmtbs=tempmissvalmtbs.as(Encoders.STRING()).collectAsList();
                        
            Dataset<Row> newfilenameavgpeaks=sp.emptyDataFrame();
            if(missval.equals("omit")){
                newfilenameavgpeaks=expavgpeaks.na().drop();
            }else if(missval.equals("replacewithzero")){
                newfilenameavgpeaks=expavgpeaks.na().fill(0);
            }else if(missval.equals("replacewithmean")){
                newfilenameavgpeaks=expavgpeaks.map((MapFunction<Row, Row>) originalrow -> {
                    if(originalrow.anyNull()){
                        int originalrowsizeofdoubles=originalrow.length()-1;
                        Double sumrow = Double.valueOf(0);
                        Double meanrow = Double.valueOf(0);
                        
                        Object[] rowValues = new Object[originalrow.length()];
                        rowValues[0]=originalrow.get(0).toString();
                        
                        List<Integer>indextobereplaced = new ArrayList<>();
                        List<Double> avgintensities = new ArrayList<>();
                        int validvalues=0;
                        for(int k=1;k<=originalrowsizeofdoubles;k++){
                            if(originalrow.isNullAt(k)){
                                indextobereplaced.add(k);
                                avgintensities.add(null);
                            }else{
                                sumrow = sumrow + originalrow.getDouble(k);
                                avgintensities.add(originalrow.getDouble(k));
                                validvalues++;
                            }
                        }
                        meanrow=sumrow/(validvalues);
                        
                        int indextobereplacedsize=indextobereplaced.size();
                        for(int i=0;i<indextobereplacedsize;i++){
                            avgintensities.set(indextobereplaced.get(i)-1, meanrow);

                        }
                        
                        for(int l=0;l<avgintensities.size();l++){
                            rowValues[l+1]=avgintensities.get(l);
                        }
                        Row newrow = RowFactory.create(rowValues);
                        
                        return newrow;
                    }else{
                        return originalrow;
                    }
                }, RowEncoder.apply(expavgpeaks.schema()));
            }else if(missval.equals("replacewithmedian")){
                newfilenameavgpeaks=expavgpeaks.map((MapFunction<Row, Row>) originalrow -> {
                    if(originalrow.anyNull()){
                        int originalrowsizeofdoubles=originalrow.length()-1;
                        Double medianrow = Double.valueOf(0);
                        
                        Object[] rowValues = new Object[originalrow.length()];
                        rowValues[0]=originalrow.get(0).toString();
                        
                        List<Integer>indextobereplaced = new ArrayList<>();
                        List<Double> medianintensities = new ArrayList<>();
                        List<Double> rowmedian = new ArrayList<>();
                        int validvalues=0;
                        for(int k=1;k<=originalrowsizeofdoubles;k++){
                            if(originalrow.isNullAt(k)){
                                indextobereplaced.add(k);
                                medianintensities.add(null);
                            }else{
                                medianintensities.add(originalrow.getDouble(k));
                                rowmedian.add(originalrow.getDouble(k));
                            }
                        }
                        int rowmediannum=rowmedian.size();
                        Double[] sortedmedianintensities= new Double[rowmediannum];
                        
                        for(int i=0;i<rowmediannum;i++){
                            sortedmedianintensities[i]=rowmedian.get(i);
                        }
                        Arrays.sort(sortedmedianintensities);
                        if (rowmediannum ==1){
                            medianrow=sortedmedianintensities[0];
                        }else if(rowmediannum>1){
                            if(rowmediannum % 2 == 0){
                                int pos1=(rowmediannum-1)/2;
                                int pos2=pos1+1;
                                medianrow=(sortedmedianintensities[pos1]+sortedmedianintensities[pos2])/2;
                            }else{
                                int pos = (rowmediannum/2);
                                medianrow=sortedmedianintensities[pos];
                            }
                        }
                        
                        int indextobereplacedsize=indextobereplaced.size();
                        for(int i=0;i<indextobereplacedsize;i++){
                            medianintensities.set(indextobereplaced.get(i)-1, medianrow);
                        }
                        for(int l=0;l<medianintensities.size();l++){
                            rowValues[l+1]=medianintensities.get(l);
                        }
                        Row newrow = RowFactory.create(rowValues);
                        
                        return newrow;
                    }else{
                        return originalrow;
                    }
                }, RowEncoder.apply(expavgpeaks.schema()));
            }
            
            Dataset<Row>normalonmtbmeans =sp.emptyDataFrame();
            if(normval.equals("normvalmeanmtb")){
                normalonmtbmeans = newfilenameavgpeaks.map((MapFunction<Row, Row>) originalrow -> {
                    Object[] rowValues = new Object[originalrow.length()];
                    rowValues[0]=originalrow.get(0).toString();
                    int originalrowsizeofdoubles=originalrow.length()-1;
                    Double tomean = Double.valueOf(0);
                    Double stdofrow = Double.valueOf(0);
                    
                    for(int k=1;k<=originalrowsizeofdoubles;k++){
                            tomean=tomean+originalrow.getDouble(k);
                    }
                    Double meanofrow = tomean/originalrowsizeofdoubles;
                    
                    Double sqrdis = Double.valueOf(0);
                    for(int k=1;k<=originalrowsizeofdoubles;k++){
                        Double tmpdis=originalrow.getDouble(k)-meanofrow;
                        sqrdis=sqrdis+(tmpdis*tmpdis);
                    }
                    stdofrow=Math.sqrt(sqrdis/originalrowsizeofdoubles);
                    
                    for(int k=1;k<=originalrowsizeofdoubles;k++){
                        rowValues[k]=(originalrow.getDouble(k)-meanofrow)/stdofrow;
                    }
                    Row newrow = RowFactory.create(rowValues);
                    return newrow;
                }, RowEncoder.apply(newfilenameavgpeaks.schema()));
            }else{
                normalonmtbmeans=newfilenameavgpeaks;
            }
            
            VectorAssembler assemblerexp = new VectorAssembler()
                    .setInputCols(expincols)
                    .setOutputCol("intensity");
            
            expoutput = assemblerexp.transform(normalonmtbmeans);
            
            Long numberofmtbs = expoutput.count();
            nrmtbsexps = "PCA is based on "+numberofmtbs+" metabolites and "+experimentlist.size()+" experiments.";
            String pcainputcol = "";
            if(numberofmtbs>1){
                //Normalization
                Dataset<Row> scaledData = sp.emptyDataFrame();
                if(normval.equals("normvalmean")){
                    StandardScaler scaler = new StandardScaler()
                            .setInputCol("intensity")
                            .setOutputCol("normintensity")
                            .setWithStd(true)
                            .setWithMean(true);

                    StandardScalerModel scalerModel = scaler.fit(expoutput);

                    scaledData = scalerModel.transform(expoutput);
                    pcainputcol = "normintensity";
                }else{
                    scaledData=expoutput;
                    pcainputcol = "intensity";
                }
                
                
                PCAModel pcaexp = new PCA()
                        .setInputCol(pcainputcol)
                        .setOutputCol("pcaFeatures")
                        .setK(2)
                        .fit(scaledData);

                Double[] varianceexps = new Double[pcaexp.explainedVariance().size()];
                for (int i = 0; i < pcaexp.explainedVariance().size(); i++) {
                    varianceexps[i] = Math.round(pcaexp.explainedVariance().apply(i)*100000)/1000d;
                }
                Double[][] pcaexps=new Double[pcaexp.pc().numRows()][pcaexp.pc().numCols()];
                for (int i = 0; i < pcaexp.pc().numRows(); i++) {
                    for (int j = 0; j < pcaexp.pc().numCols(); j++) {
                        pcaexps[i][j]=pcaexp.pc().apply(i,j);
                    }
                }
                pcsarray.add(pcaexps);
                variancesarray.add(varianceexps);
            }else if(numberofmtbs<=1){
                rowflag=1;
            }
            expavgpeaks.unpersist();

        }else{
            Dataset<Row> dfmetabolitelist = sp.createDataset(metabolitelist, Encoders.STRING()).toDF("description");
            
            Dataset<Row> metlistinitialfirst =sp.emptyDataFrame();
            if(ionmode.equals("both")){
                Encoder<ExperimentFormOnlyParKey> ExpEncoder = Encoders.bean(ExperimentFormOnlyParKey.class);
                Encoder<ExperimentFormExDesIn> ExpEncoderofthree = Encoders.bean(ExperimentFormExDesIn.class);
                Dataset<ExperimentFormOnlyParKey> dfexplistoriginal = sp.createDataset(experimentlist, Encoders.STRING()).toDF("experimentid").as(ExpEncoder);
                CassandraJavaPairRDD<ExperimentFormOnlyParKey, ExperimentFormExDesIn> predf = CassandraJavaUtil.javaFunctions(CassandraJavaUtil.javaFunctions(dfexplistoriginal.javaRDD()).repartitionByCassandraReplica("mdb","experiment",100,CassandraJavaUtil.someColumns("experimentid"),CassandraJavaUtil.mapToRow(ExperimentFormOnlyParKey.class)))
                                                                                    .joinWithCassandraTable("mdb","experiment", CassandraJavaUtil.someColumns("experimentid","description","intensity"), CassandraJavaUtil.someColumns("experimentid"), CassandraJavaUtil.mapRowTo(ExperimentFormExDesIn.class), CassandraJavaUtil.mapToRow(ExperimentFormOnlyParKey.class));
                metlistinitialfirst =sp.createDataset(predf.map(x->x._2).rdd(), ExpEncoderofthree).toDF( "description","experimentid","intensity");
            }else if(ionmode.equals("positive")){
                Encoder<ExperimentFormOnlyParKey> ExpEncoder = Encoders.bean(ExperimentFormOnlyParKey.class);
                Encoder<ExperimentFormExDesInAd> ExpEncoderofthree = Encoders.bean(ExperimentFormExDesInAd.class);
                Dataset<ExperimentFormOnlyParKey> dfexplistoriginal = sp.createDataset(experimentlist, Encoders.STRING()).toDF("experimentid").as(ExpEncoder); //description, adduct, experimentid
                CassandraJavaPairRDD<ExperimentFormOnlyParKey, ExperimentFormExDesInAd> predf = CassandraJavaUtil.javaFunctions(CassandraJavaUtil.javaFunctions(dfexplistoriginal.javaRDD()).repartitionByCassandraReplica("mdb","experiment",100,CassandraJavaUtil.someColumns("experimentid"),CassandraJavaUtil.mapToRow(ExperimentFormOnlyParKey.class)))
                                                                                    .joinWithCassandraTable("mdb","experiment", CassandraJavaUtil.someColumns("experimentid","description","adduct","intensity"), CassandraJavaUtil.someColumns("experimentid"), CassandraJavaUtil.mapRowTo(ExperimentFormExDesInAd.class), CassandraJavaUtil.mapToRow(ExperimentFormOnlyParKey.class));
                Dataset<Row> metlistinitialbef =sp.createDataset(predf.map(x->x._2).rdd(), ExpEncoderofthree).toDF( "adduct","description","experimentid","intensity");
                Dataset<Row> metlistinitialbe= metlistinitialbef.withColumn("ioniz", functions.substring(col("adduct"), -1, 1)).drop("adduct").filter(col("ioniz").equalTo("+"));
                metlistinitialfirst = metlistinitialbe.drop(col("ioniz"));
            }else if(ionmode.equals("negative")){
                Encoder<ExperimentFormOnlyParKey> ExpEncoder = Encoders.bean(ExperimentFormOnlyParKey.class);
                Encoder<ExperimentFormExDesInAd> ExpEncoderofthree = Encoders.bean(ExperimentFormExDesInAd.class);
                Dataset<ExperimentFormOnlyParKey> dfexplistoriginal = sp.createDataset(experimentlist, Encoders.STRING()).toDF("experimentid").as(ExpEncoder); //description, adduct, experimentid
                CassandraJavaPairRDD<ExperimentFormOnlyParKey, ExperimentFormExDesInAd> predf = CassandraJavaUtil.javaFunctions(CassandraJavaUtil.javaFunctions(dfexplistoriginal.javaRDD()).repartitionByCassandraReplica("mdb","experiment",100,CassandraJavaUtil.someColumns("experimentid"),CassandraJavaUtil.mapToRow(ExperimentFormOnlyParKey.class)))
                                                                                    .joinWithCassandraTable("mdb","experiment", CassandraJavaUtil.someColumns("experimentid","description","adduct","intensity"), CassandraJavaUtil.someColumns("experimentid"), CassandraJavaUtil.mapRowTo(ExperimentFormExDesInAd.class), CassandraJavaUtil.mapToRow(ExperimentFormOnlyParKey.class));
                Dataset<Row> metlistinitialbef =sp.createDataset(predf.map(x->x._2).rdd(), ExpEncoderofthree).toDF( "adduct","description","experimentid","intensity");
                Dataset<Row> metlistinitialbe= metlistinitialbef.withColumn("ioniz", functions.substring(col("adduct"), -1, 1)).drop("adduct").filter(col("ioniz").equalTo("-"));
                metlistinitialfirst = metlistinitialbe.drop(col("ioniz"));
            }
            
            String[] expincols=experimentlist.parallelStream().toArray(String[]::new);
            Dataset<Row> tempmissvalmtbs = sp.emptyDataFrame();
            Dataset<Row> newfilenameavgpeaks=sp.emptyDataFrame();
            Dataset<Row> metfinalpeaks=sp.emptyDataFrame();

            if(missval.equals("omit")){
                Dataset<Row> metlistinitial = metlistinitialfirst.join(dfmetabolitelist,"description");
                if(normval.equals("normvalmean")|| normval.equals("no")|| normval.equals("normvalmeanmtb")){
                    metfinalpeaks = metlistinitial.groupBy(metlistinitial.col("experimentid"),metlistinitial.col("description")).avg("intensity").orderBy(asc("experimentid"), asc("description"));
                    newfilenameavgpeaks = metfinalpeaks.groupBy(metfinalpeaks.col("description")).pivot("experimentid",new ArrayList<Object>(experimentlist)).mean("avg(intensity)");//.persist();
                }else if(normval.equals("normvalorig")){//or       
                    //this is z-score on description and experiment
                    Dataset<Row> tmp1 = metlistinitial.groupBy(col("experimentid"),col("description")).agg(functions.mean("intensity"),functions.stddev("intensity")).withColumnRenamed("experimentid", "expid").withColumnRenamed("description", "descri");
                    Dataset<Row> tmpexpgroups2 = metlistinitial.join(tmp1,metlistinitial.col("experimentid").equalTo(tmp1.col("expid")).and(metlistinitial.col("description").equalTo(tmp1.col("descri")))).withColumn("zscore",((col("intensity").$minus(col("avg(intensity)"))).$div(col("stddev_samp(intensity)")))).na().fill(0);
                    Dataset<Row> finalexp = tmpexpgroups2.drop("intensity","avg(intensity)","stddev_samp(intensity)","expid","descri").withColumnRenamed("zscore", "avg(intensity)");
                    newfilenameavgpeaks = finalexp.groupBy(finalexp.col("description")).pivot("experimentid",new ArrayList<Object>(experimentlist)).mean("avg(intensity)");//.persist();
                }
                
            }else{
                Dataset<Row> tmpexpavgpeaks = sp.emptyDataFrame();
                if(normval.equals("normvalmean")|| normval.equals("no")|| normval.equals("normvalmeanmtb")){
                    metfinalpeaks = metlistinitialfirst.groupBy(metlistinitialfirst.col("experimentid"),metlistinitialfirst.col("description")).avg("intensity").orderBy(asc("experimentid"), asc("description")).persist();
                    tmpexpavgpeaks = metfinalpeaks.groupBy(metfinalpeaks.col("description")).pivot("experimentid",new ArrayList<Object>(experimentlist)).mean("avg(intensity)");//.persist();
                }else if(normval.equals("normvalorig")){//or       
                    //this is z-score on description and experiment
                    Dataset<Row> tmp1 = metlistinitialfirst.groupBy(col("experimentid"),col("description")).agg(functions.mean("intensity"),functions.stddev("intensity")).withColumnRenamed("experimentid", "expid").withColumnRenamed("description", "descri");
                    Dataset<Row> tmpexpgroups2 = metlistinitialfirst.join(tmp1,metlistinitialfirst.col("experimentid").equalTo(tmp1.col("expid")).and(metlistinitialfirst.col("description").equalTo(tmp1.col("descri")))).withColumn("zscore",((col("intensity").$minus(col("avg(intensity)"))).$div(col("stddev_samp(intensity)")))).na().fill(0);
                    Dataset<Row> finalexp = tmpexpgroups2.drop("intensity","avg(intensity)","stddev_samp(intensity)","expid","descri").withColumnRenamed("zscore", "avg(intensity)");
                    metfinalpeaks = finalexp.groupBy(finalexp.col("experimentid"),finalexp.col("description")).avg("avg(intensity)").withColumnRenamed("avg(avg(intensity))", "avg(intensity)").orderBy( asc("description")).persist();
                    tmpexpavgpeaks = finalexp.groupBy(finalexp.col("description")).pivot("experimentid",new ArrayList<Object>(experimentlist)).mean("avg(intensity)");//.persist();
                }
                
                //finding missing metabolites
                StructType schema = new StructType(new StructField[] {
                            new StructField("description",DataTypes.StringType, false,Metadata.empty()),
                            new StructField("experimentid",DataTypes.createArrayType(DataTypes.StringType, true), false,Metadata.empty())});
                String[] columnnames=tmpexpavgpeaks.columns();
                int mtbswithallnullms=tmpexpavgpeaks.columns().length;
                
                tempmissvalmtbs=tmpexpavgpeaks.map((MapFunction<Row, Row>) originalrow -> {
                    if(originalrow.anyNull() && metabolitelist.contains(originalrow.get(0).toString())){
                        List<String>tmpexpidfilename=new ArrayList<>();
                        for(int i=1;i<mtbswithallnullms;i++){
                            if(originalrow.isNullAt(i)){
                                tmpexpidfilename.add(columnnames[i]);
                            }
                        }
                        String mtbname = originalrow.get(0).toString();
                        Row newrow = RowFactory.create(mtbname,tmpexpidfilename.toArray(new String[0]));
                        return newrow;
                    }else{
                        String mtbname = "notmissing";
                        List<String>tmpexpidfilename=new ArrayList<>();
                        Row newrow = RowFactory.create(mtbname,tmpexpidfilename.toArray(new String[0]));
                        return newrow;
                    }
                }, RowEncoder.apply(schema)).persist(); 
                Dataset<Row> missvalmetabs=tempmissvalmtbs.select("description").filter(col("description").notEqual("notmissing"));
                missvalmtbs=missvalmetabs.as(Encoders.STRING()).collectAsList();
                
                Dataset<Row> nullmtbs = tempmissvalmtbs.withColumn("experimentid", functions.explode_outer(col("experimentid"))).withColumn("avg(intensity)", functions.lit(null).cast(DataTypes.DoubleType)).select(col("experimentid"),col("description"),col("avg(intensity)"));
                Dataset<Row> tmpsupernewmetfinalpeaksafterrename=metfinalpeaks.union(nullmtbs);
                Dataset<Row> supernewmetfinalpeaksafterrename=tmpsupernewmetfinalpeaksafterrename.join(dfmetabolitelist,"description");
                    
                Dataset<Row> finalexpavgpeaks = supernewmetfinalpeaksafterrename.groupBy(supernewmetfinalpeaksafterrename.col("description")).pivot("experimentid",new ArrayList<Object>(experimentlist)).mean("avg(intensity)");
                
                if(missval.equals("replacewithzero")){
                    newfilenameavgpeaks=finalexpavgpeaks.na().fill(0);
                }else if(missval.equals("replacewithmean")){
                    newfilenameavgpeaks=finalexpavgpeaks.map((MapFunction<Row, Row>) originalrow -> {
                        if(originalrow.anyNull() ){
                            int originalrowsizeofdoubles=originalrow.length()-1;
                            Double sumrow = Double.valueOf(0);
                            Double meanrow = Double.valueOf(0);

                            Object[] rowValues = new Object[originalrow.length()];
                            rowValues[0]=originalrow.get(0).toString();

                            List<Integer>indextobereplaced = new ArrayList<>();
                            List<Double> avgintensities = new ArrayList<>();
                            int validvalues=0;
                            for(int k=1;k<=originalrowsizeofdoubles;k++){
                                if(originalrow.isNullAt(k)){
                                    indextobereplaced.add(k);
                                    avgintensities.add(null);
                                }else{
                                    sumrow = sumrow + originalrow.getDouble(k);
                                    avgintensities.add(originalrow.getDouble(k));
                                    validvalues++;
                                }
                            }
                            meanrow=sumrow/(validvalues);

                            int indextobereplacedsize=indextobereplaced.size();
                            for(int i=0;i<indextobereplacedsize;i++){
                                avgintensities.set(indextobereplaced.get(i)-1, meanrow);

                            }
                            for(int l=0;l<avgintensities.size();l++){
                                rowValues[l+1]=avgintensities.get(l);
                            }
                            Row newrow = RowFactory.create(rowValues);

                            return newrow;
                        }else{
                            return originalrow;
                        }
                    }, RowEncoder.apply(finalexpavgpeaks.schema()));
                    
                }else if(missval.equals("replacewithmedian")){
                    newfilenameavgpeaks=finalexpavgpeaks.map((MapFunction<Row, Row>) originalrow -> {
                        if(originalrow.anyNull() ){
                            int originalrowsizeofdoubles=originalrow.length()-1;
                            Double medianrow = Double.valueOf(0);

                            Object[] rowValues = new Object[originalrow.length()];
                            rowValues[0]=originalrow.get(0).toString();

                            List<Integer>indextobereplaced = new ArrayList<>();
                            List<Double> medianintensities = new ArrayList<>();
                            List<Double> rowmedian = new ArrayList<>();
                            int validvalues=0;
                            for(int k=1;k<=originalrowsizeofdoubles;k++){
                                if(originalrow.isNullAt(k)){
                                    indextobereplaced.add(k);
                                    medianintensities.add(null);
                                }else{
                                    medianintensities.add(originalrow.getDouble(k));
                                    rowmedian.add(originalrow.getDouble(k));
                                }
                            }

                            int rowmediannum=rowmedian.size();
                            Double[] sortedmedianintensities= new Double[rowmediannum];
                            for(int i=0;i<rowmediannum;i++){
                                sortedmedianintensities[i]=rowmedian.get(i);
                            }
                            Arrays.sort(sortedmedianintensities);
                            if (rowmediannum ==1){
                                medianrow=sortedmedianintensities[0];
                            }else if(rowmediannum>1){
                                if(rowmediannum % 2 == 0){
                                    int pos1=(rowmediannum-1)/2;
                                    int pos2=pos1+1;
                                    medianrow=(sortedmedianintensities[pos1]+sortedmedianintensities[pos2])/2;
                                }else{
                                    int pos = (rowmediannum/2);
                                    medianrow=sortedmedianintensities[pos];
                                }
                            }

                            int indextobereplacedsize=indextobereplaced.size();
                            for(int i=0;i<indextobereplacedsize;i++){
                                medianintensities.set(indextobereplaced.get(i)-1, medianrow);
                            }
                            for(int l=0;l<medianintensities.size();l++){
                                rowValues[l+1]=medianintensities.get(l);
                            }
                            Row newrow = RowFactory.create(rowValues);

                            return newrow;
                        }else{
                            return originalrow;
                        }
                    }, RowEncoder.apply(finalexpavgpeaks.schema()));
                }
            }
            
            Dataset<Row>normalonmtbmeans =sp.emptyDataFrame();
            if(normval.equals("normvalmeanmtb")){
                normalonmtbmeans = newfilenameavgpeaks.map((MapFunction<Row, Row>) originalrow -> {
                    Object[] rowValues = new Object[originalrow.length()];
                    rowValues[0]=originalrow.get(0).toString();
                    int originalrowsizeofdoubles=originalrow.length()-1;
                    Double tomean = Double.valueOf(0);
                    Double stdofrow = Double.valueOf(0);
                    
                    for(int k=1;k<=originalrowsizeofdoubles;k++){
                            tomean=tomean+originalrow.getDouble(k);
                    }
                    Double meanofrow = tomean/originalrowsizeofdoubles;
                    
                    Double sqrdis = Double.valueOf(0);
                    for(int k=1;k<=originalrowsizeofdoubles;k++){
                        Double tmpdis=originalrow.getDouble(k)-meanofrow;
                        sqrdis=sqrdis+(tmpdis*tmpdis);
                    }
                    stdofrow=Math.sqrt(sqrdis/originalrowsizeofdoubles);
                    
                    for(int k=1;k<=originalrowsizeofdoubles;k++){
                        rowValues[k]=(originalrow.getDouble(k)-meanofrow)/stdofrow;
                    }
                    Row newrow = RowFactory.create(rowValues);
                    return newrow;
                }, RowEncoder.apply(newfilenameavgpeaks.schema()));
            }else{
                normalonmtbmeans=newfilenameavgpeaks;
            }
            
            VectorAssembler assemblerexp = new VectorAssembler()
                    .setInputCols(expincols)
                    .setOutputCol("intensity");
            Dataset<Row> expoutput = assemblerexp.transform(normalonmtbmeans);
            
            Integer numberofmtbs = metabolitelist.size();
            nrmtbsexps = "PCA is based on "+numberofmtbs+" metabolites and "+experimentlist.size()+" experiments.";
            String pcainputcol = "";
            //Normalization
            Dataset<Row> scaledData = sp.emptyDataFrame();
            if(normval.equals("normvalmean")){
                StandardScaler scaler = new StandardScaler()
                        .setInputCol("intensity")
                        .setOutputCol("normintensity")
                        .setWithStd(true)
                        .setWithMean(true);

                StandardScalerModel scalerModel = scaler.fit(expoutput);

                scaledData = scalerModel.transform(expoutput);
                pcainputcol="normintensity";
            }else{
                scaledData=expoutput;
                pcainputcol="intensity";
            }
            
            PCAModel pcaexp = new PCA()
                    .setInputCol(pcainputcol)
                    .setOutputCol("pcaFeatures")
                    .setK(2)
                    .fit(scaledData);

            Double[] varianceexps = new Double[pcaexp.explainedVariance().size()];
            for (int i = 0; i < pcaexp.explainedVariance().size(); i++) {
                varianceexps[i] = Math.round(pcaexp.explainedVariance().apply(i)*100000)/1000d;
            }
            Double[][] pcaexps=new Double[pcaexp.pc().numRows()][pcaexp.pc().numCols()];
            for (int i = 0; i < pcaexp.pc().numRows(); i++) {
                for (int j = 0; j < pcaexp.pc().numCols(); j++) {
                    pcaexps[i][j]=pcaexp.pc().apply(i,j);
                }
            }
            pcsarray.add(pcaexps);
            variancesarray.add(varianceexps);
            tempmissvalmtbs.unpersist();
            metfinalpeaks.unpersist();
        }
        if(rowflag==0){
            experimentslistpca.setExpslist(experimentlist);
            experimentslistpca.setExpspca(pcsarray);
            experimentslistpca.setExpsvariance(variancesarray);
            experimentslistpca.setMissvalmtbs(missvalmtbs);
            experimentslistpca.setNrmtbsexps(nrmtbsexps);
        }else{
            experimentslistpca.setRowflag(1);
        }
        return experimentslistpca;
        
    }
    
    public Replicateslistpca replicatespca( List<String> experids,List<String> metabolitelist,String allorsome, String missval,String normval,String ionmode){
        
        String nrmtbsexps = new String();
        Integer rowflag = 0;
        
        Replicateslistpca replicateslistpca = new Replicateslistpca();
        List<String> replinames = new ArrayList<>();
        List<String> missvalmtbs = new ArrayList<>();
        List<Double[][]> pcsarray= new ArrayList<>();
        List<Double[]> variancesarray = new ArrayList<>();
        
        if(allorsome.equals("onall")){
            
            Dataset<Row> metlistinitial =sp.emptyDataFrame();
            if(ionmode.equals("both")){
                Encoder<ExperimentFormOnlyParKey> ExpEncoder = Encoders.bean(ExperimentFormOnlyParKey.class);
                Encoder<ExperimentFormExDesFlIn> ExpEncoderoffour = Encoders.bean(ExperimentFormExDesFlIn.class);
                Dataset<ExperimentFormOnlyParKey> dfexplistoriginal = sp.createDataset(experids, Encoders.STRING()).toDF("experimentid").as(ExpEncoder);
                CassandraJavaPairRDD<ExperimentFormOnlyParKey, ExperimentFormExDesFlIn> predf = CassandraJavaUtil.javaFunctions(CassandraJavaUtil.javaFunctions(dfexplistoriginal.javaRDD()).repartitionByCassandraReplica("mdb","experiment",100,CassandraJavaUtil.someColumns("experimentid"),CassandraJavaUtil.mapToRow(ExperimentFormOnlyParKey.class)))
                                                                                    .joinWithCassandraTable("mdb","experiment", CassandraJavaUtil.someColumns("experimentid","filename","description","intensity"), CassandraJavaUtil.someColumns("experimentid"), CassandraJavaUtil.mapRowTo(ExperimentFormExDesFlIn.class), CassandraJavaUtil.mapToRow(ExperimentFormOnlyParKey.class));
                metlistinitial =sp.createDataset(predf.map(x->x._2).rdd(), ExpEncoderoffour).toDF( "description","experimentid","filename","intensity").filter(col("description").notEqual("Unidentified"));
            }else if(ionmode.equals("positive")){
                Encoder<ExperimentFormOnlyParKey> ExpEncoder = Encoders.bean(ExperimentFormOnlyParKey.class);
                Encoder<ExperimentFormExDesFlInAd> ExpEncoderofthree = Encoders.bean(ExperimentFormExDesFlInAd.class);
                Dataset<ExperimentFormOnlyParKey> dfexplistoriginal = sp.createDataset(experids, Encoders.STRING()).toDF("experimentid").as(ExpEncoder);
                CassandraJavaPairRDD<ExperimentFormOnlyParKey, ExperimentFormExDesFlInAd> predf = CassandraJavaUtil.javaFunctions(CassandraJavaUtil.javaFunctions(dfexplistoriginal.javaRDD()).repartitionByCassandraReplica("mdb","experiment",100,CassandraJavaUtil.someColumns("experimentid"),CassandraJavaUtil.mapToRow(ExperimentFormOnlyParKey.class)))
                                                                                    .joinWithCassandraTable("mdb","experiment", CassandraJavaUtil.someColumns("experimentid","filename","description","adduct","intensity"), CassandraJavaUtil.someColumns("experimentid"), CassandraJavaUtil.mapRowTo(ExperimentFormExDesFlInAd.class), CassandraJavaUtil.mapToRow(ExperimentFormOnlyParKey.class));
                Dataset<Row> metlistinitialbef =sp.createDataset(predf.map(x->x._2).rdd(), ExpEncoderofthree).toDF( "adduct","description","experimentid","filename","intensity").filter(col("description").notEqual("Unidentified"));
                Dataset<Row> metlistinitialbe= metlistinitialbef.withColumn("ioniz", functions.substring(col("adduct"), -1, 1)).drop("adduct").filter(col("ioniz").equalTo("+"));
                metlistinitial = metlistinitialbe.drop(col("ioniz"));
            }else if(ionmode.equals("negative")){
                Encoder<ExperimentFormOnlyParKey> ExpEncoder = Encoders.bean(ExperimentFormOnlyParKey.class);
                Encoder<ExperimentFormExDesFlInAd> ExpEncoderofthree = Encoders.bean(ExperimentFormExDesFlInAd.class);
                Dataset<ExperimentFormOnlyParKey> dfexplistoriginal = sp.createDataset(experids, Encoders.STRING()).toDF("experimentid").as(ExpEncoder); 
                CassandraJavaPairRDD<ExperimentFormOnlyParKey, ExperimentFormExDesFlInAd> predf = CassandraJavaUtil.javaFunctions(CassandraJavaUtil.javaFunctions(dfexplistoriginal.javaRDD()).repartitionByCassandraReplica("mdb","experiment",100,CassandraJavaUtil.someColumns("experimentid"),CassandraJavaUtil.mapToRow(ExperimentFormOnlyParKey.class)))
                                                                                    .joinWithCassandraTable("mdb","experiment", CassandraJavaUtil.someColumns("experimentid","filename","description","adduct","intensity"), CassandraJavaUtil.someColumns("experimentid"), CassandraJavaUtil.mapRowTo(ExperimentFormExDesFlInAd.class), CassandraJavaUtil.mapToRow(ExperimentFormOnlyParKey.class));
                Dataset<Row> metlistinitialbef =sp.createDataset(predf.map(x->x._2).rdd(), ExpEncoderofthree).toDF( "adduct","description","experimentid","filename","intensity").filter(col("description").notEqual("Unidentified"));
                Dataset<Row> metlistinitialbe= metlistinitialbef.withColumn("ioniz", functions.substring(col("adduct"), -1, 1)).drop("adduct").filter(col("ioniz").equalTo("-"));
                metlistinitial = metlistinitialbe.drop(col("ioniz"));
            }
            
            Dataset<Row> newmetfinalpeaks = sp.emptyDataFrame();
            if(normval.equals("normvalmean")|| normval.equals("no")||normval.equals("normvalmeanmtb")){
                Dataset<Row> metfinalpeaks = metlistinitial.groupBy(metlistinitial.col("experimentid"),metlistinitial.col("filename"),metlistinitial.col("description")).avg("intensity").orderBy(asc("experimentid"), asc("description"));
                newmetfinalpeaks = metfinalpeaks.withColumn("expidfilename", functions.concat(col("experimentid"), lit("__"), col("filename"))).drop("experimentid","filename");
            }else if(normval.equals("normvalorig")){//or       
            //this is z-score on description and experiment
                Dataset<Row> tmp1 = metlistinitial.groupBy(col("experimentid"),col("filename"),col("description")).agg(functions.mean("intensity"),functions.stddev("intensity")).withColumnRenamed("experimentid", "expid").withColumnRenamed("filename", "flname").withColumnRenamed("description", "descri");
                Dataset<Row> tmpexpgroups2 = metlistinitial.join(tmp1,metlistinitial.col("experimentid").equalTo(tmp1.col("expid")).and(metlistinitial.col("filename").equalTo(tmp1.col("flname"))).and(metlistinitial.col("description").equalTo(tmp1.col("descri")))).withColumn("zscore",((col("intensity").$minus(col("avg(intensity)"))).$div(col("stddev_samp(intensity)")))).na().fill(0);
                Dataset<Row> finalexp = tmpexpgroups2.drop("intensity","avg(intensity)","stddev_samp(intensity)","expid","descri","flname").withColumnRenamed("zscore", "avg(intensity)");
                //expavgpeaks = finalexp.groupBy(finalexp.col("description")).pivot("experimentid",new ArrayList<Object>(experimentlist)).mean("avg(intensity)").persist();
                newmetfinalpeaks = finalexp.withColumn("expidfilename", functions.concat(col("experimentid"), lit("__"), col("filename"))).drop("experimentid","filename");
            }
            
            Dataset<Row> filenameoutput = null;
            Dataset<Row> newmetfinalpeaksafterrename=newmetfinalpeaks.map((MapFunction<Row, Row>) originalrow -> {
                String replicatewithdot = originalrow.get(2).toString();
                String temp = replicatewithdot.replaceAll("\\.[mzmlxdatMZMLXDAT]{4,6}$","");
                if(temp.contains(".")){
                            temp=temp.replaceAll("[.]", "_");
                        }
                Object[] rowValues = new Object[originalrow.length()];
                rowValues[0]=originalrow.get(0);
                rowValues[1]=originalrow.get(1);
                rowValues[2]=temp;
                Row newrow = RowFactory.create(rowValues);
                return newrow;
            }, RowEncoder.apply(newmetfinalpeaks.schema()));
            
            Dataset<Row> filenameavgpeaks = newmetfinalpeaksafterrename.groupBy(newmetfinalpeaksafterrename.col("description")).pivot("expidfilename").mean("avg(intensity)").persist();
            String[] temp1 = filenameavgpeaks.columns();
            List<String>temprepl = new ArrayList<>();
            for(String finame:temp1){
                if(!finame.equals("description")){
                    temprepl.add(finame);
                }
            }
            replinames=temprepl;
            String[] expincols = replinames.parallelStream().toArray(String[]::new);
            
            //finding missing metabolites
            StructType schema = new StructType(new StructField[] {
			new StructField("metabs",
					DataTypes.StringType, false,
					Metadata.empty()) });
                
            Dataset<Row> tempmissvalmtbs=filenameavgpeaks.map((MapFunction<Row, Row>) originalrow -> {
                if(originalrow.anyNull()){
                    String rowValues = new String();
                    rowValues=originalrow.get(0).toString();
                    Row newrow = RowFactory.create(rowValues);
                    return newrow;
                }else{
                    String rowValues = "notmissing";
                    Row newrow = RowFactory.create(rowValues);
                    return newrow;
                }
            }, RowEncoder.apply(schema)).filter(col("metabs").notEqual("notmissing"));
            missvalmtbs=tempmissvalmtbs.as(Encoders.STRING()).collectAsList();
            
            Dataset<Row> newfilenameavgpeaks=sp.emptyDataFrame();
            if(missval.equals("omit")){
                newfilenameavgpeaks=filenameavgpeaks.na().drop();
            }else if(missval.equals("replacewithzero")){
                newfilenameavgpeaks=filenameavgpeaks.na().fill(0);
            }else if(missval.equals("replacewithmean")){
                newfilenameavgpeaks=filenameavgpeaks.map((MapFunction<Row, Row>) originalrow -> {
                    if(originalrow.anyNull()){
                        int originalrowsizeofdoubles = originalrow.length()-1;
                        Double sumrow = Double.valueOf(0);
                        List<Double> meanrow = new ArrayList<>();
                        
                        Object[] rowValues = new Object[originalrow.length()];
                        rowValues[0]=originalrow.get(0).toString();
                        
                        String[] temporal = originalrow.schema().fieldNames();
                        String[] namesofrow = new String[originalrow.length()];
                        List<Integer> replnuminexp=new ArrayList<>();
                        int replnuminexpcounter=0;
                        int replnuminexpcounterwithvalidvalues=0;
                        List<Double> avgintensities = new ArrayList<>();
                        
                        for(int k=0;k<originalrowsizeofdoubles;k++){
                            String[] namesofrowtemp = temporal[k+1].split("__");
                            namesofrow[k]=namesofrowtemp[0];
                        }
                        for(int i=0;i<originalrowsizeofdoubles;i++){
                            replnuminexpcounter++;
                            if(!namesofrow[i].equals(namesofrow[i+1])){
                                replnuminexp.add(replnuminexpcounter);
                                replnuminexpcounter=0;
                            }
                        }
                        int experpointer=0;
                        int replpointer=1;
                        for(int k=1;k<=originalrowsizeofdoubles;k++){
                            if( replpointer<=replnuminexp.get(experpointer)){
                                if(originalrow.isNullAt(k)){
                                    avgintensities.add(null);
                                }else{
                                    replnuminexpcounterwithvalidvalues++;
                                    sumrow = sumrow + originalrow.getDouble(k);
                                    avgintensities.add(originalrow.getDouble(k));
                                }
                                if(replpointer!=replnuminexp.get(experpointer)){
                                    replpointer++;
                                }else if(replpointer==replnuminexp.get(experpointer)){
                                    if(replnuminexpcounterwithvalidvalues>0){
                                        meanrow.add(sumrow/replnuminexpcounterwithvalidvalues);
                                    }else{
                                        meanrow.add(Double.valueOf(0));
                                    }
                                    experpointer++;
                                    replpointer=1;
                                    replnuminexpcounterwithvalidvalues=0;
                                    sumrow = Double.valueOf(0);
                                }
                            }
                        }
                        int newexperpointer=0;
                        int newreplpointer=1;
                        for(int i=0;i<originalrowsizeofdoubles;i++){
                            if(avgintensities.get(i)==null){
                                if(newreplpointer<replnuminexp.get(newexperpointer)){
                                    avgintensities.set(i, meanrow.get(newexperpointer));
                                    newreplpointer++;
                                }else if(newreplpointer==replnuminexp.get(newexperpointer)){
                                    avgintensities.set(i, meanrow.get(newexperpointer));
                                    newreplpointer=1;
                                    newexperpointer++;
                                }
                            }else{
                                if(newreplpointer<replnuminexp.get(newexperpointer)){
                                    newreplpointer++;
                                }else if(newreplpointer==replnuminexp.get(newexperpointer)){
                                    newreplpointer=1;
                                    newexperpointer++;
                                }
                            }

                        }
                        for(int l=0;l<avgintensities.size();l++){
                            rowValues[l+1]=avgintensities.get(l);
                        }
                        Row newrow = RowFactory.create(rowValues);
                        return newrow;
                    }else{
                        return originalrow;
                    }
                }, RowEncoder.apply(filenameavgpeaks.schema()));
                
            }else if(missval.equals("replacewithmedian")){
                newfilenameavgpeaks=filenameavgpeaks.map((MapFunction<Row, Row>) originalrow -> {
                    if(originalrow.anyNull()){
                        int originalrowsizeofdoubles = originalrow.length()-1;
                        List<Double> medianlist = new ArrayList<>();
                        
                        Object[] rowValues = new Object[originalrow.length()];
                        rowValues[0]=originalrow.get(0).toString();
                        
                        String[] temporal = originalrow.schema().fieldNames();
                        String[] namesofrow = new String[originalrow.length()];
                        List<Integer> replnuminexp=new ArrayList<>();
                        int replnuminexpcounter=0;

                        List<Double> medianintensities = new ArrayList<>();
                        List<Double> rowmedian = new ArrayList<>();
                        
                        
                        for(int k=0;k<originalrowsizeofdoubles;k++){
                            String[] namesofrowtemp = temporal[k+1].split("__");
                            namesofrow[k]=namesofrowtemp[0];
                        }
                        for(int i=0;i<originalrowsizeofdoubles;i++){
                            replnuminexpcounter++;
                            if(!namesofrow[i].equals(namesofrow[i+1])){
                                replnuminexp.add(replnuminexpcounter);
                                replnuminexpcounter=0;
                            }
                        }
                                                
                        int experpointer=0;
                        int replpointer=1;
                        for(int k=1;k<=originalrowsizeofdoubles;k++){
                            if( replpointer<=replnuminexp.get(experpointer)){
                                if(originalrow.isNullAt(k)){
                                    medianintensities.add(null);
                                }else{
                                    rowmedian.add(originalrow.getDouble(k));
                                    medianintensities.add(originalrow.getDouble(k));
                                }
                                if(replpointer!=replnuminexp.get(experpointer)){
                                    replpointer++;
                                }else if(replpointer==replnuminexp.get(experpointer)){
                                    
                                    int rowmediannum=rowmedian.size();
                                    Double[] sortedmedianintensities= new Double[rowmediannum];

                                    for(int i=0;i<rowmediannum;i++){
                                        sortedmedianintensities[i]=rowmedian.get(i);
                                    }
                                    Arrays.sort(sortedmedianintensities);
                                    if (rowmediannum ==1){
                                        medianlist.add(sortedmedianintensities[0]);
                                    }else if(rowmediannum>1){
                                        if(rowmediannum % 2 == 0){
                                            int pos1=(rowmediannum-1)/2;
                                            int pos2=pos1+1;

                                            medianlist.add((sortedmedianintensities[pos1]+sortedmedianintensities[pos2])/2);
                                        }else{
                                            int pos = (rowmediannum/2);
                                            medianlist.add(sortedmedianintensities[pos]);
                                        }
                                    }else if(rowmediannum==0){
                                            medianlist.add(Double.valueOf(0));
                                    }
                                    experpointer++;
                                    replpointer=1;
                                    rowmedian = new ArrayList<>();
                                }
                            }
                        }
                        int newexperpointer=0;
                        int newreplpointer=1;
                        for(int i=0;i<originalrowsizeofdoubles;i++){
                            if(medianintensities.get(i)==null){
                                if(newreplpointer<replnuminexp.get(newexperpointer)){
                                    medianintensities.set(i, medianlist.get(newexperpointer));
                                    newreplpointer++;
                                }else if(newreplpointer==replnuminexp.get(newexperpointer)){
                                    medianintensities.set(i, medianlist.get(newexperpointer));
                                    newreplpointer=1;
                                    newexperpointer++;
                                }
                            }else{
                                if(newreplpointer<replnuminexp.get(newexperpointer)){
                                    newreplpointer++;
                                }else if(newreplpointer==replnuminexp.get(newexperpointer)){
                                    newreplpointer=1;
                                    newexperpointer++;
                                }
                            }

                        }
                        
                        for(int l=0;l<medianintensities.size();l++){
                            rowValues[l+1]=medianintensities.get(l);
                        }
                        Row newrow = RowFactory.create(rowValues);
                        return newrow;
                    }else{
                        return originalrow;
                    }
                }, RowEncoder.apply(filenameavgpeaks.schema()));
            }
            
            Dataset<Row>normalonmtbmeans =sp.emptyDataFrame();
            if(normval.equals("normvalmeanmtb")){
                normalonmtbmeans = newfilenameavgpeaks.map((MapFunction<Row, Row>) originalrow -> {
                    Object[] rowValues = new Object[originalrow.length()];
                    rowValues[0]=originalrow.get(0).toString();
                    int originalrowsizeofdoubles=originalrow.length()-1;
                    Double tomean = Double.valueOf(0);
                    Double stdofrow = Double.valueOf(0);
                    
                    for(int k=1;k<=originalrowsizeofdoubles;k++){
                            tomean=tomean+originalrow.getDouble(k);
                    }
                    Double meanofrow = tomean/originalrowsizeofdoubles;
                    
                    Double sqrdis = Double.valueOf(0);
                    for(int k=1;k<=originalrowsizeofdoubles;k++){
                        Double tmpdis=originalrow.getDouble(k)-meanofrow;
                        sqrdis=sqrdis+(tmpdis*tmpdis);
                    }
                    stdofrow=Math.sqrt(sqrdis/originalrowsizeofdoubles);
                    
                    for(int k=1;k<=originalrowsizeofdoubles;k++){
                        rowValues[k]=(originalrow.getDouble(k)-meanofrow)/stdofrow;
                    }
                    Row newrow = RowFactory.create(rowValues);
                    return newrow;
                }, RowEncoder.apply(newfilenameavgpeaks.schema()));
            }else{
                normalonmtbmeans=newfilenameavgpeaks;
            }
            
            VectorAssembler assemblerexp = new VectorAssembler()
                    .setInputCols(expincols)
                    .setOutputCol("intensity");
            
            filenameoutput = assemblerexp.transform(normalonmtbmeans);
            Long numberofmtbs = filenameoutput.count();
            nrmtbsexps = "PCA is based on "+numberofmtbs+" metabolites, "+(filenameoutput.columns().length-2)+" replicates and "+experids.size()+" experiments.";
            String pcainputcol = "";
            if(numberofmtbs>1){
                //Normalization
                Dataset<Row> scaledData = sp.emptyDataFrame();
                if(normval.equals("normvalmean")){
                    StandardScaler scaler = new StandardScaler()
                            .setInputCol("intensity")
                            .setOutputCol("normintensity")
                            .setWithStd(true)
                            .setWithMean(true);

                    StandardScalerModel scalerModel = scaler.fit(filenameoutput);

                    scaledData = scalerModel.transform(filenameoutput);
                    pcainputcol="normintensity";
                }else{
                    scaledData=filenameoutput;
                    pcainputcol="intensity";
                }
                
                PCAModel pcaexp = new PCA()
                        .setInputCol(pcainputcol)
                        .setOutputCol("pcaFeatures")
                        .setK(2)
                        .fit(scaledData);
                
                Double[] variancereplicates = new Double[pcaexp.explainedVariance().size()];
                for (int i = 0; i < pcaexp.explainedVariance().size(); i++) {
                    variancereplicates[i] = Math.round(pcaexp.explainedVariance().apply(i)*100000)/1000d;

                }

                Double[][] pcareplicates=new Double[pcaexp.pc().numRows()][pcaexp.pc().numCols()];
                for (int i = 0; i < pcaexp.pc().numRows(); i++) {
                    for (int j = 0; j < pcaexp.pc().numCols(); j++) {
                        pcareplicates[i][j]=pcaexp.pc().apply(i,j);
                    }
                }

                pcsarray.add(pcareplicates);
                variancesarray.add(variancereplicates);
            }else if(numberofmtbs<=1){
                rowflag=2;
            }
            filenameavgpeaks.unpersist();
        }else{
            Dataset<Row> dfmetabolitelist = sp.createDataset(metabolitelist, Encoders.STRING()).toDF("description");
            Dataset<Row> metlistinitial0 =sp.emptyDataFrame();
            if(ionmode.equals("both")){
                Encoder<ExperimentFormOnlyParKey> ExpEncoder = Encoders.bean(ExperimentFormOnlyParKey.class);
                Encoder<ExperimentFormExDesFlIn> ExpEncoderoffour = Encoders.bean(ExperimentFormExDesFlIn.class);
                Dataset<ExperimentFormOnlyParKey> dfexplistoriginal = sp.createDataset(experids, Encoders.STRING()).toDF("experimentid").as(ExpEncoder);
                CassandraJavaPairRDD<ExperimentFormOnlyParKey, ExperimentFormExDesFlIn> predf = CassandraJavaUtil.javaFunctions(CassandraJavaUtil.javaFunctions(dfexplistoriginal.javaRDD()).repartitionByCassandraReplica("mdb","experiment",100,CassandraJavaUtil.someColumns("experimentid"),CassandraJavaUtil.mapToRow(ExperimentFormOnlyParKey.class)))
                                                                                    .joinWithCassandraTable("mdb","experiment", CassandraJavaUtil.someColumns("experimentid","filename","description","intensity"), CassandraJavaUtil.someColumns("experimentid"), CassandraJavaUtil.mapRowTo(ExperimentFormExDesFlIn.class), CassandraJavaUtil.mapToRow(ExperimentFormOnlyParKey.class));
                metlistinitial0 =sp.createDataset(predf.map(x->x._2).rdd(), ExpEncoderoffour).toDF( "description","experimentid","filename","intensity").filter(col("description").notEqual("Unidentified"));
            }else if(ionmode.equals("positive")){
                Encoder<ExperimentFormOnlyParKey> ExpEncoder = Encoders.bean(ExperimentFormOnlyParKey.class);
                Encoder<ExperimentFormExDesFlInAd> ExpEncoderofthree = Encoders.bean(ExperimentFormExDesFlInAd.class);
                Dataset<ExperimentFormOnlyParKey> dfexplistoriginal = sp.createDataset(experids, Encoders.STRING()).toDF("experimentid").as(ExpEncoder);
                CassandraJavaPairRDD<ExperimentFormOnlyParKey, ExperimentFormExDesFlInAd> predf = CassandraJavaUtil.javaFunctions(CassandraJavaUtil.javaFunctions(dfexplistoriginal.javaRDD()).repartitionByCassandraReplica("mdb","experiment",100,CassandraJavaUtil.someColumns("experimentid"),CassandraJavaUtil.mapToRow(ExperimentFormOnlyParKey.class)))
                                                                                    .joinWithCassandraTable("mdb","experiment", CassandraJavaUtil.someColumns("experimentid","filename","description","adduct","intensity"), CassandraJavaUtil.someColumns("experimentid"), CassandraJavaUtil.mapRowTo(ExperimentFormExDesFlInAd.class), CassandraJavaUtil.mapToRow(ExperimentFormOnlyParKey.class));
                Dataset<Row> metlistinitialbef =sp.createDataset(predf.map(x->x._2).rdd(), ExpEncoderofthree).toDF( "adduct","description","experimentid","filename","intensity").filter(col("description").notEqual("Unidentified"));
                Dataset<Row> metlistinitialbe= metlistinitialbef.withColumn("ioniz", functions.substring(col("adduct"), -1, 1)).drop("adduct").filter(col("ioniz").equalTo("+"));
                metlistinitial0 = metlistinitialbe.drop(col("ioniz"));
            }else if(ionmode.equals("negative")){
                Encoder<ExperimentFormOnlyParKey> ExpEncoder = Encoders.bean(ExperimentFormOnlyParKey.class);
                Encoder<ExperimentFormExDesFlInAd> ExpEncoderofthree = Encoders.bean(ExperimentFormExDesFlInAd.class);
                Dataset<ExperimentFormOnlyParKey> dfexplistoriginal = sp.createDataset(experids, Encoders.STRING()).toDF("experimentid").as(ExpEncoder); 
                CassandraJavaPairRDD<ExperimentFormOnlyParKey, ExperimentFormExDesFlInAd> predf = CassandraJavaUtil.javaFunctions(CassandraJavaUtil.javaFunctions(dfexplistoriginal.javaRDD()).repartitionByCassandraReplica("mdb","experiment",100,CassandraJavaUtil.someColumns("experimentid"),CassandraJavaUtil.mapToRow(ExperimentFormOnlyParKey.class)))
                                                                                    .joinWithCassandraTable("mdb","experiment", CassandraJavaUtil.someColumns("experimentid","filename","description","adduct","intensity"), CassandraJavaUtil.someColumns("experimentid"), CassandraJavaUtil.mapRowTo(ExperimentFormExDesFlInAd.class), CassandraJavaUtil.mapToRow(ExperimentFormOnlyParKey.class));
                Dataset<Row> metlistinitialbef =sp.createDataset(predf.map(x->x._2).rdd(), ExpEncoderofthree).toDF( "adduct","description","experimentid","filename","intensity").filter(col("description").notEqual("Unidentified"));
                Dataset<Row> metlistinitialbe= metlistinitialbef.withColumn("ioniz", functions.substring(col("adduct"), -1, 1)).drop("adduct").filter(col("ioniz").equalTo("-"));
                metlistinitial0 = metlistinitialbe.drop(col("ioniz"));
            }
            
            Dataset<Row> filenameoutput = null;
            Dataset<Row> newfilenameavgpeaks=sp.emptyDataFrame();
            Dataset<Row> supernewmetfinalpeaksafterrename=sp.emptyDataFrame();
            
            String[] expincols;
            if(missval.equals("omit")){
                Dataset<Row> metlistinitial = metlistinitial0.join(dfmetabolitelist,"description");
                Dataset<Row>filenameavgpeaks = sp.emptyDataFrame();
                if(normval.equals("normvalmean")|| normval.equals("no")||normval.equals("normvalmeanmtb")){
                    Dataset<Row>metfinalpeaks = metlistinitial.groupBy(metlistinitial.col("experimentid"),metlistinitial.col("description"),metlistinitial.col("filename")).avg("intensity").orderBy(asc("experimentid"), asc("description"));
                    Dataset<Row>newmetfinalpeaks = metfinalpeaks.withColumn("expidfilename", functions.concat(col("experimentid"), lit("__"), col("filename"))).drop("experimentid","filename");
                    filenameavgpeaks = newmetfinalpeaks.groupBy(newmetfinalpeaks.col("description")).pivot("expidfilename").mean("avg(intensity)");
                }else if(normval.equals("normvalorig")){//or       
                    //this is z-score on description and experiment
                    Dataset<Row> tmp1 = metlistinitial.groupBy(col("experimentid"),metlistinitial.col("filename"),col("description")).agg(functions.mean("intensity"),functions.stddev("intensity")).withColumnRenamed("experimentid", "expid").withColumnRenamed("filename", "flname").withColumnRenamed("description", "descri");
                    Dataset<Row> tmpexpgroups2 = metlistinitial.join(tmp1,metlistinitial.col("experimentid").equalTo(tmp1.col("expid")).and(metlistinitial.col("filename").equalTo(tmp1.col("flname"))).and(metlistinitial.col("description").equalTo(tmp1.col("descri")))).withColumn("zscore",((col("intensity").$minus(col("avg(intensity)"))).$div(col("stddev_samp(intensity)")))).na().fill(0);
                    Dataset<Row> finalexp = tmpexpgroups2.drop("intensity","avg(intensity)","stddev_samp(intensity)","expid","descri","flname").withColumnRenamed("zscore", "avg(intensity)");
                    filenameavgpeaks = finalexp.withColumn("expidfilename", functions.concat(col("experimentid"), lit("__"), col("filename"))).drop("experimentid","filename").groupBy(finalexp.col("description")).pivot("expidfilename").mean("avg(intensity)");//.persist();
                }
                
                String[] temp1 = filenameavgpeaks.columns();
                List<String>temprepl = new ArrayList<>();

                for(String finame:temp1){
                    if(!finame.equals("description")){
                        String temp = finame.replaceAll("\\.[mzmlxdatMZMLXDAT]{4,6}$","");
                        if(temp.contains(".")){
                            temp=temp.replaceAll("[.]", "_");
                        }
                        temprepl.add(temp);
                        filenameavgpeaks=filenameavgpeaks.withColumnRenamed(finame, temp);
                    }
                }
                replinames=temprepl;
                expincols = replinames.parallelStream().toArray(String[]::new);
                newfilenameavgpeaks=filenameavgpeaks;
            }else{
                Dataset<Row>newmetfinalpeaks = sp.emptyDataFrame();
                if(normval.equals("normvalmean")|| normval.equals("no")||normval.equals("normvalmeanmtb")){
                    Dataset<Row>metfinalpeaks = metlistinitial0.groupBy(metlistinitial0.col("experimentid"),metlistinitial0.col("description"),metlistinitial0.col("filename")).avg("intensity").orderBy(asc("experimentid"), asc("description"));
                    newmetfinalpeaks = metfinalpeaks.withColumn("expidfilename", functions.concat(col("experimentid"), lit("__"), col("filename"))).drop("experimentid","filename");
                }else if(normval.equals("normvalorig")){//or       
                    //this is z-score on description and experiment
                    Dataset<Row> tmp1 = metlistinitial0.groupBy(col("experimentid"),metlistinitial0.col("filename"),col("description")).agg(functions.mean("intensity"),functions.stddev("intensity")).withColumnRenamed("experimentid", "expid").withColumnRenamed("filename", "flname").withColumnRenamed("description", "descri");
                    Dataset<Row> tmpexpgroups2 = metlistinitial0.join(tmp1,metlistinitial0.col("experimentid").equalTo(tmp1.col("expid")).and(metlistinitial0.col("filename").equalTo(tmp1.col("flname"))).and(metlistinitial0.col("description").equalTo(tmp1.col("descri")))).withColumn("zscore",((col("intensity").$minus(col("avg(intensity)"))).$div(col("stddev_samp(intensity)")))).na().fill(0);
                    Dataset<Row> finalexp = tmpexpgroups2.drop("intensity","avg(intensity)","stddev_samp(intensity)","expid","descri","flname").withColumnRenamed("zscore", "avg(intensity)");
                    newmetfinalpeaks = finalexp.withColumn("expidfilename", functions.concat(col("experimentid"), lit("__"), col("filename"))).drop("experimentid","filename");//.persist();
                }
                
                Dataset<Row> newmetfinalpeaksafterrename=newmetfinalpeaks.map((MapFunction<Row, Row>) originalrow -> {
                    String replicatewithdot = originalrow.get(2).toString();
                    String temp = replicatewithdot.replaceAll("\\.[mzmlxdatMZMLXDAT]{4,6}$","");
                    if(temp.contains(".")){
                                temp=temp.replaceAll("[.]", "_");
                            }
                    Object[] rowValues = new Object[originalrow.length()];
                    rowValues[0]=originalrow.get(0);
                    rowValues[1]=originalrow.get(1);
                    rowValues[2]=temp;
                    Row newrow = RowFactory.create(rowValues);
                    return newrow;
                }, RowEncoder.apply(newmetfinalpeaks.schema())).persist();
                
                Dataset<Row>filenameavgpeaks = newmetfinalpeaksafterrename.groupBy(newmetfinalpeaksafterrename.col("description")).pivot("expidfilename").mean("avg(intensity)");
                
            
                String[] temp1 = filenameavgpeaks.columns();
                List<String>temprepl = new ArrayList<>();
                for(String finame:temp1){
                    if(!finame.equals("description")){
                        
                        temprepl.add(finame);
                    }
                }
                
                replinames=temprepl;
                expincols = replinames.parallelStream().toArray(String[]::new);

                //finding missing metabolites
                StructType schema = new StructType(new StructField[] {
                            new StructField("description",DataTypes.StringType, false,Metadata.empty()),
                            new StructField("expidfilename",DataTypes.createArrayType(DataTypes.StringType, true), false,Metadata.empty())});
                String[] columnnames=filenameavgpeaks.columns();
                int mtbswithallnullms=filenameavgpeaks.columns().length;
                
                Dataset<Row>tempmissvalmtbs=filenameavgpeaks.map((MapFunction<Row, Row>) originalrow -> {
                    if(originalrow.anyNull() && metabolitelist.contains(originalrow.get(0).toString())){
                        List<String>tmpexpidfilename=new ArrayList<>();
                        for(int i=1;i<mtbswithallnullms;i++){
                            if(originalrow.isNullAt(i)){
                                tmpexpidfilename.add(columnnames[i]);
                            }
                        }
                        String mtbname = originalrow.get(0).toString();
                        Row newrow = RowFactory.create(mtbname,tmpexpidfilename.toArray(new String[0]));
                        return newrow;
                    }else{
                        String mtbname = "notmissing";
                        List<String>tmpexpidfilename=new ArrayList<>();
                        Row newrow = RowFactory.create(mtbname,tmpexpidfilename.toArray(new String[0]));
                        return newrow;
                    }
                }, RowEncoder.apply(schema)).persist(); 
                Dataset<Row> missvalmetabs=tempmissvalmtbs.select("description").filter(col("description").notEqual("notmissing"));
                missvalmtbs=missvalmetabs.as(Encoders.STRING()).collectAsList();
                
                Dataset<Row> nullmtbs = tempmissvalmtbs.withColumn("expidfilename", functions.explode_outer(col("expidfilename"))).withColumn("avg(intensity)", functions.lit(null).cast(DataTypes.DoubleType)).select(col("description"),col("avg(intensity)"),col("expidfilename"));
                Dataset<Row> tmpsupernewmetfinalpeaksafterrename=newmetfinalpeaksafterrename.union(nullmtbs);
                supernewmetfinalpeaksafterrename=tmpsupernewmetfinalpeaksafterrename.join(dfmetabolitelist,"description");
                Dataset<Row>expavgpeaks = supernewmetfinalpeaksafterrename.groupBy(supernewmetfinalpeaksafterrename.col("description")).pivot("expidfilename").mean("avg(intensity)");
                tempmissvalmtbs.unpersist();
                newmetfinalpeaksafterrename.unpersist();
                if(missval.equals("replacewithzero")){
                    newfilenameavgpeaks=expavgpeaks.na().fill(0);
                }else if(missval.equals("replacewithmean")){
                    newfilenameavgpeaks=expavgpeaks.map((MapFunction<Row, Row>) originalrow -> {
                        if(originalrow.anyNull()){
                            int originalrowsizeofdoubles = originalrow.length()-1;
                            Double sumrow = Double.valueOf(0);
                            List<Double> meanrow = new ArrayList<>();

                            Object[] rowValues = new Object[originalrow.length()];
                            rowValues[0]=originalrow.get(0).toString();

                            String[] temporal = originalrow.schema().fieldNames();
                            String[] namesofrow = new String[originalrow.length()];
                            List<Integer> replnuminexp=new ArrayList<>();
                            int replnuminexpcounter=0;
                            int replnuminexpcounterwithvalidvalues=0;
                            List<Double> avgintensities = new ArrayList<>();

                            for(int k=0;k<originalrowsizeofdoubles;k++){
                                String[] namesofrowtemp = temporal[k+1].split("__");
                                namesofrow[k]=namesofrowtemp[0];
                            }
                            for(int i=0;i<originalrowsizeofdoubles;i++){
                                replnuminexpcounter++;
                                if(!namesofrow[i].equals(namesofrow[i+1])){
                                    replnuminexp.add(replnuminexpcounter);
                                    replnuminexpcounter=0;
                                }
                            }
                            int experpointer=0;
                            int replpointer=1;
                            for(int k=1;k<=originalrowsizeofdoubles;k++){
                                if( replpointer<=replnuminexp.get(experpointer)){
                                    if(originalrow.isNullAt(k)){
                                        avgintensities.add(null);
                                    }else{
                                        replnuminexpcounterwithvalidvalues++;
                                        sumrow = sumrow + originalrow.getDouble(k);
                                        avgintensities.add(originalrow.getDouble(k));
                                    }
                                    if(replpointer!=replnuminexp.get(experpointer)){
                                        replpointer++;
                                    }else if(replpointer==replnuminexp.get(experpointer)){
                                        if(replnuminexpcounterwithvalidvalues>0){
                                            meanrow.add(sumrow/replnuminexpcounterwithvalidvalues);
                                        }else{
                                            meanrow.add(Double.valueOf(0));
                                        }
                                        experpointer++;
                                        replpointer=1;
                                        replnuminexpcounterwithvalidvalues=0;
                                        sumrow = Double.valueOf(0);
                                    }
                                }
                            }
                            int newexperpointer=0;
                            int newreplpointer=1;
                            for(int i=0;i<originalrowsizeofdoubles;i++){
                                if(avgintensities.get(i)==null){
                                    if(newreplpointer<replnuminexp.get(newexperpointer)){
                                        avgintensities.set(i, meanrow.get(newexperpointer));
                                        newreplpointer++;
                                    }else if(newreplpointer==replnuminexp.get(newexperpointer)){
                                        avgintensities.set(i, meanrow.get(newexperpointer));
                                        newreplpointer=1;
                                        newexperpointer++;
                                    }
                                }else{
                                    if(newreplpointer<replnuminexp.get(newexperpointer)){
                                        newreplpointer++;
                                    }else if(newreplpointer==replnuminexp.get(newexperpointer)){
                                        newreplpointer=1;
                                        newexperpointer++;
                                    }
                                }

                            }
                            for(int l=0;l<avgintensities.size();l++){
                                rowValues[l+1]=avgintensities.get(l);
                            }
                            Row newrow = RowFactory.create(rowValues);
                            return newrow;
                        }else{
                            return originalrow;
                        }
                    }, RowEncoder.apply(expavgpeaks.schema()));
                }else if(missval.equals("replacewithmedian")){
                    newfilenameavgpeaks=expavgpeaks.map((MapFunction<Row, Row>) originalrow -> {
                        if(originalrow.anyNull()){
                            int originalrowsizeofdoubles = originalrow.length()-1;
                            List<Double> medianlist = new ArrayList<>();

                            Object[] rowValues = new Object[originalrow.length()];
                            rowValues[0]=originalrow.get(0).toString();

                            String[] temporal = originalrow.schema().fieldNames();
                            String[] namesofrow = new String[originalrow.length()];
                            List<Integer> replnuminexp=new ArrayList<>();
                            int replnuminexpcounter=0;

                            List<Double> medianintensities = new ArrayList<>();
                            List<Double> rowmedian = new ArrayList<>();


                            for(int k=0;k<originalrowsizeofdoubles;k++){
                                String[] namesofrowtemp = temporal[k+1].split("__");
                                namesofrow[k]=namesofrowtemp[0];
                            }
                            for(int i=0;i<originalrowsizeofdoubles;i++){
                                replnuminexpcounter++;
                                if(!namesofrow[i].equals(namesofrow[i+1])){
                                    replnuminexp.add(replnuminexpcounter);
                                    replnuminexpcounter=0;
                                }
                            }

                            int experpointer=0;
                            int replpointer=1;
                            for(int k=1;k<=originalrowsizeofdoubles;k++){
                                if( replpointer<=replnuminexp.get(experpointer)){
                                    if(originalrow.isNullAt(k)){
                                        medianintensities.add(null);
                                    }else{
                                        rowmedian.add(originalrow.getDouble(k));
                                        medianintensities.add(originalrow.getDouble(k));
                                    }
                                    if(replpointer!=replnuminexp.get(experpointer)){
                                        replpointer++;
                                    }else if(replpointer==replnuminexp.get(experpointer)){

                                        int rowmediannum=rowmedian.size();
                                        Double[] sortedmedianintensities= new Double[rowmediannum];

                                        for(int i=0;i<rowmediannum;i++){
                                            sortedmedianintensities[i]=rowmedian.get(i);
                                        }
                                        Arrays.sort(sortedmedianintensities);
                                        if (rowmediannum ==1){
                                            medianlist.add(sortedmedianintensities[0]);
                                        }else if(rowmediannum>1){
                                            if(rowmediannum % 2 == 0){
                                                int pos1=(rowmediannum-1)/2;
                                                int pos2=pos1+1;

                                                medianlist.add((sortedmedianintensities[pos1]+sortedmedianintensities[pos2])/2);
                                            }else{
                                                int pos = (rowmediannum/2);
                                                medianlist.add(sortedmedianintensities[pos]);
                                            }
                                        }else if(rowmediannum==0){
                                                medianlist.add(Double.valueOf(0));
                                        }
                                        experpointer++;
                                        replpointer=1;
                                        rowmedian = new ArrayList<>();
                                    }
                                }
                            }
                            int newexperpointer=0;
                            int newreplpointer=1;
                            for(int i=0;i<originalrowsizeofdoubles;i++){
                                if(medianintensities.get(i)==null){
                                    if(newreplpointer<replnuminexp.get(newexperpointer)){
                                        medianintensities.set(i, medianlist.get(newexperpointer));
                                        newreplpointer++;
                                    }else if(newreplpointer==replnuminexp.get(newexperpointer)){
                                        medianintensities.set(i, medianlist.get(newexperpointer));
                                        newreplpointer=1;
                                        newexperpointer++;
                                    }
                                }else{
                                    if(newreplpointer<replnuminexp.get(newexperpointer)){
                                        newreplpointer++;
                                    }else if(newreplpointer==replnuminexp.get(newexperpointer)){
                                        newreplpointer=1;
                                        newexperpointer++;
                                    }
                                }

                            }

                            for(int l=0;l<medianintensities.size();l++){
                                rowValues[l+1]=medianintensities.get(l);
                            }
                            Row newrow = RowFactory.create(rowValues);
                            return newrow;
                        }else{
                            return originalrow;
                        }
                    }, RowEncoder.apply(expavgpeaks.schema()));
                }

            }

            Dataset<Row>normalonmtbmeans =sp.emptyDataFrame();
            if(normval.equals("normvalmeanmtb")){
                normalonmtbmeans = newfilenameavgpeaks.map((MapFunction<Row, Row>) originalrow -> {
                    Object[] rowValues = new Object[originalrow.length()];
                    rowValues[0]=originalrow.get(0).toString();
                    int originalrowsizeofdoubles=originalrow.length()-1;
                    Double tomean = Double.valueOf(0);
                    Double stdofrow = Double.valueOf(0);
                    
                    for(int k=1;k<=originalrowsizeofdoubles;k++){
                            tomean=tomean+originalrow.getDouble(k);
                    }
                    Double meanofrow = tomean/originalrowsizeofdoubles;
                    
                    Double sqrdis = Double.valueOf(0);
                    for(int k=1;k<=originalrowsizeofdoubles;k++){
                        Double tmpdis=originalrow.getDouble(k)-meanofrow;
                        sqrdis=sqrdis+(tmpdis*tmpdis);
                    }
                    stdofrow=Math.sqrt(sqrdis/originalrowsizeofdoubles);
                    
                    for(int k=1;k<=originalrowsizeofdoubles;k++){
                        rowValues[k]=(originalrow.getDouble(k)-meanofrow)/stdofrow;
                    }
                    Row newrow = RowFactory.create(rowValues);
                    return newrow;
                }, RowEncoder.apply(newfilenameavgpeaks.schema()));
            }else{
                normalonmtbmeans=newfilenameavgpeaks;
            }
            
            VectorAssembler assemblerexp = new VectorAssembler()
                    .setInputCols(expincols)
                    .setOutputCol("intensity");
            
            filenameoutput = assemblerexp.transform(normalonmtbmeans);
            
            Integer numberofmtbs = metabolitelist.size();
            nrmtbsexps = "PCA is based on "+numberofmtbs+" metabolites, "+(filenameoutput.columns().length-2)+" replicates and "+experids.size()+" experiments.";
            String pcainputcol="";
            //Normalization
            Dataset<Row> scaledData = sp.emptyDataFrame();
            if(normval.equals("normvalmean")){
                StandardScaler scaler = new StandardScaler()
                        .setInputCol("intensity")
                        .setOutputCol("normintensity")
                        .setWithStd(true)
                        .setWithMean(true);

                StandardScalerModel scalerModel = scaler.fit(filenameoutput);

                scaledData = scalerModel.transform(filenameoutput);
                pcainputcol="normintensity";
            }else{
                scaledData=filenameoutput;
                pcainputcol="intensity";
            }
            
            PCAModel pcaexp = new PCA()
                    .setInputCol(pcainputcol)
                    .setOutputCol("pcaFeatures")
                    .setK(2)
                    .fit(scaledData);

            Double[] variancereplicates = new Double[pcaexp.explainedVariance().size()];
            for (int i = 0; i < pcaexp.explainedVariance().size(); i++) {
                variancereplicates[i] = Math.round(pcaexp.explainedVariance().apply(i)*100000)/1000d;

            }

            Double[][] pcareplicates=new Double[pcaexp.pc().numRows()][pcaexp.pc().numCols()];
            for (int i = 0; i < pcaexp.pc().numRows(); i++) {
                for (int j = 0; j < pcaexp.pc().numCols(); j++) {
                    pcareplicates[i][j]=pcaexp.pc().apply(i,j);
                }
            }

            pcsarray.add(pcareplicates);
            variancesarray.add(variancereplicates);
        }
        
        if(rowflag==0){
            replicateslistpca.setReplilist(replinames);
            replicateslistpca.setReplicpca(pcsarray);
            replicateslistpca.setReplicvariance(variancesarray);
            replicateslistpca.setMissvalmtbs(missvalmtbs);
            replicateslistpca.setNrmtbsexps(nrmtbsexps);
        }else{
            replicateslistpca.setRowflag(2);
        }
        return replicateslistpca;
    }
    
    
    
    public Metaboliteslistpca metabolitespca(List<String> experimentlist,List<String> metabolitelist, String missval,String normval,String ionmode){
        
        String nrmtbsexps = new String();
        
        Metaboliteslistpca metaboliteslistpca = new Metaboliteslistpca();
        Collections.sort(metabolitelist);
        List<String> metlist = metabolitelist;
        Dataset<Row> dfmetabolitelist = sp.createDataset(metlist, Encoders.STRING()).toDF("description");
        List<String> missvalmtbs = new ArrayList<>();
        List<Double[][]> pcsarray= new ArrayList<>();
        List<Double[]> variancesarray = new ArrayList<>();
        
        Dataset<Row> metlistinitial2 =sp.emptyDataFrame();
            if(ionmode.equals("both")){
                Encoder<ExperimentFormOnlyParKey> ExpEncoder = Encoders.bean(ExperimentFormOnlyParKey.class);
                Encoder<ExperimentFormExDesIn> ExpEncoderofthree = Encoders.bean(ExperimentFormExDesIn.class);
                Dataset<ExperimentFormOnlyParKey> dfexplistoriginal = sp.createDataset(experimentlist, Encoders.STRING()).toDF("experimentid").as(ExpEncoder);
                CassandraJavaPairRDD<ExperimentFormOnlyParKey, ExperimentFormExDesIn> predf = CassandraJavaUtil.javaFunctions(CassandraJavaUtil.javaFunctions(dfexplistoriginal.javaRDD()).repartitionByCassandraReplica("mdb","experiment",100,CassandraJavaUtil.someColumns("experimentid"),CassandraJavaUtil.mapToRow(ExperimentFormOnlyParKey.class)))
                                                                                    .joinWithCassandraTable("mdb","experiment", CassandraJavaUtil.someColumns("experimentid","description","intensity"), CassandraJavaUtil.someColumns("experimentid"), CassandraJavaUtil.mapRowTo(ExperimentFormExDesIn.class), CassandraJavaUtil.mapToRow(ExperimentFormOnlyParKey.class));
                metlistinitial2 =sp.createDataset(predf.map(x->x._2).rdd(), ExpEncoderofthree).toDF( "description","experimentid","intensity").filter(col("description").notEqual("Unidentified"));
            }else if(ionmode.equals("positive")){
                Encoder<ExperimentFormOnlyParKey> ExpEncoder = Encoders.bean(ExperimentFormOnlyParKey.class);
                Encoder<ExperimentFormExDesInAd> ExpEncoderofthree = Encoders.bean(ExperimentFormExDesInAd.class);
                Dataset<ExperimentFormOnlyParKey> dfexplistoriginal = sp.createDataset(experimentlist, Encoders.STRING()).toDF("experimentid").as(ExpEncoder); 
                CassandraJavaPairRDD<ExperimentFormOnlyParKey, ExperimentFormExDesInAd> predf = CassandraJavaUtil.javaFunctions(CassandraJavaUtil.javaFunctions(dfexplistoriginal.javaRDD()).repartitionByCassandraReplica("mdb","experiment",100,CassandraJavaUtil.someColumns("experimentid"),CassandraJavaUtil.mapToRow(ExperimentFormOnlyParKey.class)))
                                                                                    .joinWithCassandraTable("mdb","experiment", CassandraJavaUtil.someColumns("experimentid","description","adduct","intensity"), CassandraJavaUtil.someColumns("experimentid"), CassandraJavaUtil.mapRowTo(ExperimentFormExDesInAd.class), CassandraJavaUtil.mapToRow(ExperimentFormOnlyParKey.class));
                Dataset<Row> metlistinitialbef =sp.createDataset(predf.map(x->x._2).rdd(), ExpEncoderofthree).toDF( "adduct","description","experimentid","intensity").filter(col("description").notEqual("Unidentified"));
                Dataset<Row> metlistinitialbe= metlistinitialbef.withColumn("ioniz", functions.substring(col("adduct"), -1, 1)).drop("adduct").filter(col("ioniz").equalTo("+"));
                metlistinitial2 = metlistinitialbe.drop(col("ioniz"));
            }else if(ionmode.equals("negative")){
                Encoder<ExperimentFormOnlyParKey> ExpEncoder = Encoders.bean(ExperimentFormOnlyParKey.class);
                Encoder<ExperimentFormExDesInAd> ExpEncoderofthree = Encoders.bean(ExperimentFormExDesInAd.class);
                Dataset<ExperimentFormOnlyParKey> dfexplistoriginal = sp.createDataset(experimentlist, Encoders.STRING()).toDF("experimentid").as(ExpEncoder); 
                CassandraJavaPairRDD<ExperimentFormOnlyParKey, ExperimentFormExDesInAd> predf = CassandraJavaUtil.javaFunctions(CassandraJavaUtil.javaFunctions(dfexplistoriginal.javaRDD()).repartitionByCassandraReplica("mdb","experiment",100,CassandraJavaUtil.someColumns("experimentid"),CassandraJavaUtil.mapToRow(ExperimentFormOnlyParKey.class)))
                                                                                    .joinWithCassandraTable("mdb","experiment", CassandraJavaUtil.someColumns("experimentid","description","adduct","intensity"), CassandraJavaUtil.someColumns("experimentid"), CassandraJavaUtil.mapRowTo(ExperimentFormExDesInAd.class), CassandraJavaUtil.mapToRow(ExperimentFormOnlyParKey.class));
                Dataset<Row> metlistinitialbef =sp.createDataset(predf.map(x->x._2).rdd(), ExpEncoderofthree).toDF( "adduct","description","experimentid","intensity").filter(col("description").notEqual("Unidentified"));
                Dataset<Row> metlistinitialbe= metlistinitialbef.withColumn("ioniz", functions.substring(col("adduct"), -1, 1)).drop("adduct").filter(col("ioniz").equalTo("-"));
                metlistinitial2 = metlistinitialbe.drop(col("ioniz"));
            }
        
        Dataset<Row> expavgpeaks = sp.emptyDataFrame();
        Dataset<Row> metfinalpeaks = sp.emptyDataFrame();
        if(normval.equals("normvalmean")|| normval.equals("no")|| normval.equals("normvalmeanmtb")){
            metfinalpeaks = metlistinitial2.groupBy(metlistinitial2.col("experimentid"),metlistinitial2.col("description")).avg("intensity").orderBy( asc("description")).persist();
            expavgpeaks = metfinalpeaks.groupBy(metfinalpeaks.col("description")).pivot("experimentid",new ArrayList<Object>(experimentlist)).mean("avg(intensity)");
        }else if(normval.equals("normvalorig")){//or       
        //this is z-score on description and experiment
            Dataset<Row> tmp1 = metlistinitial2.groupBy(col("experimentid"),col("description")).agg(functions.mean("intensity"),functions.stddev("intensity")).withColumnRenamed("experimentid", "expid").withColumnRenamed("description", "descri");
            Dataset<Row> tmpexpgroups2 = metlistinitial2.join(tmp1,metlistinitial2.col("experimentid").equalTo(tmp1.col("expid")).and(metlistinitial2.col("description").equalTo(tmp1.col("descri")))).withColumn("zscore",((col("intensity").$minus(col("avg(intensity)"))).$div(col("stddev_samp(intensity)")))).na().fill(0);
            Dataset<Row> finalexp = tmpexpgroups2.drop("intensity","avg(intensity)","stddev_samp(intensity)","expid","descri").withColumnRenamed("zscore", "avg(intensity)");
            metfinalpeaks = finalexp.groupBy(finalexp.col("experimentid"),finalexp.col("description")).avg("avg(intensity)").withColumnRenamed("avg(avg(intensity))", "avg(intensity)").orderBy( asc("description")).persist();
            expavgpeaks = finalexp.groupBy(finalexp.col("description")).pivot("experimentid",new ArrayList<Object>(experimentlist)).mean("avg(intensity)");//.persist();
        }
        
        Dataset<Row> tempmissvalmtbs = sp.emptyDataFrame();
        Dataset<Row> newfilenameavgpeaks=sp.emptyDataFrame();

        if(missval.equals("omit")){
            //finding missing mtbs
            StructType schema = new StructType(new StructField[] {
                        new StructField("metabs",
                                        DataTypes.StringType, false,
                                        Metadata.empty()) });

            tempmissvalmtbs=expavgpeaks.map((MapFunction<Row, Row>) originalrow -> {
                if(originalrow.anyNull()){
                    String rowValues = new String();
                    rowValues=originalrow.get(0).toString();
                    Row newrow = RowFactory.create(rowValues);
                    return newrow;
                }else{
                    String rowValues = "notmissing";
                    Row newrow = RowFactory.create(rowValues);
                    return newrow;
                }
            }, RowEncoder.apply(schema)).filter(col("metabs").notEqual("notmissing"));
            missvalmtbs=tempmissvalmtbs.as(Encoders.STRING()).collectAsList();
            metaboliteslistpca.setMissvalmtbs(missvalmtbs);
            
            newfilenameavgpeaks = metfinalpeaks.join(dfmetabolitelist,"description");
             
        }else{
            
            //finding missing metabolites
            StructType schema = new StructType(new StructField[] {
                        new StructField("description",DataTypes.StringType, false,Metadata.empty()),
                        new StructField("experimentid",DataTypes.createArrayType(DataTypes.StringType, true), false,Metadata.empty())});
            String[] columnnames=expavgpeaks.columns();
            int mtbswithallnullms=expavgpeaks.columns().length;

            tempmissvalmtbs=expavgpeaks.map((MapFunction<Row, Row>) originalrow -> {
                if(originalrow.anyNull() && metabolitelist.contains(originalrow.get(0).toString())){
                    List<String>tmpexpidfilename=new ArrayList<>();
                    for(int i=1;i<mtbswithallnullms;i++){
                        if(originalrow.isNullAt(i)){
                            tmpexpidfilename.add(columnnames[i]);
                        }
                    }
                    String mtbname = originalrow.get(0).toString();
                    Row newrow = RowFactory.create(mtbname,tmpexpidfilename.toArray(new String[0]));
                    return newrow;
                }else{
                    String mtbname = "notmissing";
                    List<String>tmpexpidfilename=new ArrayList<>();
                    Row newrow = RowFactory.create(mtbname,tmpexpidfilename.toArray(new String[0]));
                    return newrow;
                }
            }, RowEncoder.apply(schema)).persist(); 
            Dataset<Row> missvalmetabs=tempmissvalmtbs.select("description").filter(col("description").notEqual("notmissing"));
            missvalmtbs=missvalmetabs.as(Encoders.STRING()).collectAsList();
            metaboliteslistpca.setMissvalmtbs(missvalmtbs);

            Dataset<Row> nullmtbs = tempmissvalmtbs.filter(col("description").notEqual("notmissing")).withColumn("experimentid", functions.explode_outer(col("experimentid"))).withColumn("avg(intensity)", functions.lit(null).cast(DataTypes.DoubleType)).select(col("experimentid"),col("description"),col("avg(intensity)"));

            Dataset<Row> tmpsupernewmetfinalpeaksafterrename=metfinalpeaks.union(nullmtbs);
            Dataset<Row> supernewmetfinalpeaksafterrename=tmpsupernewmetfinalpeaksafterrename.join(dfmetabolitelist,"description");
            
            if(missval.equals("replacewithzero")){
                newfilenameavgpeaks=supernewmetfinalpeaksafterrename.na().fill(0);
            }else if(missval.equals("replacewithmean")){
                
                Dataset<Row>tmp1newfilenameavgpeaks = supernewmetfinalpeaksafterrename.groupBy(supernewmetfinalpeaksafterrename.col("description")).mean("avg(intensity)");
                Dataset<Row>tmp2newfilenameavgpeaks = supernewmetfinalpeaksafterrename.join(tmp1newfilenameavgpeaks,"description");
                newfilenameavgpeaks=tmp2newfilenameavgpeaks.map((MapFunction<Row, Row>) originalrow -> {
                    if(originalrow.anyNull()){
                        String mtbname = originalrow.get(0).toString();
                        String expid = originalrow.get(1).toString();
                        double inten = Double.parseDouble(originalrow.get(3).toString());
                        Row newrow = RowFactory.create(mtbname,expid,inten);
                        return newrow;
                    }else{
                        String mtbname = originalrow.get(0).toString();
                        String expid = originalrow.get(1).toString();
                        double inten = Double.parseDouble(originalrow.get(2).toString());
                        Row newrow = RowFactory.create(mtbname,expid,inten);
                        return newrow;
                    }
                }, RowEncoder.apply(tmp2newfilenameavgpeaks.schema())).drop("avg(avg(intensity))");
            }else if(missval.equals("replacewithmedian")){
                Dataset<Row>tmp1newfilenameavgpeaks = supernewmetfinalpeaksafterrename.groupBy(supernewmetfinalpeaksafterrename.col("description")).agg(callUDF("percentile_approx", col("avg(intensity)"), lit(0.5)).as("median"));
                Dataset<Row>tmp2newfilenameavgpeaks = supernewmetfinalpeaksafterrename.join(tmp1newfilenameavgpeaks,"description");
                newfilenameavgpeaks=tmp2newfilenameavgpeaks.map((MapFunction<Row, Row>) originalrow -> {
                    if(originalrow.anyNull()){
                        String mtbname = originalrow.get(0).toString();
                        String expid = originalrow.get(1).toString();
                        double inten = Double.parseDouble(originalrow.get(3).toString());
                        Row newrow = RowFactory.create(mtbname,expid,inten);
                        return newrow;
                    }else{
                        String mtbname = originalrow.get(0).toString();
                        String expid = originalrow.get(1).toString();
                        double inten = Double.parseDouble(originalrow.get(2).toString());
                        Row newrow = RowFactory.create(mtbname,expid,inten);
                        return newrow;
                    }
                }, RowEncoder.apply(tmp2newfilenameavgpeaks.schema())).drop("median");
                
            }
        }
        
            Dataset<Row>normalonmtbmeans =sp.emptyDataFrame();
            if(normval.equals("normvalmeanmtb")){
                Dataset<Row> sthto = newfilenameavgpeaks.groupBy(col("description")).agg(functions.mean("avg(intensity)"),functions.stddev("avg(intensity)")).withColumnRenamed("description", "descri");
                Dataset<Row>sthbefore = newfilenameavgpeaks.join(sthto,newfilenameavgpeaks.col("description").equalTo(sthto.col("descri"))).withColumn("zscore",((col("avg(intensity)").$minus(col("avg(avg(intensity))"))).$div(col("stddev_samp(avg(intensity))")))).na().fill(0);
                normalonmtbmeans=sthbefore.drop("avg(intensity)","avg(avg(intensity))","stddev_samp(avg(intensity))","descri").withColumnRenamed("zscore", "intens").groupBy(col("experimentid"),col("description")).mean("intens").withColumnRenamed("avg(intens)", "avg(intensity)");
                
            }else{
                normalonmtbmeans=newfilenameavgpeaks;
            }

        Dataset<Row> dfwithnulls = normalonmtbmeans.groupBy(normalonmtbmeans.col("experimentid")).agg(functions.collect_list(functions.map(col("description"),col("avg(intensity)")))).toDF("experimentid","descinten");
        StructType schema = new StructType(new StructField[] {
                    new StructField("expids",DataTypes.StringType, false,Metadata.empty()),
                    new StructField("intensity",new VectorUDT(),false,Metadata.empty())
        });
        Dataset<Row> expsandvectorsdf=dfwithnulls.map((MapFunction<Row, Row>) originalrow -> {
                String firstpos = new String();
                firstpos=originalrow.get(0).toString();
                List<scala.collection.Map<String,Double>>mplist=originalrow.getList(1);
                int s = mplist.size();
                Map<String,Double>treemp=new TreeMap<>();
                for(int k=0;k<s;k++){
                    Object[] desc = JavaConversions.mapAsJavaMap(mplist.get(k)).keySet().toArray();
                    Object[] kvlist= JavaConversions.mapAsJavaMap(mplist.get(k)).values().toArray();
                    treemp.put(desc[0].toString(),Double.parseDouble(kvlist[0].toString()));
                }
                Object[] tempobj = treemp.values().toArray();
                double[] tmplist= new double[s];
                for(int i=0;i<s;i++){
                    tmplist[i]=Double.parseDouble(tempobj[i].toString());
                }
                Row newrow = RowFactory.create(firstpos,Vectors.dense(tmplist));
                return newrow;

        }, RowEncoder.apply(schema));


        Integer numberofmtbs = metlist.size();
        nrmtbsexps = "PCA is based on "+numberofmtbs+" metabolites and "+experimentlist.size()+" experiments.";
        String pcainputcol="";
        //Normalization
        Dataset<Row> scaledData = sp.emptyDataFrame();
        if(normval.equals("normvalmean")){
            StandardScaler scaler = new StandardScaler()
                    .setInputCol("intensity")
                    .setOutputCol("normintensity")
                    .setWithStd(true)
                    .setWithMean(true);

            StandardScalerModel scalerModel = scaler.fit(expsandvectorsdf);

            scaledData = scalerModel.transform(expsandvectorsdf);
            pcainputcol="normintensity";
        }else{
            scaledData=expsandvectorsdf;
            pcainputcol="intensity";
        }

        PCAModel pcaexp = new PCA()
                .setInputCol(pcainputcol)
                .setOutputCol("pcaFeatures")
                .setK(2)
                .fit(scaledData);//expoutput

        Double[] variancemtbs = new Double[pcaexp.explainedVariance().size()];
        for (int i = 0; i < pcaexp.explainedVariance().size(); i++) {
            variancemtbs[i] = Math.round(pcaexp.explainedVariance().apply(i)*100000)/1000d;
        }
        Double[][] pcaexps=new Double[pcaexp.pc().numRows()][pcaexp.pc().numCols()];
        for (int i = 0; i < pcaexp.pc().numRows(); i++) {
            for (int j = 0; j < pcaexp.pc().numCols(); j++) {
                pcaexps[i][j]=pcaexp.pc().apply(i,j);
            }
        }
        pcsarray.add(pcaexps);
        variancesarray.add(variancemtbs);
        metfinalpeaks.unpersist();
        tempmissvalmtbs.unpersist();
        
        
        metaboliteslistpca.setMtbslist(metlist);
        metaboliteslistpca.setMtbspca(pcsarray);
        metaboliteslistpca.setMtbsvariance(variancesarray);
        metaboliteslistpca.setNrmtbsexps(nrmtbsexps);
        
        return metaboliteslistpca;
    }
    
}

