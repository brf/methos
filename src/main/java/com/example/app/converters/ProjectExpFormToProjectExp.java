/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.example.app.converters;

/**
 *
 * @author kostas
 */
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;

import com.example.app.domain.ProjectExperiment;
import com.example.app.commands.ProjectsExpForm;
/**
 *
 * @author kostas
 */
@Component
public class ProjectExpFormToProjectExp implements Converter<ProjectsExpForm, ProjectExperiment> {

    @Override 
    public ProjectExperiment convert(ProjectsExpForm proexpform) {
        ProjectExperiment projectexp = new ProjectExperiment();
        
        if (proexpform.getProjectExperimentKey() != null && !StringUtils.isEmpty(proexpform.getProjectExperimentKey())) {
            projectexp.setProjectExperimentKey(proexpform.getProjectExperimentKey());
        }
       
        projectexp.setExpname(proexpform.getExpname());
        projectexp.setExpdate(proexpform.getExpdate());
        projectexp.setReplicatesnum(proexpform.getReplicatesnum());
        projectexp.setReplicatesnames(proexpform.getReplicatesnames());
        projectexp.setPreprocessed(proexpform.getPreprocessed());
        projectexp.setIdentified(proexpform.getIdentified());
        projectexp.setUnidentified(proexpform.getUnidentified());
        projectexp.setWorkflow(proexpform.getWorkflow());
        return projectexp;
    }
    
}
