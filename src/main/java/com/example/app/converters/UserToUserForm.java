/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.example.app.converters;

import com.example.app.commands.UserForm;
import com.example.app.domain.User;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

/**
 *
 * @author kostas
 */
@Component
public class UserToUserForm implements Converter<User, UserForm>{
    
    @Override
    public UserForm convert(User user){
        UserForm userform =new UserForm();
        userform.setUsername(user.getUsername());
        userform.setAccountdatecreated(user.getAccountdatecreated());
        userform.setEmail(user.getEmail());
        userform.setPassword(user.getPassword());
        userform.setVerificationcode(user.getVerificationcode());
        userform.setVerifiedacc(user.getVerifiedacc());
        userform.setResetpswcode(user.getResetpaswcode());
        return userform;
    }

}
