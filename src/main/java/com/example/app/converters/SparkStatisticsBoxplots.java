/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.example.app.converters;

import com.datastax.spark.connector.japi.CassandraJavaUtil;
import com.datastax.spark.connector.japi.rdd.CassandraJavaPairRDD;
import com.example.app.commands.ProjectExperimentFormExNa;
import com.example.app.domain.ExperimentFormExDes;
import com.example.app.domain.ExperimentFormExDesAd;
import com.example.app.domain.ExperimentFormExIn;
import com.example.app.domain.ExperimentFormExDesIn;
import com.example.app.domain.ExperimentFormExDesInAd;
import com.example.app.domain.ExperimentFormOnlyParKey;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import org.apache.spark.sql.Dataset;
import org.apache.spark.sql.Encoder;
import org.apache.spark.sql.Encoders;
import org.apache.spark.sql.Row;
import org.apache.spark.sql.SparkSession;
import org.apache.spark.sql.functions;
import static org.apache.spark.sql.functions.asc;
import static org.apache.spark.sql.functions.col;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 *
 * @author kostas
 */
@Component
public class SparkStatisticsBoxplots {
    @Autowired
    private SparkSession sp;
    
    public List<String> allMetInExperiments(List<String> expids,String ionmode){
        List<String> metlist = new ArrayList<>();
        
        Dataset<Row> metlistinitial =sp.emptyDataFrame();
        if(ionmode.equals("both")){
            Encoder<ExperimentFormOnlyParKey> ExpEncoder = Encoders.bean(ExperimentFormOnlyParKey.class);
            Encoder<ExperimentFormExDes> ExpEncoderoftwo = Encoders.bean(ExperimentFormExDes.class);
            Dataset<ExperimentFormOnlyParKey> dfexplistoriginal = sp.createDataset(expids, Encoders.STRING()).toDF("experimentid").as(ExpEncoder);
            CassandraJavaPairRDD<ExperimentFormOnlyParKey, ExperimentFormExDes> predf = CassandraJavaUtil.javaFunctions(CassandraJavaUtil.javaFunctions(dfexplistoriginal.javaRDD()).repartitionByCassandraReplica("mdb","experiment",100,CassandraJavaUtil.someColumns("experimentid"),CassandraJavaUtil.mapToRow(ExperimentFormOnlyParKey.class)))
                                                                                .joinWithCassandraTable("mdb","experiment", CassandraJavaUtil.someColumns("experimentid","description"), CassandraJavaUtil.someColumns("experimentid"), CassandraJavaUtil.mapRowTo(ExperimentFormExDes.class), CassandraJavaUtil.mapToRow(ExperimentFormOnlyParKey.class));
            metlistinitial =sp.createDataset(predf.map(x->x._2).rdd(), ExpEncoderoftwo).toDF( "description","experimentid").filter(col("description").notEqual("Unidentified"));
        }else if(ionmode.equals("positive")){
            Encoder<ExperimentFormOnlyParKey> ExpEncoder = Encoders.bean(ExperimentFormOnlyParKey.class);
            Encoder<ExperimentFormExDesAd> ExpEncoderofthree = Encoders.bean(ExperimentFormExDesAd.class);
            Dataset<ExperimentFormOnlyParKey> dfexplistoriginal = sp.createDataset(expids, Encoders.STRING()).toDF("experimentid").as(ExpEncoder); //description, adduct, experimentid
            CassandraJavaPairRDD<ExperimentFormOnlyParKey, ExperimentFormExDesAd> predf = CassandraJavaUtil.javaFunctions(CassandraJavaUtil.javaFunctions(dfexplistoriginal.javaRDD()).repartitionByCassandraReplica("mdb","experiment",100,CassandraJavaUtil.someColumns("experimentid"),CassandraJavaUtil.mapToRow(ExperimentFormOnlyParKey.class)))
                                                                                .joinWithCassandraTable("mdb","experiment", CassandraJavaUtil.someColumns("experimentid","description","adduct"), CassandraJavaUtil.someColumns("experimentid"), CassandraJavaUtil.mapRowTo(ExperimentFormExDesAd.class), CassandraJavaUtil.mapToRow(ExperimentFormOnlyParKey.class));
            Dataset<Row> metlistinitialbef =sp.createDataset(predf.map(x->x._2).rdd(), ExpEncoderofthree).toDF( "adduct","description","experimentid").filter(col("description").notEqual("Unidentified"));
            Dataset<Row> metlistinitialbe= metlistinitialbef.withColumn("ioniz", functions.substring(col("adduct"), -1, 1)).drop("adduct").filter(col("ioniz").equalTo("+"));
            metlistinitial = metlistinitialbe.drop(col("ioniz"));
        }
        else if(ionmode.equals("negative")){
            Encoder<ExperimentFormOnlyParKey> ExpEncoder = Encoders.bean(ExperimentFormOnlyParKey.class);
            Encoder<ExperimentFormExDesAd> ExpEncoderofthree = Encoders.bean(ExperimentFormExDesAd.class);
            Dataset<ExperimentFormOnlyParKey> dfexplistoriginal = sp.createDataset(expids, Encoders.STRING()).toDF("experimentid").as(ExpEncoder); //description, adduct, experimentid
            CassandraJavaPairRDD<ExperimentFormOnlyParKey, ExperimentFormExDesAd> predf = CassandraJavaUtil.javaFunctions(CassandraJavaUtil.javaFunctions(dfexplistoriginal.javaRDD()).repartitionByCassandraReplica("mdb","experiment",100,CassandraJavaUtil.someColumns("experimentid"),CassandraJavaUtil.mapToRow(ExperimentFormOnlyParKey.class)))
                                                                                .joinWithCassandraTable("mdb","experiment", CassandraJavaUtil.someColumns("experimentid","description","adduct"), CassandraJavaUtil.someColumns("experimentid"), CassandraJavaUtil.mapRowTo(ExperimentFormExDesAd.class), CassandraJavaUtil.mapToRow(ExperimentFormOnlyParKey.class));
            Dataset<Row> metlistinitialbef =sp.createDataset(predf.map(x->x._2).rdd(), ExpEncoderofthree).toDF( "adduct","description","experimentid").filter(col("description").notEqual("Unidentified"));
            Dataset<Row> metlistinitialbe= metlistinitialbef.withColumn("ioniz", functions.substring(col("adduct"), -1, 1)).drop("adduct").filter(col("ioniz").equalTo("-"));
            metlistinitial = metlistinitialbe.drop(col("ioniz"));
        }
        
        metlist = metlistinitial.select(col("description")).distinct().as(Encoders.STRING()).collectAsList();
        return metlist;
    }
    
     public List<ProjectExperimentFormExNa> boxplotsidname(List<String> experimentlist){
         Dataset<Row> dfexplist = sp.createDataset(experimentlist, Encoders.STRING()).toDF("experimentid");
         List<ProjectExperimentFormExNa> boxplots = new ArrayList<>();
         Encoder<ProjectExperimentFormExNa> BoxplotmtbsEncoder1 = Encoders.bean(ProjectExperimentFormExNa.class);
         
         Dataset<Row> mtblistinitial = sp.read().format("org.apache.spark.sql.cassandra")
                .options(new HashMap<String, String>() {
                    {
                        put("keyspace", "mdb");
                        put("table", "projectexperiment");
                    }
                })
                .load().select(col("experimentid"), col("expname")).join(dfexplist,"experimentid");//.groupBy(col("experimentid"),col("description")).avg("intensity").withColumnRenamed("avg(intensity)", "intensity");
         Dataset<ProjectExperimentFormExNa> finalboxplot = mtblistinitial.as(BoxplotmtbsEncoder1);
         boxplots=finalboxplot.collectAsList();
         
         return boxplots;
     }
     
     
     public List<ExperimentFormExIn> boxplotsajax(List<String> experimentlist,String onemetabolite,String transf,String ionmode){

        List<String> dfonemetabolite = new ArrayList<>();
        dfonemetabolite.add(onemetabolite);
        List<ExperimentFormExIn> boxplots = new ArrayList<>();
        Encoder<ExperimentFormExIn> BoxplotmtbsEncoder2 = Encoders.bean(ExperimentFormExIn.class);
        
        Dataset<Row> mtblistinitial =sp.emptyDataFrame();
            if(ionmode.equals("both")){
                Encoder<ExperimentFormOnlyParKey> ExpEncoder = Encoders.bean(ExperimentFormOnlyParKey.class);
                Encoder<ExperimentFormExDesIn> ExpEncoderofthree = Encoders.bean(ExperimentFormExDesIn.class);
                Dataset<ExperimentFormOnlyParKey> dfexplistoriginal = sp.createDataset(experimentlist, Encoders.STRING()).toDF("experimentid").as(ExpEncoder);
                CassandraJavaPairRDD<ExperimentFormOnlyParKey, ExperimentFormExDesIn> predf = CassandraJavaUtil.javaFunctions(CassandraJavaUtil.javaFunctions(dfexplistoriginal.javaRDD()).repartitionByCassandraReplica("mdb","experiment",100,CassandraJavaUtil.someColumns("experimentid"),CassandraJavaUtil.mapToRow(ExperimentFormOnlyParKey.class)))
                                                                                    .joinWithCassandraTable("mdb","experiment", CassandraJavaUtil.someColumns("experimentid","description","intensity"), CassandraJavaUtil.someColumns("experimentid"), CassandraJavaUtil.mapRowTo(ExperimentFormExDesIn.class), CassandraJavaUtil.mapToRow(ExperimentFormOnlyParKey.class));
                mtblistinitial =sp.createDataset(predf.map(x->x._2).rdd(), ExpEncoderofthree).toDF( "description","experimentid","intensity").filter(col("description").isin(dfonemetabolite.toArray()));
            }else if(ionmode.equals("positive")){
                Encoder<ExperimentFormOnlyParKey> ExpEncoder = Encoders.bean(ExperimentFormOnlyParKey.class);
                Encoder<ExperimentFormExDesInAd> ExpEncoderofthree = Encoders.bean(ExperimentFormExDesInAd.class);
                Dataset<ExperimentFormOnlyParKey> dfexplistoriginal = sp.createDataset(experimentlist, Encoders.STRING()).toDF("experimentid").as(ExpEncoder); //description, adduct, experimentid
                CassandraJavaPairRDD<ExperimentFormOnlyParKey, ExperimentFormExDesInAd> predf = CassandraJavaUtil.javaFunctions(CassandraJavaUtil.javaFunctions(dfexplistoriginal.javaRDD()).repartitionByCassandraReplica("mdb","experiment",100,CassandraJavaUtil.someColumns("experimentid"),CassandraJavaUtil.mapToRow(ExperimentFormOnlyParKey.class)))
                                                                                    .joinWithCassandraTable("mdb","experiment", CassandraJavaUtil.someColumns("experimentid","description","adduct","intensity"), CassandraJavaUtil.someColumns("experimentid"), CassandraJavaUtil.mapRowTo(ExperimentFormExDesInAd.class), CassandraJavaUtil.mapToRow(ExperimentFormOnlyParKey.class));
                Dataset<Row> metlistinitialbef =sp.createDataset(predf.map(x->x._2).rdd(), ExpEncoderofthree).toDF( "adduct","description","experimentid","intensity");
                Dataset<Row> metlistinitialbe= metlistinitialbef.withColumn("ioniz", functions.substring(col("adduct"), -1, 1)).drop("adduct").filter(col("ioniz").equalTo("+"));
                mtblistinitial = metlistinitialbe.drop(col("ioniz")).filter(col("description").isin(dfonemetabolite.toArray()));
            }else if(ionmode.equals("negative")){
                Encoder<ExperimentFormOnlyParKey> ExpEncoder = Encoders.bean(ExperimentFormOnlyParKey.class);
                Encoder<ExperimentFormExDesInAd> ExpEncoderofthree = Encoders.bean(ExperimentFormExDesInAd.class);
                Dataset<ExperimentFormOnlyParKey> dfexplistoriginal = sp.createDataset(experimentlist, Encoders.STRING()).toDF("experimentid").as(ExpEncoder); //description, adduct, experimentid
                CassandraJavaPairRDD<ExperimentFormOnlyParKey, ExperimentFormExDesInAd> predf = CassandraJavaUtil.javaFunctions(CassandraJavaUtil.javaFunctions(dfexplistoriginal.javaRDD()).repartitionByCassandraReplica("mdb","experiment",100,CassandraJavaUtil.someColumns("experimentid"),CassandraJavaUtil.mapToRow(ExperimentFormOnlyParKey.class)))
                                                                                    .joinWithCassandraTable("mdb","experiment", CassandraJavaUtil.someColumns("experimentid","description","adduct","intensity"), CassandraJavaUtil.someColumns("experimentid"), CassandraJavaUtil.mapRowTo(ExperimentFormExDesInAd.class), CassandraJavaUtil.mapToRow(ExperimentFormOnlyParKey.class));
                Dataset<Row> metlistinitialbef =sp.createDataset(predf.map(x->x._2).rdd(), ExpEncoderofthree).toDF( "adduct","description","experimentid","intensity");
                Dataset<Row> metlistinitialbe= metlistinitialbef.withColumn("ioniz", functions.substring(col("adduct"), -1, 1)).drop("adduct").filter(col("ioniz").equalTo("-"));
                mtblistinitial = metlistinitialbe.drop(col("ioniz")).filter(col("description").isin(dfonemetabolite.toArray()));
            }
        
        Dataset<Row> beforefinalboxplot  = sp.emptyDataFrame();
        if(transf.equals("log2tra")){
            beforefinalboxplot = mtblistinitial.select(col("experimentid"),functions.log2(col("intensity")).alias("intensity"));
            
        }else{
            beforefinalboxplot = mtblistinitial.select(col("experimentid"),col("intensity"));
        }
         Dataset<ExperimentFormExIn> finalboxplot  = beforefinalboxplot.as(BoxplotmtbsEncoder2);
         boxplots=finalboxplot.collectAsList();
         
         return boxplots;
     }
     
}
