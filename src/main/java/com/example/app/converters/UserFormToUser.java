/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.example.app.converters;

import com.example.app.commands.UserForm;
import com.example.app.domain.User;
import org.springframework.stereotype.Component;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;

/**
 *
 * @author kostas
 */
@Component
public class UserFormToUser implements Converter<UserForm, User> {
    @Override 
    public User convert(UserForm usrform) {
        User user = new User();
        
        user.setUsername(usrform.getUsername());
        user.setAccountdatecreated(usrform.getAccountdatecreated());
        user.setEmail(usrform.getEmail());
        user.setPassword(usrform.getPassword());
        user.setVerificationcode(usrform.getVerificationcode());
        user.setVerifiedacc(usrform.getVerifiedacc());
        user.setResetpswcode(usrform.getResetpaswcode());
        return user;
    }
}
