/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.example.app.converters;

import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

import com.example.app.domain.ProjectExperiment;
import com.example.app.commands.ProjectsExpForm;
/**
 *
 * @author kostas
 */
@Component
public class ProjectExpToProjectExpForm implements Converter<ProjectExperiment, ProjectsExpForm>{
    @Override
    public ProjectsExpForm convert(ProjectExperiment projectsexp){
        ProjectsExpForm proexpform =new ProjectsExpForm();
        proexpform.setProjectExperimentKey(projectsexp.getProjectExperimentKey());
        proexpform.setExpname(projectsexp.getExpname());
        proexpform.setExpdate(projectsexp.getExpdate());
        proexpform.setReplicatesnum(projectsexp.getReplicatesnum());
        proexpform.setReplicatesnames(projectsexp.getReplicatesnames());
        proexpform.setPreprocessed(projectsexp.getPreprocessed());
        proexpform.setIdentified(projectsexp.getIdentified());
        proexpform.setUnidentified(projectsexp.getUnidentified());
        proexpform.setWorkflow(projectsexp.getWorkflow());
        return proexpform;
    }
}
