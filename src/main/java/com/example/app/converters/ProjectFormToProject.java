/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.example.app.converters;

import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;


import com.example.app.domain.Project;
import com.example.app.commands.ProjectsForm;
/**
 *
 * @author kostas
 */
@Component
public class ProjectFormToProject implements Converter<ProjectsForm, Project> {

    @Override 
    public Project convert(ProjectsForm proform) {
        Project project = new Project();
        if (proform.getProjectid() != null && !StringUtils.isEmpty(proform.getProjectid())) {
            project.setProjectid(proform.getProjectid());
        }
        project.setDatabasename(proform.getDatabasename());
        project.setProname(proform.getProname());
        project.setProdatecreated(proform.getProdatecreated());
        project.setProdateedited(proform.getProdateedited());
        project.setProcomments(proform.getProcomments());
        return project;
    }
    
}
