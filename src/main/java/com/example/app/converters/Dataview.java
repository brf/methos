/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.example.app.converters;

import com.datastax.spark.connector.japi.CassandraJavaUtil;
import com.datastax.spark.connector.japi.rdd.CassandraJavaPairRDD;
import com.example.app.domain.ExperimentFormAllFields;
import com.example.app.domain.ExperimentFormDesRtInMzFlChIdAdGr;
import com.example.app.domain.ExperimentFormExDesIn;
import com.example.app.domain.ExperimentFormOnlyParKey;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import org.apache.spark.api.java.function.ForeachFunction;
import org.apache.spark.sql.Dataset;
import org.apache.spark.sql.Encoder;
import org.apache.spark.sql.Encoders;
import org.apache.spark.sql.Row;
import org.apache.spark.sql.SparkSession;
import static org.apache.spark.sql.functions.col;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 *
 * @author kostas
 */
 @Component
public class Dataview {
   
    @Autowired
    private SparkSession sp;
    
    public List<ExperimentFormDesRtInMzFlChIdAdGr> searchmtbs(String experimentid){
        List<String> expidlist= new ArrayList<>();
        expidlist.add(experimentid);
       
        Encoder<ExperimentFormOnlyParKey> ExpEncoder = Encoders.bean(ExperimentFormOnlyParKey.class);
        Encoder<ExperimentFormAllFields> ExpEncoderofall = Encoders.bean(ExperimentFormAllFields.class);
        Dataset<ExperimentFormOnlyParKey> dfexplistoriginal = sp.createDataset(expidlist, Encoders.STRING()).toDF("experimentid").as(ExpEncoder);
        CassandraJavaPairRDD<ExperimentFormOnlyParKey, ExperimentFormAllFields> predf = CassandraJavaUtil.javaFunctions(CassandraJavaUtil.javaFunctions(dfexplistoriginal.javaRDD()).repartitionByCassandraReplica("mdb","experiment",100,CassandraJavaUtil.someColumns("experimentid"),CassandraJavaUtil.mapToRow(ExperimentFormOnlyParKey.class)))
                                                                                .joinWithCassandraTable("mdb","experiment", CassandraJavaUtil.someColumns("experimentid","description","rt","intensity","mz","identifier","chemical_formula","filename","adduct","groupid"), CassandraJavaUtil.someColumns("experimentid"), CassandraJavaUtil.mapRowTo(ExperimentFormAllFields.class), CassandraJavaUtil.mapToRow(ExperimentFormOnlyParKey.class));
        Dataset<Row> exper =sp.createDataset(predf.map(x->x._2).rdd(), ExpEncoderofall).toDF( "adduct","chemical_formula","description","experimentid","filename","groupid","identifier","intensity","mz","rt");
       
        Encoder<ExperimentFormDesRtInMzFlChIdAdGr> ExpEncoderofnine = Encoders.bean(ExperimentFormDesRtInMzFlChIdAdGr.class);
        Dataset<ExperimentFormDesRtInMzFlChIdAdGr> expinfo = exper.select(col("filename"),col("groupid"),col("description"),col("adduct"),col("mz"),col("rt"),col("intensity"),col("identifier"),col("chemical_formula")).as(ExpEncoderofnine);
        
        List<ExperimentFormDesRtInMzFlChIdAdGr> cassandramtbs = expinfo.collectAsList();
        
        return cassandramtbs;
    }
}
