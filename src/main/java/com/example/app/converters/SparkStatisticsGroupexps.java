/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.example.app.converters;

import com.datastax.oss.protocol.internal.ProtocolConstants;
import com.datastax.spark.connector.japi.CassandraJavaUtil;
import com.datastax.spark.connector.japi.rdd.CassandraJavaPairRDD;
import com.example.app.domain.ExperimentFormExDes;
import com.example.app.domain.ExperimentFormExDesIn;
import com.example.app.domain.ExperimentFormOnlyParKey;
import com.example.app.commands.Findmissvalforgroupexps;
import com.example.app.commands.Groupexps;
import com.example.app.domain.ExperimentFormExDesAd;
import com.example.app.domain.ExperimentFormExDesInAd;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.TreeMap;
import org.apache.spark.api.java.function.MapFunction;
import org.apache.spark.ml.feature.StandardScaler;
import org.apache.spark.ml.feature.StandardScalerModel;
import org.apache.spark.ml.feature.UnivariateFeatureSelector;
import org.apache.spark.ml.feature.UnivariateFeatureSelectorModel;
import org.apache.spark.ml.feature.VectorAssembler;
import org.apache.spark.ml.linalg.VectorUDT;
import org.apache.spark.ml.linalg.Vectors;
import org.apache.spark.sql.Dataset;
import org.apache.spark.sql.Encoder;
import org.apache.spark.sql.Encoders;
import org.apache.spark.sql.Row;
import org.apache.spark.sql.RowFactory;
import org.apache.spark.sql.SparkSession;
import org.apache.spark.sql.catalyst.encoders.RowEncoder;
import org.apache.spark.sql.functions;
import static org.apache.spark.sql.functions.asc;
import static org.apache.spark.sql.functions.callUDF;
import static org.apache.spark.sql.functions.col;
import static org.apache.spark.sql.functions.desc;
import static org.apache.spark.sql.functions.lit;
import org.apache.spark.sql.types.DataTypes;
import org.apache.spark.sql.types.Metadata;
import org.apache.spark.sql.types.StructField;
import org.apache.spark.sql.types.StructType;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import scala.collection.JavaConversions;

import org.apache.spark.ml.stat.*;
import org.apache.spark.sql.expressions.Window;
import static org.apache.spark.sql.functions.col;
import org.apache.spark.storage.StorageLevel;

/**
 *
 * @author kostas
 */
@Component
public class SparkStatisticsGroupexps {
    
    @Autowired
    private SparkSession sp;
    
    public Findmissvalforgroupexps findmissvalforgroupexps(List<String> experimentidgroup1, List<String> experimentidgroup2, String missval,String ionmode){
        Findmissvalforgroupexps missvalforgroupexps = new Findmissvalforgroupexps ();
        List<String> missvalmtbs = new ArrayList<>();
        List<String>allexpids = new ArrayList<>();
        allexpids.addAll(experimentidgroup1);
        allexpids.addAll(experimentidgroup2);
        Dataset<Row> expgroups1 =sp.emptyDataFrame();
        if(ionmode.equals("both")){
            Encoder<ExperimentFormOnlyParKey> ExpEncoder = Encoders.bean(ExperimentFormOnlyParKey.class);
            Encoder<ExperimentFormExDesIn> ExpEncoderofthree = Encoders.bean(ExperimentFormExDesIn.class);
            Dataset<ExperimentFormOnlyParKey> dfexplistoriginal = sp.createDataset(allexpids, Encoders.STRING()).toDF("experimentid").as(ExpEncoder);
            CassandraJavaPairRDD<ExperimentFormOnlyParKey, ExperimentFormExDesIn> predf = CassandraJavaUtil.javaFunctions(CassandraJavaUtil.javaFunctions(dfexplistoriginal.javaRDD()).repartitionByCassandraReplica("mdb","experiment",100,CassandraJavaUtil.someColumns("experimentid"),CassandraJavaUtil.mapToRow(ExperimentFormOnlyParKey.class)))
                                                                                    .joinWithCassandraTable("mdb","experiment", CassandraJavaUtil.someColumns("experimentid","description","intensity"), CassandraJavaUtil.someColumns("experimentid"), CassandraJavaUtil.mapRowTo(ExperimentFormExDesIn.class), CassandraJavaUtil.mapToRow(ExperimentFormOnlyParKey.class));
            expgroups1 =sp.createDataset(predf.map(x->x._2).rdd(), ExpEncoderofthree).toDF( "description","experimentid","intensity").filter(col("description").notEqual("Unidentified"));
        }else if(ionmode.equals("positive")){
            Encoder<ExperimentFormOnlyParKey> ExpEncoder = Encoders.bean(ExperimentFormOnlyParKey.class);
            Encoder<ExperimentFormExDesInAd> ExpEncoderofthree = Encoders.bean(ExperimentFormExDesInAd.class);
            Dataset<ExperimentFormOnlyParKey> dfexplistoriginal = sp.createDataset(allexpids, Encoders.STRING()).toDF("experimentid").as(ExpEncoder);
            CassandraJavaPairRDD<ExperimentFormOnlyParKey, ExperimentFormExDesInAd> predf = CassandraJavaUtil.javaFunctions(CassandraJavaUtil.javaFunctions(dfexplistoriginal.javaRDD()).repartitionByCassandraReplica("mdb","experiment",100,CassandraJavaUtil.someColumns("experimentid"),CassandraJavaUtil.mapToRow(ExperimentFormOnlyParKey.class)))
                                                                                .joinWithCassandraTable("mdb","experiment", CassandraJavaUtil.someColumns("experimentid","description","adduct","intensity"), CassandraJavaUtil.someColumns("experimentid"), CassandraJavaUtil.mapRowTo(ExperimentFormExDesInAd.class), CassandraJavaUtil.mapToRow(ExperimentFormOnlyParKey.class));
            Dataset<Row> metlistinitialbef =sp.createDataset(predf.map(x->x._2).rdd(), ExpEncoderofthree).toDF( "adduct","description","experimentid","intensity").filter(col("description").notEqual("Unidentified"));
            Dataset<Row> metlistinitialbe= metlistinitialbef.withColumn("ioniz", functions.substring(col("adduct"), -1, 1)).drop("adduct").filter(col("ioniz").equalTo("+"));
            expgroups1 = metlistinitialbe.drop(col("ioniz"));
        }
        else if(ionmode.equals("negative")){
            Encoder<ExperimentFormOnlyParKey> ExpEncoder = Encoders.bean(ExperimentFormOnlyParKey.class);
            Encoder<ExperimentFormExDesInAd> ExpEncoderofthree = Encoders.bean(ExperimentFormExDesInAd.class);
            Dataset<ExperimentFormOnlyParKey> dfexplistoriginal = sp.createDataset(allexpids, Encoders.STRING()).toDF("experimentid").as(ExpEncoder); 
            CassandraJavaPairRDD<ExperimentFormOnlyParKey, ExperimentFormExDesInAd> predf = CassandraJavaUtil.javaFunctions(CassandraJavaUtil.javaFunctions(dfexplistoriginal.javaRDD()).repartitionByCassandraReplica("mdb","experiment",100,CassandraJavaUtil.someColumns("experimentid"),CassandraJavaUtil.mapToRow(ExperimentFormOnlyParKey.class)))
                                                                                .joinWithCassandraTable("mdb","experiment", CassandraJavaUtil.someColumns("experimentid","description","adduct","intensity"), CassandraJavaUtil.someColumns("experimentid"), CassandraJavaUtil.mapRowTo(ExperimentFormExDesInAd.class), CassandraJavaUtil.mapToRow(ExperimentFormOnlyParKey.class));
            Dataset<Row> metlistinitialbef =sp.createDataset(predf.map(x->x._2).rdd(), ExpEncoderofthree).toDF( "adduct","description","experimentid","intensity").filter(col("description").notEqual("Unidentified"));
            Dataset<Row> metlistinitialbe= metlistinitialbef.withColumn("ioniz", functions.substring(col("adduct"), -1, 1)).drop("adduct").filter(col("ioniz").equalTo("-"));
            expgroups1 = metlistinitialbe.drop(col("ioniz"));
        }
        Dataset<Row> expgroups2 = expgroups1.groupBy(col("experimentid"),col("description")).mean("intensity").withColumnRenamed("avg(intensity)", "meanormedian");
        Dataset<Row> expgroups2towards3beforemisssingvalues =expgroups2.groupBy(col("description")).pivot("experimentid",new ArrayList<Object>(allexpids)).mean("meanormedian");
        
        if(missval.equals("omit")){
            StructType schematmp = new StructType(new StructField[] {
                            new StructField("metabs",
                                            DataTypes.StringType, false,
                                            Metadata.empty()) });

                Dataset<Row> tempmissvalmtbs=expgroups2towards3beforemisssingvalues.map((MapFunction<Row, Row>) originalrow -> {
                    if(originalrow.anyNull()){
                        String rowValues = new String();
                        rowValues=originalrow.get(0).toString();
                        Row newrow = RowFactory.create(rowValues);
                        return newrow;
                    }else{
                        String rowValues = "notmissing";
                        Row newrow = RowFactory.create(rowValues);
                        return newrow;
                    }
                }, RowEncoder.apply(schematmp)).filter(col("metabs").notEqual("notmissing"));
                missvalmtbs=tempmissvalmtbs.as(Encoders.STRING()).collectAsList();
                missvalforgroupexps.setMissvalmtbs(missvalmtbs);
                Dataset<Row> emptydf = sp.emptyDataFrame();
                missvalforgroupexps.setMissingmtbstobeunited(emptydf);
        }else{
            StructType schema = new StructType(new StructField[] {
                                new StructField("description",DataTypes.StringType, false,Metadata.empty()),
                                new StructField("experimentid",DataTypes.createArrayType(DataTypes.StringType, true), false,Metadata.empty())});
                    String[] columnnames=expgroups2towards3beforemisssingvalues.columns();
                    int mtbswithallnullms=columnnames.length;

                    Dataset<Row> tempmissvalmtbs=expgroups2towards3beforemisssingvalues.map((MapFunction<Row, Row>) originalrow -> {
                        if(originalrow.anyNull()){
                            List<String>tmpexpidfilename=new ArrayList<>();
                            for(int i=1;i<mtbswithallnullms;i++){
                                if(originalrow.isNullAt(i)){
                                    tmpexpidfilename.add(columnnames[i]);
                                }
                            }
                            String mtbname = originalrow.get(0).toString();
                            Row newrow = RowFactory.create(mtbname,tmpexpidfilename.toArray(new String[0]));
                            return newrow;
                        }else{
                            String mtbname = "notmissing";
                            List<String>tmpexpidfilename=new ArrayList<>();
                            Row newrow = RowFactory.create(mtbname,tmpexpidfilename.toArray(new String[0]));
                            return newrow;
                        }
                    }, RowEncoder.apply(schema)).filter(col("description").notEqual("notmissing")).persist();
                    missvalmtbs=tempmissvalmtbs.select("description").as(Encoders.STRING()).collectAsList();
                    missvalforgroupexps.setMissvalmtbs(missvalmtbs);
                    Dataset<Row> nullmtbs = tempmissvalmtbs.withColumn("experimentid", functions.explode_outer(col("experimentid"))).withColumn("avg(intensity)", functions.lit(null).cast(DataTypes.DoubleType)).withColumnRenamed("avg(intensity)", "meanormedian").select(col("experimentid"),col("description"),col("meanormedian"));
                    missvalforgroupexps.setMissingmtbstobeunited(nullmtbs);
                    tempmissvalmtbs.unpersist();
        }
        
        return missvalforgroupexps;
    }
    
    public List<String> omitInExperiments(List<String> expids,String ionmode){
        
        List<String> metlist = new ArrayList<>();
        Dataset<Row> metlistinitial =sp.emptyDataFrame();
        if(ionmode.equals("both")){
            Encoder<ExperimentFormOnlyParKey> ExpEncoder = Encoders.bean(ExperimentFormOnlyParKey.class);
            Encoder<ExperimentFormExDes> ExpEncoderoftwo = Encoders.bean(ExperimentFormExDes.class);
            Dataset<ExperimentFormOnlyParKey> dfexplistoriginal = sp.createDataset(expids, Encoders.STRING()).toDF("experimentid").as(ExpEncoder);
            CassandraJavaPairRDD<ExperimentFormOnlyParKey, ExperimentFormExDes> predf = CassandraJavaUtil.javaFunctions(CassandraJavaUtil.javaFunctions(dfexplistoriginal.javaRDD()).repartitionByCassandraReplica("mdb","experiment",100,CassandraJavaUtil.someColumns("experimentid"),CassandraJavaUtil.mapToRow(ExperimentFormOnlyParKey.class)))
                                                                                .joinWithCassandraTable("mdb","experiment", CassandraJavaUtil.someColumns("experimentid","description"), CassandraJavaUtil.someColumns("experimentid"), CassandraJavaUtil.mapRowTo(ExperimentFormExDes.class), CassandraJavaUtil.mapToRow(ExperimentFormOnlyParKey.class));
            metlistinitial =sp.createDataset(predf.map(x->x._2).rdd(), ExpEncoderoftwo).toDF( "description","experimentid").groupBy(col("experimentid"),col("description")).count();
        }else if(ionmode.equals("positive")){
            Encoder<ExperimentFormOnlyParKey> ExpEncoder = Encoders.bean(ExperimentFormOnlyParKey.class);
            Encoder<ExperimentFormExDesAd> ExpEncoderofthree = Encoders.bean(ExperimentFormExDesAd.class);
            Dataset<ExperimentFormOnlyParKey> dfexplistoriginal = sp.createDataset(expids, Encoders.STRING()).toDF("experimentid").as(ExpEncoder);
            CassandraJavaPairRDD<ExperimentFormOnlyParKey, ExperimentFormExDesAd> predf = CassandraJavaUtil.javaFunctions(CassandraJavaUtil.javaFunctions(dfexplistoriginal.javaRDD()).repartitionByCassandraReplica("mdb","experiment",100,CassandraJavaUtil.someColumns("experimentid"),CassandraJavaUtil.mapToRow(ExperimentFormOnlyParKey.class)))
                                                                                .joinWithCassandraTable("mdb","experiment", CassandraJavaUtil.someColumns("experimentid","description","adduct"), CassandraJavaUtil.someColumns("experimentid"), CassandraJavaUtil.mapRowTo(ExperimentFormExDesAd.class), CassandraJavaUtil.mapToRow(ExperimentFormOnlyParKey.class));
            Dataset<Row> metlistinitialbef =sp.createDataset(predf.map(x->x._2).rdd(), ExpEncoderofthree).toDF( "adduct","description","experimentid");
            Dataset<Row> metlistinitialbe= metlistinitialbef.withColumn("ioniz", functions.substring(col("adduct"), -1, 1)).drop("adduct").filter(col("ioniz").equalTo("+"));
            metlistinitial = metlistinitialbe.drop(col("ioniz")).groupBy(col("experimentid"),col("description")).count();
        }else if(ionmode.equals("negative")){
            Encoder<ExperimentFormOnlyParKey> ExpEncoder = Encoders.bean(ExperimentFormOnlyParKey.class);
            Encoder<ExperimentFormExDesAd> ExpEncoderofthree = Encoders.bean(ExperimentFormExDesAd.class);
            Dataset<ExperimentFormOnlyParKey> dfexplistoriginal = sp.createDataset(expids, Encoders.STRING()).toDF("experimentid").as(ExpEncoder);
            CassandraJavaPairRDD<ExperimentFormOnlyParKey, ExperimentFormExDesAd> predf = CassandraJavaUtil.javaFunctions(CassandraJavaUtil.javaFunctions(dfexplistoriginal.javaRDD()).repartitionByCassandraReplica("mdb","experiment",100,CassandraJavaUtil.someColumns("experimentid"),CassandraJavaUtil.mapToRow(ExperimentFormOnlyParKey.class)))
                                                                                .joinWithCassandraTable("mdb","experiment", CassandraJavaUtil.someColumns("experimentid","description","adduct"), CassandraJavaUtil.someColumns("experimentid"), CassandraJavaUtil.mapRowTo(ExperimentFormExDesAd.class), CassandraJavaUtil.mapToRow(ExperimentFormOnlyParKey.class));
            Dataset<Row> metlistinitialbef =sp.createDataset(predf.map(x->x._2).rdd(), ExpEncoderofthree).toDF( "adduct","description","experimentid");
            Dataset<Row> metlistinitialbe= metlistinitialbef.withColumn("ioniz", functions.substring(col("adduct"), -1, 1)).drop("adduct").filter(col("ioniz").equalTo("-"));
            metlistinitial = metlistinitialbe.drop(col("ioniz")).groupBy(col("experimentid"),col("description")).count();
        }
        Dataset<Row> metnamesandocur=metlistinitial.select(col("description")).groupBy(col("description")).count().orderBy( asc("description"));
        int expnum=expids.size();
        Dataset<Row> metnamesandocurinall =metnamesandocur.select(col("description"),col("count")).filter(col("description").notEqual("Unidentified")).filter(col("count").equalTo(expnum));
        metlist = metnamesandocurinall.select(col("description")).as(Encoders.STRING()).collectAsList();
        return metlist;
    }
    
    public List<String> replaceWithValueInExperiments (List<String> expids,String ionmode){
        List<String> metlist = new ArrayList<>();
        Dataset<Row> metlistinitial =sp.emptyDataFrame();
        if(ionmode.equals("both")){
            Encoder<ExperimentFormOnlyParKey> ExpEncoder = Encoders.bean(ExperimentFormOnlyParKey.class);
            Encoder<ExperimentFormExDes> ExpEncoderoftwo = Encoders.bean(ExperimentFormExDes.class);
            Dataset<ExperimentFormOnlyParKey> dfexplistoriginal = sp.createDataset(expids, Encoders.STRING()).toDF("experimentid").as(ExpEncoder);
            CassandraJavaPairRDD<ExperimentFormOnlyParKey, ExperimentFormExDes> predf = CassandraJavaUtil.javaFunctions(CassandraJavaUtil.javaFunctions(dfexplistoriginal.javaRDD()).repartitionByCassandraReplica("mdb","experiment",100,CassandraJavaUtil.someColumns("experimentid"),CassandraJavaUtil.mapToRow(ExperimentFormOnlyParKey.class)))
                                                                                .joinWithCassandraTable("mdb","experiment", CassandraJavaUtil.someColumns("experimentid","description"), CassandraJavaUtil.someColumns("experimentid"), CassandraJavaUtil.mapRowTo(ExperimentFormExDes.class), CassandraJavaUtil.mapToRow(ExperimentFormOnlyParKey.class));
            metlistinitial =sp.createDataset(predf.map(x->x._2).rdd(), ExpEncoderoftwo).toDF( "description","experimentid").filter(col("description").notEqual("Unidentified"));
        }else if(ionmode.equals("positive")){
            Encoder<ExperimentFormOnlyParKey> ExpEncoder = Encoders.bean(ExperimentFormOnlyParKey.class);
            Encoder<ExperimentFormExDesAd> ExpEncoderofthree = Encoders.bean(ExperimentFormExDesAd.class);
            Dataset<ExperimentFormOnlyParKey> dfexplistoriginal = sp.createDataset(expids, Encoders.STRING()).toDF("experimentid").as(ExpEncoder);
            CassandraJavaPairRDD<ExperimentFormOnlyParKey, ExperimentFormExDesAd> predf = CassandraJavaUtil.javaFunctions(CassandraJavaUtil.javaFunctions(dfexplistoriginal.javaRDD()).repartitionByCassandraReplica("mdb","experiment",100,CassandraJavaUtil.someColumns("experimentid"),CassandraJavaUtil.mapToRow(ExperimentFormOnlyParKey.class)))
                                                                                .joinWithCassandraTable("mdb","experiment", CassandraJavaUtil.someColumns("experimentid","description","adduct"), CassandraJavaUtil.someColumns("experimentid"), CassandraJavaUtil.mapRowTo(ExperimentFormExDesAd.class), CassandraJavaUtil.mapToRow(ExperimentFormOnlyParKey.class));
            Dataset<Row> metlistinitialbef =sp.createDataset(predf.map(x->x._2).rdd(), ExpEncoderofthree).toDF( "adduct","description","experimentid").filter(col("description").notEqual("Unidentified"));
            Dataset<Row> metlistinitialbe= metlistinitialbef.withColumn("ioniz", functions.substring(col("adduct"), -1, 1)).drop("adduct").filter(col("ioniz").equalTo("+"));
            metlistinitial = metlistinitialbe.drop(col("ioniz"));
        }else if(ionmode.equals("negative")){
            Encoder<ExperimentFormOnlyParKey> ExpEncoder = Encoders.bean(ExperimentFormOnlyParKey.class);
            Encoder<ExperimentFormExDesAd> ExpEncoderofthree = Encoders.bean(ExperimentFormExDesAd.class);
            Dataset<ExperimentFormOnlyParKey> dfexplistoriginal = sp.createDataset(expids, Encoders.STRING()).toDF("experimentid").as(ExpEncoder);
            CassandraJavaPairRDD<ExperimentFormOnlyParKey, ExperimentFormExDesAd> predf = CassandraJavaUtil.javaFunctions(CassandraJavaUtil.javaFunctions(dfexplistoriginal.javaRDD()).repartitionByCassandraReplica("mdb","experiment",100,CassandraJavaUtil.someColumns("experimentid"),CassandraJavaUtil.mapToRow(ExperimentFormOnlyParKey.class)))
                                                                                .joinWithCassandraTable("mdb","experiment", CassandraJavaUtil.someColumns("experimentid","description","adduct"), CassandraJavaUtil.someColumns("experimentid"), CassandraJavaUtil.mapRowTo(ExperimentFormExDesAd.class), CassandraJavaUtil.mapToRow(ExperimentFormOnlyParKey.class));
            Dataset<Row> metlistinitialbef =sp.createDataset(predf.map(x->x._2).rdd(), ExpEncoderofthree).toDF( "adduct","description","experimentid").filter(col("description").notEqual("Unidentified"));
            Dataset<Row> metlistinitialbe= metlistinitialbef.withColumn("ioniz", functions.substring(col("adduct"), -1, 1)).drop("adduct").filter(col("ioniz").equalTo("-"));
             metlistinitial = metlistinitialbe.drop(col("ioniz"));
        }
        Dataset<Row> mtbdistinct =metlistinitial.select(col("description")).distinct();
        metlist = mtbdistinct.as(Encoders.STRING()).collectAsList();
        return metlist;
    }
    
    public List<Groupexps> mtbfiltering(List<String> experimentidgroup1, List<String> experimentidgroup2, String missval, String meanormedian,Dataset<Row> missvaltobeunited,String normval,String ionmode){

        List<String>allexpids = new ArrayList<>();
        allexpids.addAll(experimentidgroup1);
        allexpids.addAll(experimentidgroup2);
        List<Integer> grouplist = new ArrayList<>();
        Integer rowflag = 0;
        List<String> metlist = new ArrayList<>();
        List<Groupexps> listgroupexps = new ArrayList<>();
        grouplist.add(1);
        grouplist.add(2);
        Dataset<Row> expgroups1 = sp.emptyDataFrame();
        if(ionmode.equals("both")){
            Encoder<ExperimentFormOnlyParKey> ExpEncoder = Encoders.bean(ExperimentFormOnlyParKey.class);
            Encoder<ExperimentFormExDesIn> ExpEncoderofthree = Encoders.bean(ExperimentFormExDesIn.class);
            Dataset<ExperimentFormOnlyParKey> dfexplistoriginal = sp.createDataset(allexpids, Encoders.STRING()).toDF("experimentid").as(ExpEncoder);
            CassandraJavaPairRDD<ExperimentFormOnlyParKey, ExperimentFormExDesIn> predf = CassandraJavaUtil.javaFunctions(CassandraJavaUtil.javaFunctions(dfexplistoriginal.javaRDD()).repartitionByCassandraReplica("mdb","experiment",100,CassandraJavaUtil.someColumns("experimentid"),CassandraJavaUtil.mapToRow(ExperimentFormOnlyParKey.class)))
                                                                                    .joinWithCassandraTable("mdb","experiment", CassandraJavaUtil.someColumns("experimentid","description","intensity"), CassandraJavaUtil.someColumns("experimentid"), CassandraJavaUtil.mapRowTo(ExperimentFormExDesIn.class), CassandraJavaUtil.mapToRow(ExperimentFormOnlyParKey.class));
            expgroups1 =sp.createDataset(predf.map(x->x._2).rdd(), ExpEncoderofthree).toDF( "description","experimentid","intensity").filter(col("description").notEqual("Unidentified")).withColumn("group", functions.when(col("experimentid").isin(experimentidgroup1.toArray()), functions.lit(1)).when(col("experimentid").isin(experimentidgroup2.toArray()), functions.lit(2))).persist();
        }else if(ionmode.equals("positive")){
            Encoder<ExperimentFormOnlyParKey> ExpEncoder = Encoders.bean(ExperimentFormOnlyParKey.class);
            Encoder<ExperimentFormExDesInAd> ExpEncoderofthree = Encoders.bean(ExperimentFormExDesInAd.class);
            Dataset<ExperimentFormOnlyParKey> dfexplistoriginal = sp.createDataset(allexpids, Encoders.STRING()).toDF("experimentid").as(ExpEncoder); 
            CassandraJavaPairRDD<ExperimentFormOnlyParKey, ExperimentFormExDesInAd> predf = CassandraJavaUtil.javaFunctions(CassandraJavaUtil.javaFunctions(dfexplistoriginal.javaRDD()).repartitionByCassandraReplica("mdb","experiment",100,CassandraJavaUtil.someColumns("experimentid"),CassandraJavaUtil.mapToRow(ExperimentFormOnlyParKey.class)))
                                                                                .joinWithCassandraTable("mdb","experiment", CassandraJavaUtil.someColumns("experimentid","description","adduct","intensity"), CassandraJavaUtil.someColumns("experimentid"), CassandraJavaUtil.mapRowTo(ExperimentFormExDesInAd.class), CassandraJavaUtil.mapToRow(ExperimentFormOnlyParKey.class));
            Dataset<Row> metlistinitialbef =sp.createDataset(predf.map(x->x._2).rdd(), ExpEncoderofthree).toDF( "adduct","description","experimentid","intensity").filter(col("description").notEqual("Unidentified"));
            Dataset<Row> metlistinitialbe= metlistinitialbef.withColumn("ioniz", functions.substring(col("adduct"), -1, 1)).drop("adduct").filter(col("ioniz").equalTo("+"));
            expgroups1 = metlistinitialbe.drop(col("ioniz")).withColumn("group", functions.when(col("experimentid").isin(experimentidgroup1.toArray()), functions.lit(1)).when(col("experimentid").isin(experimentidgroup2.toArray()), functions.lit(2))).persist();
        }else if(ionmode.equals("negative")){
            Encoder<ExperimentFormOnlyParKey> ExpEncoder = Encoders.bean(ExperimentFormOnlyParKey.class);
            Encoder<ExperimentFormExDesInAd> ExpEncoderofthree = Encoders.bean(ExperimentFormExDesInAd.class);
            Dataset<ExperimentFormOnlyParKey> dfexplistoriginal = sp.createDataset(allexpids, Encoders.STRING()).toDF("experimentid").as(ExpEncoder); 
            CassandraJavaPairRDD<ExperimentFormOnlyParKey, ExperimentFormExDesInAd> predf = CassandraJavaUtil.javaFunctions(CassandraJavaUtil.javaFunctions(dfexplistoriginal.javaRDD()).repartitionByCassandraReplica("mdb","experiment",100,CassandraJavaUtil.someColumns("experimentid"),CassandraJavaUtil.mapToRow(ExperimentFormOnlyParKey.class)))
                                                                                .joinWithCassandraTable("mdb","experiment", CassandraJavaUtil.someColumns("experimentid","description","adduct","intensity"), CassandraJavaUtil.someColumns("experimentid"), CassandraJavaUtil.mapRowTo(ExperimentFormExDesInAd.class), CassandraJavaUtil.mapToRow(ExperimentFormOnlyParKey.class));
            Dataset<Row> metlistinitialbef =sp.createDataset(predf.map(x->x._2).rdd(), ExpEncoderofthree).toDF( "adduct","description","experimentid","intensity").filter(col("description").notEqual("Unidentified"));
            Dataset<Row> metlistinitialbe= metlistinitialbef.withColumn("ioniz", functions.substring(col("adduct"), -1, 1)).drop("adduct").filter(col("ioniz").equalTo("-"));
            expgroups1 = metlistinitialbe.drop(col("ioniz")).withColumn("group", functions.when(col("experimentid").isin(experimentidgroup1.toArray()), functions.lit(1)).when(col("experimentid").isin(experimentidgroup2.toArray()), functions.lit(2))).persist();
        }
        Dataset<Row> expgroups2 = sp.emptyDataFrame();
        Dataset<Row> dfmetabolitelist = sp.emptyDataFrame();
        Dataset<Row> log2fc = sp.emptyDataFrame();
        Integer numfeatures = null;
        
        if(meanormedian.equals("meanval") && missval.equals("omit")){
            metlist=omitInExperiments(allexpids,ionmode);
            if (metlist != null && !metlist.isEmpty()) {
                numfeatures=metlist.size();
                dfmetabolitelist = sp.createDataset(metlist, Encoders.STRING()).toDF("description").orderBy(asc("description")).persist();
                                
                if(normval.equals("no")){
                    log2fc= expgroups1.groupBy(col("description")).pivot("group",new ArrayList<Object>(grouplist)).avg("intensity").withColumn("log2FC",functions.log2(col("2").divide(col("1"))));
                    expgroups2 = expgroups1.join(dfmetabolitelist,"description").groupBy(col("experimentid"),col("description"),col("group")).mean("intensity").withColumnRenamed("avg(intensity)", "meanormedian").persist();
                
                }else if(normval.equals("normvalexp")){
                   
                    //mean and std are of experiment level 
                    log2fc= expgroups1.groupBy(col("description")).pivot("group",new ArrayList<Object>(grouplist)).avg("intensity").withColumn("log2FC",functions.log2(col("2").divide(col("1"))));
                    Dataset<Row> tmp1 = expgroups1.join(dfmetabolitelist,"description").groupBy(col("experimentid"),col("description")).agg(functions.mean("intensity"),functions.stddev("intensity")).withColumnRenamed("experimentid", "expid").withColumnRenamed("description", "descri");
                    Dataset<Row> tmpexpgroups2 = expgroups1.join(tmp1,expgroups1.col("experimentid").equalTo(tmp1.col("expid")).and(expgroups1.col("description").equalTo(tmp1.col("descri")))).withColumn("zscore",((col("intensity").$minus(col("avg(intensity)"))).$div(col("stddev_samp(intensity)")))).na().fill(0);

                    Dataset<Row> finalexp = tmpexpgroups2.drop("intensity","avg(intensity)","stddev_samp(intensity)","expid","descri").withColumnRenamed("zscore", "intens");
                    expgroups2 = finalexp.groupBy(col("experimentid"),col("description"),col("group")).mean("intens").withColumnRenamed("avg(intens)", "meanormedian").persist();
                    
                }else if(normval.equals("normvalgroup")){
                    //mean and std are of group level 
                    log2fc= expgroups1.groupBy(col("description")).pivot("group",new ArrayList<Object>(grouplist)).avg("intensity").withColumn("log2FC",functions.log2(col("2").divide(col("1"))));
                    Dataset<Row> tmp1 = expgroups1.join(dfmetabolitelist,"description").groupBy(col("description"),col("group")).agg(functions.mean("intensity"),functions.stddev("intensity")).withColumnRenamed("description", "descri").withColumnRenamed("group", "gro");
                    Dataset<Row> tmpexps1 = sp.createDataset(experimentidgroup1, Encoders.STRING()).toDF("expid").withColumn("gro", functions.lit(1));
                    Dataset<Row> tmpexps2 = sp.createDataset(experimentidgroup2, Encoders.STRING()).toDF("expid").withColumn("gro", functions.lit(2));
                    Dataset<Row> tmpexps12=tmpexps1.union(tmpexps2);
                    Dataset<Row> tmp2 = tmp1.join(tmpexps12,"gro");
                    Dataset<Row> tmpexpgroups2 = expgroups1.join(tmp2,expgroups1.col("experimentid").equalTo(tmp2.col("expid")).and(expgroups1.col("description").equalTo(tmp2.col("descri"))).and(expgroups1.col("group").equalTo(tmp2.col("gro")))).withColumn("zscore",((col("intensity").$minus(col("avg(intensity)"))).$div(col("stddev_samp(intensity)")))).na().fill(0);
                    
                    Dataset<Row> finalexp = tmpexpgroups2.drop("intensity","avg(intensity)","stddev_samp(intensity)","descri","gro","expid").withColumnRenamed("zscore", "intens");
                    expgroups2 = finalexp.groupBy(col("experimentid"),col("description"),col("group")).mean("intens").withColumnRenamed("avg(intens)", "meanormedian").persist();
                }
                
            }else{
                rowflag = 1;
            }                    
        }else if(meanormedian.equals("meanval") && missval.equals("replacewithzero")){
            metlist=replaceWithValueInExperiments(allexpids,ionmode);
            if (metlist != null && !metlist.isEmpty()) {
                numfeatures=metlist.size();
                dfmetabolitelist = sp.createDataset(metlist, Encoders.STRING()).toDF("description").orderBy(asc("description")).persist();
                
                if(normval.equals("no")){
                    log2fc= expgroups1.groupBy(col("description")).pivot("group",new ArrayList<Object>(grouplist)).avg("intensity").withColumn("log2FC",functions.log2(col("2").divide(col("1"))));
                    Dataset<Row> tmpexpgroups2 = expgroups1.groupBy(col("experimentid"),col("description"),col("group")).mean("intensity").withColumnRenamed("avg(intensity)", "meanormedian");
                    Dataset<Row> nullmtbs = missvaltobeunited.withColumn("group", functions.when(col("experimentid").isin(experimentidgroup1.toArray()), functions.lit(1)).when(col("experimentid").isin(experimentidgroup2.toArray()), functions.lit(2)))
                            .select(col("experimentid"),col("description"),col("group"),col("meanormedian"));
                    Dataset<Row> withmissingvalues=tmpexpgroups2.union(nullmtbs);
                    expgroups2=withmissingvalues.na().fill(0).persist();
                }else if(normval.equals("normvalexp")){
                    log2fc= expgroups1.groupBy(col("description")).pivot("group",new ArrayList<Object>(grouplist)).avg("intensity").withColumn("log2FC",functions.log2(col("2").divide(col("1"))));
                    
                    //mean and std are of experiment level
                    Dataset<Row> tmp1 = expgroups1.groupBy(col("experimentid"),col("description")).agg(functions.mean("intensity"),functions.stddev("intensity")).withColumnRenamed("experimentid", "expid").withColumnRenamed("description", "descri");
                    Dataset<Row> tmpexpgr2 = expgroups1.join(tmp1,expgroups1.col("experimentid").equalTo(tmp1.col("expid")).and(expgroups1.col("description").equalTo(tmp1.col("descri")))).withColumn("zscore",((col("intensity").$minus(col("avg(intensity)"))).$div(col("stddev_samp(intensity)")))).na().fill(0);
                    Dataset<Row> finalexp = tmpexpgr2.drop("intensity","avg(intensity)","stddev_samp(intensity)","expid","descri").withColumnRenamed("zscore", "intens");
                    
                    Dataset<Row> tmpexpgroups2 = finalexp.groupBy(col("experimentid"),col("description"),col("group")).mean("intens").withColumnRenamed("avg(intens)", "meanormedian");
                    Dataset<Row> nullmtbs = missvaltobeunited.withColumn("group", functions.when(col("experimentid").isin(experimentidgroup1.toArray()), functions.lit(1)).when(col("experimentid").isin(experimentidgroup2.toArray()), functions.lit(2)))
                            .select(col("experimentid"),col("description"),col("group"),col("meanormedian"));
                    Dataset<Row> withmissingvalues=tmpexpgroups2.union(nullmtbs);
                    expgroups2=withmissingvalues.na().fill(0).persist();
                    
                }else if(normval.equals("normvalgroup")){
                    //mean and std are of group level
                    log2fc= expgroups1.groupBy(col("description")).pivot("group",new ArrayList<Object>(grouplist)).avg("intensity").withColumn("log2FC",functions.log2(col("2").divide(col("1"))));
                    Dataset<Row> tmp1 = expgroups1.groupBy(col("description"),col("group")).agg(functions.mean("intensity"),functions.stddev("intensity")).withColumnRenamed("description", "descri").withColumnRenamed("group", "gro");
                    Dataset<Row> tmpexps1 = sp.createDataset(experimentidgroup1, Encoders.STRING()).toDF("expid").withColumn("gro", functions.lit(1));
                    Dataset<Row> tmpexps2 = sp.createDataset(experimentidgroup2, Encoders.STRING()).toDF("expid").withColumn("gro", functions.lit(2));
                    Dataset<Row> tmpexps12=tmpexps1.union(tmpexps2);
                    Dataset<Row> tmp2 = tmp1.join(tmpexps12,"gro");
                    Dataset<Row> tmpexpgroups2 = expgroups1.join(tmp2,expgroups1.col("experimentid").equalTo(tmp2.col("expid")).and(expgroups1.col("description").equalTo(tmp2.col("descri"))).and(expgroups1.col("group").equalTo(tmp2.col("gro")))).withColumn("zscore",((col("intensity").$minus(col("avg(intensity)"))).$div(col("stddev_samp(intensity)")))).na().fill(0);
                    Dataset<Row> finalexp = tmpexpgroups2.drop("intensity","avg(intensity)","stddev_samp(intensity)","descri","gro","expid").withColumnRenamed("zscore", "intens");
                    Dataset<Row> tmpexpgroups3 = finalexp.groupBy(col("experimentid"),col("description"),col("group")).mean("intens").withColumnRenamed("avg(intens)", "meanormedian");//.persist();
                    Dataset<Row> nullmtbs = missvaltobeunited.withColumn("group", functions.when(col("experimentid").isin(experimentidgroup1.toArray()), functions.lit(1)).when(col("experimentid").isin(experimentidgroup2.toArray()), functions.lit(2)))
                            .select(col("experimentid"),col("description"),col("group"),col("meanormedian"));
                    Dataset<Row> withmissingvalues=tmpexpgroups3.union(nullmtbs);
                    expgroups2=withmissingvalues.na().fill(0).persist();
                }

            }else{
                rowflag = 1;
            }
        }else if(meanormedian.equals("medianval") && missval.equals("omit")){
            metlist=omitInExperiments(allexpids,ionmode);
            if (metlist != null && !metlist.isEmpty()) {
                numfeatures=metlist.size();
                dfmetabolitelist = sp.createDataset(metlist, Encoders.STRING()).toDF("description").orderBy(asc("description")).persist();
                if(normval.equals("no")){
                    log2fc= expgroups1.groupBy(col("description")).pivot("group",new ArrayList<Object>(grouplist)).avg("intensity").withColumn("log2FC",functions.log2(col("2").divide(col("1"))));
                    expgroups2 = expgroups1.join(dfmetabolitelist,"description").groupBy(col("experimentid"),col("description"),col("group")).agg(callUDF("percentile_approx", col("intensity"), lit(0.5)).as("meanormedian")).persist();
                }else if(normval.equals("normvalexp")){
                    log2fc= expgroups1.groupBy(col("description")).pivot("group",new ArrayList<Object>(grouplist)).avg("intensity").withColumn("log2FC",functions.log2(col("2").divide(col("1"))));
                    //mean and std are of experiment level
                    Dataset<Row> tmp1 = expgroups1.join(dfmetabolitelist,"description").groupBy(col("experimentid"),col("description")).agg(functions.mean("intensity"),functions.stddev("intensity")).withColumnRenamed("experimentid", "expid").withColumnRenamed("description", "descri");
                    Dataset<Row> tmpexpgroups2 = expgroups1.join(tmp1,expgroups1.col("experimentid").equalTo(tmp1.col("expid")).and(expgroups1.col("description").equalTo(tmp1.col("descri")))).withColumn("zscore",((col("intensity").$minus(col("avg(intensity)"))).$div(col("stddev_samp(intensity)")))).na().fill(0);
                    Dataset<Row> finalexp = tmpexpgroups2.drop("intensity","avg(intensity)","stddev_samp(intensity)","expid","descri").withColumnRenamed("zscore", "intens");
                    expgroups2 = finalexp.groupBy(col("experimentid"),col("description"),col("group")).agg(callUDF("percentile_approx", col("intens"), lit(0.5)).as("meanormedian")).persist();
                
                }else if(normval.equals("normvalgroup")){
                    log2fc= expgroups1.groupBy(col("description")).pivot("group",new ArrayList<Object>(grouplist)).avg("intensity").withColumn("log2FC",functions.log2(col("2").divide(col("1"))));
                    Dataset<Row> tmp1 = expgroups1.join(dfmetabolitelist,"description").groupBy(col("description"),col("group")).agg(functions.mean("intensity"),functions.stddev("intensity")).withColumnRenamed("description", "descri").withColumnRenamed("group", "gro");
                    Dataset<Row> tmpexps1 = sp.createDataset(experimentidgroup1, Encoders.STRING()).toDF("expid").withColumn("gro", functions.lit(1));
                    Dataset<Row> tmpexps2 = sp.createDataset(experimentidgroup2, Encoders.STRING()).toDF("expid").withColumn("gro", functions.lit(2));
                    Dataset<Row> tmpexps12=tmpexps1.union(tmpexps2);
                    Dataset<Row> tmp2 = tmp1.join(tmpexps12,"gro");
                    Dataset<Row> tmpexpgroups2 = expgroups1.join(tmp2,expgroups1.col("experimentid").equalTo(tmp2.col("expid")).and(expgroups1.col("description").equalTo(tmp2.col("descri"))).and(expgroups1.col("group").equalTo(tmp2.col("gro")))).withColumn("zscore",((col("intensity").$minus(col("avg(intensity)"))).$div(col("stddev_samp(intensity)")))).na().fill(0);
                    Dataset<Row> finalexp = tmpexpgroups2.drop("intensity","avg(intensity)","stddev_samp(intensity)","descri","gro","expid").withColumnRenamed("zscore", "intens");
                    expgroups2 = finalexp.groupBy(col("experimentid"),col("description"),col("group")).agg(callUDF("percentile_approx", col("intens"), lit(0.5)).as("meanormedian")).persist();
                }
            }else{
                rowflag = 1;
            }
        }else if(meanormedian.equals("medianval") && missval.equals("replacewithzero")){
            metlist=replaceWithValueInExperiments(allexpids,ionmode);
            if (metlist != null && !metlist.isEmpty()) {
                numfeatures=metlist.size();
                dfmetabolitelist = sp.createDataset(metlist, Encoders.STRING()).toDF("description").orderBy(asc("description")).persist();
                if(normval.equals("no")){
                    log2fc= expgroups1.groupBy(col("description")).pivot("group",new ArrayList<Object>(grouplist)).avg("intensity").withColumn("log2FC",functions.log2(col("2").divide(col("1"))));
                    Dataset<Row> tmpexpgroups2 = expgroups1.groupBy(col("experimentid"),col("description"),col("group")).agg(callUDF("percentile_approx", col("intensity"), lit(0.5)).as("meanormedian"));
                    Dataset<Row> nullmtbs = missvaltobeunited.withColumn("group", functions.when(col("experimentid").isin(experimentidgroup1.toArray()), functions.lit(1)).when(col("experimentid").isin(experimentidgroup2.toArray()), functions.lit(2)))
                            .select(col("experimentid"),col("description"),col("group"),col("meanormedian"));
                    Dataset<Row> withmissingvalues=tmpexpgroups2.union(nullmtbs);
                    expgroups2=withmissingvalues.na().fill(0).persist();            
                    
                }else if(normval.equals("normvalexp")){
                    log2fc= expgroups1.groupBy(col("description")).pivot("group",new ArrayList<Object>(grouplist)).avg("intensity").withColumn("log2FC",functions.log2(col("2").divide(col("1"))));
                    //mean and std are of experiment level
                    Dataset<Row> tmp1 = expgroups1.groupBy(col("experimentid"),col("description")).agg(functions.mean("intensity"),functions.stddev("intensity")).withColumnRenamed("experimentid", "expid").withColumnRenamed("description", "descri");
                    Dataset<Row> tmpexpgr2 = expgroups1.join(tmp1,expgroups1.col("experimentid").equalTo(tmp1.col("expid")).and(expgroups1.col("description").equalTo(tmp1.col("descri")))).withColumn("zscore",((col("intensity").$minus(col("avg(intensity)"))).$div(col("stddev_samp(intensity)")))).na().fill(0);
                    Dataset<Row> finalexp = tmpexpgr2.drop("intensity","avg(intensity)","stddev_samp(intensity)","expid","descri").withColumnRenamed("zscore", "intens");
                    Dataset<Row> tmpexpgroups2 = finalexp.groupBy(col("experimentid"),col("description"),col("group")).agg(callUDF("percentile_approx", col("intens"), lit(0.5)).as("meanormedian"));
                    Dataset<Row> nullmtbs = missvaltobeunited.withColumn("group", functions.when(col("experimentid").isin(experimentidgroup1.toArray()), functions.lit(1)).when(col("experimentid").isin(experimentidgroup2.toArray()), functions.lit(2)))
                            .select(col("experimentid"),col("description"),col("group"),col("meanormedian"));
                    Dataset<Row> withmissingvalues=tmpexpgroups2.union(nullmtbs);
                    expgroups2=withmissingvalues.na().fill(0).persist();
                } if(normval.equals("normvalgroup")){
                    log2fc= expgroups1.groupBy(col("description")).pivot("group",new ArrayList<Object>(grouplist)).avg("intensity").withColumn("log2FC",functions.log2(col("2").divide(col("1"))));
                    Dataset<Row> tmp1 = expgroups1.groupBy(col("description"),col("group")).agg(functions.mean("intensity"),functions.stddev("intensity")).withColumnRenamed("description", "descri").withColumnRenamed("group", "gro");
                    Dataset<Row> tmpexps1 = sp.createDataset(experimentidgroup1, Encoders.STRING()).toDF("expid").withColumn("gro", functions.lit(1));
                    Dataset<Row> tmpexps2 = sp.createDataset(experimentidgroup2, Encoders.STRING()).toDF("expid").withColumn("gro", functions.lit(2));
                    Dataset<Row> tmpexps12=tmpexps1.union(tmpexps2);
                    Dataset<Row> tmp2 = tmp1.join(tmpexps12,"gro");
                    Dataset<Row> tmpexpgroups2 = expgroups1.join(tmp2,expgroups1.col("experimentid").equalTo(tmp2.col("expid")).and(expgroups1.col("description").equalTo(tmp2.col("descri"))).and(expgroups1.col("group").equalTo(tmp2.col("gro")))).withColumn("zscore",((col("intensity").$minus(col("avg(intensity)"))).$div(col("stddev_samp(intensity)")))).na().fill(0);
                    Dataset<Row> finalexp = tmpexpgroups2.drop("intensity","avg(intensity)","stddev_samp(intensity)","descri","gro","expid").withColumnRenamed("zscore", "intens");
                    Dataset<Row> tmpexpgroups3 = finalexp.groupBy(col("experimentid"),col("description"),col("group")).agg(callUDF("percentile_approx", col("intens"), lit(0.5)).as("meanormedian"));
                    Dataset<Row> nullmtbs = missvaltobeunited.withColumn("group", functions.when(col("experimentid").isin(experimentidgroup1.toArray()), functions.lit(1)).when(col("experimentid").isin(experimentidgroup2.toArray()), functions.lit(2)))
                            .select(col("experimentid"),col("description"),col("group"),col("meanormedian"));
                    Dataset<Row> withmissingvalues=tmpexpgroups3.union(nullmtbs);
                    expgroups2=withmissingvalues.na().fill(0).persist();
                }
            }else{
                rowflag = 1;
            }
        }
        
        
        if(!rowflag.equals(1)){
                //convert to vector
                Dataset<Row> expgroups3 = expgroups2.groupBy(col("experimentid"),col("group")).agg(functions.collect_list(functions.map(col("description"),col("meanormedian")))).toDF("experimentid","group","descinten");

                StructType schema = new StructType(new StructField[] {
                            new StructField("experimentid",DataTypes.StringType, false,Metadata.empty()),
                            new StructField("group",DataTypes.IntegerType, false,Metadata.empty()),
                            new StructField("desc", DataTypes.createArrayType(DataTypes.StringType, true), false,Metadata.empty()),
                            new StructField("inten",new VectorUDT(),false,Metadata.empty())
                });
                Dataset<Row> expaftervector=expgroups3.map((MapFunction<Row, Row>) originalrow -> {
                        String firstpos=originalrow.get(0).toString();
                        Integer secondpos=Integer.parseInt(originalrow.get(1).toString());

                        List<scala.collection.Map<String,Double>>mplist=originalrow.getList(2);
                        int s = mplist.size();
                        Map<String,Double>treemp=new TreeMap<>();
                        for(int k=0;k<s;k++){
                            Object[] desc = JavaConversions.mapAsJavaMap(mplist.get(k)).keySet().toArray();
                            Object[] kvlist= JavaConversions.mapAsJavaMap(mplist.get(k)).values().toArray();
                            treemp.put(desc[0].toString(),Double.parseDouble(kvlist[0].toString()));
                        }
                        Object[] tempobj1 = treemp.values().toArray();
                        Object[] tempobj2 = treemp.keySet().toArray();
                        double[] tmplistint= new double[s];
                        String[] tmplistdesc=new String[s];
                        for(int i=0;i<s;i++){
                            tmplistint[i]=Double.parseDouble(tempobj1[i].toString());
                            tmplistdesc[i]=tempobj2[i].toString();
                        }
                        Row newrow = RowFactory.create(firstpos,secondpos,tmplistdesc,Vectors.dense(tmplistint));
                        
                        return newrow;

                }, RowEncoder.apply(schema));

            Dataset<Row> dfanovawithoutnames = ANOVATest.test(expaftervector, "inten", "group", true).filter(col("pValue").isNaN().equalTo(false));
            Dataset<Row> selmtbswithnums = dfmetabolitelist.withColumn("featureIndex", functions.row_number().over(Window.orderBy(asc("description"))).cast(DataTypes.IntegerType).$minus(1));
            Dataset<Row> dfanovawithnames = selmtbswithnums.join(dfanovawithoutnames,"featureIndex");
            Dataset<Row> anovawithfc = dfanovawithnames.join(log2fc,"description").drop("featureIndex","1","2");
            Dataset<Row> anovafinalresults =anovawithfc
                    .withColumn("adjpvalue", functions.when(col("pValue").$times(numfeatures).$greater(1.0),functions.lit(1.0)).otherwise(col("pValue").$times(numfeatures)))
                    .withColumn("fValue", functions.round(col("fValue"),3))
                    .withColumn("log2FC", functions.round(col("log2FC"),3))
                    .select(col("description"),col("pValue"),col("adjpvalue"),col("fValue"),col("log2FC"),col("degreesOfFreedom"))
                    .withColumnRenamed("pValue", "pvalue").withColumnRenamed("fValue", "fvalue").withColumnRenamed("log2FC", "foldch").withColumnRenamed("degreesOfFreedom", "degfr");

            Encoder<Groupexps> BasicstatsmtbsEncoder1 = Encoders.bean(Groupexps.class);
            listgroupexps =  anovafinalresults.as(BasicstatsmtbsEncoder1).collectAsList();
            expgroups2.unpersist();
            dfmetabolitelist.unpersist();
            expgroups1.unpersist();

        }else{
            listgroupexps=null;
        }

        return listgroupexps;
    }
    
}
