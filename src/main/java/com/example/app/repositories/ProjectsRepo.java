/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.example.app.repositories;

import java.util.UUID;
import org.springframework.data.repository.CrudRepository;
import com.example.app.domain.Project;
import java.util.List;

/**
 *
 * @author kostas
 */

public interface ProjectsRepo extends CrudRepository<Project,UUID> {

}


