/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.example.app.repositories;

import com.example.app.domain.User;
import com.example.app.domain.UserProject;
import com.example.app.domain.UserProjectKey;
import java.util.List;
import org.springframework.data.repository.CrudRepository;

/**
 *
 * @author kostas
 */

public interface UserProjectRepo extends CrudRepository<UserProject,UserProjectKey> {
          public List<UserProject> findAllByUserProjectKey_Username(String username);
          public List<UserProject> findByUserProjectKey_Username(String username);
          
}