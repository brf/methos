/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.example.app.repositories;

import com.example.app.domain.User;
import org.springframework.data.repository.CrudRepository;

/**
 *
 * @author kostas
 */
public interface UserRepo extends CrudRepository<User,String>{
    
    User findByUsername(String username);
}
