/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.example.app.repositories;

import org.springframework.data.repository.CrudRepository;
import com.example.app.domain.ProjectExperiment;
import com.example.app.domain.ProjectExperimentKey;
import java.util.List;
import java.util.UUID;

/**
 *
 * @author kostas
 */
public interface ProjectsExpRepo extends CrudRepository<ProjectExperiment,ProjectExperimentKey >{
    
    
    public List<ProjectExperiment> findAllByProjectExperimentKey_Projectid(UUID proid);
    
}
