/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.example.app.services;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.app.repositories.ProjectsRepo;
import com.example.app.converters.ProjectFormToProject;

import com.example.app.domain.Project;
import com.example.app.commands.ProjectsForm;
import com.example.app.domain.ProjectExperimentKey;
import com.example.app.repositories.ProjectsExpRepo;

/**
 *
 * @author kostas
 */
@Service
public class ProjectServiceImpl implements ProjectService{
    
    private ProjectsRepo prorepo;
    private ProjectFormToProject proformpro;
    
    @Autowired
    public ProjectServiceImpl(ProjectsRepo prorepo, ProjectFormToProject proformpro){
        this.prorepo=prorepo;
        this.proformpro=proformpro;
        
    }
    
    @Override
    public List<Project>listAllprojects(){
        List<Project> projects = new ArrayList<>();
        prorepo.findAll().forEach(projects::add);
        return projects;
    }
    
    @Override
    public Project getByProjectId(UUID projectid) {
        return prorepo.findById(projectid).orElse(null);
    }
    
    @Override
    public Project saveOrUpdate(Project projects) {
        prorepo.save(projects);
        return projects;
    }
    
    @Override
    public Project saveOrUpdateProjectsForm(ProjectsForm proform) {
        Project savedpro = saveOrUpdate(proformpro.convert(proform));
        return savedpro;
    }
   
}
