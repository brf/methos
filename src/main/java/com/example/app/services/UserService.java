/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.example.app.services;

import com.example.app.commands.UserForm;
import com.example.app.domain.User;
import java.util.List;

/**
 *
 * @author kostas
 */
public interface UserService {
    
    List<String> listallusernames(String username);
    String changepsw(String password);
    User findUserByUsername(String username);
    void createresetpswtokenandsendemail(String email, String siteURL);
    Boolean verify(String username, String verificationcode);
    Boolean doesuserexist(String username);
    Boolean doesemailexist(String email);
    Boolean doesresetcodeexist(String rescode);
    void register(UserForm usrform, String siteURL);
    User saveOrUpdate(User user);
    User saveOrUpdateUserForm(UserForm usrform);
}
