/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.example.app.services;

import com.example.app.commands.UserForm;
import com.example.app.commands.UserProjectForm;
import com.example.app.converters.UserFormToUser;
import com.example.app.domain.User;
import com.example.app.domain.UserProject;
import com.example.app.repositories.UserRepo;
import java.io.UnsupportedEncodingException;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.mail.MessagingException;
import javax.mail.internet.MimeMessage;
import net.bytebuddy.utility.RandomString;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.security.authentication.LockedException;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.factory.PasswordEncoderFactories;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

/**
 *
 * @author kostas
 */
@Service
public class UserServiceImpl implements UserDetailsService,UserService{

    @Autowired
    private UserRepo userRepo;
    
    @Autowired
    private UserFormToUser usrformusr;
    
    @Autowired
    private PasswordEncoder passwordEncoder;
    
    @Autowired
    private JavaMailSender mailSender;
    
    
    
    @Override
    public String changepsw(String password){
        String psw = passwordEncoder.encode(password);
        return psw;
    }
    
    @Override
    public List<String> listallusernames(String username){
        List<User> tmpusrlist = new ArrayList<>();
        userRepo.findAll().forEach(tmpusrlist::add);
        List<String>usrlist = new ArrayList<>();
        for(int i=0;i<tmpusrlist.size();i++){
            if(!tmpusrlist.get(i).getUsername().equals(username)){
                usrlist.add(tmpusrlist.get(i).getUsername());
            }
        }
        return usrlist;
    }
    
    @Override
    public UserDetails loadUserByUsername(String username) {
        User user = null;
        Set < GrantedAuthority > grantedAuthorities = new HashSet < > ();
        if(username!=null && !username.isEmpty()){
            if(username.equals("guest") ){
                user = userRepo.findByUsername("guest");
                grantedAuthorities.add(new SimpleGrantedAuthority("GUEST"));
            }else{
                user = userRepo.findByUsername(username);
                
                grantedAuthorities.add(new SimpleGrantedAuthority("USER"));
                grantedAuthorities.add(new SimpleGrantedAuthority("ADMIN"));
            }
        }else{
            username="unregistered_user";
        }
        
        if (user == null || !user.getVerifiedacc()){
            throw new UsernameNotFoundException(username);
        }

        return new org.springframework.security.core.userdetails.User(user.getUsername(), user.getPassword(),grantedAuthorities);
    }
    
    @Override
    public Boolean doesuserexist(String username) {
        Boolean doesusrexist = false;

        if(userRepo.findByUsername(username)!=null){
            doesusrexist=true;
        }
        return doesusrexist;
    }
    
    @Override
    public Boolean doesemailexist(String email) {
        Boolean doesemlexist = false;
        List<User> tmpusers = new ArrayList<>();
        userRepo.findAll().forEach(tmpusers::add);
        for(int i=0;i<tmpusers.size();i++){
            if(tmpusers.get(i).getEmail().equals(email)){
                doesemlexist=true;
            }
        }
        return doesemlexist;
    }
    
    @Override
    public Boolean doesresetcodeexist(String rescode) {
        Boolean doesrescodeexist = false;
        List<User> tmpusers = new ArrayList<>();
        userRepo.findAll().forEach(tmpusers::add);
        for(int i=0;i<tmpusers.size();i++){
            if(tmpusers.get(i).getResetpaswcode()!=null){
                if(tmpusers.get(i).getResetpaswcode().equals(rescode)){
                    doesrescodeexist=true;
                }
            }
        }
        return doesrescodeexist;
    }
    
    @Override
    public User saveOrUpdate(User user) {
        userRepo.save(user);
        return user;
    }
    @Override
    public User saveOrUpdateUserForm(UserForm usrform) {
        User savedusr = saveOrUpdate(usrformusr.convert(usrform));
        return savedusr;
    }
    
    @Override
    public void register(UserForm usrform, String siteURL) {
        String encodedPassword = passwordEncoder.encode(usrform.getPassword());
        usrform.setPassword(encodedPassword);

        String randomCode = RandomString.make(64);
        usrform.setVerificationcode(randomCode);
        
        LocalDateTime dateTime = LocalDateTime.now();
        String timeStamp = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss").format(dateTime);
        usrform.setAccountdatecreated(timeStamp);
        usrform.setVerifiedacc(Boolean.FALSE);
        usrform.setResetpswcode("NotChanged");
        saveOrUpdateUserForm(usrform);

        try {
            sendVerificationEmail(usrform, siteURL);
        } catch (MessagingException ex) {
            Logger.getLogger(UserServiceImpl.class.getName()).log(Level.SEVERE, null, ex);
        } catch (UnsupportedEncodingException ex) {
            Logger.getLogger(UserServiceImpl.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    
    private void sendVerificationEmail(UserForm user, String siteURL) throws MessagingException, UnsupportedEncodingException {
        String toAddress = user.getEmail();
        String fromAddress = "ktzan@cebitec.uni-bielefeld.de";
        String senderName = "MetHoS";
        String subject = "MetHoS - Please verify your registration";
        String content = "Dear [[name]],<br>"
                + "One last step!<br>"
                + "Please click the link below to verify your account and complete your registration:<br>"
                + "<h3><a href=\"[[URL]]\" target=\"_self\">VERIFY</a></h3>"
                + "Thank you very much,<br>"
                + "The MetHoS team.";

        MimeMessage message = mailSender.createMimeMessage();
        MimeMessageHelper helper = new MimeMessageHelper(message);

        helper.setFrom(fromAddress, senderName);
        helper.setTo(toAddress);
        helper.setSubject(subject);
        content = content.replace("[[name]]", user.getUsername());
        String verifyURL = siteURL + "/verify?code=" + user.getVerificationcode() +"&username=" + user.getUsername();
        content = content.replace("[[URL]]", verifyURL);
        helper.setText(content, true);

        mailSender.send(message);
    }
    
    @Override
    public Boolean verify(String username, String verificationcode) {
        User user = userRepo.findByUsername(username);
        
        if (user == null || user.getVerifiedacc()) {
            return false;
        } else {
            if(user.getVerificationcode().equals(verificationcode)){
                user.setVerificationcode("Verified");
                user.setVerifiedacc(true);
                userRepo.save(user);
                return true;
            }else{
                return false;
            }
        }

    }
    
    @Override
    public User findUserByUsername(String username){
        return userRepo.findByUsername(username);
    }
    
    @Override
    public void createresetpswtokenandsendemail(String email, String siteURL) {
        UserForm usrform = new UserForm();
        List<User> tmpusers = new ArrayList<>();
        userRepo.findAll().forEach(tmpusers::add);
        for(int i=0;i<tmpusers.size();i++){
            if(tmpusers.get(i).getEmail().equals(email)){
                usrform.setUsername(tmpusers.get(i).getUsername());
                usrform.setAccountdatecreated(tmpusers.get(i).getAccountdatecreated());
                usrform.setPassword(tmpusers.get(i).getPassword());
                usrform.setEmail(tmpusers.get(i).getEmail());
                usrform.setVerificationcode(tmpusers.get(i).getVerificationcode());
                usrform.setVerifiedacc(tmpusers.get(i).getVerifiedacc());
                String randomCode = RandomString.make(64);
                usrform.setResetpswcode(randomCode);
                break;
            }
        }
        
        saveOrUpdateUserForm(usrform);

        try {
            sendResetPassEmail(usrform, siteURL);
        } catch (MessagingException ex) {
            Logger.getLogger(UserServiceImpl.class.getName()).log(Level.SEVERE, null, ex);
        } catch (UnsupportedEncodingException ex) {
            Logger.getLogger(UserServiceImpl.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    
    private void sendResetPassEmail(UserForm user, String siteURL) throws MessagingException, UnsupportedEncodingException {
        String toAddress = user.getEmail();
        String fromAddress = "ktzan@cebitec.uni-bielefeld.de";
        String senderName = "MetHoS";
        String subject = "MetHoS - Reset your password";
        String content = "Dear [[name]],<br>"
                + "You have requested to reset your password.<br>"
                + "Please click the link below to change your password:<br>"
                + "<h3><a href=\"[[URL]]\" target=\"_self\">Change my password</a></h3>"
                + "Thank you very much,<br>"
                + "The MetHoS team."
                + "<p>Ignore this email if you do remember your password, "
                + "or you have not made the request.</p>";

        MimeMessage message = mailSender.createMimeMessage();
        MimeMessageHelper helper = new MimeMessageHelper(message);

        helper.setFrom(fromAddress, senderName);
        helper.setTo(toAddress);
        helper.setSubject(subject);
        content = content.replace("[[name]]", user.getUsername());
        String verifyURL = siteURL + "/resetpassformview?resetpswcode=" + user.getResetpaswcode()+"&username=" + user.getUsername();

        content = content.replace("[[URL]]", verifyURL);
        helper.setText(content, true);

        mailSender.send(message);
    }
    
    
}
