/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.example.app.services;

import com.example.app.commands.UserForm;
import com.example.app.commands.UserProjectForm;
import com.example.app.commands.UserRights;
import com.example.app.converters.UserProjectFormToUserProject;
import com.example.app.domain.UserProject;
import com.example.app.domain.UserProjectKey;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.example.app.repositories.UserProjectRepo;
import com.example.app.repositories.UserRepo;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;
import org.springframework.context.annotation.Bean;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.factory.PasswordEncoderFactories;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author kostas
 */
@Service
public class UserProjectServiceImpl implements UserProjectService/*,UserDetailsService*/{
    
    @Autowired
    private UserProjectRepo userProjectRepo;
    
    @Autowired
    private UserService userserv;
    
    @Autowired
    private UserProjectFormToUserProject userproformuserpro;
    
    
    @Override
    public UserProject saveOrUpdate(UserProject userproject) {
        userProjectRepo.save(userproject);
        return userproject;
    }
    @Override
    public UserProject saveOrUpdateUserProjectForm(UserProjectForm userproform) {
        UserProject savedpro = saveOrUpdate(userproformuserpro.convert(userproform));
        return savedpro;
    }
    
    @Override
    public List<UserProject> listAllUserProject(String username) {
        List<UserProject> tempuserprojects = new ArrayList<>();
        List<UserProject> userprojects = new ArrayList<>();
        userProjectRepo.findAll().forEach(tempuserprojects::add);
        for(UserProject user:tempuserprojects){
            if(!user.getUserProjectKey().getUsername().equals(username)){
                userprojects.add(user);
            }
        }
        return userprojects;
    }
    
    @Override
    public Boolean ifuserprojectexists(String username,String projectid) {
        UserProjectKey tempkey = new UserProjectKey();
        tempkey.setUsername(username);
        tempkey.setProjectid(projectid);
        Boolean userprojectsexistence= userProjectRepo.existsById(tempkey);
        return userprojectsexistence;
    }
    
    @Override
    public Boolean ifongoingpreprocessing(){
        Boolean ongoingpreprocessing = Boolean.FALSE;
        List<UserProject> tempuserprojects = new ArrayList<>();
        userProjectRepo.findAll().forEach(tempuserprojects::add);
        for(UserProject user:tempuserprojects){
            if(user.getKnimeprocessing()){
                ongoingpreprocessing=Boolean.TRUE;
                break;
            }
        }
        return ongoingpreprocessing;
    }
    @Override
    public Boolean ifongoinganalysis(){
        Boolean ifongoinganalysis = Boolean.FALSE;
        List<UserProject> tempuserprojects = new ArrayList<>();
        userProjectRepo.findAll().forEach(tempuserprojects::add);
        for(UserProject user:tempuserprojects){
            if(user.getSparkanalysis()){
                ifongoinganalysis=Boolean.TRUE;
                break;
            }
        }
        return ifongoinganalysis;
    }
    
    @Override
    public List<UserProject> listAllUserProjectByUsername(String username) {
        
        return userProjectRepo.findAllByUserProjectKey_Username(username);
    }
    
    @Override
    public UserProject findUserByUsername(String username){
        List<UserProject> users=userProjectRepo.findByUserProjectKey_Username(username);
        return users.get(0);
    }
    
    @Override
    public Boolean doesuserprojectexist(String username){
        Boolean doesusrprojectexist = false;
        if(!userProjectRepo.findByUserProjectKey_Username(username).isEmpty()){
            doesusrprojectexist=true;
        }
        return doesusrprojectexist;
    }
    
    @Override
    public UserProject findUserByUserProjectKey(String username, String projectid){
        List<UserProject> users=userProjectRepo.findByUserProjectKey_Username(username);
        UserProject currentuser = new UserProject();
        for(UserProject user:users){
            if(user.getUserProjectKey().getProjectid().equals(projectid)){
                currentuser=user;
            }
        }
        return currentuser;
    }
    
    @Override
    public UserRights listalluserswithrights(String username, String projectid){
        UserRights userRights =new UserRights();
        List<String> tempuserwithaccess= new ArrayList<>();
        List<String> tempuserwithnoaccess= new ArrayList<>();
        List<String> userwithaccess= new ArrayList<>();
        List<String> userwithnoaccess= new ArrayList<>();
        List<UserProject> userprojects = new ArrayList<>();
        userProjectRepo.findAll().forEach(userprojects::add);
        
        List<String> allusersindb = userserv.listallusernames(username);
        
        for(UserProject userpro:userprojects){
            if(!userpro.getUserProjectKey().getUsername().equals(username) && userpro.getUserProjectKey().getProjectid().equals(projectid) && userpro.getAccess()){
                tempuserwithaccess.add(userpro.getUserProjectKey().getUsername());
            }
        }
        HashSet<String> hashuserlistwithaccess = new HashSet(tempuserwithaccess);
        for(String tempuserwith : hashuserlistwithaccess){
            if(!tempuserwith.equals("guest")){
                userwithaccess.add(tempuserwith);
            }
        }
        
        for(UserProject userpro:userprojects){
            if(!userwithaccess.contains(userpro.getUserProjectKey().getUsername()) && !userpro.getUserProjectKey().getUsername().equals(username)){
                tempuserwithnoaccess.add(userpro.getUserProjectKey().getUsername());
            }
        }
        HashSet<String> hashuserlistwithnoaccess = new HashSet(tempuserwithnoaccess);
        for(String tempuserwithno:hashuserlistwithnoaccess){
            if(!tempuserwithno.equals("guest")){
                userwithnoaccess.add(tempuserwithno);
            }
        }
        
        for(int i=0;i<allusersindb.size();i++){
            if(!userwithnoaccess.contains(allusersindb.get(i)) && !userwithaccess.contains(allusersindb.get(i)) && !allusersindb.get(i).equals("guest")){
                userwithnoaccess.add(allusersindb.get(i));
            }
        }
        
        userRights.setUserswithnoaccess(userwithnoaccess);
        userRights.setUserswithaccess(userwithaccess);
        return userRights;
    }
}
