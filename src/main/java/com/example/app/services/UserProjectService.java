/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.example.app.services;

import com.example.app.commands.UserProjectForm;
import com.example.app.commands.UserRights;
import com.example.app.domain.UserProject;
import java.util.List;
import org.springframework.security.core.userdetails.UserDetails;

/**
 *
 * @author kostas
 */
public interface UserProjectService {
    
    List<UserProject> listAllUserProjectByUsername(String username);
    Boolean doesuserprojectexist(String username);
    UserProject findUserByUsername(String username);
    UserProject findUserByUserProjectKey(String username, String projectid);
    UserRights listalluserswithrights(String username, String projectid);
    List<UserProject> listAllUserProject(String username);
    Boolean ifuserprojectexists(String username,String projectid);
    Boolean ifongoingpreprocessing();
    Boolean ifongoinganalysis();
    UserProject saveOrUpdate(UserProject userproject);
    UserProject saveOrUpdateUserProjectForm(UserProjectForm userproform);
    
}
