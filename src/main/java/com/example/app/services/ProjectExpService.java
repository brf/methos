/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.example.app.services;

import java.util.List;
import java.util.UUID;

import com.example.app.domain.ProjectExperiment;
import com.example.app.domain.ProjectExperimentKey;
import com.example.app.commands.ProjectsExpForm;
/**
 *
 * @author kostas
 */
public interface ProjectExpService {
    ProjectExperiment getByProExpKey(ProjectExperimentKey id);
    ProjectExperiment saveOrUpdate(ProjectExperiment projectexperiment);
    ProjectExperiment saveOrUpdateProjectsExpForm(ProjectsExpForm proexpform);
    
    List<ProjectExperiment> listAllprojectexperimentbyproid(UUID projectid);

}
