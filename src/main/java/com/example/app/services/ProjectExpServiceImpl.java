/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.example.app.services;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.app.repositories.ProjectsExpRepo;

import com.example.app.converters.ProjectExpFormToProjectExp;

import com.example.app.domain.ProjectExperiment;
import com.example.app.domain.ProjectExperimentKey;

import com.example.app.commands.ProjectsExpForm;
/**
 *
 * @author kostas
 */
@Service
public class ProjectExpServiceImpl implements ProjectExpService{
    
    private ProjectsExpRepo proexprepo;
    private ProjectExpFormToProjectExp proexpformproexp;
    
    
    @Autowired
    public ProjectExpServiceImpl(ProjectsExpRepo proexprepo, ProjectExpFormToProjectExp proexpformproexp){
        this.proexprepo=proexprepo;
        this.proexpformproexp=proexpformproexp;
    }
    
    @Override
    public ProjectExperiment getByProExpKey(ProjectExperimentKey proexpid) {
        return proexprepo.findById(proexpid).orElse(null);
    }
    
    @Override 
    public List<ProjectExperiment> listAllprojectexperimentbyproid(UUID proid) {        
        return proexprepo.findAllByProjectExperimentKey_Projectid(proid);
    }
    
    @Override
    public ProjectExperiment saveOrUpdate(ProjectExperiment proexp) {
        proexprepo.save(proexp);
        return proexp;
    }
    
    @Override
    public ProjectExperiment saveOrUpdateProjectsExpForm(ProjectsExpForm proexpform) {
        ProjectExperiment savedproexp = saveOrUpdate(proexpformproexp.convert(proexpform)); 
        return savedproexp;
    }
    
}
