/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.example.app.services;

import java.util.List;
import java.util.UUID;

import com.example.app.domain.Project;
import com.example.app.commands.ProjectsForm;
import com.example.app.domain.ProjectExperimentKey;
/**
 *
 * @author kostas
 */
public interface ProjectService {
    
    List<Project> listAllprojects();
    Project getByProjectId(UUID id);
    Project saveOrUpdate(Project projects);
    Project saveOrUpdateProjectsForm(ProjectsForm proform);
}
