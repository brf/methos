/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.example.app.services;

/**
 *
 * @author kostas
 */

import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.model.CannedAccessControlList;
import com.amazonaws.services.s3.model.PutObjectRequest;
import java.io.IOException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;
import org.springframework.web.multipart.MultipartFile;
import com.example.app.storage.StorageException;
import java.io.File;
import java.io.FileOutputStream;
import java.util.logging.Level;
import java.util.logging.Logger;

@Service
public class StorageServiceImpl implements StorageService {
    
    @Autowired
    private AmazonS3 amazons3;
   
    @Override
    public void createfolderproject(String projectid){
        
        PutObjectRequest request1 = new PutObjectRequest("msfiles/ProjectList",projectid+"/","");
        amazons3.putObject(request1);
        
    }
    
    
    private File convertMultiPartToFile(MultipartFile file) throws IOException {
        File convFile = new File(file.getOriginalFilename());
        FileOutputStream fos = new FileOutputStream(convFile);
        fos.write(file.getBytes());
        fos.close();
        return convFile;
    }
    
    @Override
    public void store(MultipartFile[] files,String projectid,String experimentidname) {
        
        String[] filenames = new String[files.length];
        for(int i=0;i<files.length;i++){
            filenames[i]=StringUtils.cleanPath(files[i].getOriginalFilename());
        }
        for(int k=0;k<filenames.length;k++){
            try {
                int flag1=0;
                int flag2=0;
                
                File convFile = convertMultiPartToFile(files[k]);
                if (files[k].isEmpty()) {
                    flag1++;
                    throw new StorageException("Failed to store empty file " + filenames[k]);
                }
                if (filenames[k].contains("..")) {
                    flag2++;
                    // This is a security check
                    throw new StorageException(
                            "Cannot store file with relative path outside current directory "
                                    + filenames[k]);
                }
                if(flag1==0 && flag2==0){
                    if(k==0){
                        PutObjectRequest request1 = new PutObjectRequest("msfiles/ProjectList/"+projectid,experimentidname+"/","");
                        amazons3.putObject(request1);
                    }
                    PutObjectRequest request2 = new PutObjectRequest("msfiles/ProjectList/"+projectid,experimentidname+"/"+filenames[k], convFile).withCannedAcl(CannedAccessControlList.PublicRead);
                    amazons3.putObject(request2);
                    convFile.delete();
                }
            } catch (IOException ex) {
                Logger.getLogger(StorageServiceImpl.class.getName()).log(Level.SEVERE, null, ex);
            }
            
        }
    }
    
}
