/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.example.app.commands;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author kostas
 */
public class Groupexps {
    private String description;
    private Double pvalue;
    private Double adjpvalue;
    private Double fvalue;
    private Double foldch;
    private BigInteger degfr;
    
    
    public String getDescription() {
        return description;
    }
    public void setDescription(String description) {
        this.description = description;
    }
    public Double getPvalue() {
        return pvalue;
    }
    public void setPvalue(Double pvalue) {
        this.pvalue = pvalue;
    }
    public Double getAdjpvalue() {
        return adjpvalue;
    }
    public void setAdjpvalue(Double adjpvalue) {
        this.adjpvalue = adjpvalue;
    }
    public Double getFvalue() {
        return fvalue;
    }
    public void setFvalue(Double fvalue) {
        this.fvalue = fvalue;
    }
    public BigInteger getDegfr() {
        return degfr;
    }
    public void setDegfr(BigInteger degfr) {
        this.degfr = degfr;
    }
    public Double getFoldch() {
        return foldch;
    }
    public void setFoldch(Double foldch) {
        this.foldch = foldch;
    }
    
    
    /*public List<String> getMissvalmtbs() {
        return missvalmtbs;
    }
    public void setMissvalmtbs(List<String> missvalmtbs) {
        this.missvalmtbs = missvalmtbs;
    }
    public Double getStdgroup1() {
        return stdgroup1;
    }
    public void setStdgroup1(Double stdgroup1) {
        this.stdgroup1 = stdgroup1;
    }
    public Double getStdgroup2() {
        return stdgroup2;
    }
    public void setStdgroup2(Double stdgroup2) {
        this.stdgroup2 = stdgroup2;
    }
    public Double getDiffofstd() {
        return diffofstd;
    }
    public void setDiffofstd(Double diffofstd) {
        this.diffofstd = diffofstd;
    }
    
    public Integer getDiffofappear() {
        return diffofappear;
    }
    public void setDiffofappear(Integer diffofappear) {
        this.diffofappear = diffofappear;
    }
    public Double getMeangroup1() {
        return meangroup1;
    }
    public void setMeangroup1(Double meangroup1) {
        this.meangroup1 = meangroup1;
    }
    public Double getMeangroup2() {
        return meangroup2;
    }
    public void setMeangroup2(Double meangroup2) {
        this.meangroup2 = meangroup2;
    }
    public Double getDiffofmean() {
        return diffofmean;
    }
    public void setDiffofmean(Double diffofmean) {
        this.diffofmean = diffofmean;
    }*/
}
