/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.example.app.commands;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author kostas
 */
public class Explistrepliclistwithcorrpearsonspearman {
     
    private List<String> explist = new ArrayList<>();
    private List<String> replilist = new ArrayList<>();
    private List<String> metlist = new ArrayList<>();
    private List<Double[][]> expcorrreplcorr = new ArrayList<>();
    private List<String> missvalmtbsexp = new ArrayList<>();
    private List<String> missvalmtbsrepl = new ArrayList<>();
    private Integer rowflagexp = 0;
    private Integer rowflagrepl = 0;
    
    public List<String> getExplist() {
        return explist;
    }
    public void setExplist(List<String> explist) {
        this.explist = explist;
    }
    public List<String> getReplilist() {
        return replilist;
    }
    public void setReplilist(List<String> replilist) {
        this.replilist = replilist;
    }
    public List<String> getMetlist() {
        return metlist;
    }
    public void setMetlist(List<String> metlist) {
        this.metlist = metlist;
    }
    
    public List<Double[][]> getExpcorrreplcorr() {
        return expcorrreplcorr;
    }
    public void setExpcorrreplcorr(List<Double[][]> expcorrreplcorr) {
        this.expcorrreplcorr = expcorrreplcorr;
    }
    public List<String> getMissvalmtbsexp() {
        return missvalmtbsexp;
    }
    public void setMissvalmtbsexp(List<String> missvalmtbsexp) {
        this.missvalmtbsexp = missvalmtbsexp;
    }
    public List<String> getMissvalmtbsrepl() {
        return missvalmtbsrepl;
    }
    public void setMissvalmtbsrepl(List<String> missvalmtbsrepl) {
        this.missvalmtbsrepl = missvalmtbsrepl;
    }
    public Integer getRowflagexp() {
        return rowflagexp;
    }
    public void setRowflagexp(Integer rowflagexp) {
        this.rowflagexp = rowflagexp;
    }
    public Integer getRowflagrepl() {
        return rowflagrepl;
    }
    public void setRowflagrepl(Integer rowflagrepl) {
        this.rowflagrepl = rowflagrepl;
    }
    
}
