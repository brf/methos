/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.example.app.commands;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author kostas
 */
public class ProjectExperimentListForm {
    
    private List<ProjectsExpForm> projectexperimentlist = new ArrayList<ProjectsExpForm>();
    
    
    public List<ProjectsExpForm> getProjectexperimentlist() {
        return projectexperimentlist;
    }
    public void setProjectexperimentlists(List<ProjectsExpForm> projectexperimentlist) {
        this.projectexperimentlist = projectexperimentlist;
    }
    
    
}
