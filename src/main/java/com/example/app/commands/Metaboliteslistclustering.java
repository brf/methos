/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.example.app.commands;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author kostas
 */
public class Metaboliteslistclustering {
    private List<String> expslist = new ArrayList<>();
    private List<String> mtbslist = new ArrayList<>();
    private List<Integer> clusts = new ArrayList<>();
    private List<Long> clustinfo = new ArrayList<>();
    private double silhouette;
    private List<String> missvalmtbs = new ArrayList<>();
    private List<Double[]> mtbsclustscenters = new ArrayList<>();
    private Integer rowflag = 0;
    
    public List<String> getExpslist() {
        return expslist;
    }
    public void setExpslist(List<String> expslist) {
        this.expslist = expslist;
    }
    public List<String> getMtbslist() {
        return mtbslist;
    }
    public void setMtbslist(List<String> mtbslist) {
        this.mtbslist = mtbslist;
    }
    public List<Integer> getClusts() {
        return clusts;
    }
    public void setClusts(List<Integer> clusts) {
        this.clusts = clusts;
    }
    public double getSilhouette() {
        return silhouette;
    }
    public void setSilhouette(double silhouette) {
        this.silhouette = silhouette;
    }
    
    public List<String> getMissvalmtbs() {
        return missvalmtbs;
    }
    public void setMissvalmtbs(List<String> missvalmtbs) {
        this.missvalmtbs = missvalmtbs;
    }
    public List<Double[]> getMtbsclustscenters(){
        return mtbsclustscenters;
    }
    public void setMtbsclustscenters(List<Double[]> mtbsclustscenters) {
        this.mtbsclustscenters = mtbsclustscenters;
    }
     public List<Long> getClustinfo() {
        return clustinfo;
    }
    public void setClustinfo(List<Long> clustinfo) {
        this.clustinfo = clustinfo;
    }
    public Integer getRowflag() {
        return rowflag;
    }
    public void setRowflag(Integer rowflag) {
        this.rowflag = rowflag;
    }
}
