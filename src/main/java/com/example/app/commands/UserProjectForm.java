/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.example.app.commands;

import com.example.app.domain.UserProjectKey;

/**
 *
 * @author kostas
 */
public class UserProjectForm {
    private UserProjectKey userProjectKey;
    private Boolean access;
    private Boolean owner;
    private String password;
    private Boolean knimeprocessing;
    private Boolean sparkanalysis;
    
    public UserProjectKey getUserProjectKey() {
        return userProjectKey;
    }
    public void setUserProjectKey(UserProjectKey userProjectKey) {
        this.userProjectKey = userProjectKey;
    }
    
    public Boolean getAccess() {
        return access;
    }
    public void setAccess(Boolean access) {
        this.access = access;
    }
    public Boolean getOwner() {
        return owner;
    }
    public void setOwner(Boolean owner) {
        this.owner = owner;
    }
    public String getPassword() {
        return password;
    }
    public void setPassword(String password) {
        this.password = password;
    }
    
    public Boolean getKnimeprocessing() {
        return knimeprocessing;
    }
    public void setKnimeprocessing(Boolean knimeprocessing) {
        this.knimeprocessing = knimeprocessing;
    }
    
    public Boolean getSparkanalysis() {
        return sparkanalysis;
    }
    public void setSparkanalysis(Boolean sparkanalysis) {
        this.sparkanalysis = sparkanalysis;
    }        
}
