/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.example.app.commands;

import java.util.List;

/**
 *
 * @author kostas
 */
public class UserRights {
    private List<String> userswithaccess;
    private List<String> userswithnoaccess;
    
    public List<String> getUserswithaccess() {
        return userswithaccess;
    }
    public void setUserswithaccess(List<String> userswithaccess) {
        this.userswithaccess = userswithaccess;
    }
    
    public List<String> getUserswithnoaccess() {
        return userswithnoaccess;
    }
    public void setUserswithnoaccess(List<String> userswithnoaccess) {
        this.userswithnoaccess = userswithnoaccess;
    }
}
