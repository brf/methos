/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.example.app.commands;

import java.util.List;

/**
 *
 * @author kostas
 */
public class ProjectExperimentFormExNa {
    private String experimentid;
    private String expname;
    
    public String getExperimentid(){
        return experimentid;
    }
    public void setExperimentid(String experimentid){
        this.experimentid=experimentid;
    }
    
    public String getExpname() {
        return expname;
    }
    public void setExpname(String expname) {
        this.expname = expname;
    }
    
}
