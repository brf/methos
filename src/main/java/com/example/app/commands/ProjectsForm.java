/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.example.app.commands;

import java.util.UUID;

/**
 *
 * @author kostas
 */
public class ProjectsForm {
    private UUID projectid;
    private String databasename;
    private String proname;
    private String prodatecreated;
    private String prodateedited;
    private String procomments;

    public UUID getProjectid() {
        return projectid;
    }
    public void setProjectid(UUID projectid) {
        this.projectid = projectid;
    }
    
    public String getDatabasename() {
        return databasename;
    }
    public void setDatabasename(String databasename) {
        this.databasename = databasename;
    }
    
    public String getProname() {
        return proname;
    }
    public void setProname(String proname) {
        this.proname = proname;
    }
    
    public String getProdatecreated() {
        return prodatecreated;
    }
    public void setProdatecreated(String prodatecreated) {
        this.prodatecreated = prodatecreated;
    }
    
    public String getProdateedited() {
        return prodateedited;
    }
    public void setProdateedited(String prodateedited) {
        this.prodateedited = prodateedited;
    }
    
    public String getProcomments() {
        return procomments;
    }
    public void setProcomments(String procomments) {
        this.procomments = procomments;
    }
    
}
