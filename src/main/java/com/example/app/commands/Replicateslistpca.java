/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.example.app.commands;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author kostas
 */
public class Replicateslistpca {
    
    private List<String> replilist = new ArrayList<>();
    private List<String> missvalmtbs = new ArrayList<>();
    private List<Double[][]> replicpca = new ArrayList<>();
    private List<Double[]> replicvariance = new ArrayList<>();
    private String nrmtbsexps = new String();
    private Integer rowflag = 0;
    
    public List<String> getMissvalmtbs() {
        return missvalmtbs;
    }
    public void setMissvalmtbs(List<String> missvalmtbs) {
        this.missvalmtbs = missvalmtbs;
    }
    public List<String> getReplilist() {
        return replilist;
    }
    public void setReplilist(List<String> replilist) {
        this.replilist = replilist;
    }
    
    public List<Double[][]> getReplicpca() {
        return replicpca;
    }
    public void setReplicpca(List<Double[][]> replicpca) {
        this.replicpca = replicpca;
    }
    
    public List<Double[]> getReplicvariance(){
        return replicvariance;
    }
    public void setReplicvariance(List<Double[]> replicvariance) {
        this.replicvariance = replicvariance;
    }
    public String getNrmtbsexps() {
        return nrmtbsexps;
    }
    public void setNrmtbsexps(String nrmtbsexps) {
        this.nrmtbsexps = nrmtbsexps;
    }
    public Integer getRowflag() {
        return rowflag;
    }
    public void setRowflag(Integer rowflag) {
        this.rowflag = rowflag;
    }
}
