/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.example.app.commands;

import java.util.List;
import org.apache.spark.sql.Dataset;
import org.apache.spark.sql.Row;

/**
 *
 * @author kostas
 */
public class Findmissvalforgroupexps {

    private List<String> missvalmtbs;
    private Dataset<Row> missingmtbstobeunited;

    
    public List<String> getMissvalmtbs() {
        return missvalmtbs;
    }
    public void setMissvalmtbs(List<String> missvalmtbs) {
        this.missvalmtbs = missvalmtbs;
    }
    public Dataset<Row> getMissingmtbstobeunited() {
        return missingmtbstobeunited;
    }
    public void setMissingmtbstobeunited(Dataset<Row> missingmtbstobeunited) {
        this.missingmtbstobeunited = missingmtbstobeunited;
    }
  

}
