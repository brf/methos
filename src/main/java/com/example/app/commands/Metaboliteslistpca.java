/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.example.app.commands;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author kostas
 */
public class Metaboliteslistpca {
    private List<String> mtbslist = new ArrayList<>();
    private List<String> missvalmtbs = new ArrayList<>();
    private List<Double[][]> mtbspca = new ArrayList<>();
    private List<Double[]> mtbsvariance = new ArrayList<>();
    private String nrmtbsexps = new String();
    private Integer rowflag = 0;
    
    public List<String> getMissvalmtbs() {
        return missvalmtbs;
    }
    public void setMissvalmtbs(List<String> missvalmtbs) {
        this.missvalmtbs = missvalmtbs;
    }
    public List<String> getMtbslist() {
        return mtbslist;
    }
    public void setMtbslist(List<String> mtbslist) {
        this.mtbslist = mtbslist;
    }
    
    public List<Double[][]> getMtbspca() {
        return mtbspca;
    }
    public void setMtbspca(List<Double[][]> mtbspca) {
        this.mtbspca = mtbspca;
    }
    
    public List<Double[]> getMtbsvariance() {
        return mtbsvariance;
    }
    public void setMtbsvariance(List<Double[]> mtbsvariance) {
        this.mtbsvariance = mtbsvariance;
    }
    public String getNrmtbsexps() {
        return nrmtbsexps;
    }
    public void setNrmtbsexps(String nrmtbsexps) {
        this.nrmtbsexps = nrmtbsexps;
    }
    public Integer getRowflag() {
        return rowflag;
    }
    public void setRowflag(Integer rowflag) {
        this.rowflag = rowflag;
    }
}
