/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.example.app.commands;

import java.math.BigDecimal;
import java.math.RoundingMode;

/**
 *
 * @author kostas
 */
public class Basicstatsmtbs {
    private String experimentid;
    private String description;
    private Double mean;
    private Double std;
    
    public String getExperimentid(){
        return experimentid;
    }
    public void setExperimentid(String experimentid){
        this.experimentid=experimentid;
    }
    
    public String getDescription() {
        return description;
    }
    public void setDescription(String description) {
        this.description = description;
    }
    
    public Double getMean() {
        return mean;
    }
    public void setMean(Double mean) {
        if(!mean.isNaN()){
            BigDecimal bd = new BigDecimal(mean).setScale(3, RoundingMode.HALF_UP);
            this.mean = bd.doubleValue();
        }else{
            this.mean = mean;
        }
    }
    public Double getStd() {
        return std;
    }
    public void setStd(Double std) {
        if(!std.isNaN()){
            BigDecimal bd = new BigDecimal(std).setScale(3, RoundingMode.HALF_UP);
            this.std = bd.doubleValue();
        }else{
            this.std=std;
        }
    }
    
}
