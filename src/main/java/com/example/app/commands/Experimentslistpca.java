/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.example.app.commands;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author kostas
 */
public class Experimentslistpca {
    private List<String> expslist = new ArrayList<>();
    private List<String> missvalmtbs = new ArrayList<>();
    private List<Double[][]> expspca = new ArrayList<>();
    private List<Double[]> expsvariance = new ArrayList<>();
    private String nrmtbsexps = new String();
    private Integer rowflag = 0;
    
    public List<String> getExpslist() {
        return expslist;
    }
    public void setExpslist(List<String> expslist) {
        this.expslist = expslist;
    }
    
    public List<String> getMissvalmtbs() {
        return missvalmtbs;
    }
    public void setMissvalmtbs(List<String> missvalmtbs) {
        this.missvalmtbs = missvalmtbs;
    }
   
    public List<Double[][]> getExpspca() {
        return expspca;
    }
    public void setExpspca(List<Double[][]> expspca) {
        this.expspca = expspca;
    }
    
    public List<Double[]> getExpsvariance(){
        return expsvariance;
    }
    public void setExpsvariance(List<Double[]> expsvariance) {
        this.expsvariance = expsvariance;
    }
    public String getNrmtbsexps() {
        return nrmtbsexps;
    }
    public void setNrmtbsexps(String nrmtbsexps) {
        this.nrmtbsexps = nrmtbsexps;
    }
    public Integer getRowflag() {
        return rowflag;
    }
    public void setRowflag(Integer rowflag) {
        this.rowflag = rowflag;
    }
}
