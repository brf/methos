/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.example.app.controllers;

import com.example.app.commands.Basicstatsmtbs;
import com.example.app.commands.ProjectExperimentFormExNa;
import com.example.app.domain.ExperimentFormExIn;
import com.example.app.domain.ExperimentFormAllFields;
import com.example.app.domain.ExperimentFormDesRtInMzFlChIdAdGr;
import com.example.app.commands.Experimentslistpca;
import com.example.app.commands.Explistrepliclistwithcorrpearsonspearman;
import com.example.app.commands.Experimentslistclustering;
import com.example.app.commands.Findmissvalforgroupexps;
import com.example.app.commands.Groupexps;
import com.example.app.commands.Metaboliteslistclustering;
import com.example.app.commands.Metaboliteslistpca;
import com.example.app.commands.ProjectExperimentListForm;
import com.example.app.commands.ProjectsExpForm;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;
import javax.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.example.app.services.ProjectService;
import com.example.app.services.ProjectExpService;

import com.example.app.domain.Project;
import com.example.app.domain.ProjectExperimentKey;

import com.example.app.converters.ProjectToProjectForm;
import com.example.app.commands.ProjectsForm;
import com.example.app.commands.Replicateslistclustering;
import com.example.app.commands.Replicateslistpca;
import com.example.app.commands.UserForm;
import com.example.app.commands.UserProjectForm;
import com.example.app.commands.UserRights;
import com.example.app.converters.Dataview;
import com.example.app.converters.ProjectExpToProjectExpForm;
import com.example.app.converters.SparkStatisticsBasicstats;
import com.example.app.converters.SparkStatisticsBoxplots;
import com.example.app.converters.SparkStatisticsClustering;
import com.example.app.converters.SparkStatisticsGroupexps;
import com.example.app.converters.SparkStatisticsPca;
import com.example.app.converters.SparkStatisticsPearson;
import com.example.app.converters.SparkStatisticsSpearman;
import com.example.app.converters.SpringSparkKnime;
import com.example.app.converters.UserProjectToUserProjectForm;
import com.example.app.converters.UserToUserForm;
import com.example.app.domain.Identifiedunidentified;
import com.example.app.domain.ProjectExperiment;
import com.example.app.domain.User;
import com.example.app.domain.UserProject;
import com.example.app.domain.UserProjectKey;
import com.example.app.services.StorageService;
import com.example.app.services.UserProjectService;
import com.example.app.services.UserService;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.math.BigInteger;
import java.util.Arrays;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.mail.MessagingException;
import javax.servlet.http.HttpServletRequest;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;


/**
 *
 * @author kostas
 */
@RestController
public class Controller1 {
    
    private ProjectService proserv;
    private UserService usrserv;
    private ProjectToProjectForm proproform;
    private UserProjectToUserProjectForm userprouserproform;
    private ProjectExpToProjectExpForm proexpproexpform;
    private UserToUserForm usrusrform;
    private ProjectExpService proexpserv;
    private StorageService storageService;
    private SpringSparkKnime springSparkKnime;
    private SparkStatisticsPearson sparkStatisticsPearson;
    private SparkStatisticsSpearman sparkStatisticsSpearman;
    private SparkStatisticsPca sparkStatisticsPca;
    private SparkStatisticsClustering sparkStatisticsClustering;
    private SparkStatisticsBoxplots sparkStatisticsBoxplots;
    private Dataview dataview;
    private UserProjectService userProjectService;
    private SparkStatisticsBasicstats sparkStatisticsBasicstats;
    private SparkStatisticsGroupexps sparkStatisticsGroupexps;
   
    @Autowired
    public void setusertouserform(UserToUserForm usrusrform){
        this.usrusrform=usrusrform;
    }
    
    @Autowired
    public void setuserprojecttouserprojectform(UserProjectToUserProjectForm userprouserproform){
        this.userprouserproform=userprouserproform;
    }
    
    @Autowired
    public void setuserprojectservice(UserProjectService userProjectService){
        this.userProjectService=userProjectService;
    }
    @Autowired
    public void setprojectexpservice(ProjectExpService proexpserv){
        this.proexpserv=proexpserv;
    }
    
    @Autowired
    public void setuserservice(UserService usrserv){
        this.usrserv=usrserv;
    }
    
    @Autowired
    public void setstorageService(StorageService storageService) {
        this.storageService = storageService;
    }
    @Autowired
    public void setprojectservice(ProjectService proserv){
        this.proserv=proserv;
    }
    
    @Autowired
    public void setprojecttoprojectform(ProjectToProjectForm proproform){
        this.proproform=proproform;
    }
    
    @Autowired
    public void setprojectexptoprojectexpform(ProjectExpToProjectExpForm proexpproexpform){
        this.proexpproexpform=proexpproexpform;
    }
    
    @Autowired
    public void setspringsparkknime(SpringSparkKnime springSparkKnime){
        this.springSparkKnime = springSparkKnime;
    }
    @Autowired
    public void setsparkStatisticsPearson(SparkStatisticsPearson sparkStatisticsPearson){
        this.sparkStatisticsPearson=sparkStatisticsPearson;
    }
    @Autowired
    public void setsparkStatisticsBoxplots(SparkStatisticsBoxplots sparkStatisticsBoxplots){
        this.sparkStatisticsBoxplots=sparkStatisticsBoxplots;
    }
    @Autowired
    public void setsparkStatisticsSpearman(SparkStatisticsSpearman sparkStatisticsSpearman){
        this.sparkStatisticsSpearman=sparkStatisticsSpearman;
    }
    @Autowired
    public void setsparkStatisticsBasicstats(SparkStatisticsBasicstats sparkStatisticsBasicstats){
        this.sparkStatisticsBasicstats=sparkStatisticsBasicstats;
    }
    @Autowired
    public void setsparkStatisticsPca(SparkStatisticsPca sparkStatisticsPca){
        this.sparkStatisticsPca=sparkStatisticsPca;
    }
    @Autowired
    public void setsparkStatisticsClustering(SparkStatisticsClustering sparkStatisticsClustering){
        this.sparkStatisticsClustering=sparkStatisticsClustering;
    }
    @Autowired
    public void setdataview(Dataview dataview){
        this.dataview=dataview;
    }
    @Autowired
    public void setsparkStatisticsGroupexps(SparkStatisticsGroupexps sparkStatisticsGroupexps){
        this.sparkStatisticsGroupexps=sparkStatisticsGroupexps;
    }
    
    
    @RequestMapping(value="/uploadview/{projectid}",method=RequestMethod.GET)
    public ModelAndView uploa(@PathVariable String projectid) throws IOException{
        ModelAndView modelAndView = new ModelAndView("upload");
        Project project = proserv.getByProjectId(UUID.fromString(projectid));
        modelAndView.addObject("projectupload", project);
        modelAndView.addObject("proexplistform", new ProjectExperimentListForm());
        
        return modelAndView;
    }
    
    @RequestMapping(value="/uploadview",method=RequestMethod.POST)
    public ModelAndView handleFileUpload(@Valid ProjectExperimentListForm proexplistform, BindingResult bindingResult, RedirectAttributes redirectAttributes,@RequestParam(value="projid") String projectid, @RequestParam(value="file") MultipartFile[] files) {
        
        
        if(bindingResult.hasErrors()){
                redirectAttributes.addFlashAttribute("message", "Failure! Please try again!");
                redirectAttributes.addFlashAttribute("alertClass", "alert-danger");
            return new ModelAndView("redirect:/uploadview/"+projectid);
        }
        for(int i=0;i<proexplistform.getProjectexperimentlist().size();i++){
            
            if(!(proexplistform.getProjectexperimentlist().get(i).getExpname()==null)){
                proexplistform.getProjectexperimentlist().get(i).getProjectExperimentKey().setExperimentid(UUID.randomUUID().toString());
                proexplistform.getProjectexperimentlist().get(i).setPreprocessed(Boolean.FALSE);
                proexpserv.saveOrUpdateProjectsExpForm(proexplistform.getProjectexperimentlist().get(i));
                
                int numfiles= proexplistform.getProjectexperimentlist().get(i).getReplicatesnum().intValue();
                MultipartFile[] replicates = new MultipartFile[numfiles];
                for(int k=0; k<replicates.length;k++){
                    for(int l=0;l<files.length;l++){
                        if(proexplistform.getProjectexperimentlist().get(i).getReplicatesnames().get(k).equals(StringUtils.cleanPath(files[l].getOriginalFilename()))){
                            replicates[k]= files[l];
                        }
                    }
                    
                }
                String experimentidname = proexplistform.getProjectexperimentlist().get(i).getExpname().concat("_"+proexplistform.getProjectexperimentlist().get(i).getProjectExperimentKey().getExperimentid().toString());
                Project project = proserv.getByProjectId(UUID.fromString(projectid));
                String projectidtostore = project.getProjectid().toString();
                storageService.store(replicates,projectidtostore,experimentidname);
                
                
            }
            
        }
        
        return new ModelAndView("redirect:/projecttasksview/"+projectid);
    }
    
    @RequestMapping(value="/welcomeview", method = RequestMethod.GET)
    public ModelAndView wel() {
        ModelAndView modelAndView = new ModelAndView("welcome");
        return modelAndView;
    }
    
    @RequestMapping(value="/definitionsview",method=RequestMethod.GET)
    public ModelAndView defin() {
        ModelAndView modelAndView = new ModelAndView("definitions");
        return modelAndView;
    }
    @RequestMapping(value="/contactview",method=RequestMethod.GET)
    public ModelAndView cont() {
        ModelAndView modelAndView = new ModelAndView("contact");
        return modelAndView;
    }
    @RequestMapping(value="/aboutview",method=RequestMethod.GET)
    public ModelAndView about() {
        ModelAndView modelAndView = new ModelAndView("about");
        return modelAndView;
    }
    
    @RequestMapping(value="/", method = RequestMethod.GET)
    public ModelAndView nopage() {
        return new ModelAndView("redirect:/projectsview");
    }    

    @RequestMapping(value="/registrationview",method=RequestMethod.GET)
    public ModelAndView reg() {
        ModelAndView modelAndView = new ModelAndView("registration");
        modelAndView.addObject("usrform", new UserForm());
        return modelAndView;
    }
    
    @RequestMapping(value="/resetpassview",method=RequestMethod.GET)
    public ModelAndView res() {
        ModelAndView modelAndView = new ModelAndView("resetpass");
        return modelAndView;
    }
    
    @RequestMapping(value="/resetpassview",method=RequestMethod.POST)
    public ModelAndView respas(@RequestParam(value="email") String email) {
        ModelAndView modelAndView = new ModelAndView("resetpass");
        if(usrserv.doesemailexist(email)){
            //sent email with link
            usrserv.createresetpswtokenandsendemail(email, "https://methos.cebitec.uni-bielefeld.de");
            modelAndView = new ModelAndView("resetpassemailsent");
            
        }else{
            modelAndView.addObject("email", email);
            modelAndView.addObject("msg1", "E-mail does not exist.");
        }

        return modelAndView;
    }
    @RequestMapping(value="/resetpassformview",method=RequestMethod.GET)
    public ModelAndView resform(@RequestParam(value="resetpswcode") String resetpswcode,@RequestParam(value="username") String username) {
        ModelAndView modelAndView = new ModelAndView("resetpassform");
        if(usrserv.doesresetcodeexist(resetpswcode)){
            modelAndView.addObject("username", username);
            modelAndView.addObject("resetpswcode", resetpswcode);
        }else{
            modelAndView.addObject("msg", "Invalid token! Please go back and try again or contact the MetHoS team.");
        }
        return modelAndView;
    }
    
    @RequestMapping(value="/resetpassformview",method=RequestMethod.POST)
    public ModelAndView resformsuccess(@RequestParam(value="username") String username,@RequestParam(value="password") String password,@RequestParam(value="resetpswcode") String resetpswcode) {//maybe redirect to welcome page with successful reset password message
        ModelAndView modelAndView = new ModelAndView("resetpassformsuccess");
        //change password in db
        User user = usrserv.findUserByUsername(username);
        UserForm usrform = usrusrform.convert(user);
        usrform.setResetpswcode("Changed");
        usrform.setPassword(usrserv.changepsw(password));
        
        usrserv.saveOrUpdateUserForm(usrform);
        return modelAndView;
    }
    
    @RequestMapping(value = "/registrationview", method = RequestMethod.POST)//,HttpServletRequest request
    public ModelAndView regacc(@Valid UserForm usrform, BindingResult bindingResult,RedirectAttributes redirectAttributes) throws UnsupportedEncodingException, MessagingException {
        if(bindingResult.hasErrors()){
            redirectAttributes.addFlashAttribute("msg1", "There was an unexpected failure! Please go back and try again!");
            redirectAttributes.addFlashAttribute("alertClass", "alert-danger");
            return new ModelAndView("redirect:/registrationview");
        }else{
            ModelAndView modelAndView1 = new ModelAndView("registration");
            Boolean userexists = false;
            Boolean emailexists = false;
            Boolean doesusernameexist = usrserv.doesuserexist(usrform.getUsername());
            Boolean doesemailexist = usrserv.doesemailexist(usrform.getEmail());//usrform.getUserKey().getUsername(),
            if(doesusernameexist){
                userexists=true;
                modelAndView1.addObject("usrform", usrform);
                modelAndView1.addObject("msg2", "Username already exists.");
            }
            
            if(doesemailexist){
                emailexists=true;
                modelAndView1.addObject("usrform", usrform);
                modelAndView1.addObject("msg3", "E-mail already exists.");
            }
            
            if(!userexists && !emailexists){
                usrserv.register(usrform, "https://methos.cebitec.uni-bielefeld.de");
                modelAndView1 = new ModelAndView("regsuccess");
            }
            return modelAndView1;
        }
        
        
    }
    
    @RequestMapping(value="/verify", method = RequestMethod.GET)
    public ModelAndView verifyUser(@RequestParam(value="code") String code,@RequestParam(value="username") String username,RedirectAttributes redirectAttributes) {
        if (usrserv.verify(username,code)) {
            return new ModelAndView("redirect:/welcomeview?verificationsuccess");
        } else {
            return new ModelAndView("redirect:/welcomeview?verificationerror");
        }
        
    }
    
    @RequestMapping(value="/createprojectview"/*maybe account ID*/, method=RequestMethod.GET)
    public ModelAndView createproj() {
        ModelAndView modelAndView = new ModelAndView("createproject");
        modelAndView.addObject("proform", new ProjectsForm());
        return modelAndView;
    }
    
    @RequestMapping(value = "/createprojectview"/*maybe account ID*/, method = RequestMethod.POST)
    public ModelAndView createProject(@Valid ProjectsForm proform, BindingResult bindingResult, RedirectAttributes redirectAttributes){
        
        if(bindingResult.hasErrors()){
                redirectAttributes.addFlashAttribute("message", "Failure! Please go back and try again!");
                redirectAttributes.addFlashAttribute("alertClass", "alert-danger");
            return new ModelAndView("redirect:/createprojectview");
        }else{
        
        Project prosaved = proserv.saveOrUpdateProjectsForm(proform);
        UserProjectForm userproform = new UserProjectForm();
        UserProjectKey userProjectKey = new UserProjectKey();
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        String username = auth.getName();
        userProjectKey.setUsername(username);
        userProjectKey.setProjectid(prosaved.getProjectid().toString());
        userproform.setUserProjectKey(userProjectKey);
        userproform.setOwner(Boolean.TRUE);
        userproform.setAccess(Boolean.TRUE);
        userproform.setKnimeprocessing(Boolean.FALSE);
        userproform.setSparkanalysis(Boolean.FALSE);
        userProjectService.saveOrUpdateUserProjectForm(userproform);
        String projectid = prosaved.getProjectid().toString();
        storageService.createfolderproject(projectid);
        
        return new ModelAndView("redirect:/projectsview");
        }
    }
    
    
    @RequestMapping(value="/projectsview", method = RequestMethod.GET)
    public ModelAndView proj() {
        ModelAndView modelAndView = new ModelAndView("projects");
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        String username = auth.getName();
        List<UserProject> userprojectlist=userProjectService.listAllUserProjectByUsername(username);
        List<Project> projectlist = new ArrayList<>();
        for (UserProject userproject:userprojectlist){
            String tempproid=userproject.getUserProjectKey().getProjectid();
            if(userproject.getAccess()){
                Project temppro=proserv.getByProjectId(UUID.fromString(tempproid));
                projectlist.add(temppro);
            }
        }
        modelAndView.addObject("projects",projectlist);
        if(auth.getAuthorities().stream().anyMatch(a -> a.getAuthority().equals("GUEST"))){
            Boolean gu = true;
            modelAndView.addObject("guestuser",gu);
        }
        return modelAndView;
    }
    @RequestMapping(value="/projecttasksview/{projectid}", method = RequestMethod.GET)
    public ModelAndView showproj(@PathVariable String projectid) {
        ModelAndView modelAndView = new ModelAndView("projecttasks");
        modelAndView.addObject("project", proserv.getByProjectId(UUID.fromString(projectid)));
        List<ProjectExperiment> proexplist =proexpserv.listAllprojectexperimentbyproid(UUID.fromString(projectid));
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        String username = auth.getName();
        UserProject user = userProjectService.findUserByUserProjectKey(username, projectid);
        modelAndView.addObject("projectexps", proexplist);

        Boolean preprocessedexist = false;
        for(ProjectExperiment proexp:proexplist){
            if(proexp.getPreprocessed()){
                preprocessedexist=true;
                break;
            }
        }
        Boolean preprocessedwhenbothexistandnon = true;
        for(ProjectExperiment proexp:proexplist){
            if(!proexp.getPreprocessed()){
                preprocessedwhenbothexistandnon=false;
                break;
            }
        }
        Boolean owner = true;
        if(!user.getOwner()){
            owner=false;
        }
        modelAndView.addObject("owner",owner);
        modelAndView.addObject("preprocessedexist",preprocessedexist);
        modelAndView.addObject("preprocessedwhenbothexistandnon",preprocessedwhenbothexistandnon);
        if(auth.getAuthorities().stream().anyMatch(a -> a.getAuthority().equals("GUEST"))){
            Boolean gu = true;
            modelAndView.addObject("guestuser",gu);
        }
        return modelAndView;
    }

    @RequestMapping(value="/shareview/{projectid}",method=RequestMethod.GET)
    public ModelAndView shar(@PathVariable String projectid) {
        ModelAndView modelAndView = new ModelAndView("share");
        Project project = proserv.getByProjectId(UUID.fromString(projectid));
        modelAndView.addObject("projectshare", project);
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        String username = auth.getName();
        UserRights userRights=userProjectService.listalluserswithrights(username,projectid);
        modelAndView.addObject("userlistwithnoaccess", userRights.getUserswithnoaccess());
        modelAndView.addObject("userlistwithaccess", userRights.getUserswithaccess());
        
        
        return modelAndView;
    }
    @RequestMapping(value="/shareview",method=RequestMethod.POST)
    public ModelAndView sharerights(@RequestParam(value="projectid") String projectid, @RequestParam(value="userswith",required = false)List<String> userswith,@RequestParam(value="emptylist")Boolean emptylist){
        
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        String username = auth.getName();
                
        if(!emptylist){
            for(String userwith:userswith){
                if(userProjectService.ifuserprojectexists(userwith, projectid)){
                    UserProject userprojectwithfromdb = userProjectService.findUserByUserProjectKey(userwith, projectid);
                    UserProjectForm userproformwith = userprouserproform.convert(userprojectwithfromdb);
                    userproformwith.setUserProjectKey(userprojectwithfromdb.getUserProjectKey());
                    userproformwith.setOwner(userprojectwithfromdb.getOwner());
                    userproformwith.setKnimeprocessing(userprojectwithfromdb.getKnimeprocessing());
                    userproformwith.setSparkanalysis(userprojectwithfromdb.getSparkanalysis());
                    userproformwith.setAccess(Boolean.TRUE);
                    userProjectService.saveOrUpdateUserProjectForm(userproformwith);

                }else {
                    if(userProjectService.doesuserprojectexist(userwith)){
                        UserProject userprojectwithfromdb2 = userProjectService.findUserByUsername(userwith);
                        UserProjectForm userproformwith = userprouserproform.convert(userprojectwithfromdb2);
                        UserProjectKey tmpkey = new UserProjectKey();
                        tmpkey.setUsername(userwith);
                        tmpkey.setProjectid(projectid);
                        userproformwith.setUserProjectKey(tmpkey);
                        userproformwith.setOwner(Boolean.FALSE);
                        userproformwith.setAccess(Boolean.TRUE);
                        userproformwith.setKnimeprocessing(Boolean.FALSE);
                        userproformwith.setSparkanalysis(Boolean.FALSE);
                        userProjectService.saveOrUpdateUserProjectForm(userproformwith);
                    }else{
                        UserProjectForm userproformwith = new UserProjectForm();
                        UserProjectKey tmpkey = new UserProjectKey();
                        tmpkey.setUsername(userwith);
                        tmpkey.setProjectid(projectid);
                        userproformwith.setUserProjectKey(tmpkey);
                        userproformwith.setOwner(Boolean.FALSE);
                        userproformwith.setAccess(Boolean.TRUE);
                        userproformwith.setKnimeprocessing(Boolean.FALSE);
                        userproformwith.setSparkanalysis(Boolean.FALSE);
                        userProjectService.saveOrUpdateUserProjectForm(userproformwith);
                    }
                }
            }
            List<UserProject> allusers = userProjectService.listAllUserProject(username);
            for(UserProject user:allusers){
                if(user.getUserProjectKey().getProjectid().equals(projectid) && !userswith.contains(user.getUserProjectKey().getUsername()) && !user.getUserProjectKey().getUsername().equals("guest")){
                    UserProjectForm userproformnooneaccess = userprouserproform.convert(user);
                    userproformnooneaccess.setAccess(Boolean.FALSE);
                    userProjectService.saveOrUpdateUserProjectForm(userproformnooneaccess);
                }
            }
        }else{
            List<UserProject> allusers = userProjectService.listAllUserProject(username);
            for(UserProject user:allusers){
                if(user.getUserProjectKey().getProjectid().equals(projectid) && !user.getUserProjectKey().getUsername().equals("guest")){
                    UserProjectForm userproformnooneaccess = userprouserproform.convert(user);
                    userproformnooneaccess.setAccess(Boolean.FALSE);
                    userProjectService.saveOrUpdateUserProjectForm(userproformnooneaccess);
                }
            }
        }
          
            
        return new ModelAndView("redirect:/projecttasksview/"+projectid);
        
    }
    
    
    @RequestMapping(value="/analyzeview/{projectid}",method=RequestMethod.GET)
    public ModelAndView searchv(@PathVariable String projectid) {
        ModelAndView modelAndView = new ModelAndView("analyze");
        Project project = proserv.getByProjectId(UUID.fromString(projectid));
        modelAndView.addObject("projectanalyze", project);

        return modelAndView;
    }
    
    @RequestMapping(value="/pearsonview/{projectid}",method=RequestMethod.GET)
    public ModelAndView pearget(@PathVariable String projectid) {
        ModelAndView modelAndView = new ModelAndView("pearson");
        Project project = proserv.getByProjectId(UUID.fromString(projectid));
        modelAndView.addObject("projectpearson", project);
        List<ProjectExperiment> proexplistbefore =proexpserv.listAllprojectexperimentbyproid(UUID.fromString(projectid));
        List<ProjectExperiment> proexplistafter = new ArrayList<ProjectExperiment>();
        for(ProjectExperiment proexp:proexplistbefore){
            if(proexp.getPreprocessed()){
                proexplistafter.add(proexp);
            }
        }
        Boolean ifongoingprocessing = Boolean.FALSE;
        modelAndView.addObject("ifongoingprocessing", ifongoingprocessing);
        modelAndView.addObject("experimentlist",proexplistafter);
        return modelAndView;
    }
    @RequestMapping(value="/pearsonview",method=RequestMethod.POST)
    public ModelAndView pearpost(@RequestParam(value="projectid") String projectid,@RequestParam(value="ionmode",required = false)String ionmode,@RequestParam(value="allorsome") String allorsome,@RequestParam(value="experimentid") List<String> experimentids,@RequestParam(value="missval") String missval) {
        
        ModelAndView modelAndView = new ModelAndView();
        Boolean ifongoingprocessing = userProjectService.ifongoingpreprocessing();
        if(!ifongoingprocessing){
            Authentication auth = SecurityContextHolder.getContext().getAuthentication();
            String username = auth.getName();
            UserProject userprojectfromdb = userProjectService.findUserByUserProjectKey(username, projectid);
            UserProjectForm userproform = userprouserproform.convert(userprojectfromdb);
            userproform.setUserProjectKey(userprojectfromdb.getUserProjectKey());
            userproform.setOwner(userprojectfromdb.getOwner());
            userproform.setKnimeprocessing(userprojectfromdb.getKnimeprocessing());
            userproform.setSparkanalysis(Boolean.TRUE);
            userproform.setAccess(userprojectfromdb.getAccess());
            userProjectService.saveOrUpdateUserProjectForm(userproform);
            
            if(allorsome.equals("onallexprepl")){
                ModelAndView modelAndView1 = new ModelAndView("pearsonresults");
                Project project = proserv.getByProjectId(UUID.fromString(projectid));
                modelAndView1.addObject("projectpearsonresults", project);
                List<String> mtblist = new ArrayList<>();
                Explistrepliclistwithcorrpearsonspearman exlistwithexpcorrandmetlistwithmetcorr = sparkStatisticsPearson.pearson(experimentids,mtblist,allorsome,missval,ionmode);
                List<String> proexplistafter = new ArrayList<>();
                List<String> proreplilistafter = new ArrayList<>();
                List<String> proexplistafterfinal = new ArrayList<>();
                List<ProjectExperiment> proexplistbefore =proexpserv.listAllprojectexperimentbyproid(UUID.fromString(projectid));
                for(String expid:experimentids){
                    for(ProjectExperiment proexpname:proexplistbefore){
                        if(expid.equals(proexpname.getProjectExperimentKey().getExperimentid())){
                            proexplistafter.add(proexpname.getExpname()+"__"+proexpname.getProjectExperimentKey().getExperimentid());
                            proexplistafterfinal.add(proexpname.getExpname());
                        }
                    }
                }
                List<String> proreplilistbefore=exlistwithexpcorrandmetlistwithmetcorr.getReplilist();
                for(String replitem:proreplilistbefore){
                    String[] temprepl=replitem.split("__");
                    for(String expitem:proexplistafter ){
                        String[] tempexp=expitem.split("__");
                        if(tempexp[1].equals(temprepl[0])){
                            proreplilistafter.add(tempexp[0]+"__"+temprepl[1]);
                        }
                    }
                }
                modelAndView1.addObject("explist",proexplistafterfinal);
                modelAndView1.addObject("replilist",proreplilistafter);
                if(exlistwithexpcorrandmetlistwithmetcorr.getRowflagexp()==0 && exlistwithexpcorrandmetlistwithmetcorr.getRowflagrepl()==0){
                    modelAndView1.addObject("expcorronalltable", exlistwithexpcorrandmetlistwithmetcorr.getExpcorrreplcorr().get(0));
                    modelAndView1.addObject("replcorronalltable", exlistwithexpcorrandmetlistwithmetcorr.getExpcorrreplcorr().get(1));
                }else if(exlistwithexpcorrandmetlistwithmetcorr.getRowflagexp()==0 && exlistwithexpcorrandmetlistwithmetcorr.getRowflagrepl()==1){
                    modelAndView1.addObject("expcorronalltable", exlistwithexpcorrandmetlistwithmetcorr.getExpcorrreplcorr().get(0));
                }
                else if(exlistwithexpcorrandmetlistwithmetcorr.getRowflagexp()==1 && exlistwithexpcorrandmetlistwithmetcorr.getRowflagrepl()==0){
                    modelAndView1.addObject("replcorronalltable", exlistwithexpcorrandmetlistwithmetcorr.getExpcorrreplcorr().get(0));
                }
                modelAndView1.addObject("allorsome",allorsome);
                modelAndView1.addObject("missval",missval);
                modelAndView1.addObject("missvalmtbsexp",exlistwithexpcorrandmetlistwithmetcorr.getMissvalmtbsexp());
                modelAndView1.addObject("missvalmtbsrepl",exlistwithexpcorrandmetlistwithmetcorr.getMissvalmtbsrepl());
                modelAndView1.addObject("rowflagexp",exlistwithexpcorrandmetlistwithmetcorr.getRowflagexp());
                modelAndView1.addObject("rowflagrepl",exlistwithexpcorrandmetlistwithmetcorr.getRowflagrepl());
                modelAndView=modelAndView1;
            }else if(allorsome.equals("onselectedexp")){
                ModelAndView modelAndView2 = new ModelAndView("pearson2");
                Project project = proserv.getByProjectId(UUID.fromString(projectid));
                modelAndView2.addObject("projectpearson2", project);
                List<String> metlist=new ArrayList<>();
                if(missval.equals("omit")){
                    metlist=sparkStatisticsPearson.omitInExperiments(experimentids,ionmode);
                }else {
                    metlist=sparkStatisticsPearson.replaceWithValueInExperiments(experimentids,ionmode);
                }
                if (metlist != null && !metlist.isEmpty() && metlist.size()>1) {
                    Integer rowflag=0;
                    modelAndView2.addObject("mtblist",metlist);
                    modelAndView2.addObject("explist",experimentids);
                    modelAndView2.addObject("allorsome",allorsome);
                    modelAndView2.addObject("missval",missval);
                    modelAndView2.addObject("rowflag",rowflag);
                }else{
                    Integer rowflag=1;
                    modelAndView2.addObject("rowflag",rowflag);
                }
                modelAndView=modelAndView2;
            }else if(allorsome.equals("onselectedrepl")){
                ModelAndView modelAndView2 = new ModelAndView("pearson2");
                Project project = proserv.getByProjectId(UUID.fromString(projectid));
                modelAndView2.addObject("projectpearson2", project);
                List<String> metlist=new ArrayList<>();
                if(missval.equals("omit")){
                    metlist=sparkStatisticsPearson.omitInReplicates(experimentids);
                }else {
                    metlist=sparkStatisticsPearson.replaceWithValueInReplicates(experimentids);
                }
                if (metlist != null && !metlist.isEmpty() && metlist.size()>1) {
                    Integer rowflag=0;
                    modelAndView2.addObject("mtblist",metlist);
                    modelAndView2.addObject("explist",experimentids);
                    modelAndView2.addObject("allorsome",allorsome);
                    modelAndView2.addObject("missval",missval);
                    modelAndView2.addObject("rowflag",rowflag);
                }else{
                    Integer rowflag=2;
                    modelAndView2.addObject("rowflag",rowflag);
                }
                modelAndView=modelAndView2;
            }else if(allorsome.equals("onselectedmtb")){
                ModelAndView modelAndView2 = new ModelAndView("pearson2");
                Project project = proserv.getByProjectId(UUID.fromString(projectid));
                modelAndView2.addObject("projectpearson2", project);
                List<String> metlist=new ArrayList<>();
                if(missval.equals("omit")){
                    metlist=sparkStatisticsPearson.omitInExperiments(experimentids,ionmode);
                }else {
                    metlist=sparkStatisticsPearson.replaceWithValueInExperiments(experimentids,ionmode);
                }
                if (metlist != null && !metlist.isEmpty() && metlist.size()>1) {
                    Integer rowflag=0;
                    modelAndView2.addObject("mtblist",metlist);
                    modelAndView2.addObject("explist",experimentids);
                    modelAndView2.addObject("allorsome",allorsome);
                    modelAndView2.addObject("missval",missval);
                    modelAndView2.addObject("rowflag",rowflag);
                    modelAndView2.addObject("ionmode",ionmode);
                    
                }else{
                    Integer rowflag=1;
                    modelAndView2.addObject("rowflag",rowflag);
                }
                modelAndView=modelAndView2;
            }
            UserProject userprojectfromdbafter = userProjectService.findUserByUserProjectKey(username, projectid);
            UserProjectForm userproformafter = userprouserproform.convert(userprojectfromdbafter);
            userproformafter.setUserProjectKey(userprojectfromdbafter.getUserProjectKey());
            userproformafter.setOwner(userprojectfromdbafter.getOwner());
            userproformafter.setKnimeprocessing(userprojectfromdbafter.getKnimeprocessing());
            userproformafter.setSparkanalysis(Boolean.FALSE);
            userproformafter.setAccess(userprojectfromdbafter.getAccess());
            userProjectService.saveOrUpdateUserProjectForm(userproformafter);
        }else{
            modelAndView = new ModelAndView("pearson");
            Project project = proserv.getByProjectId(UUID.fromString(projectid));
            modelAndView.addObject("projectpearson", project);
            modelAndView.addObject("ifongoingprocessing", ifongoingprocessing);
        }
        return modelAndView;
    }
    
    
    @RequestMapping(value="/pearsonresultsview",method=RequestMethod.POST)
    public ModelAndView pear2post(@RequestParam(value="projectid") String projectid,@RequestParam(value="ionmode",required = false)String ionmode,@RequestParam(value="description") List<String> mtblist,@RequestParam(value="experimentid") List<String> experimentids,@RequestParam(value="allorsome") String allorsome,@RequestParam(value="missval") String missval){
        
        Boolean ifongoingprocessing = userProjectService.ifongoingpreprocessing();
        if(!ifongoingprocessing){
            Authentication auth = SecurityContextHolder.getContext().getAuthentication();
            String username = auth.getName();
            UserProject userprojectfromdb = userProjectService.findUserByUserProjectKey(username, projectid);
            UserProjectForm userproform = userprouserproform.convert(userprojectfromdb);
            userproform.setUserProjectKey(userprojectfromdb.getUserProjectKey());
            userproform.setOwner(userprojectfromdb.getOwner());
            userproform.setKnimeprocessing(userprojectfromdb.getKnimeprocessing());
            userproform.setSparkanalysis(Boolean.TRUE);
            userproform.setAccess(userprojectfromdb.getAccess());
            userProjectService.saveOrUpdateUserProjectForm(userproform);
            
            ModelAndView modelAndView = new ModelAndView("pearsonresults");
            Project project = proserv.getByProjectId(UUID.fromString(projectid));
            modelAndView.addObject("projectpearsonresults", project);
            if(allorsome.equals("onselectedexp")){
                Explistrepliclistwithcorrpearsonspearman exlistwithexpcorrandmetlistwithmetcorr = sparkStatisticsPearson.pearson(experimentids,mtblist,allorsome,missval,ionmode);
                List<String> proexplistafter = new ArrayList<>();
                List<ProjectExperiment> proexplistbefore =proexpserv.listAllprojectexperimentbyproid(UUID.fromString(projectid));
                for(String expid:experimentids){
                    for(ProjectExperiment proexpname:proexplistbefore){
                        if(expid.equals(proexpname.getProjectExperimentKey().getExperimentid())){
                            proexplistafter.add(proexpname.getExpname());
                        }
                    }
                }

                modelAndView.addObject("explist",proexplistafter);
                modelAndView.addObject("expcorronselectedtable", exlistwithexpcorrandmetlistwithmetcorr.getExpcorrreplcorr().get(0));
                modelAndView.addObject("allorsome",allorsome);
                modelAndView.addObject("missval",missval);
                modelAndView.addObject("missvalmtbsexp",exlistwithexpcorrandmetlistwithmetcorr.getMissvalmtbsexp());
            }else if(allorsome.equals("onselectedrepl")){
                Explistrepliclistwithcorrpearsonspearman exlistwithexpcorrandmetlistwithmetcorr = sparkStatisticsPearson.pearson(experimentids,mtblist,allorsome,missval,ionmode);
                List<String> proexplistafter = new ArrayList<>();
                List<String> proreplilistafter = new ArrayList<>();
                List<ProjectExperiment> proexplistbefore =proexpserv.listAllprojectexperimentbyproid(UUID.fromString(projectid));
                for(String expid:experimentids){
                    for(ProjectExperiment proexpname:proexplistbefore){
                        if(expid.equals(proexpname.getProjectExperimentKey().getExperimentid())){
                            proexplistafter.add(proexpname.getExpname()+"__"+proexpname.getProjectExperimentKey().getExperimentid());
                        }
                    }
                }
                List<String> proreplilistbefore=exlistwithexpcorrandmetlistwithmetcorr.getReplilist();
                for(String replitem:proreplilistbefore){
                    String[] temprepl=replitem.split("__");
                    for(String expitem:proexplistafter ){
                        String[] tempexp=expitem.split("__");
                        if(tempexp[1].equals(temprepl[0])){
                            proreplilistafter.add(tempexp[0]+"__"+temprepl[1]);
                        }
                    }
                }
                modelAndView.addObject("replilist",proreplilistafter);
                modelAndView.addObject("replcorronselectedtable", exlistwithexpcorrandmetlistwithmetcorr.getExpcorrreplcorr().get(0));
                modelAndView.addObject("allorsome",allorsome);
                modelAndView.addObject("missval",missval);
                modelAndView.addObject("missvalmtbsrepl",exlistwithexpcorrandmetlistwithmetcorr.getMissvalmtbsrepl());
            }else if(allorsome.equals("onselectedmtb")){
                Explistrepliclistwithcorrpearsonspearman exlistwithexpcorrandmetlistwithmetcorr = sparkStatisticsPearson.pearson(experimentids,mtblist,allorsome,missval,ionmode);
                modelAndView.addObject("mtblist",mtblist);
                modelAndView.addObject("metcorronselectedtable", exlistwithexpcorrandmetlistwithmetcorr.getExpcorrreplcorr().get(0));
                modelAndView.addObject("allorsome",allorsome);
                modelAndView.addObject("missval",missval);
                modelAndView.addObject("ionmode",ionmode);
                modelAndView.addObject("missvalmtbsexp",exlistwithexpcorrandmetlistwithmetcorr.getMissvalmtbsexp());
            }
            UserProject userprojectfromdbafter = userProjectService.findUserByUserProjectKey(username, projectid);
            UserProjectForm userproformafter = userprouserproform.convert(userprojectfromdbafter);
            userproformafter.setUserProjectKey(userprojectfromdbafter.getUserProjectKey());
            userproformafter.setOwner(userprojectfromdbafter.getOwner());
            userproformafter.setKnimeprocessing(userprojectfromdbafter.getKnimeprocessing());
            userproformafter.setSparkanalysis(Boolean.FALSE);
            userproformafter.setAccess(userprojectfromdbafter.getAccess());
            userProjectService.saveOrUpdateUserProjectForm(userproformafter);
            return modelAndView;
        }else{
            ModelAndView modelAndView = new ModelAndView("pearson");
            Project project = proserv.getByProjectId(UUID.fromString(projectid));
            modelAndView.addObject("projectpearson", project);
            modelAndView.addObject("ifongoingprocessing", ifongoingprocessing);
            return modelAndView;
        }
    }

    @RequestMapping(value="/spearmanview/{projectid}",method=RequestMethod.GET)
    public ModelAndView spearget(@PathVariable String projectid) {
        ModelAndView modelAndView = new ModelAndView("spearman");
        Project project = proserv.getByProjectId(UUID.fromString(projectid));
        modelAndView.addObject("projectspearman", project);
        List<ProjectExperiment> proexplistbefore =proexpserv.listAllprojectexperimentbyproid(UUID.fromString(projectid));
        List<ProjectExperiment> proexplistafter = new ArrayList<ProjectExperiment>();
        for(ProjectExperiment proexp:proexplistbefore){
            if(proexp.getPreprocessed()){
                proexplistafter.add(proexp);
            }
        }
        Boolean ifongoingprocessing = Boolean.FALSE;
        modelAndView.addObject("ifongoingprocessing", ifongoingprocessing);
        modelAndView.addObject("experimentlist",proexplistafter);
        return modelAndView;
    }
    @RequestMapping(value="/spearmanview",method=RequestMethod.POST)
    public ModelAndView spearpost(@RequestParam(value="projectid") String projectid,@RequestParam(value="ionmode",required = false)String ionmode,@RequestParam(value="allorsome") String allorsome,@RequestParam(value="experimentid") List<String> experimentids,@RequestParam(value="missval") String missval) {
        
        ModelAndView modelAndView = new ModelAndView();
        Boolean ifongoingprocessing = userProjectService.ifongoingpreprocessing();
        if(!ifongoingprocessing){
            Authentication auth = SecurityContextHolder.getContext().getAuthentication();
            String username = auth.getName();
            UserProject userprojectfromdb = userProjectService.findUserByUserProjectKey(username, projectid);
            UserProjectForm userproform = userprouserproform.convert(userprojectfromdb);
            userproform.setUserProjectKey(userprojectfromdb.getUserProjectKey());
            userproform.setOwner(userprojectfromdb.getOwner());
            userproform.setKnimeprocessing(userprojectfromdb.getKnimeprocessing());
            userproform.setSparkanalysis(Boolean.TRUE);
            userproform.setAccess(userprojectfromdb.getAccess());
            userProjectService.saveOrUpdateUserProjectForm(userproform);
            
            if(allorsome.equals("onallexprepl")){
                ModelAndView modelAndView1 = new ModelAndView("spearmanresults");
                Project project = proserv.getByProjectId(UUID.fromString(projectid));
                modelAndView1.addObject("projectspearmanresults", project);
                List<String> mtblist = new ArrayList<>();
                Explistrepliclistwithcorrpearsonspearman exlistwithexpcorrandmetlistwithmetcorr = sparkStatisticsSpearman.spearman(experimentids,mtblist,allorsome,missval,ionmode);

                List<String> proexplistafter = new ArrayList<>();
                List<String> proreplilistafter = new ArrayList<>();
                List<String> proexplistafterfinal = new ArrayList<>();
                List<ProjectExperiment> proexplistbefore =proexpserv.listAllprojectexperimentbyproid(UUID.fromString(projectid));
                for(String expid:experimentids){
                    for(ProjectExperiment proexpname:proexplistbefore){
                        if(expid.equals(proexpname.getProjectExperimentKey().getExperimentid())){
                            proexplistafter.add(proexpname.getExpname()+"__"+proexpname.getProjectExperimentKey().getExperimentid());
                            proexplistafterfinal.add(proexpname.getExpname());
                        }
                    }
                }

                List<String> proreplilistbefore=exlistwithexpcorrandmetlistwithmetcorr.getReplilist();
                for(String replitem:proreplilistbefore){
                    String[] temprepl=replitem.split("__");
                    for(String expitem:proexplistafter ){
                        String[] tempexp=expitem.split("__");
                        if(tempexp[1].equals(temprepl[0])){
                            proreplilistafter.add(tempexp[0]+"__"+temprepl[1]);
                        }
                    }
                }

                modelAndView1.addObject("explist",proexplistafterfinal);
                modelAndView1.addObject("replilist",proreplilistafter);
                if(exlistwithexpcorrandmetlistwithmetcorr.getRowflagexp()==0 && exlistwithexpcorrandmetlistwithmetcorr.getRowflagrepl()==0){
                    modelAndView1.addObject("expcorronalltable", exlistwithexpcorrandmetlistwithmetcorr.getExpcorrreplcorr().get(0));
                    modelAndView1.addObject("replcorronalltable", exlistwithexpcorrandmetlistwithmetcorr.getExpcorrreplcorr().get(1));
                }else if(exlistwithexpcorrandmetlistwithmetcorr.getRowflagexp()==0 && exlistwithexpcorrandmetlistwithmetcorr.getRowflagrepl()==1){
                    modelAndView1.addObject("expcorronalltable", exlistwithexpcorrandmetlistwithmetcorr.getExpcorrreplcorr().get(0));
                }
                else if(exlistwithexpcorrandmetlistwithmetcorr.getRowflagexp()==1 && exlistwithexpcorrandmetlistwithmetcorr.getRowflagrepl()==0){
                    modelAndView1.addObject("replcorronalltable", exlistwithexpcorrandmetlistwithmetcorr.getExpcorrreplcorr().get(0));
                }
                modelAndView1.addObject("allorsome",allorsome);
                modelAndView1.addObject("missval",missval);
                modelAndView1.addObject("missvalmtbsexp",exlistwithexpcorrandmetlistwithmetcorr.getMissvalmtbsexp());
                modelAndView1.addObject("missvalmtbsrepl",exlistwithexpcorrandmetlistwithmetcorr.getMissvalmtbsrepl());
                modelAndView1.addObject("rowflagexp",exlistwithexpcorrandmetlistwithmetcorr.getRowflagexp());
                modelAndView1.addObject("rowflagrepl",exlistwithexpcorrandmetlistwithmetcorr.getRowflagrepl());
                modelAndView=modelAndView1;
            }else if(allorsome.equals("onselectedexp")){
                ModelAndView modelAndView2 = new ModelAndView("spearman2");
                Project project = proserv.getByProjectId(UUID.fromString(projectid));
                modelAndView2.addObject("projectspearman2", project);
                List<String> metlist=new ArrayList<>();
                if(missval.equals("omit")){
                    metlist=sparkStatisticsSpearman.omitInExperiments(experimentids,ionmode);
                }else {
                    metlist=sparkStatisticsSpearman.replaceWithValueInExperiments(experimentids,ionmode);
                }
                if (metlist != null && !metlist.isEmpty() && metlist.size()>1) {
                    Integer rowflag=0;
                    modelAndView2.addObject("mtblist",metlist);
                    modelAndView2.addObject("explist",experimentids);
                    modelAndView2.addObject("allorsome",allorsome);
                    modelAndView2.addObject("missval",missval);
                    modelAndView2.addObject("rowflag",rowflag);
                }else{
                    Integer rowflag=1;
                    modelAndView2.addObject("rowflag",rowflag);
                }
                modelAndView=modelAndView2;
            }else if(allorsome.equals("onselectedrepl")){
                ModelAndView modelAndView2 = new ModelAndView("spearman2");
                Project project = proserv.getByProjectId(UUID.fromString(projectid));
                modelAndView2.addObject("projectspearman2", project);
                List<String> metlist=new ArrayList<>();
                if(missval.equals("omit")){
                    metlist=sparkStatisticsSpearman.omitInReplicates(experimentids);
                }else {
                    metlist=sparkStatisticsSpearman.replaceWithValueInReplicates(experimentids);
                }
                if (metlist != null && !metlist.isEmpty() && metlist.size()>1) {
                    Integer rowflag=0;
                    modelAndView2.addObject("mtblist",metlist);
                    modelAndView2.addObject("explist",experimentids);
                    modelAndView2.addObject("allorsome",allorsome);
                    modelAndView2.addObject("missval",missval);
                    modelAndView2.addObject("rowflag",rowflag);
                }else{
                    Integer rowflag=2;
                    modelAndView2.addObject("rowflag",rowflag);
                }
                modelAndView=modelAndView2;
            }else if(allorsome.equals("onselectedmtb")){
                ModelAndView modelAndView2 = new ModelAndView("spearman2");
                Project project = proserv.getByProjectId(UUID.fromString(projectid));
                modelAndView2.addObject("projectspearman2", project);
                List<String> metlist=new ArrayList<>();
                if(missval.equals("omit")){
                    metlist=sparkStatisticsSpearman.omitInExperiments(experimentids,ionmode);
                }else {
                    metlist=sparkStatisticsSpearman.replaceWithValueInExperiments(experimentids,ionmode);
                }
                if (metlist != null && !metlist.isEmpty() && metlist.size()>1) {
                    Integer rowflag=0;
                    modelAndView2.addObject("mtblist",metlist);
                    modelAndView2.addObject("explist",experimentids);
                    modelAndView2.addObject("allorsome",allorsome);
                    modelAndView2.addObject("missval",missval);
                    modelAndView2.addObject("ionmode",ionmode);
                    modelAndView2.addObject("rowflag",rowflag);
                }else{
                    Integer rowflag=1;
                    modelAndView2.addObject("rowflag",rowflag);
                }
                modelAndView=modelAndView2;
            }
            UserProject userprojectfromdbafter = userProjectService.findUserByUserProjectKey(username, projectid);
            UserProjectForm userproformafter = userprouserproform.convert(userprojectfromdbafter);
            userproformafter.setUserProjectKey(userprojectfromdbafter.getUserProjectKey());
            userproformafter.setOwner(userprojectfromdbafter.getOwner());
            userproformafter.setKnimeprocessing(userprojectfromdbafter.getKnimeprocessing());
            userproformafter.setSparkanalysis(Boolean.FALSE);
            userproformafter.setAccess(userprojectfromdbafter.getAccess());
            userProjectService.saveOrUpdateUserProjectForm(userproformafter);
        }else{
            modelAndView = new ModelAndView("spearman");
            Project project = proserv.getByProjectId(UUID.fromString(projectid));
            modelAndView.addObject("projectspearman", project);
            modelAndView.addObject("ifongoingprocessing", ifongoingprocessing);
        }
        return modelAndView;
    }
    
    
    @RequestMapping(value="/spearmanresultsview",method=RequestMethod.POST)
    public ModelAndView spear2post(@RequestParam(value="projectid") String projectid,@RequestParam(value="ionmode",required = false)String ionmode,@RequestParam(value="description") List<String> mtblist,@RequestParam(value="experimentid") List<String> experimentids,@RequestParam(value="allorsome") String allorsome,@RequestParam(value="missval") String missval){
        Boolean ifongoingprocessing = userProjectService.ifongoingpreprocessing();
        if(!ifongoingprocessing){
            Authentication auth = SecurityContextHolder.getContext().getAuthentication();
            String username = auth.getName();
            UserProject userprojectfromdb = userProjectService.findUserByUserProjectKey(username, projectid);
            UserProjectForm userproform = userprouserproform.convert(userprojectfromdb);
            userproform.setUserProjectKey(userprojectfromdb.getUserProjectKey());
            userproform.setOwner(userprojectfromdb.getOwner());
            userproform.setKnimeprocessing(userprojectfromdb.getKnimeprocessing());
            userproform.setSparkanalysis(Boolean.TRUE);
            userproform.setAccess(userprojectfromdb.getAccess());
            userProjectService.saveOrUpdateUserProjectForm(userproform);
            
            ModelAndView modelAndView = new ModelAndView("spearmanresults");
            Project project = proserv.getByProjectId(UUID.fromString(projectid));
            modelAndView.addObject("projectspearmanresults", project);

            if(allorsome.equals("onselectedexp")){
                Explistrepliclistwithcorrpearsonspearman exlistwithexpcorrandmetlistwithmetcorr = sparkStatisticsSpearman.spearman(experimentids,mtblist,allorsome,missval,ionmode);
                List<String> proexplistafter = new ArrayList<>();
                List<ProjectExperiment> proexplistbefore =proexpserv.listAllprojectexperimentbyproid(UUID.fromString(projectid));
                for(String expid:experimentids){
                    for(ProjectExperiment proexpname:proexplistbefore){
                        if(expid.equals(proexpname.getProjectExperimentKey().getExperimentid())){
                            proexplistafter.add(proexpname.getExpname());
                        }
                    }
                }

                modelAndView.addObject("explist",proexplistafter);
                modelAndView.addObject("expcorronselectedtable", exlistwithexpcorrandmetlistwithmetcorr.getExpcorrreplcorr().get(0));
                modelAndView.addObject("allorsome",allorsome);
                modelAndView.addObject("missval",missval);
                modelAndView.addObject("missvalmtbsexp",exlistwithexpcorrandmetlistwithmetcorr.getMissvalmtbsexp());
            }else if(allorsome.equals("onselectedrepl")){
                Explistrepliclistwithcorrpearsonspearman exlistwithexpcorrandmetlistwithmetcorr = sparkStatisticsSpearman.spearman(experimentids,mtblist,allorsome,missval,ionmode);
                List<String> proexplistafter = new ArrayList<>();
                List<String> proreplilistafter = new ArrayList<>();
                List<ProjectExperiment> proexplistbefore =proexpserv.listAllprojectexperimentbyproid(UUID.fromString(projectid));
                for(String expid:experimentids){
                    for(ProjectExperiment proexpname:proexplistbefore){
                        if(expid.equals(proexpname.getProjectExperimentKey().getExperimentid())){
                            proexplistafter.add(proexpname.getExpname()+"__"+proexpname.getProjectExperimentKey().getExperimentid());
                        }
                    }
                }
                List<String> proreplilistbefore=exlistwithexpcorrandmetlistwithmetcorr.getReplilist();
                for(String replitem:proreplilistbefore){
                    String[] temprepl=replitem.split("__");
                    for(String expitem:proexplistafter ){
                        String[] tempexp=expitem.split("__");
                        if(tempexp[1].equals(temprepl[0])){
                            proreplilistafter.add(tempexp[0]+"__"+temprepl[1]);
                        }
                    }
                }
                modelAndView.addObject("replilist",proreplilistafter);
                modelAndView.addObject("replcorronselectedtable", exlistwithexpcorrandmetlistwithmetcorr.getExpcorrreplcorr().get(0));
                modelAndView.addObject("allorsome",allorsome);
                modelAndView.addObject("missval",missval);
                modelAndView.addObject("missvalmtbsrepl",exlistwithexpcorrandmetlistwithmetcorr.getMissvalmtbsrepl());
            }else if(allorsome.equals("onselectedmtb")){
                Explistrepliclistwithcorrpearsonspearman exlistwithexpcorrandmetlistwithmetcorr = sparkStatisticsSpearman.spearman(experimentids,mtblist,allorsome,missval,ionmode);
                modelAndView.addObject("mtblist",mtblist);
                modelAndView.addObject("metcorronselectedtable", exlistwithexpcorrandmetlistwithmetcorr.getExpcorrreplcorr().get(0));
                modelAndView.addObject("allorsome",allorsome);
                modelAndView.addObject("missval",missval);
                modelAndView.addObject("ionmode",ionmode);
                modelAndView.addObject("missvalmtbsexp",exlistwithexpcorrandmetlistwithmetcorr.getMissvalmtbsexp());
            }
            UserProject userprojectfromdbafter = userProjectService.findUserByUserProjectKey(username, projectid);
            UserProjectForm userproformafter = userprouserproform.convert(userprojectfromdbafter);
            userproformafter.setUserProjectKey(userprojectfromdbafter.getUserProjectKey());
            userproformafter.setOwner(userprojectfromdbafter.getOwner());
            userproformafter.setKnimeprocessing(userprojectfromdbafter.getKnimeprocessing());
            userproformafter.setSparkanalysis(Boolean.FALSE);
            userproformafter.setAccess(userprojectfromdbafter.getAccess());
            userProjectService.saveOrUpdateUserProjectForm(userproformafter);
            return modelAndView;
        }else{
            ModelAndView modelAndView = new ModelAndView("spearman");
            Project project = proserv.getByProjectId(UUID.fromString(projectid));
            modelAndView.addObject("projectspearman", project);
            modelAndView.addObject("ifongoingprocessing", ifongoingprocessing);
            return modelAndView;
        }
    }
    
    @RequestMapping(value="/pcaview/{projectid}",method=RequestMethod.GET)
    public ModelAndView pcanalysis(@PathVariable String projectid) {
        ModelAndView modelAndView = new ModelAndView("pca");
        Project project = proserv.getByProjectId(UUID.fromString(projectid));
        modelAndView.addObject("projectpca", project);
        List<ProjectExperiment> proexplistbefore =proexpserv.listAllprojectexperimentbyproid(UUID.fromString(projectid));
        List<ProjectExperiment> proexplistafter = new ArrayList<>();
        for(ProjectExperiment proexp:proexplistbefore){
            if(proexp.getPreprocessed()){
                proexplistafter.add(proexp);
            }
        }
        Boolean ifongoingprocessing = Boolean.FALSE;
        modelAndView.addObject("ifongoingprocessing", ifongoingprocessing);
        modelAndView.addObject("experimentlist",proexplistafter);
        return modelAndView;
    }
    @RequestMapping(value="/pcaview",method=RequestMethod.POST)
    public ModelAndView pcan(@RequestParam(value="projectid") String projectid,@RequestParam(value="experimentid") List<String> experimentids,@RequestParam(value="nrmtbs",required=false) Integer nrmtbs,@RequestParam(value="allorsome",required=false) String allorsome,@RequestParam(value="includemore",required=false) String includemore,@RequestParam(value="missval") String missval,@RequestParam(value="pcalevel") String pcalevel,@RequestParam(value="normval") String normval,@RequestParam(value="ionmode") String ionmode) {
        
        
        Boolean ifongoingprocessing = userProjectService.ifongoingpreprocessing();
        if(!ifongoingprocessing){
            Authentication auth = SecurityContextHolder.getContext().getAuthentication();
            String username = auth.getName();
            UserProject userprojectfromdb = userProjectService.findUserByUserProjectKey(username, projectid);
            UserProjectForm userproform = userprouserproform.convert(userprojectfromdb);
            userproform.setUserProjectKey(userprojectfromdb.getUserProjectKey());
            userproform.setOwner(userprojectfromdb.getOwner());
            userproform.setKnimeprocessing(userprojectfromdb.getKnimeprocessing());
            userproform.setSparkanalysis(Boolean.TRUE);
            userproform.setAccess(userprojectfromdb.getAccess());
            userProjectService.saveOrUpdateUserProjectForm(userproform);
            
            ModelAndView modelAndView1 = new ModelAndView("pcaresults");
            Project project = proserv.getByProjectId(UUID.fromString(projectid));
            modelAndView1.addObject("projectpcaresults", project);
            if(allorsome!=null && allorsome.equals("onall")){
                List<String> proexplistafter = new ArrayList<>();
                List<ProjectExperiment> proexplistbefore =proexpserv.listAllprojectexperimentbyproid(UUID.fromString(projectid));
                for(String expid:experimentids){
                    for(ProjectExperiment proexpname:proexplistbefore){
                        if(expid.equals(proexpname.getProjectExperimentKey().getExperimentid())){
                            proexplistafter.add(proexpname.getExpname()+"__"+proexpname.getProjectExperimentKey().getExperimentid());
                        }
                    }
                }
                List<String> metabolitelist = new ArrayList<>();
                modelAndView1.addObject("pcalevel",pcalevel);
                modelAndView1.addObject("missval",missval);
                modelAndView1.addObject("allorsome",allorsome);
                if(pcalevel.equals("onexps")){
                    //PCA for experiments
                    Experimentslistpca experimentslistpca = sparkStatisticsPca.expspca(experimentids,metabolitelist,allorsome,missval,normval,ionmode);
                    modelAndView1.addObject("expslist",proexplistafter);
                    modelAndView1.addObject("expspcatable",experimentslistpca.getExpspca());
                    modelAndView1.addObject("expsvariances", experimentslistpca.getExpsvariance());
                    modelAndView1.addObject("missvalmtbs",experimentslistpca.getMissvalmtbs());
                    modelAndView1.addObject("rowflag",experimentslistpca.getRowflag());
                    modelAndView1.addObject("nrmtbsexps",experimentslistpca.getNrmtbsexps());
                }else if(pcalevel.equals("onrepls")){
                    //PCA for replicates
                    Replicateslistpca replicateslistpca = sparkStatisticsPca.replicatespca(experimentids,metabolitelist,allorsome, missval,normval,ionmode);
                    modelAndView1.addObject("expslist",proexplistafter);
                    modelAndView1.addObject("replilist",replicateslistpca.getReplilist());
                    modelAndView1.addObject("replipcatable", replicateslistpca.getReplicpca());
                    modelAndView1.addObject("replivariances",replicateslistpca.getReplicvariance());
                    modelAndView1.addObject("missvalmtbs",replicateslistpca.getMissvalmtbs());
                    modelAndView1.addObject("rowflag",replicateslistpca.getRowflag());
                    modelAndView1.addObject("nrmtbsexps",replicateslistpca.getNrmtbsexps());
                }
                UserProject userprojectfromdbafter = userProjectService.findUserByUserProjectKey(username, projectid);
                UserProjectForm userproformafter = userprouserproform.convert(userprojectfromdbafter);
                userproformafter.setUserProjectKey(userprojectfromdbafter.getUserProjectKey());
                userproformafter.setOwner(userprojectfromdbafter.getOwner());
                userproformafter.setKnimeprocessing(userprojectfromdbafter.getKnimeprocessing());
                userproformafter.setSparkanalysis(Boolean.FALSE);
                userproformafter.setAccess(userprojectfromdbafter.getAccess());
                userProjectService.saveOrUpdateUserProjectForm(userproformafter);
                return modelAndView1;

            }else if(allorsome!=null && allorsome.equals("onselected")){
                ModelAndView modelAndView2 = new ModelAndView("pca2");
                modelAndView2.addObject("projectpca2", project);
                List<String> metlist = new ArrayList<>();

                if(pcalevel.equals("onexps") && missval.equals("omit")){
                    metlist = sparkStatisticsPca.omitInExperiments(experimentids,ionmode);
                }else if(pcalevel.equals("onexps") && !missval.equals("omit")){
                    metlist = sparkStatisticsPca.replaceWithValueInExperiments(experimentids,ionmode);
                }else if(pcalevel.equals("onrepls") && missval.equals("omit")){
                    metlist = sparkStatisticsPca.omitInReplicates(experimentids,ionmode);            
                }else if(pcalevel.equals("onrepls") && !missval.equals("omit")){
                    metlist = sparkStatisticsPca.replaceWithValueInReplicates(experimentids,ionmode);
                }
                Integer rowflag=0;
                if (metlist != null && !metlist.isEmpty() && metlist.size()>1) {
                    modelAndView2.addObject("mtblist",metlist);
                    modelAndView2.addObject("explist",experimentids);
                    modelAndView2.addObject("allorsome",allorsome);
                    modelAndView2.addObject("pcalevel",pcalevel);
                    modelAndView2.addObject("missval",missval);
                    modelAndView2.addObject("normval",normval);
                    modelAndView2.addObject("ionmode",ionmode);
                    modelAndView2.addObject("rowflag",rowflag);
                }else{
                    if(pcalevel.equals("onrepls")){
                        rowflag=2;
                        modelAndView2.addObject("rowflag",rowflag);
                    }else{
                        rowflag=1;
                        modelAndView2.addObject("rowflag",rowflag);
                    }
                }
                UserProject userprojectfromdbafter = userProjectService.findUserByUserProjectKey(username, projectid);
                UserProjectForm userproformafter = userprouserproform.convert(userprojectfromdbafter);
                userproformafter.setUserProjectKey(userprojectfromdbafter.getUserProjectKey());
                userproformafter.setOwner(userprojectfromdbafter.getOwner());
                userproformafter.setKnimeprocessing(userprojectfromdbafter.getKnimeprocessing());
                userproformafter.setSparkanalysis(Boolean.FALSE);
                userproformafter.setAccess(userprojectfromdbafter.getAccess());
                userProjectService.saveOrUpdateUserProjectForm(userproformafter);
                return modelAndView2;            
            }else{
                Integer rowflag=0;
                Integer specialcase = 0;
                List<String> readylist = new ArrayList<>();
                List<String> metabolitelist = new ArrayList<>();
                if(missval.equals("omit")){
                    List<String> allmtbs = sparkStatisticsPca.omitInExperiments(experimentids,ionmode);
                    if(allmtbs != null && !allmtbs.isEmpty() && allmtbs.size()>1){
                        metabolitelist = sparkStatisticsPca.findtopmtbs(experimentids,allmtbs,nrmtbs,ionmode);
                        readylist.addAll(allmtbs);
                        readylist.removeAll(metabolitelist);
                        if(readylist.isEmpty() && includemore.equals("yes")){
                            includemore="no";
                            specialcase=1;
                            readylist=allmtbs;
                        }
                    }else{
                        rowflag=1;
                    }
                }else{
                    List<String> allmtbs = sparkStatisticsPca.replaceWithValueInExperiments(experimentids,ionmode);
                    if(allmtbs != null && !allmtbs.isEmpty() && allmtbs.size()>1){
                        metabolitelist = sparkStatisticsPca.findtopmtbs(experimentids,allmtbs,nrmtbs,ionmode);
                        readylist.addAll(allmtbs);
                        readylist.removeAll(metabolitelist);
                        if(readylist.isEmpty() && includemore.equals("yes")){
                            includemore="no";
                            specialcase=1;
                            readylist=allmtbs;
                        }
                    }else{
                        rowflag=1;
                    }
                }
                
                if(includemore.equals("yes")){
                    ModelAndView modelAndView2 = new ModelAndView("pca2");
                    modelAndView2.addObject("projectpca2", project);

                    modelAndView2.addObject("mtblist",readylist);
                    modelAndView2.addObject("topmtbs",metabolitelist);
                    modelAndView2.addObject("explist",experimentids);
                    modelAndView2.addObject("pcalevel",pcalevel);
                    modelAndView2.addObject("missval",missval);
                    modelAndView2.addObject("normval",normval);
                    modelAndView2.addObject("ionmode",ionmode);
                    modelAndView2.addObject("rowflag",rowflag);
                    UserProject userprojectfromdbafter = userProjectService.findUserByUserProjectKey(username, projectid);
                    UserProjectForm userproformafter = userprouserproform.convert(userprojectfromdbafter);
                    userproformafter.setUserProjectKey(userprojectfromdbafter.getUserProjectKey());
                    userproformafter.setOwner(userprojectfromdbafter.getOwner());
                    userproformafter.setKnimeprocessing(userprojectfromdbafter.getKnimeprocessing());
                    userproformafter.setSparkanalysis(Boolean.FALSE);
                    userproformafter.setAccess(userprojectfromdbafter.getAccess());
                    userProjectService.saveOrUpdateUserProjectForm(userproformafter);
                    return modelAndView2;   
                }else{
                    if(rowflag==0){
                        //PCA for metabolites
                        Metaboliteslistpca metaboliteslistpca = sparkStatisticsPca.metabolitespca(experimentids,metabolitelist,missval,normval,ionmode);
                        metaboliteslistpca.setRowflag(rowflag);
                        modelAndView1.addObject("mtbslist",metaboliteslistpca.getMtbslist());
                        modelAndView1.addObject("mtbspcatable", metaboliteslistpca.getMtbspca());
                        modelAndView1.addObject("mtbsvariances",metaboliteslistpca.getMtbsvariance());
                        modelAndView1.addObject("missvalmtbs",metaboliteslistpca.getMissvalmtbs());
                        modelAndView1.addObject("rowflag",metaboliteslistpca.getRowflag());
                        modelAndView1.addObject("nrmtbsexps",metaboliteslistpca.getNrmtbsexps());
                        modelAndView1.addObject("pcalevel",pcalevel);
                        modelAndView1.addObject("missval",missval);
                        modelAndView1.addObject("ionmode",ionmode);
                        modelAndView1.addObject("specialcase",specialcase);
                    }else{
                        modelAndView1.addObject("rowflag",rowflag);
                    }
                    UserProject userprojectfromdbafter = userProjectService.findUserByUserProjectKey(username, projectid);
                    UserProjectForm userproformafter = userprouserproform.convert(userprojectfromdbafter);
                    userproformafter.setUserProjectKey(userprojectfromdbafter.getUserProjectKey());
                    userproformafter.setOwner(userprojectfromdbafter.getOwner());
                    userproformafter.setKnimeprocessing(userprojectfromdbafter.getKnimeprocessing());
                    userproformafter.setSparkanalysis(Boolean.FALSE);
                    userproformafter.setAccess(userprojectfromdbafter.getAccess());
                    userProjectService.saveOrUpdateUserProjectForm(userproformafter);
                    return modelAndView1;   
                }
            }
            
        }else{
            ModelAndView modelAndView = new ModelAndView("pca");
            Project project = proserv.getByProjectId(UUID.fromString(projectid));
            modelAndView.addObject("projectpca", project);
            modelAndView.addObject("ifongoingprocessing", ifongoingprocessing);
            return modelAndView;
        }
        
    }
    
    @RequestMapping(value="/pcaresultsview",method=RequestMethod.POST)
    public ModelAndView pcan2(@RequestParam(value="projectid") String projectid,@RequestParam(value="experimentid") List<String> experimentids,@RequestParam(value="description") List<String> mtblist,@RequestParam(value="topmtb",required=false) List<String> topmtbs,@RequestParam(value="allorsome",required=false) String allorsome,@RequestParam(value="missval") String missval,@RequestParam(value="pcalevel") String pcalevel, @RequestParam(value="normval") String normval,@RequestParam(value="ionmode") String ionmode){
        Boolean ifongoingprocessing = userProjectService.ifongoingpreprocessing();
        if(!ifongoingprocessing){
            Authentication auth = SecurityContextHolder.getContext().getAuthentication();
            String username = auth.getName();
            UserProject userprojectfromdb = userProjectService.findUserByUserProjectKey(username, projectid);
            UserProjectForm userproform = userprouserproform.convert(userprojectfromdb);
            userproform.setUserProjectKey(userprojectfromdb.getUserProjectKey());
            userproform.setOwner(userprojectfromdb.getOwner());
            userproform.setKnimeprocessing(userprojectfromdb.getKnimeprocessing());
            userproform.setSparkanalysis(Boolean.TRUE);
            userproform.setAccess(userprojectfromdb.getAccess());
            userProjectService.saveOrUpdateUserProjectForm(userproform);
            
            ModelAndView modelAndView = new ModelAndView("pcaresults");
            Project project = proserv.getByProjectId(UUID.fromString(projectid));
            modelAndView.addObject("projectpcaresults", project);
            List<String> proexplistafter = new ArrayList<>();
            List<ProjectExperiment> proexplistbefore =proexpserv.listAllprojectexperimentbyproid(UUID.fromString(projectid));
            for(String expid:experimentids){
                for(ProjectExperiment proexpname:proexplistbefore){
                    if(expid.equals(proexpname.getProjectExperimentKey().getExperimentid())){
                        proexplistafter.add(proexpname.getExpname()+"__"+proexpname.getProjectExperimentKey().getExperimentid());
                    }
                }
            }
            if(pcalevel.equals("onexps")){
                //PCA for experiments
                Experimentslistpca experimentslistpca = sparkStatisticsPca.expspca(experimentids,mtblist,allorsome,missval,normval,ionmode);
                modelAndView.addObject("expslist",proexplistafter);
                modelAndView.addObject("expspcatable",experimentslistpca.getExpspca());
                modelAndView.addObject("expsvariances", experimentslistpca.getExpsvariance());
                modelAndView.addObject("missvalmtbs",experimentslistpca.getMissvalmtbs());
                modelAndView.addObject("rowflag",experimentslistpca.getRowflag());
                modelAndView.addObject("nrmtbsexps",experimentslistpca.getNrmtbsexps());
            }else if(pcalevel.equals("onrepls")){
                //PCA for replicates
                Replicateslistpca replicateslistpca = sparkStatisticsPca.replicatespca(experimentids,mtblist, allorsome,missval,normval,ionmode);
                modelAndView.addObject("expslist",proexplistafter);
                modelAndView.addObject("replilist",replicateslistpca.getReplilist());
                modelAndView.addObject("replipcatable", replicateslistpca.getReplicpca());
                modelAndView.addObject("replivariances",replicateslistpca.getReplicvariance());
                modelAndView.addObject("missvalmtbs",replicateslistpca.getMissvalmtbs());
                modelAndView.addObject("rowflag",replicateslistpca.getRowflag());
                modelAndView.addObject("nrmtbsexps",replicateslistpca.getNrmtbsexps());
            }else if(pcalevel.equals("onmtbs")){
                List<String>bothtopandusermtbs = new ArrayList<>();
                bothtopandusermtbs.addAll(topmtbs);
                bothtopandusermtbs.addAll(mtblist);
                //PCA for metabolites
                Metaboliteslistpca metaboliteslistpca = sparkStatisticsPca.metabolitespca(experimentids,bothtopandusermtbs,missval,normval,ionmode);
                modelAndView.addObject("mtbslist",metaboliteslistpca.getMtbslist());
                modelAndView.addObject("mtbspcatable", metaboliteslistpca.getMtbspca());
                modelAndView.addObject("mtbsvariances",metaboliteslistpca.getMtbsvariance());
                modelAndView.addObject("missvalmtbs",metaboliteslistpca.getMissvalmtbs());
                modelAndView.addObject("rowflag",0);
                modelAndView.addObject("nrmtbsexps",metaboliteslistpca.getNrmtbsexps());
                modelAndView.addObject("pcalevel",pcalevel);
                modelAndView.addObject("missval",missval);
                modelAndView.addObject("specialcase",0);
            }
            modelAndView.addObject("pcalevel",pcalevel);
            modelAndView.addObject("missval",missval);
            modelAndView.addObject("allorsome",allorsome);

            UserProject userprojectfromdbafter = userProjectService.findUserByUserProjectKey(username, projectid);
            UserProjectForm userproformafter = userprouserproform.convert(userprojectfromdbafter);
            userproformafter.setUserProjectKey(userprojectfromdbafter.getUserProjectKey());
            userproformafter.setOwner(userprojectfromdbafter.getOwner());
            userproformafter.setKnimeprocessing(userprojectfromdbafter.getKnimeprocessing());
            userproformafter.setSparkanalysis(Boolean.FALSE);
            userproformafter.setAccess(userprojectfromdbafter.getAccess());
            userProjectService.saveOrUpdateUserProjectForm(userproformafter);
            return modelAndView;
        }else{
            ModelAndView modelAndView = new ModelAndView("pca");
            Project project = proserv.getByProjectId(UUID.fromString(projectid));
            modelAndView.addObject("projectpca", project);
            modelAndView.addObject("ifongoingprocessing", ifongoingprocessing);
            return modelAndView;
        }
    }

    
    
    @RequestMapping(value="/clusteringview/{projectid}",method=RequestMethod.GET)
    public ModelAndView clusterin(@PathVariable String projectid) {
        ModelAndView modelAndView = new ModelAndView("clustering");
        Project project = proserv.getByProjectId(UUID.fromString(projectid));
        modelAndView.addObject("projectclustering", project);
        
        List<ProjectExperiment> proexplistbefore =proexpserv.listAllprojectexperimentbyproid(UUID.fromString(projectid));
        List<ProjectExperiment> proexplistafter = new ArrayList<>();
        
        for(ProjectExperiment proexp:proexplistbefore){
            if(proexp.getPreprocessed()){
                proexplistafter.add(proexp);
            }
        }
        int initialval=proexplistafter.size()/2;
        Boolean ifongoingprocessing = Boolean.FALSE;
        modelAndView.addObject("ifongoingprocessing", ifongoingprocessing);
        modelAndView.addObject("experimentlist", proexplistafter);
        modelAndView.addObject("initialval", initialval);
        return modelAndView;
    }
    
    
    @RequestMapping(value="/clusteringview",method=RequestMethod.POST)
    public ModelAndView clusteri(@RequestParam(value="projectid") String projectid,@RequestParam(value="experimentid") List<String> experimentids,@RequestParam(value="ionmode") String ionmode,@RequestParam(value="allorsome") String allorsome,@RequestParam(value="missval") String missval,@RequestParam(value="clusteringlevel") String clusteringlevel,@RequestParam(value="clusteringmethod") String clusteringmethod,@RequestParam(value="initialval") Integer initialval,@RequestParam(value="normval") String normval) {
        Boolean ifongoingprocessing = userProjectService.ifongoingpreprocessing();
        if(!ifongoingprocessing){
            Authentication auth = SecurityContextHolder.getContext().getAuthentication();
            String username = auth.getName();
            UserProject userprojectfromdb = userProjectService.findUserByUserProjectKey(username, projectid);
            UserProjectForm userproform = userprouserproform.convert(userprojectfromdb);
            userproform.setUserProjectKey(userprojectfromdb.getUserProjectKey());
            userproform.setOwner(userprojectfromdb.getOwner());
            userproform.setKnimeprocessing(userprojectfromdb.getKnimeprocessing());
            userproform.setSparkanalysis(Boolean.TRUE);
            userproform.setAccess(userprojectfromdb.getAccess());
            userProjectService.saveOrUpdateUserProjectForm(userproform);
            
            ModelAndView modelAndView1 = new ModelAndView("clusteringresults");
            Project project = proserv.getByProjectId(UUID.fromString(projectid));
            modelAndView1.addObject("projectclusteringresults", project);
            List<String> proexplistafter = new ArrayList<>();
            
            if(allorsome.equals("onall")){
                List<String> mtblist = new ArrayList<>();
                if(clusteringlevel.equals("onexps")){
                    
                    //Clustering for experiments
                    Experimentslistclustering experimentslistclustering = sparkStatisticsClustering.expsclustering(experimentids,mtblist,allorsome,missval,clusteringmethod,initialval,ionmode,normval);
                    proexplistafter=experimentslistclustering.getExpslist();
                    List<String> expsnameslist = new ArrayList<>();
                    for(String expid:proexplistafter){
                        ProjectExperimentKey tempkey = new ProjectExperimentKey();
                        tempkey.setProjectid(UUID.fromString(projectid));
                        tempkey.setExperimentid(expid);
                        ProjectExperiment tempproexp = proexpserv.getByProExpKey(tempkey);
                       
                        expsnameslist.add(tempproexp.getExpname());
                    }
                    modelAndView1.addObject("expsnameslist",expsnameslist);
                    modelAndView1.addObject("mtbsnameslist",experimentslistclustering.getMtbslist());
                    modelAndView1.addObject("expsclusts", experimentslistclustering.getClusts());
                    modelAndView1.addObject("clustinfo",experimentslistclustering.getClustinfo());
                    modelAndView1.addObject("missvalmtbs",experimentslistclustering.getMissvalmtbs());
                    modelAndView1.addObject("expsclustcenters",experimentslistclustering.getExpsclustscenters());
                    modelAndView1.addObject("silhouette",experimentslistclustering.getSilhouette());
                    modelAndView1.addObject("rowflag",experimentslistclustering.getRowflag());


                }else if(clusteringlevel.equals("onrepls")){
                    //Clustering for replicates
                    Replicateslistclustering replicateslistclustering = sparkStatisticsClustering.repliclustering(experimentids, mtblist, allorsome, missval, clusteringmethod, initialval,ionmode,normval);
                    List<String> expsnameslist = new ArrayList<>();
                    List<String> replinameslist=new ArrayList<>();
                    proexplistafter=replicateslistclustering.getReplilist();
                    List<String>expidrepl = new ArrayList<>();
                    for(String finame:proexplistafter){
                        String[] temp1 = finame.split("[.]");
                        expidrepl.add(temp1[0]);
                    }
                    int expidreplnum=expidrepl.size();

                    for(int i=0;i<expidreplnum;i++){
                        String[] temp2 = expidrepl.get(i).split("__");
                        ProjectExperimentKey tempkey = new ProjectExperimentKey();
                        tempkey.setProjectid(UUID.fromString(projectid));
                        tempkey.setExperimentid(temp2[0]);
                        ProjectExperiment tempproexp = proexpserv.getByProExpKey(tempkey);
                        
                        expsnameslist.add(tempproexp.getExpname());
                        replinameslist.add(temp2[1]);
                    }
                    modelAndView1.addObject("expsnameslist",expsnameslist);
                    modelAndView1.addObject("replinameslist",replinameslist);
                    modelAndView1.addObject("mtbsnameslist",replicateslistclustering.getMtbslist());
                    modelAndView1.addObject("repliclusts", replicateslistclustering.getClusts());
                    modelAndView1.addObject("clustinfo",replicateslistclustering.getClustinfo());
                    modelAndView1.addObject("missvalmtbs",replicateslistclustering.getMissvalmtbs());
                    modelAndView1.addObject("repliclustcenters",replicateslistclustering.getRepliclustscenters());
                    modelAndView1.addObject("silhouette",replicateslistclustering.getSilhouette());
                    modelAndView1.addObject("rowflag",replicateslistclustering.getRowflag());
                }else if(clusteringlevel.equals("onmtbs")){
                    
                    //Clustering for metabolites
                    Metaboliteslistclustering metaboliteslistclustering = sparkStatisticsClustering.metaboliteclustering(experimentids,mtblist,allorsome,missval,clusteringmethod,initialval,ionmode,normval);
                    //we need the names of experiments also
                    proexplistafter=metaboliteslistclustering.getExpslist();
                    List<String> expsnameslist = new ArrayList<>();
                    for(String expid:proexplistafter){
                        ProjectExperimentKey tempkey = new ProjectExperimentKey();
                        tempkey.setProjectid(UUID.fromString(projectid));
                        tempkey.setExperimentid(expid);
                        ProjectExperiment tempproexp = proexpserv.getByProExpKey(tempkey);
                        
                        expsnameslist.add(tempproexp.getExpname());
                    }
                    modelAndView1.addObject("expsnameslist",expsnameslist);
                    modelAndView1.addObject("mtbsnameslist",metaboliteslistclustering.getMtbslist());
                    modelAndView1.addObject("mtbsclusts", metaboliteslistclustering.getClusts());
                    modelAndView1.addObject("clustinfo",metaboliteslistclustering.getClustinfo());
                    modelAndView1.addObject("missvalmtbs",metaboliteslistclustering.getMissvalmtbs());
                    modelAndView1.addObject("mtbsclustcenters",metaboliteslistclustering.getMtbsclustscenters());
                    modelAndView1.addObject("silhouette",metaboliteslistclustering.getSilhouette());
                    modelAndView1.addObject("rowflag",metaboliteslistclustering.getRowflag());
                }

                modelAndView1.addObject("clusteringlevel",clusteringlevel);
                modelAndView1.addObject("clusteringmethod",clusteringmethod);
                modelAndView1.addObject("missval",missval);
                modelAndView1.addObject("allorsome",allorsome);
                
                UserProject userprojectfromdbafter = userProjectService.findUserByUserProjectKey(username, projectid);
                UserProjectForm userproformafter = userprouserproform.convert(userprojectfromdbafter);
                userproformafter.setUserProjectKey(userprojectfromdbafter.getUserProjectKey());
                userproformafter.setOwner(userprojectfromdbafter.getOwner());
                userproformafter.setKnimeprocessing(userprojectfromdbafter.getKnimeprocessing());
                userproformafter.setSparkanalysis(Boolean.FALSE);
                userproformafter.setAccess(userprojectfromdbafter.getAccess());
                userProjectService.saveOrUpdateUserProjectForm(userproformafter);
            
                return modelAndView1;

            }else{
                ModelAndView modelAndView2 = new ModelAndView("clustering2");
                modelAndView2.addObject("projectclustering2", project);
                List<String> metlist = new ArrayList<>();

                if(clusteringlevel.equals("onexps") && missval.equals("omit")){
                    metlist = sparkStatisticsClustering.omitInExperiments(experimentids,ionmode);
                }else if(clusteringlevel.equals("onexps") && !missval.equals("omit")){
                    metlist = sparkStatisticsClustering.replaceWithValueInExperiments(experimentids,ionmode);
                }else if(clusteringlevel.equals("onrepls") && missval.equals("omit")){
                    metlist = sparkStatisticsClustering.omitInReplicates(experimentids,ionmode);            
                }else if(clusteringlevel.equals("onrepls") && !missval.equals("omit")){
                    metlist = sparkStatisticsClustering.replaceWithValueInReplicates(experimentids,ionmode);
                }else if(clusteringlevel.equals("onmtbs") && missval.equals("omit")){
                    metlist = sparkStatisticsClustering.omitInExperiments(experimentids,ionmode);
                }else if(clusteringlevel.equals("onmtbs") && !missval.equals("omit")){
                    metlist = sparkStatisticsClustering.replaceWithValueInExperiments(experimentids,ionmode);
                }
                Integer rowflag=0;
                if (metlist != null && !metlist.isEmpty()) {
                    modelAndView2.addObject("mtblist",metlist);
                    modelAndView2.addObject("initialval",initialval);
                    modelAndView2.addObject("explist",experimentids);
                    modelAndView2.addObject("allorsome",allorsome);
                    modelAndView2.addObject("clusteringlevel",clusteringlevel);
                    modelAndView2.addObject("missval",missval);
                    modelAndView2.addObject("normval",normval);
                    modelAndView2.addObject("ionmode",ionmode);
                    modelAndView2.addObject("clusteringmethod",clusteringmethod);
                    modelAndView2.addObject("rowflag",rowflag);
                }else{
                    if(clusteringlevel.equals("onrepls")){
                        rowflag=2;
                        modelAndView2.addObject("rowflag",rowflag);
                    }else{
                        rowflag=1;
                        modelAndView2.addObject("rowflag",rowflag);
                    }
                }
                UserProject userprojectfromdbafter = userProjectService.findUserByUserProjectKey(username, projectid);
                UserProjectForm userproformafter = userprouserproform.convert(userprojectfromdbafter);
                userproformafter.setUserProjectKey(userprojectfromdbafter.getUserProjectKey());
                userproformafter.setOwner(userprojectfromdbafter.getOwner());
                userproformafter.setKnimeprocessing(userprojectfromdbafter.getKnimeprocessing());
                userproformafter.setSparkanalysis(Boolean.FALSE);
                userproformafter.setAccess(userprojectfromdbafter.getAccess());
                userProjectService.saveOrUpdateUserProjectForm(userproformafter);
                return modelAndView2;            
            }
        }else{
            ModelAndView modelAndView = new ModelAndView("clustering");
            Project project = proserv.getByProjectId(UUID.fromString(projectid));
            modelAndView.addObject("projectclustering", project);
            modelAndView.addObject("ifongoingprocessing", ifongoingprocessing);
            return modelAndView;
        }
        
    }
  
    
    @RequestMapping(value="/clusteringresultsview",method=RequestMethod.POST)
    public ModelAndView clusteri2(@RequestParam(value="projectid") String projectid,@RequestParam(value="ionmode") String ionmode,@RequestParam(value="description") List<String> mtblist,@RequestParam(value="experimentid") List<String> experimentids, @RequestParam(value="allorsome") String allorsome,@RequestParam(value="missval") String missval,@RequestParam(value="clusteringlevel") String clusteringlevel,@RequestParam(value="clusteringmethod") String clusteringmethod,@RequestParam(value="initialval") Integer initialval,@RequestParam(value="normval") String normval){
        Boolean ifongoingprocessing = userProjectService.ifongoingpreprocessing();
        if(!ifongoingprocessing){
            Authentication auth = SecurityContextHolder.getContext().getAuthentication();
            String username = auth.getName();
            UserProject userprojectfromdb = userProjectService.findUserByUserProjectKey(username, projectid);
            UserProjectForm userproform = userprouserproform.convert(userprojectfromdb);
            userproform.setUserProjectKey(userprojectfromdb.getUserProjectKey());
            userproform.setOwner(userprojectfromdb.getOwner());
            userproform.setKnimeprocessing(userprojectfromdb.getKnimeprocessing());
            userproform.setSparkanalysis(Boolean.TRUE);
            userproform.setAccess(userprojectfromdb.getAccess());
            userProjectService.saveOrUpdateUserProjectForm(userproform);
            
            ModelAndView modelAndView = new ModelAndView("clusteringresults");
            Project project = proserv.getByProjectId(UUID.fromString(projectid));
            modelAndView.addObject("projectclusteringresults", project);
            List<String> proexplistafter = new ArrayList<>();
            if(clusteringlevel.equals("onexps")){
                //Clustering for experiments
                Experimentslistclustering experimentslistclustering = sparkStatisticsClustering.expsclustering(experimentids,mtblist,allorsome,missval,clusteringmethod,initialval,ionmode,normval);
                proexplistafter=experimentslistclustering.getExpslist();
                List<String> expsnameslist = new ArrayList<>();
                for(String expid:proexplistafter){
                    ProjectExperimentKey tempkey = new ProjectExperimentKey();
                    tempkey.setProjectid(UUID.fromString(projectid));
                    tempkey.setExperimentid(expid);
                    ProjectExperiment tempproexp = proexpserv.getByProExpKey(tempkey);
                    expsnameslist.add(tempproexp.getExpname());
                }
                modelAndView.addObject("rowflag",experimentslistclustering.getRowflag());
                modelAndView.addObject("expsnameslist",expsnameslist);
                modelAndView.addObject("mtbsnameslist",experimentslistclustering.getMtbslist());
                modelAndView.addObject("expsclusts", experimentslistclustering.getClusts());
                modelAndView.addObject("clustinfo",experimentslistclustering.getClustinfo());
                modelAndView.addObject("missvalmtbs",experimentslistclustering.getMissvalmtbs());
                modelAndView.addObject("expsclustcenters",experimentslistclustering.getExpsclustscenters());
                modelAndView.addObject("silhouette",experimentslistclustering.getSilhouette());
              
            }else if(clusteringlevel.equals("onrepls")){
                //Clustering for replicates
                Replicateslistclustering replicateslistclustering = sparkStatisticsClustering.repliclustering(experimentids, mtblist, allorsome, missval, clusteringmethod, initialval,ionmode,normval);
                List<String> expsnameslist = new ArrayList<>();
                List<String> replinameslist=new ArrayList<>();
                proexplistafter=replicateslistclustering.getReplilist();
                List<String>expidrepl = new ArrayList<>();
                for(String finame:proexplistafter){
                    String[] temp1 = finame.split("[.]");
                    expidrepl.add(temp1[0]);
                }
                int expidreplnum=expidrepl.size();

                for(int i=0;i<expidreplnum;i++){
                    String[] temp2 = expidrepl.get(i).split("__");
                    ProjectExperimentKey tempkey = new ProjectExperimentKey();
                    tempkey.setProjectid(UUID.fromString(projectid));
                    tempkey.setExperimentid(temp2[0]);
                    ProjectExperiment tempproexp = proexpserv.getByProExpKey(tempkey);
                    expsnameslist.add(tempproexp.getExpname());
                    replinameslist.add(temp2[1]);
                }
                modelAndView.addObject("rowflag",replicateslistclustering.getRowflag());
                modelAndView.addObject("expsnameslist",expsnameslist);
                modelAndView.addObject("replinameslist",replinameslist);
                modelAndView.addObject("mtbsnameslist",replicateslistclustering.getMtbslist());
                modelAndView.addObject("repliclusts", replicateslistclustering.getClusts());
                modelAndView.addObject("clustinfo",replicateslistclustering.getClustinfo());
                modelAndView.addObject("missvalmtbs",replicateslistclustering.getMissvalmtbs());
                modelAndView.addObject("repliclustcenters",replicateslistclustering.getRepliclustscenters());
                modelAndView.addObject("silhouette",replicateslistclustering.getSilhouette());
                
            }else if(clusteringlevel.equals("onmtbs")){
                //Clustering for metabolites
                Metaboliteslistclustering metaboliteslistclustering = sparkStatisticsClustering.metaboliteclustering(experimentids,mtblist,allorsome,missval,clusteringmethod,initialval,ionmode,normval);
                //we need the names of experiments also
                proexplistafter=metaboliteslistclustering.getExpslist();
                List<String> expsnameslist = new ArrayList<>();
                for(String expid:proexplistafter){
                    ProjectExperimentKey tempkey = new ProjectExperimentKey();
                    tempkey.setProjectid(UUID.fromString(projectid));
                    tempkey.setExperimentid(expid);
                    ProjectExperiment tempproexp = proexpserv.getByProExpKey(tempkey);
                    expsnameslist.add(tempproexp.getExpname());
                }
                modelAndView.addObject("rowflag",metaboliteslistclustering.getRowflag());
                modelAndView.addObject("expsnameslist",expsnameslist);
                modelAndView.addObject("mtbsnameslist",metaboliteslistclustering.getMtbslist());
                modelAndView.addObject("mtbsclusts", metaboliteslistclustering.getClusts());
                modelAndView.addObject("clustinfo",metaboliteslistclustering.getClustinfo());
                modelAndView.addObject("missvalmtbs",metaboliteslistclustering.getMissvalmtbs());
                modelAndView.addObject("mtbsclustcenters",metaboliteslistclustering.getMtbsclustscenters());
                modelAndView.addObject("silhouette",metaboliteslistclustering.getSilhouette());

            }
            modelAndView.addObject("clusteringlevel",clusteringlevel);
            modelAndView.addObject("missval",missval);
            modelAndView.addObject("allorsome",allorsome);
            modelAndView.addObject("clusteringmethod",clusteringmethod);
            
            UserProject userprojectfromdbafter = userProjectService.findUserByUserProjectKey(username, projectid);
            UserProjectForm userproformafter = userprouserproform.convert(userprojectfromdbafter);
            userproformafter.setUserProjectKey(userprojectfromdbafter.getUserProjectKey());
            userproformafter.setOwner(userprojectfromdbafter.getOwner());
            userproformafter.setKnimeprocessing(userprojectfromdbafter.getKnimeprocessing());
            userproformafter.setSparkanalysis(Boolean.FALSE);
            userproformafter.setAccess(userprojectfromdbafter.getAccess());
            userProjectService.saveOrUpdateUserProjectForm(userproformafter);
            return modelAndView;
        }else{
            ModelAndView modelAndView = new ModelAndView("clustering");
            Project project = proserv.getByProjectId(UUID.fromString(projectid));
            modelAndView.addObject("projectclustering", project);
            modelAndView.addObject("ifongoingprocessing", ifongoingprocessing);
            return modelAndView;
        }
    }
    
    @RequestMapping(value="/preprocessview/{projectid}",method=RequestMethod.GET)
    public ModelAndView prep(@PathVariable String projectid) {
        ModelAndView modelAndView = new ModelAndView("preprocess");
        Project project = proserv.getByProjectId(UUID.fromString(projectid));
        modelAndView.addObject("projectpreprocess", project);
        List<ProjectExperiment> proexplistbefore =proexpserv.listAllprojectexperimentbyproid(UUID.fromString(projectid));
        List<ProjectExperiment> proexplistafter = new ArrayList<>();
        
        for(ProjectExperiment proexp:proexplistbefore){
            if(!proexp.getPreprocessed()){
                proexplistafter.add(proexp);
            }
        }
        Boolean ifongoingprocessing = Boolean.FALSE;
        Boolean ifongoinganalysis = Boolean.FALSE;
        modelAndView.addObject("ifongoingprocessing", ifongoingprocessing);
        modelAndView.addObject("ifongoinganalysis", ifongoinganalysis);
        modelAndView.addObject("proexplist", proexplistafter);
        Boolean massbankdb = false;
        if(project.getDatabasename().equals("MassBank")){
            massbankdb = true;
        }else{
            massbankdb = false;
        }
        modelAndView.addObject("massbankdb", massbankdb);
        return modelAndView;
    }
    @RequestMapping(value="/preprocessview",method=RequestMethod.POST)
    public ModelAndView preproc(@RequestParam(value="experimentid") List<String> experimentids,@RequestParam(value="projectid") String projectid,@RequestParam(value="workflow") String wrk) {
        ModelAndView modelAndView = new ModelAndView();
        Boolean ifongoingprocessing = userProjectService.ifongoingpreprocessing();
        Boolean ifongoinganalysis = userProjectService.ifongoinganalysis();
        if(!ifongoingprocessing && !ifongoinganalysis){
            
            Authentication auth = SecurityContextHolder.getContext().getAuthentication();
            String username = auth.getName();
            UserProject userprojectfromdb = userProjectService.findUserByUserProjectKey(username, projectid);
            UserProjectForm userproform = userprouserproform.convert(userprojectfromdb);
            userproform.setUserProjectKey(userprojectfromdb.getUserProjectKey());
            userproform.setOwner(userprojectfromdb.getOwner());
            userproform.setKnimeprocessing(Boolean.TRUE);
            userproform.setSparkanalysis(userprojectfromdb.getSparkanalysis());
            userproform.setAccess(userprojectfromdb.getAccess());
            userProjectService.saveOrUpdateUserProjectForm(userproform);
                    
            List<String> proexpnamelist = new ArrayList<>();
            Project project = proserv.getByProjectId(UUID.fromString(projectid));
            String dbname = project.getDatabasename();
            String projectidtoprocess =project.getProjectid().toString();
            Integer partit=0;
            for(String experimentid:experimentids){
                    ProjectExperimentKey newkey1 = new ProjectExperimentKey();
                    newkey1.setProjectid(UUID.fromString(projectid));
                    newkey1.setExperimentid(experimentid);

                    ProjectExperiment proexp = proexpserv.getByProExpKey(newkey1);
                    String expname=proexp.getExpname();
                    proexpnamelist.add(expname.concat("_"+experimentid));
                    partit++;
            }
            String pathofselectedexpsfile = springSparkKnime.listselectedexp(projectidtoprocess,proexpnamelist);
            springSparkKnime.preprocess(pathofselectedexpsfile,projectidtoprocess,partit,dbname,wrk);        

            for(String experimentid:experimentids){
                    ProjectExperimentKey newkey2 = new ProjectExperimentKey();
                    newkey2.setProjectid(UUID.fromString(projectid));
                    newkey2.setExperimentid(experimentid);
                    ProjectExperiment proexp = proexpserv.getByProExpKey(newkey2);
                    proexp.setPreprocessed(Boolean.TRUE);
                    ProjectsExpForm proexpform= proexpproexpform.convert(proexp);
                    proexpserv.saveOrUpdateProjectsExpForm(proexpform);

            }
            List<Identifiedunidentified> expidunid = springSparkKnime.countIdentifiedUnidentified(experimentids,wrk);
            for(String experimentid:experimentids){
                ProjectExperimentKey newkey1 = new ProjectExperimentKey();
                newkey1.setProjectid(UUID.fromString(projectid));
                newkey1.setExperimentid(experimentid);
                ProjectExperiment proexpwithidun = proexpserv.getByProExpKey(newkey1);
                for(Identifiedunidentified row:expidunid){
                    if(row.getExperimentid().equals(experimentid)){
                        proexpwithidun.setUnidentified(row.getUnidentified());
                        proexpwithidun.setIdentified(row.getIdentified());
                        proexpwithidun.setWorkflow(wrk);
                        ProjectsExpForm proexpform= proexpproexpform.convert(proexpwithidun);
                        proexpserv.saveOrUpdateProjectsExpForm(proexpform);
                    }
                }
            }
            UserProject userprojectfromdbafter = userProjectService.findUserByUserProjectKey(username, projectid);
            UserProjectForm userproformafter = userprouserproform.convert(userprojectfromdbafter);
            userproformafter.setUserProjectKey(userprojectfromdbafter.getUserProjectKey());
            userproformafter.setOwner(userprojectfromdbafter.getOwner());
            userproformafter.setKnimeprocessing(Boolean.FALSE);
            userproformafter.setSparkanalysis(userprojectfromdbafter.getSparkanalysis());
            userproformafter.setAccess(userprojectfromdbafter.getAccess());
            userProjectService.saveOrUpdateUserProjectForm(userproformafter);
            modelAndView = new ModelAndView("redirect:/projecttasksview/"+projectid);
        }else{
            modelAndView = new ModelAndView("preprocess");
            Project project = proserv.getByProjectId(UUID.fromString(projectid));
            modelAndView.addObject("projectpreprocess", project);
            modelAndView.addObject("ifongoingprocessing", ifongoingprocessing);
            modelAndView.addObject("ifongoinganalysis", ifongoinganalysis);
        }
        return  modelAndView;
    }
    
    
    @RequestMapping(value="/editview/{projectid}", method=RequestMethod.GET)
    public ModelAndView edi(@PathVariable String projectid) {
        ModelAndView modelAndView = new ModelAndView("edit");
        Project project = proserv.getByProjectId(UUID.fromString(projectid));
        ProjectsForm proform = proproform.convert(project);
        modelAndView.addObject("projectedit", project);
        modelAndView.addObject("proform", proform);        
                
        return modelAndView;
    }
    
    @RequestMapping(value = "/editview", method = RequestMethod.POST)
    public ModelAndView editsave(@Valid ProjectsForm proform, BindingResult bindingResult, 
                                    RedirectAttributes redirectAttributes, 
                                    @RequestParam(value="projectid") String projectid,
                                    @RequestParam(value="prodateedited") String prodateedited,                          
                                    @RequestParam(value="experimentid",required=false) List<String> experimentids){
        redirectAttributes.addFlashAttribute("message", "Failure! Please go back and try again!");
        redirectAttributes.addFlashAttribute("alertClass", "alert-danger");
        if(bindingResult.hasErrors()){
            return new ModelAndView("redirect:/editview/"+projectid);
        }
        proserv.saveOrUpdateProjectsForm(proform);
           
        return new ModelAndView("redirect:/projecttasksview/"+projectid);
    }
    
    
    @RequestMapping(value="/dataview/{projectid}",method=RequestMethod.GET)
    public ModelAndView dataview(@PathVariable String projectid) {
        ModelAndView modelAndView = new ModelAndView("dataview");
        Project project = proserv.getByProjectId(UUID.fromString(projectid));
        modelAndView.addObject("projectdataview", project);

        return modelAndView;
    }
    
    @RequestMapping(value="/boxplotsviewexps/{projectid}",method=RequestMethod.GET)
    public ModelAndView boxplotsviewexps(@PathVariable String projectid) {
        ModelAndView modelAndView = new ModelAndView("boxplotsviewexps");
        Project project = proserv.getByProjectId(UUID.fromString(projectid));
        modelAndView.addObject("projectboxplotsviewexps", project);
        List<ProjectExperiment> proexplistbefore =proexpserv.listAllprojectexperimentbyproid(UUID.fromString(projectid));
        List<ProjectExperiment> proexplistafter = new ArrayList<>();
        for(ProjectExperiment proexp:proexplistbefore){
            if(proexp.getPreprocessed()){
                proexplistafter.add(proexp);
            }
        }
        Boolean ifongoingprocessing = Boolean.FALSE;
        modelAndView.addObject("ifongoingprocessing", ifongoingprocessing);
        modelAndView.addObject("experimentlist", proexplistafter);
        return modelAndView;
    }
    
    @RequestMapping(value="/boxplotsviewexps",method=RequestMethod.POST)
    public ModelAndView boxplotsviewex(@RequestParam(value="transf") String transf,@RequestParam(value="ionmode") String ionmode,@RequestParam(value="eachorgroup") String eachorgroup,@RequestParam(value="experimentid",required=false) List<String> experimentids,@RequestParam(value="projectid") String projectid,@RequestParam(value="experimentidgroup1",required=false) List<String> experimentidsgroup1,@RequestParam(value="experimentidgroup2",required=false) List<String> experimentidsgroup2,@RequestParam(value="experimentidgroup3",required=false) List<String> experimentidsgroup3) {
        
        Boolean ifongoingprocessing = userProjectService.ifongoingpreprocessing();
        if(!ifongoingprocessing){
            Authentication auth = SecurityContextHolder.getContext().getAuthentication();
            String username = auth.getName();
            UserProject userprojectfromdb = userProjectService.findUserByUserProjectKey(username, projectid);
            UserProjectForm userproform = userprouserproform.convert(userprojectfromdb);
            userproform.setUserProjectKey(userprojectfromdb.getUserProjectKey());
            userproform.setOwner(userprojectfromdb.getOwner());
            userproform.setKnimeprocessing(userprojectfromdb.getKnimeprocessing());
            userproform.setSparkanalysis(Boolean.TRUE);
            userproform.setAccess(userprojectfromdb.getAccess());
            userProjectService.saveOrUpdateUserProjectForm(userproform);

            ModelAndView modelAndView = new ModelAndView("boxplotsviewmtbs");
            modelAndView.addObject("ifongoingprocessing", ifongoingprocessing);
            Project project = proserv.getByProjectId(UUID.fromString(projectid));
            modelAndView.addObject("projectboxplotsviewmtbs", project);
            modelAndView.addObject("eachorgroup",eachorgroup);
            modelAndView.addObject("transf",transf);
            modelAndView.addObject("ionmode",ionmode);
            
            List<String> metlist = new ArrayList<>();
            if(eachorgroup.equals("eachval")){
                List<ProjectExperimentFormExNa> expidexpname = sparkStatisticsBoxplots.boxplotsidname(experimentids);
                metlist = sparkStatisticsBoxplots.allMetInExperiments(experimentids,ionmode);
                modelAndView.addObject("expidexpname",expidexpname);
            }else if(eachorgroup.equals("groupval2")){
                modelAndView.addObject("experimentidsgroup1",experimentidsgroup1);
                modelAndView.addObject("experimentidsgroup2",experimentidsgroup2);
                List<String>experimentidsboth = new ArrayList<String>();
                experimentidsboth.addAll(experimentidsgroup1);
                experimentidsboth.addAll(experimentidsgroup2);
                metlist = sparkStatisticsBoxplots.allMetInExperiments(experimentidsboth,ionmode);
            }else if(eachorgroup.equals("groupval3")){
                modelAndView.addObject("experimentidsgroup1",experimentidsgroup1);
                modelAndView.addObject("experimentidsgroup2",experimentidsgroup2);
                modelAndView.addObject("experimentidsgroup3",experimentidsgroup3);
                List<String>experimentidsthree = new ArrayList<String>();
                experimentidsthree.addAll(experimentidsgroup1);
                experimentidsthree.addAll(experimentidsgroup2);
                experimentidsthree.addAll(experimentidsgroup3);
                metlist = sparkStatisticsBoxplots.allMetInExperiments(experimentidsthree,ionmode);
            }
            if(!metlist.isEmpty()){
                modelAndView.addObject("allmetabolitelist",metlist);
                String onemetabolite=metlist.get(0);
                modelAndView.addObject("onemetabolite",onemetabolite);
            }else{
                modelAndView.addObject("rowflag",1);
            }
            UserProject userprojectfromdbafter = userProjectService.findUserByUserProjectKey(username, projectid);
            UserProjectForm userproformafter = userprouserproform.convert(userprojectfromdbafter);
            userproformafter.setUserProjectKey(userprojectfromdbafter.getUserProjectKey());
            userproformafter.setOwner(userprojectfromdbafter.getOwner());
            userproformafter.setKnimeprocessing(userprojectfromdbafter.getKnimeprocessing());
            userproformafter.setSparkanalysis(Boolean.FALSE);
            userproformafter.setAccess(userprojectfromdbafter.getAccess());
            userProjectService.saveOrUpdateUserProjectForm(userproformafter);
            
            return modelAndView;
        }else{
            ModelAndView modelAndView = new ModelAndView("boxplotsviewexps");
            Project project = proserv.getByProjectId(UUID.fromString(projectid));
            modelAndView.addObject("projectboxplotsviewexps", project);
            modelAndView.addObject("ifongoingprocessing", ifongoingprocessing);
            return modelAndView;
        }
        
    }
    
    
    @RequestMapping(value="/fetchboxplotsviewmtbs",method=RequestMethod.GET, produces = "application/json")
    public @ResponseBody String jsonboxplotsviewmtbs(@RequestParam(value="transf") String transf,@RequestParam(value="ionmode") String ionmode,@RequestParam(value="eachorgroup") String eachorgroup,@RequestParam(value="experimentids3",required=false) List<String> experimentids3,@RequestParam(value="experimentids2",required=false) List<String> experimentids2,@RequestParam(value="experimentids1",required=false) List<String> experimentids1,@RequestParam(value="experimentids",required=false) List<String> experimentids, @RequestParam(value="description") String selectedmtb, @RequestParam(value="projectid") String projectid){
        Boolean ifongoingprocessing = userProjectService.ifongoingpreprocessing();
        if(!ifongoingprocessing){
            Authentication auth = SecurityContextHolder.getContext().getAuthentication();
            String username = auth.getName();
            UserProject userprojectfromdb = userProjectService.findUserByUserProjectKey(username, projectid);
            UserProjectForm userproform = userprouserproform.convert(userprojectfromdb);
            userproform.setUserProjectKey(userprojectfromdb.getUserProjectKey());
            userproform.setOwner(userprojectfromdb.getOwner());
            userproform.setKnimeprocessing(userprojectfromdb.getKnimeprocessing());
            userproform.setSparkanalysis(Boolean.TRUE);
            userproform.setAccess(userprojectfromdb.getAccess());
            userProjectService.saveOrUpdateUserProjectForm(userproform);
            List<ExperimentFormExIn> boxplots = new ArrayList<ExperimentFormExIn>();
            if(eachorgroup.equals("eachval")){
                boxplots = sparkStatisticsBoxplots.boxplotsajax(experimentids,selectedmtb,transf,ionmode);
            }else if(eachorgroup.equals("groupval2")){
                List<ExperimentFormExIn> boxplots1 = sparkStatisticsBoxplots.boxplotsajax(experimentids1,selectedmtb,transf,ionmode);
                for(ExperimentFormExIn item1:boxplots1){
                    item1.setExperimentid("group1");
                }
                boxplots.addAll(boxplots1);
                List<ExperimentFormExIn> boxplots2 = sparkStatisticsBoxplots.boxplotsajax(experimentids2,selectedmtb,transf,ionmode);
                for(ExperimentFormExIn item2:boxplots2){
                    item2.setExperimentid("group2");
                }
                boxplots.addAll(boxplots2);
            }else if(eachorgroup.equals("groupval3")){
                List<ExperimentFormExIn> boxplots1 = sparkStatisticsBoxplots.boxplotsajax(experimentids1,selectedmtb,transf,ionmode);
                for(ExperimentFormExIn item1:boxplots1){
                    item1.setExperimentid("group1");
                }
                boxplots.addAll(boxplots1);
                List<ExperimentFormExIn> boxplots2 = sparkStatisticsBoxplots.boxplotsajax(experimentids2,selectedmtb,transf,ionmode);
                for(ExperimentFormExIn item2:boxplots2){
                    item2.setExperimentid("group2");
                }
                boxplots.addAll(boxplots2);
                List<ExperimentFormExIn> boxplots3 = sparkStatisticsBoxplots.boxplotsajax(experimentids3,selectedmtb,transf,ionmode);
                for(ExperimentFormExIn item3:boxplots3){
                    item3.setExperimentid("group3");
                }
                boxplots.addAll(boxplots3);
            }
            try{
                ObjectMapper mapper = new ObjectMapper();
                String jsonString = mapper.writeValueAsString(boxplots);

                UserProject userprojectfromdbafter = userProjectService.findUserByUserProjectKey(username, projectid);
                UserProjectForm userproformafter = userprouserproform.convert(userprojectfromdbafter);
                userproformafter.setUserProjectKey(userprojectfromdbafter.getUserProjectKey());
                userproformafter.setOwner(userprojectfromdbafter.getOwner());
                userproformafter.setKnimeprocessing(userprojectfromdbafter.getKnimeprocessing());
                userproformafter.setSparkanalysis(Boolean.FALSE);
                userproformafter.setAccess(userprojectfromdbafter.getAccess());
                userProjectService.saveOrUpdateUserProjectForm(userproformafter);
                return "{ \"data\": " + jsonString + "}";

             }catch (JsonProcessingException ex) {
                Logger.getLogger(Controller1.class.getName()).log(Level.SEVERE, null, ex);
             } 
        }else{
            Boolean jsonString = true;
            return "{ \"data\": " + jsonString + "}";
        }
        return "jsonstr";
    }
    
    
    @RequestMapping(value="/basicstatisticsview/{projectid}",method=RequestMethod.GET)
    public ModelAndView basicstats(@PathVariable String projectid) {
        ModelAndView modelAndView = new ModelAndView("basicstatistics");
        Project project = proserv.getByProjectId(UUID.fromString(projectid));
        modelAndView.addObject("projectbasicstatistics", project);
        List<ProjectExperiment> proexplistbefore =proexpserv.listAllprojectexperimentbyproid(UUID.fromString(projectid));
        List<ProjectExperiment> proexplistafter = new ArrayList<>();
        for(ProjectExperiment proexp:proexplistbefore){
            if(proexp.getPreprocessed()){
                proexplistafter.add(proexp);
            }
        }
        Boolean ifongoingprocessing = Boolean.FALSE;
        modelAndView.addObject("ifongoingprocessing", ifongoingprocessing);
        modelAndView.addObject("experimentlist", proexplistafter);
        return modelAndView;
    }
    
    @RequestMapping(value="/basicstatisticsview",method=RequestMethod.POST)
    public ModelAndView basicstatsres(@RequestParam(value="projectid") String projectid,@RequestParam(value="ionmode") String ionmode,@RequestParam(value="experimentid") List<String> experimentids,@RequestParam(value="allorcommon") String allorcommon,@RequestParam(value="alloreach") String alloreach) {
       
        Boolean ifongoingprocessing = userProjectService.ifongoingpreprocessing();
        if(!ifongoingprocessing){
            Authentication auth = SecurityContextHolder.getContext().getAuthentication();
            String username = auth.getName();
            UserProject userprojectfromdb = userProjectService.findUserByUserProjectKey(username, projectid);
            UserProjectForm userproform = userprouserproform.convert(userprojectfromdb);
            userproform.setUserProjectKey(userprojectfromdb.getUserProjectKey());
            userproform.setOwner(userprojectfromdb.getOwner());
            userproform.setKnimeprocessing(userprojectfromdb.getKnimeprocessing());
            userproform.setSparkanalysis(Boolean.TRUE);
            userproform.setAccess(userprojectfromdb.getAccess());
            userProjectService.saveOrUpdateUserProjectForm(userproform);
            
            ModelAndView modelAndView = new ModelAndView("basicstatisticsresults");
            Project project = proserv.getByProjectId(UUID.fromString(projectid));
            modelAndView.addObject("projectbasicstatisticsresults", project);
            List<ProjectExperimentFormExNa> expidexpname = sparkStatisticsBasicstats.bsidname(experimentids);
            List<String> metlist = new ArrayList<>();
            if(allorcommon.equals("onallmtbs")){
                metlist = sparkStatisticsBasicstats.allMetInExperiments(experimentids,ionmode);
            }else if(allorcommon.equals("oncommonmtbs")){
                metlist = sparkStatisticsBasicstats.commonMetInExperiments(experimentids,ionmode);
            }
            if(metlist != null && !metlist.isEmpty()){
                Integer rowflag=0;
                modelAndView.addObject("expidexpname",expidexpname);
                modelAndView.addObject("allorcommon",allorcommon);
                modelAndView.addObject("ionmode",ionmode);
                modelAndView.addObject("metlist",metlist);
                modelAndView.addObject("rowflag",rowflag);
                modelAndView.addObject("alloreach",alloreach);
                if(alloreach.equals("allexps")){
                    List<Basicstatsmtbs> bsmtbs = sparkStatisticsBasicstats.basicstats2(experimentids, metlist,ionmode);
                    modelAndView.addObject("bsmtbsrows",bsmtbs);
                }
            }else{
                Integer rowflag=1;
                modelAndView.addObject("rowflag",rowflag);
            }
            UserProject userprojectfromdbafter = userProjectService.findUserByUserProjectKey(username, projectid);
            UserProjectForm userproformafter = userprouserproform.convert(userprojectfromdbafter);
            userproformafter.setUserProjectKey(userprojectfromdbafter.getUserProjectKey());
            userproformafter.setOwner(userprojectfromdbafter.getOwner());
            userproformafter.setKnimeprocessing(userprojectfromdbafter.getKnimeprocessing());
            userproformafter.setSparkanalysis(Boolean.FALSE);
            userproformafter.setAccess(userprojectfromdbafter.getAccess());
            userProjectService.saveOrUpdateUserProjectForm(userproformafter);
            return modelAndView;
        }else{
            ModelAndView modelAndView = new ModelAndView("basicstatistics");
            Project project = proserv.getByProjectId(UUID.fromString(projectid));
            modelAndView.addObject("projectbasicstatistics", project);
            modelAndView.addObject("ifongoingprocessing", ifongoingprocessing);
            return modelAndView;
        }
    }
    
    @RequestMapping(value="/fetchbasicstatistics",method=RequestMethod.GET, produces = "application/json")
    public @ResponseBody String jsonbasicstatistics(@RequestParam(value="experimentids") List<String> experimentids,@RequestParam(value="ionmode") String ionmode,@RequestParam(value="allorcommon") String allorcommon,@RequestParam(value="projectid") String projectid){
        Boolean ifongoingprocessing = userProjectService.ifongoingpreprocessing();
        if(!ifongoingprocessing){
            Authentication auth = SecurityContextHolder.getContext().getAuthentication();
            String username = auth.getName();
            UserProject userprojectfromdb = userProjectService.findUserByUserProjectKey(username, projectid);
            UserProjectForm userproform = userprouserproform.convert(userprojectfromdb);
            userproform.setUserProjectKey(userprojectfromdb.getUserProjectKey());
            userproform.setOwner(userprojectfromdb.getOwner());
            userproform.setKnimeprocessing(userprojectfromdb.getKnimeprocessing());
            userproform.setSparkanalysis(Boolean.TRUE);
            userproform.setAccess(userprojectfromdb.getAccess());
            userProjectService.saveOrUpdateUserProjectForm(userproform);

            List<String> metlist = new ArrayList<>();
            if(allorcommon.equals("onallmtbs")){
                metlist = sparkStatisticsBasicstats.allMetInExperiments(experimentids,ionmode);
            }else if(allorcommon.equals("oncommonmtbs")){
                metlist = sparkStatisticsBasicstats.commonMetInExperiments(experimentids,ionmode);
            }
            
            List<Basicstatsmtbs> bsmtbs = sparkStatisticsBasicstats.basicstats(experimentids, metlist,ionmode);

            try {
                ObjectMapper mapper = new ObjectMapper();
                String jsonString = mapper.writeValueAsString(bsmtbs);
                UserProject userprojectfromdbafter = userProjectService.findUserByUserProjectKey(username, projectid);
                UserProjectForm userproformafter = userprouserproform.convert(userprojectfromdbafter);
                userproformafter.setUserProjectKey(userprojectfromdbafter.getUserProjectKey());
                userproformafter.setOwner(userprojectfromdbafter.getOwner());
                userproformafter.setKnimeprocessing(userprojectfromdbafter.getKnimeprocessing());
                userproformafter.setSparkanalysis(Boolean.FALSE);
                userproformafter.setAccess(userprojectfromdbafter.getAccess());
                userProjectService.saveOrUpdateUserProjectForm(userproformafter);
                return "{ \"data\": " + jsonString + "}";

             } catch (JsonProcessingException ex) {
                Logger.getLogger(Controller1.class.getName()).log(Level.SEVERE, null, ex);
            }
        }else{
            Boolean jsonString = true;
            return "{ \"data\": " + jsonString + "}";
        }
        return "jsonstr";
    }
    
    @RequestMapping(value="/groupexpsview/{projectid}",method=RequestMethod.GET)
    public ModelAndView groupexpsvi(@PathVariable String projectid) {
        
        ModelAndView modelAndView = new ModelAndView("groupexps");
        Project project = proserv.getByProjectId(UUID.fromString(projectid));
        modelAndView.addObject("projectgroupexps", project);
        List<ProjectExperiment> proexplistbefore =proexpserv.listAllprojectexperimentbyproid(UUID.fromString(projectid));
        List<ProjectExperiment> proexplistafter = new ArrayList<>();
        for(ProjectExperiment proexp:proexplistbefore){
            if(proexp.getPreprocessed()){
                proexplistafter.add(proexp);
            }
        }
        Boolean ifongoingprocessing = Boolean.FALSE;
        modelAndView.addObject("ifongoingprocessing", ifongoingprocessing);
        modelAndView.addObject("experimentlist", proexplistafter);
        
        return modelAndView;
    }
    
    @RequestMapping(value="/groupexpsview",method=RequestMethod.POST)
    public ModelAndView groupexpsv(@RequestParam(value="projectid") String projectid,@RequestParam(value="ionmode") String ionmode,@RequestParam(value="normval") String normval,@RequestParam(value="experimentidgroup1") List<String> experimentidgroup1,@RequestParam(value="experimentidgroup2") List<String> experimentidgroup2,@RequestParam(value="missval") String missval,@RequestParam(value="meanormedian") String meanormedian/*,@RequestParam(value="nrmtbs",required=false) Integer nrmtbs,@RequestParam(value="allorsome",required=false) String allorsome,@RequestParam(value="missval") String missval*/) {
       
        Boolean ifongoingprocessing = userProjectService.ifongoingpreprocessing();
        if(!ifongoingprocessing){
            Authentication auth = SecurityContextHolder.getContext().getAuthentication();
            String username = auth.getName();
            UserProject userprojectfromdb = userProjectService.findUserByUserProjectKey(username, projectid);
            UserProjectForm userproform = userprouserproform.convert(userprojectfromdb);
            userproform.setUserProjectKey(userprojectfromdb.getUserProjectKey());
            userproform.setOwner(userprojectfromdb.getOwner());
            userproform.setKnimeprocessing(userprojectfromdb.getKnimeprocessing());
            userproform.setSparkanalysis(Boolean.TRUE);
            userproform.setAccess(userprojectfromdb.getAccess());
            userProjectService.saveOrUpdateUserProjectForm(userproform);
            
            ModelAndView modelAndView = new ModelAndView("groupexpsresults");
            Project project = proserv.getByProjectId(UUID.fromString(projectid));
            modelAndView.addObject("projectgroupexpsresults", project);
            Findmissvalforgroupexps missvalmtbs = sparkStatisticsGroupexps.findmissvalforgroupexps(experimentidgroup1, experimentidgroup2, missval,ionmode);
            List<Groupexps> groupexps = sparkStatisticsGroupexps.mtbfiltering(experimentidgroup1, experimentidgroup2, missval, meanormedian,missvalmtbs.getMissingmtbstobeunited(),normval,ionmode);

            if(groupexps!=null && !groupexps.isEmpty()){
                BigInteger degfreed = groupexps.get(0).getDegfr();
                modelAndView.addObject("groupexps", groupexps);
                modelAndView.addObject("degfr", "Degrees of freedom: "+degfreed);
                modelAndView.addObject("missvalmtbs",missvalmtbs.getMissvalmtbs());
                modelAndView.addObject("missval", missval);
                modelAndView.addObject("meanormedian", meanormedian);
                modelAndView.addObject("rowflag", 0);
                
                UserProject userprojectfromdbafter = userProjectService.findUserByUserProjectKey(username, projectid);
                UserProjectForm userproformafter = userprouserproform.convert(userprojectfromdbafter);
                userproformafter.setUserProjectKey(userprojectfromdbafter.getUserProjectKey());
                userproformafter.setOwner(userprojectfromdbafter.getOwner());
                userproformafter.setKnimeprocessing(userprojectfromdbafter.getKnimeprocessing());
                userproformafter.setSparkanalysis(Boolean.FALSE);
                userproformafter.setAccess(userprojectfromdbafter.getAccess());
                userProjectService.saveOrUpdateUserProjectForm(userproformafter);
            }else{
                modelAndView.addObject("rowflag", 1);
            }
            return modelAndView;
        }else{
            ModelAndView modelAndView = new ModelAndView("groupexps");
            Project project = proserv.getByProjectId(UUID.fromString(projectid));
            modelAndView.addObject("projectgroupexps", project);
            modelAndView.addObject("ifongoingprocessing", ifongoingprocessing);
            return modelAndView;
        }
    }
    
    
    
    @RequestMapping(value="/dataviewexps/{projectid}",method=RequestMethod.GET)
    public ModelAndView dataviewex(@PathVariable String projectid) {
        ModelAndView modelAndView = new ModelAndView("dataviewexps");
        Project project = proserv.getByProjectId(UUID.fromString(projectid));
        modelAndView.addObject("projectdataviewexps", project);
        List<ProjectExperiment> proexplistbefore =proexpserv.listAllprojectexperimentbyproid(UUID.fromString(projectid));
        List<ProjectExperiment> proexplistafter = new ArrayList<>();
        for(ProjectExperiment proexp:proexplistbefore){
            if(proexp.getPreprocessed()){
                proexplistafter.add(proexp);
            }
        }
        Boolean ifongoingprocessing = Boolean.FALSE;
        modelAndView.addObject("ifongoingprocessing", ifongoingprocessing);
        modelAndView.addObject("experimentlist", proexplistafter);
        
        return modelAndView;
    }
    
    
    @RequestMapping(value="/dataviewexps",method=RequestMethod.POST)
    public ModelAndView dataviewe(@RequestParam(value="experimentid") String experimentid,@RequestParam(value="projectid") String projectid) {
        Boolean ifongoingprocessing = userProjectService.ifongoingpreprocessing();
        if(!ifongoingprocessing){
            Authentication auth = SecurityContextHolder.getContext().getAuthentication();
            String username = auth.getName();
            UserProject userprojectfromdb = userProjectService.findUserByUserProjectKey(username, projectid);
            UserProjectForm userproform = userprouserproform.convert(userprojectfromdb);
            userproform.setUserProjectKey(userprojectfromdb.getUserProjectKey());
            userproform.setOwner(userprojectfromdb.getOwner());
            userproform.setKnimeprocessing(userprojectfromdb.getKnimeprocessing());
            userproform.setSparkanalysis(Boolean.TRUE);
            userproform.setAccess(userprojectfromdb.getAccess());
            userProjectService.saveOrUpdateUserProjectForm(userproform);
            
            ModelAndView modelAndView = new ModelAndView("dataviewmtbs");
            Project project = proserv.getByProjectId(UUID.fromString(projectid));
            modelAndView.addObject("projectdataviewmtbs", project);
            modelAndView.addObject("experimentid", experimentid);

           
            UserProject userprojectfromdbafter = userProjectService.findUserByUserProjectKey(username, projectid);
            UserProjectForm userproformafter = userprouserproform.convert(userprojectfromdbafter);
            userproformafter.setUserProjectKey(userprojectfromdbafter.getUserProjectKey());
            userproformafter.setOwner(userprojectfromdbafter.getOwner());
            userproformafter.setKnimeprocessing(userprojectfromdbafter.getKnimeprocessing());
            userproformafter.setSparkanalysis(Boolean.FALSE);
            userproformafter.setAccess(userprojectfromdbafter.getAccess());
            userProjectService.saveOrUpdateUserProjectForm(userproformafter);
            return modelAndView;
        }else{
            ModelAndView modelAndView = new ModelAndView("dataviewexps");
            Project project = proserv.getByProjectId(UUID.fromString(projectid));
            modelAndView.addObject("projectdataviewexps", project);
            modelAndView.addObject("ifongoingprocessing", ifongoingprocessing);
            return modelAndView;
        }
    }
    @RequestMapping(value="/fetchdataview",method=RequestMethod.GET, produces = "application/json")
    public @ResponseBody String jsondataviewexps(@RequestParam(value="experimentid") String experimentid,@RequestParam(value="projectid") String projectid){
        
        Boolean ifongoingprocessing = userProjectService.ifongoingpreprocessing();
        if(!ifongoingprocessing){
            Authentication auth = SecurityContextHolder.getContext().getAuthentication();
            String username = auth.getName();
            UserProject userprojectfromdb = userProjectService.findUserByUserProjectKey(username, projectid);
            UserProjectForm userproform = userprouserproform.convert(userprojectfromdb);
            userproform.setUserProjectKey(userprojectfromdb.getUserProjectKey());
            userproform.setOwner(userprojectfromdb.getOwner());
            userproform.setKnimeprocessing(userprojectfromdb.getKnimeprocessing());
            userproform.setSparkanalysis(Boolean.TRUE);
            userproform.setAccess(userprojectfromdb.getAccess());
            userProjectService.saveOrUpdateUserProjectForm(userproform);

            List<ExperimentFormDesRtInMzFlChIdAdGr> cassmtbs = dataview.searchmtbs(experimentid);
            try {
                ObjectMapper mapper = new ObjectMapper();
                String jsonString = mapper.writeValueAsString(cassmtbs);
                UserProject userprojectfromdbafter = userProjectService.findUserByUserProjectKey(username, projectid);
                UserProjectForm userproformafter = userprouserproform.convert(userprojectfromdbafter);
                userproformafter.setUserProjectKey(userprojectfromdbafter.getUserProjectKey());
                userproformafter.setOwner(userprojectfromdbafter.getOwner());
                userproformafter.setKnimeprocessing(userprojectfromdbafter.getKnimeprocessing());
                userproformafter.setSparkanalysis(Boolean.FALSE);
                userproformafter.setAccess(userprojectfromdbafter.getAccess());
                userProjectService.saveOrUpdateUserProjectForm(userproformafter);
                return "{ \"data\": " + jsonString + "}";

             } catch (JsonProcessingException ex) {
                Logger.getLogger(Controller1.class.getName()).log(Level.SEVERE, null, ex);
            }
        }else{
            Boolean jsonString = true;
            return "{ \"data\": " + jsonString + "}";
        }
        return "jsonstr";
    }
    
}
