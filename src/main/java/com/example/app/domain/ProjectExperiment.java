/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.example.app.domain;

import com.datastax.driver.mapping.annotations.Table;
import java.io.Serializable;
import java.util.List;
import org.springframework.data.cassandra.core.mapping.PrimaryKey;

/**
 *
 * @author kostas
 */
@Table(name= "projectexperiment")
public class ProjectExperiment implements Serializable{
    
    @PrimaryKey
    private ProjectExperimentKey projectExperimentKey;
    
    private String expname;
    private Long replicatesnum;
    private List<String> replicatesnames;
    private String expdate;
    private Boolean preprocessed;
    private Long identified;
    private Long unidentified;
    private String workflow;

    
    public ProjectExperimentKey getProjectExperimentKey() {
        return projectExperimentKey;
    }
    public void setProjectExperimentKey(ProjectExperimentKey projectExperimentKey) {
        this.projectExperimentKey = projectExperimentKey;
    }
    
    public String getExpname() {
        return expname;
    }
    public void setExpname(String expname) {
        this.expname = expname;
    }
    
    public String getExpdate() {
        return expdate;
    }
    public void setExpdate(String expdate) {
        this.expdate = expdate;
    }
    public List<String> getReplicatesnames() {
        return replicatesnames;
    }
    public void setReplicatesnames(List<String> replicatesnames) {
        this.replicatesnames = replicatesnames;
    }
    public Long getReplicatesnum() {
        return replicatesnum;
    }
    public void setReplicatesnum(Long replicatesnum) {
        this.replicatesnum = replicatesnum;
    }
    public Boolean getPreprocessed() {
        return preprocessed;
    }
    public void setPreprocessed(Boolean preprocessed) {
        this.preprocessed = preprocessed;
    }
    public Long getIdentified() {
        return identified;
    }
    public void setIdentified(Long identified) {
        this.identified = identified;
    }
    public Long getUnidentified() {
        return unidentified;
    }
    public void setUnidentified(Long unidentified) {
        this.unidentified = unidentified;
    }
    public String getWorkflow() {
        return workflow;
    }
    public void setWorkflow(String workflow) {
        this.workflow = workflow;
    }
    
}