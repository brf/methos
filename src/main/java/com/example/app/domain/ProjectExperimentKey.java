/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.example.app.domain;

import com.datastax.driver.core.DataType;
import java.io.Serializable;
import java.util.UUID;
import org.springframework.data.cassandra.core.cql.PrimaryKeyType;
import org.springframework.data.cassandra.core.mapping.CassandraType;
import org.springframework.data.cassandra.core.mapping.PrimaryKeyClass;
import org.springframework.data.cassandra.core.mapping.PrimaryKeyColumn;

/**
 *
 * @author kostas
 */

@PrimaryKeyClass
public class ProjectExperimentKey implements Serializable{
    
    @PrimaryKeyColumn(name = "projectid",ordinal = 0,type = PrimaryKeyType.PARTITIONED)
    @CassandraType(type=DataType.Name.UUID)
    private UUID projectid;
    
    @PrimaryKeyColumn(name = "experimentid",ordinal = 1,type = PrimaryKeyType.CLUSTERED)
    @CassandraType(type=DataType.Name.VARCHAR)
    private String experimentid;
   
    public UUID getProjectid() {
        return projectid;
    }
    public void setProjectid(UUID projectid) {
        this.projectid = projectid;
    }
    public String getExperimentid() {
        return experimentid;
    }
    public void setExperimentid(String experimentid) {
        this.experimentid = experimentid;
    }
    
    public ProjectExperimentKey(){
    }

    
}
