/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.example.app.domain;

import java.io.Serializable;

/**
 *
 * @author kostas
 */
public class Identifiedunidentified implements Serializable{
    
    private String experimentid;
    private Long identified;
    private Long unidentified;
    
    public String getExperimentid() {
        return experimentid;
    }
    public void setExperimentid(String experimentid) {
        this.experimentid = experimentid;
    }
    
    public Long getIdentified(){
        return identified;
    }
    public void setIdentified(Long identified){
        this.identified=identified;
    }
    
    public Long getUnidentified(){
        return unidentified;
    }
    public void setUnidentified(Long unidentified){
        this.unidentified=unidentified;
    }
    public Identifiedunidentified(){
        
    }
}
