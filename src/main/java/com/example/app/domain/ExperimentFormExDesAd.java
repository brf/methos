/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.example.app.domain;

/**
 *
 * @author kostas
 */
public class ExperimentFormExDesAd {
    private String experimentid;
    private String description;
    private String adduct;
    
    public String getExperimentid() {
        return experimentid;
    }
    public void setExperimentid(String experimentid) {
        this.experimentid = experimentid;
    }
    
    public String getDescription() {
        return description;
    }
    public void setDescription(String description) {
        this.description = description;
    }
    public String getAdduct() {
        return adduct;
    }
    public void setAdduct(String adduct) {
        this.adduct = adduct;
    }
}
