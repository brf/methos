/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.example.app.domain;

import com.datastax.driver.core.DataType;
import com.datastax.driver.core.utils.UUIDs;
import com.datastax.driver.mapping.annotations.Table;
import org.springframework.data.cassandra.core.mapping.PrimaryKey;
import java.io.Serializable;
import java.util.UUID;
import org.springframework.data.cassandra.core.mapping.CassandraType;
/**
 *
 * @author kostas
 */
@Table(name= "project")
public class Project implements Serializable{
    
    @PrimaryKey
    @CassandraType(type=DataType.Name.UUID)
    private UUID projectid;
    
    private String databasename;
    private String proname;
    private String prodatecreated;
    private String prodateedited;
    private String procomments;
    
    public UUID getProjectid() {
        return projectid;
    }
    public void setProjectid(UUID projectid) {
        this.projectid = projectid;
    }
    
    public String getDatabasename() {
        return databasename;
    }
    public void setDatabasename(String databasename) {
        this.databasename = databasename;
    }
    
    public String getProname() {
        return proname;
    }
    public void setProname(String proname) {
        this.proname = proname;
    }
    
    public String getProdatecreated() {
        return prodatecreated;
    }
    public void setProdatecreated(String prodatecreated) {
        this.prodatecreated = prodatecreated;
    }
    
    public String getProdateedited() {
        return prodateedited;
    }
    public void setProdateedited(String prodateedited) {
        this.prodateedited = prodateedited;
    }
    
    public String getProcomments() {
        return procomments;
    }
    public void setProcomments(String procomments) {
        this.procomments = procomments;
    }

    public Project(){
        projectid=UUIDs.timeBased();
        
    }
}