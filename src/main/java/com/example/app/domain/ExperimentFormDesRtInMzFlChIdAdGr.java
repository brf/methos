/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.example.app.domain;

import java.io.Serializable;

/**
 *
 * @author kostas
 */
public class ExperimentFormDesRtInMzFlChIdAdGr implements Serializable{

    private String filename;
    private String groupid;
    private String description;
    private String adduct;
    private Float mz;
    private Float rt;
    private Float intensity;
    private String identifier;
    private String chemical_formula;

    public String getDescription() {
        return description;
    }
    public void setDescription(String description) {
        this.description = description;
    }
    
    public Float getRt() {
        return rt;
    }
    public void setRt(Float rt) {
        this.rt = rt;
    }
    
    public Float getIntensity() {
        return intensity;
    }
    public void setIntensity(Float intensity) {
        this.intensity = intensity;
    }
    public Float getMz() {
        return mz;
    }
    public void setMz(Float mz) {
        this.mz = mz;
    }
    public String getIdentifier() {
        return identifier;
    }
    public void setIdentifier(String identifier) {
        this.identifier = identifier;
    }
    public String getChemical_formula() {
        return chemical_formula;
    }
    public void setChemical_formula(String chemical_formula) {
        this.chemical_formula = chemical_formula;
    }
    public String getFilename() {
        return filename;
    }
    public void setFilename(String filename) {
        this.filename = filename;
    }
    public String getAdduct() {
        return adduct;
    }
    public void setAdduct(String adduct) {
        this.adduct = adduct;
    }
    public String getGroupid() {
        return groupid;
    }
    public void setGroupid(String groupid) {
        this.groupid = groupid;
    }
}
