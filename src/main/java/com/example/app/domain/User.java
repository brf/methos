/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.example.app.domain;

import com.datastax.driver.core.DataType;
import com.datastax.driver.mapping.annotations.Table;
import java.io.Serializable;
import org.springframework.data.cassandra.core.cql.PrimaryKeyType;
import org.springframework.data.cassandra.core.mapping.CassandraType;
import org.springframework.data.cassandra.core.mapping.PrimaryKey;
import org.springframework.data.cassandra.core.mapping.PrimaryKeyColumn;

/**
 *
 * @author kostas
 */
@Table(name= "user")
public class User implements Serializable{
    
    @PrimaryKeyColumn(name = "username",ordinal = 0,type = PrimaryKeyType.PARTITIONED)
    @CassandraType(type=DataType.Name.VARCHAR)
    private String username;
    private String accountdatecreated;
    private String email;
    private String password;
    private String verificationcode;
    private Boolean verifiedacc;
    private String resetpswcode;
    
    
    public String getUsername() {
        return username;
    }
    public void setUsername(String username) {
        this.username = username;
    }
    
    public String getAccountdatecreated() {
        return accountdatecreated;
    }
    public void setAccountdatecreated(String accountdatecreated) {
        this.accountdatecreated = accountdatecreated;
    }
    
    public String getEmail() {
        return email;
    }
    public void setEmail(String email) {
        this.email = email;
    }
    
    public String getPassword() {
        return password;
    }
    public void setPassword(String password) {
        this.password = password;
    }
    
    public Boolean getVerifiedacc() {
        return verifiedacc;
    }
    public void setVerifiedacc(Boolean verifiedacc) {
        this.verifiedacc = verifiedacc;
    }
    
    public String getVerificationcode() {
        return verificationcode;
    }
    public void setVerificationcode(String verificationcode) {
        this.verificationcode = verificationcode;
    }
    
    public void setResetpswcode(String resetpswcode) {
        this.resetpswcode = resetpswcode;
    }
    
    public String getResetpaswcode() {
        return resetpswcode;
    }
    
    public User(){
        
    }
}
