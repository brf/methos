/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.example.app.config;

import com.amazonaws.auth.AWSCredentials;
import com.amazonaws.auth.AWSStaticCredentialsProvider;
import com.amazonaws.auth.BasicAWSCredentials;
import com.amazonaws.client.builder.AwsClientBuilder;
import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.AmazonS3Builder;
import com.amazonaws.services.s3.AmazonS3ClientBuilder;
import org.apache.spark.api.java.JavaSparkContext;
import org.apache.spark.sql.SparkSession;
import org.springframework.boot.CommandLineRunner;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import com.example.app.services.StorageService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;

/**
 *
 * @author kostas
 */
@Configuration
@EnableWebSecurity
public class SpringConf extends WebSecurityConfigurerAdapter{
 
    @Autowired
    private UserDetailsService userService;
    
    
    @Override
    protected void configure(HttpSecurity http) throws Exception {
            http
                .authorizeRequests()
                    .antMatchers("/welcomeview","/css/**","/bootstrap_4_1_0/**","/googleapisjquery.min.js","/popper.min.js","/jquery-3.3.1.slim.min.js","/webfonts/**","/favicon.ico","/img/methoslogo.png","/walkthrough.pdf","/img/rightmark.png","/img/wrongmark.png","/js/registrationscope.js","/registrationview","/resetpassview","/resetpassformview","/js/resetpassformscope.js","/verify")
                    .permitAll()
                    .anyRequest().authenticated()
                    .and()
                .formLogin()
                    .loginPage("/welcomeview")
                    .permitAll()
                    .defaultSuccessUrl("/projectsview", true)
                    .failureUrl("/welcomeview?error")
                    .and()
                .logout()
                    .logoutSuccessUrl("/welcomeview?logout")
                    .and()
                .csrf().disable();
    }
    
    @Bean
    public PasswordEncoder passwordEncoder() {
        return new BCryptPasswordEncoder();
    }
    
    @Autowired
    public void configureGlobal(AuthenticationManagerBuilder auth) throws Exception {
        auth.userDetailsService(userService).passwordEncoder(passwordEncoder());
              
    }
    
    
    
    @Bean
    public AmazonS3 objloc(){
        
        AWSCredentials credentials = new BasicAWSCredentials("openstackuser","openstackpsw");
        AwsClientBuilder.EndpointConfiguration regionOne = new AmazonS3Builder.EndpointConfiguration("openstackcebitec:port", "regionname");
        
        AmazonS3 amazonS3 = AmazonS3ClientBuilder.standard().withCredentials(new AWSStaticCredentialsProvider(credentials)).withEndpointConfiguration(regionOne).build();        
        
        return amazonS3;
    }
    
    @Bean
    public SparkSession sparksession() {
         SparkSession sp = SparkSession
                 .builder()
        
        .config("spark.authenticate.secret","extrasecret")
        .config("spark.cassandra.connection.host","IPs of the 16 computer instances")
        .master("spark://IP:port")
        .config("spark.sql.extensions","com.datastax.spark.connector.CassandraSparkExtensions")
        .config("directJoinSetting","off")
        .config("spark.sql.broadcastTimeout", "86400")
        .config("spark.executor.heartbeatInterval","600s")
        .config("spark.network.timeout","7200s")
        .appName("biomet")
        .config("spark.local.dir","/home/ubuntu/sparktmp/")
        .config("spark.driver.maxResultSize", "16g")
        .config("spark.jars", "app-0.0.1-SNAPSHOT.jar")
        .config("spark.sql.shuffle.partitions",96)
        .config("spark.executor.memory","23g")
        .config("spark.sql.pivotMaxValues","50000") 
        .config("spark.sql.caseSensitive",true)
        .config("spark.hadoop.fs.s3a.access.key","accesskey")
        .config("spark.hadoop.fs.s3a.secret.key","secretkey")
                 .getOrCreate();
         return sp;
     }
    
    @Bean
    public JavaSparkContext sc(){
        JavaSparkContext sc = new JavaSparkContext(sparksession().sparkContext());
        return sc;
    }
    
    
    @Bean
    CommandLineRunner init(StorageService storageService) {
        return (args) -> {
            
        };
    }
    
    
}
