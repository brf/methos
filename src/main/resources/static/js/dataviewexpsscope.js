/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


$(document).ready(function() {
    
    var table=$('#protable').on( 'draw.dt', function () {
                $("#msgcontainer").attr("id","msgcontainerafter");
                $("#loadercontainer").css("display","none");
              } ).DataTable( {
        responsive: true,
        bLengthChange: false,
        columnDefs: [ {
            orderable: false,
            targets:   0
        } ],
        pageLength: 15,
        "language": {
            "info": "Showing _START_ to _END_ of _TOTAL_ experiments"
      }
    } );
    
    $('#protable tr:gt(0)').click(function() {
        ele = $(this).find('td input:checkbox')[0];
        ele.checked = ! ele.checked;
      });
      $('input:checkbox').click(function(e){
        e.stopPropagation();
      });

    
    $("#disbtn").click(function(){
        var boxes = table.cells( ).nodes();
        var chboxes = $( boxes ).find(':checkbox');
        var k = 0;
        for(var i = 0; i < chboxes.length; i++){
            if(chboxes[i].checked===true){
                k++;
            }
        }
        if(k<1||k>1){
            alert("Select only one experiment.");
            return (false);
        }else {
            $(document).on('click', ".update",function (){
                    $('#analysisspinner').show();
                    //$('.alert').hide();
                    document.getElementById("disbtn").disabled=true;
                    $("#disbtn").removeClass("setbtn");
                    $("#myform").submit();
                });
            return (true);
        }
    });
    
    
} );

