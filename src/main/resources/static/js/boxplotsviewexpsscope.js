/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


$(document).ready(function() {
    document.getElementById("eachval").checked=true;
    document.getElementById("notra").checked=true;
    document.getElementById("ionboth").checked=true;
          
    var table=$('#protable').on( 'draw.dt', function () {
                $("#msgcontainer").attr("id","msgcontainerafter");
                $("#loadercontainer").css("display","none");
              } ).DataTable( {
        responsive: true,
        bLengthChange: false,
        columnDefs: [ {orderable: false,targets: 0 }//,
                    ],
        pageLength: 10,
        "language": {
            "info": "Showing _START_ to _END_ of _TOTAL_ experiments"
      }
      
    } );
    
    
    $("input[name=eachorgroup]").click(function(){
        if($("#eachval").is(":checked")){
            var rows = table.rows({ 'search': 'applied' }).nodes();
            $("#selectall1").prop('checked',false);
            $("#selectall2").prop('checked',false);
            $("#selectall3").prop('checked',false);
            $( rows ).find('input[name=experimentidgroup1]:checkbox').prop('checked', false);
            $( rows ).find('input[name=experimentidgroup2]:checkbox').prop('checked', false);
            $( rows ).find('input[name=experimentidgroup3]:checkbox').prop('checked', false);
        }
        if($("#groupval2").is(":checked")){
            var rows = table.rows({ 'search': 'applied' }).nodes();
            $("#selectall").prop('checked',false);
            $("#selectall3").prop('checked',false);
            $( rows ).find('input[name=experimentid]:checkbox').prop('checked', false);
            $( rows ).find('input[name=experimentidgroup3]:checkbox').prop('checked', false);
        }
        if($("#groupval3").is(":checked")){
            var rows = table.rows({ 'search': 'applied' }).nodes();
            $("#selectall").prop('checked',false);
            $("#selectall1").prop('checked',false);
            $("#selectall2").prop('checked',false);
            $( rows ).find('input[name=experimentid]:checkbox').prop('checked', false);
            $( rows ).find('input[name=experimentidgroup1]:checkbox').prop('checked', false);
            $( rows ).find('input[name=experimentidgroup2]:checkbox').prop('checked', false);
        }
    });


    $("#protable").on("click","tr:gt(0)",function(e){
        if($("#eachval").is(":checked")){
            ele = $(this).find('td input:checkbox')[0];
            ele.checked = ! ele.checked;
            e.stopPropagation();
        }
      });
    $("#protable").on("click", "tr:gt(0) .gr", function(e) {
        ele = $(this).find('input:checkbox')[0];
        ele.checked = !ele.checked;
        e.stopPropagation();
    });
    $("#protable").on("click","tr:gt(0) td .group", function(e) {
      
         e.stopPropagation();
    });
    $('#selectall').change(function(){
        var rows = table.rows({ 'search': 'applied' }).nodes();
        $( rows ).find('input[name=experimentid]:checkbox').prop('checked', $(this).is(':checked'));
    });

      
      
    $("#protable").on("click", "tr:gt(0) .gr1", function(e) {
        ele = $(this).find('input:checkbox')[0];
        ele.checked = ! ele.checked;
        if($(".group1").is(':checked')){
             $(this).closest("tr").find('.group2').prop('checked', false);
             if($("#groupval3").is(":checked")){
                 $(this).closest("tr").find('.group3').prop('checked', false);
             }
         }
        e.stopPropagation();
    });
    
    $("#protable").on("click", "tr:gt(0) .gr2", function(e) {
        ele = $(this).find('input:checkbox')[0];
        ele.checked = ! ele.checked;
        if($(".group2").is(':checked')){
             $(this).closest("tr").find('.group1').prop('checked', false);
             if($("#groupval3").is(":checked")){
                 $(this).closest("tr").find('.group3').prop('checked', false);
             }
        }
        e.stopPropagation();
    });
    
    $("#protable").on("click", "tr:gt(0) .gr3", function(e) {
        ele = $(this).find('input:checkbox')[0];
        ele.checked = ! ele.checked;
        if($(".group3").is(':checked')){
             $(this).closest("tr").find('.group1').prop('checked', false);
             $(this).closest("tr").find('.group2').prop('checked', false);
        }
        e.stopPropagation();
    });
    
    
    
    $("#protable").on("click","tr:gt(0) td .group1", function(e) {
        if($(this).is(':checked')){
             $(this).closest("tr").find('.group2').prop('checked', false);
             if($("#groupval3").is(":checked")){
                 $(this).closest("tr").find('.group3').prop('checked', false);
             }
         }
         e.stopPropagation();
    });
    
    $("#protable").on("click","tr:gt(0) td .group2", function(e) {
        if($(this).is(':checked')){
             $(this).closest("tr").find('.group1').prop('checked', false);
             if($("#groupval3").is(":checked")){
                 $(this).closest("tr").find('.group3').prop('checked', false);
             }
         }
         e.stopPropagation();
    });
    
    $("#protable").on("click","tr:gt(0) td .group3", function(e) {
        if($(this).is(':checked')){
             $(this).closest("tr").find('.group1').prop('checked', false);
             $(this).closest("tr").find('.group2').prop('checked', false);
         }
         e.stopPropagation();
    });

    $('#selectall1').click(function(){
        
        var rows = table.rows({ 'search': 'applied' }).nodes();
        $( rows ).find('input[name=experimentidgroup1]:checkbox').prop('checked', $(this).is(':checked'));
        if($(this).is(':checked')){
            $( rows ).find('input[name=experimentidgroup2]:checkbox').prop('checked', false);
            if($("#groupval3").is(":checked")){
                 $( rows ).find('input[name=experimentidgroup3]:checkbox').prop('checked', false);
             }
        }
        
    });
    $('#selectall2').click(function(){
        
        var rows = table.rows({ 'search': 'applied' }).nodes();
        $( rows ).find('input[name=experimentidgroup2]:checkbox').prop('checked', $(this).is(':checked'));
        if($(this).is(':checked')){
            $( rows ).find('input[name=experimentidgroup1]:checkbox').prop('checked', false);
            if($("#groupval3").is(":checked")){
                 $( rows ).find('input[name=experimentidgroup3]:checkbox').prop('checked', false);
             }
        }
    });
    $('#selectall3').click(function(){
        
        var rows = table.rows({ 'search': 'applied' }).nodes();
        $( rows ).find('input[name=experimentidgroup3]:checkbox').prop('checked', $(this).is(':checked'));
        if($(this).is(':checked')){
            $( rows ).find('input[name=experimentidgroup1]:checkbox').prop('checked', false);
            $( rows ).find('input[name=experimentidgroup2]:checkbox').prop('checked', false);
        }
    });
    
    table.columns([3,4,5]).visible(false);
    $(".colclick").click(function(){
        if($("#eachval").is(":checked")){
            table.column(0).visible(true);
            table.columns([3,4,5]).visible(false);
        }else if($("#groupval2").is(":checked")){
            table.column(0).visible(false);
            table.columns([3,4]).visible(true);
            table.column(5).visible(false);
        }else if($("#groupval3").is(":checked")){
            table.column(0).visible(false);
            table.columns([3,4,5]).visible(true);
        }
    });
    
    $('#myform').on('submit', function(){
        var $form = $(this);
        // Iterate over all checkboxes in the table
        table.$('input[type="checkbox"]').each(function(){
           // If checkbox doesn't exist in DOM
           if(!$.contains(document, this)){
              // If checkbox is checked
              if(this.checked){
                 // Create a hidden element 
                 $form.append(
                    $('<input>')
                       .attr('type', 'hidden')
                       .attr('name', this.name)
                       .val(this.value)
                 );
              }
           } 
        });
    });
    
   
    $("#disbtn").click(function(){
        var boxes = table.cells( ).nodes();
        if($("#eachval").is(":checked")){
            var chboxes = $( boxes ).find(':checkbox');
            var k = 0;
            for(var i = 0; i < chboxes.length; i++){
                if(chboxes[i].checked===true){
                    k++;
                }
            }
            if(k<1 || k>30){
                alert("For representation reasons please select from 1 to 30 experiments for analysis.");
                return (false);
            }else{
                $(document).on('click', ".update",function (){
                        $('#analysisspinner').show();
                        document.getElementById("disbtn").disabled=true;
                        $("#disbtn").removeClass("setbtn");
                        $("#myform").submit();
                    });
                return (true);
            }
        }else if($("#groupval2").is(":checked")){
            var chkboxes1=$( boxes ).find('input[name=experimentidgroup1]:checkbox');
            var nrgroup1 = 0;
            for(var i = 0; i < chkboxes1.length; i++){
                if(chkboxes1[i].checked===true){
                    nrgroup1++;
                }
            }
            var chkboxes2=$( boxes ).find('input[name=experimentidgroup2]:checkbox');
            var nrgroup2 = 0;        
            for(var i = 0; i < chkboxes2.length; i++){
                if(chkboxes2[i].checked===true){
                    nrgroup2++;
                }
            }
            
            if(nrgroup1<1 || nrgroup2<1 || nrgroup1+nrgroup2<2){
                alert("Please select at least two experiments in total. Every group must be assigned with at least one experiment.");
                return (false);
            }else{
                $(document).on('click', ".update",function (){
                        $('#analysisspinner').show();
                        document.getElementById("disbtn").disabled=true;
                        $("#disbtn").removeClass("setbtn");
                        $("#myform").submit();
                    });
                return (true);
            }
        }else if($("#groupval3").is(":checked")){
            var chkboxes1=$( boxes ).find('input[name=experimentidgroup1]:checkbox');
            var nrgroup1 = 0;
            for(var i = 0; i < chkboxes1.length; i++){
                if(chkboxes1[i].checked===true){
                    nrgroup1++;
                }
            }
            var chkboxes2=$( boxes ).find('input[name=experimentidgroup2]:checkbox');
            var nrgroup2 = 0;        
            for(var i = 0; i < chkboxes2.length; i++){
                if(chkboxes2[i].checked===true){
                    nrgroup2++;
                }
            }
            var chkboxes3=$( boxes ).find('input[name=experimentidgroup3]:checkbox');
            var nrgroup3 = 0;        
            for(var i = 0; i < chkboxes3.length; i++){
                if(chkboxes3[i].checked===true){
                    nrgroup3++;
                }
            }
            
            if(nrgroup1<1 || nrgroup2<1 || nrgroup3<1 || nrgroup1+nrgroup2+nrgroup3<3){
                alert("Please select at least three experiments in total. Every group must be assigned with at least one experiment.");
                return (false);
            }else{
                $(document).on('click', ".update",function (){
                        $('#analysisspinner').show();
                        document.getElementById("disbtn").disabled=true;
                        $("#disbtn").removeClass("setbtn");
                        $("#myform").submit();
                    });
                return (true);
            }
        }
        
    });
    
} );

