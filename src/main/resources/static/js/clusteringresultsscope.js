/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

$(document).ready(function() {
    var table1 = $('#protable1').on( 'draw.dt', function () {
                $("#msgcontainer1").attr("id","msgcontainerafter1");
                $("#loadercontainer1").css("display","none");
              } ).DataTable( {
        responsive: true,
        bLengthChange: false,
        columnDefs: [ {
            orderable: false,
            targets:   0
        } ],
        pageLength: 10,
        "language": {
            "info": "Showing _START_ to _END_ of _TOTAL_ experiments"
        }
        
    });

    var tableclusters = $('#protableclusters').on( 'draw.dt', function () {
                $("#msgcontainer2").attr("id","msgcontainerafter2");
                $("#loadercontainer2").css("display","none");
              } ).DataTable( {
        responsive: true,
        bLengthChange: false,
        columnDefs: [ {
            orderable: false,
            targets:   0
        } ],
        pageLength: 10,
        "language": {
            "info": "Showing _START_ to _END_ of _TOTAL_ clusters"
        }
        
    });
    
    
    var table3 = $('#protable3').on( 'draw.dt', function () {
                $("#msgcontainer1").attr("id","msgcontainerafter1");
                $("#loadercontainer1").css("display","none");
              } ).DataTable( {
        responsive: true,
        bLengthChange: false,
        columnDefs: [ {
            orderable: false,
            targets:   0
        } ],
        pageLength: 10,
        "language": {
            "info": "Showing _START_ to _END_ of _TOTAL_ experiments and replicates"
        }
        
    });
   
    var table5 = $('#protable5').on( 'draw.dt', function () {
                $("#msgcontainer1").attr("id","msgcontainerafter1");
                $("#loadercontainer1").css("display","none");
              } ).DataTable( {
        responsive: true,
        bLengthChange: false,
        columnDefs: [ {
            orderable: false,
            targets:   0
        } ],
        pageLength: 10,
        "language": {
            "info": "Showing _START_ to _END_ of _TOTAL_ metabolites"
        }
        
    });
    
    var tablemiss = $('#protablemiss').on( 'draw.dt', function () {
                $("#msgcontainer3").attr("id","msgcontainerafter3");
                $("#loadercontainer3").css("display","none");
              } ).DataTable( {
        responsive: true,
        bLengthChange: false,
        columnDefs: [ {
            orderable: false,
            targets:   0
        } ],
        pageLength: 10,
        "language": {
            "info": "Showing _START_ to _END_ of _TOTAL_ metabolites"
        }
        
    });
    
    var clusteringlevel = document.getElementById("clusteringlevel");
    function clustering(rescale){
        
        if(clusteringlevel.value==="onexps"){
            var mtbstable = document.getElementById("mtbstable");
            var expstable = document.getElementById("expstable");
            var expsdata=[];
            var expstablelengthrows =expstable.rows.length;

            var mtbsnameslist=[];
            var k=0;
            var l=0;
            for(k=0;k<expstablelengthrows;k++){
                var expstablelengthcells =expstable.rows[k].cells.length;
                for(l=1;l<expstablelengthcells;l++){
                    expsdata.push({"clustindex":expstable.rows[k].cells[0].innerHTML,"mtbname":mtbstable.rows[l-1].cells[0].innerHTML,"clustcenters":expstable.rows[k].cells[l].innerHTML});
                    mtbsnameslist.push(mtbstable.rows[l-1].cells[0].innerHTML);

                }
            }

            var formatNumber = d3.format(".1f"),
                    formatTrillion = function(x) { return formatNumber(x / 1e12) + "T"; },
                    formatBillion = function(x) { return formatNumber(x / 1e9) + "B"; },
                    formatMillion = function(x) { return formatNumber(x / 1e6) + "M"; },
                    formatThousand = function(x) { return formatNumber(x / 1e3) + "k"; },
                    formatHundred = function(x) { return x; };


                function formatAbbreviation(x) {
                  var v = Math.abs(x);
                  return (v >= .9995e12 ? formatTrillion
                      :v >= .9995e9 ? formatBillion
                      : v >= .9995e6 ? formatMillion
                      : v >= .9995e3 ? formatThousand
                      : formatHundred)(x);
                }

            // group the data: I want to draw one line per group
            // nest function allows to group the calculation per level of a factor
            var groupclusters = d3.nest()
                .key(function(d) { return d.clustindex; })
                .entries(expsdata);

            var margin = {top: 30, right: 0, bottom: 30, left: 50},
                width = 290 - margin.left - margin.right,
                height = 240 - margin.top - margin.bottom;

            // Create the svg canvas in the "graph" div
            if(rescale==="yes"){
                d3.selectAll("svg").remove();
                var svg = d3.select("#container3")
                    .selectAll("svg")
                    .data(groupclusters)
                    .enter()
                    .append("svg")
                    .attr("width", width + margin.left + margin.right)
                    .attr("height", height + margin.top + margin.bottom)
                    .append("g")
                    .attr("transform","translate(" + margin.left + "," + margin.top + ")")
                    .each(function(d, i){
                        var eachclust = d.values;
                        var svg = d3.select(this);
                        
                        var yMax = d3.max(eachclust, function(d) { return +d.clustcenters; });
                        var yMin = d3.min(eachclust, function(d) { return +d.clustcenters; });
                        
                        var y = d3.scaleLinear().domain([yMin, yMax]).range([height, 0]);
                        
                        svg.append("g")
                                .attr("class", "y axis")
                                .call(d3.axisLeft(y).ticks(10).tickFormat(formatAbbreviation));
                        
                        var x = d3.scaleBand()
                            .domain(mtbsnameslist)
                            .range([ 0, width ]);

                    var yZero = y(0);
                        svg.append('line')
                          .attr('x1', 0)
                          .attr('x2', width)
                          .attr('y1', yZero)
                          .attr('y2', yZero)
                          .style('stroke', '#000');
                  
                        svg.append("path")
                                .attr("fill", "none")
                                .attr("stroke", '#a65628')
                                .attr("stroke-width", 1)
                                .attr("class", "line")
                                .attr("d", function(d){
                                  return d3.line()
                                .x(function(d) { return x(d.mtbname); })
                                .y(function(d) { return y(+d.clustcenters); })(d.values);
                                });

                        svg.append("text")
                            .attr("text-anchor", "start")
                            .attr("y", -5)
                            .attr("x", 0)
                            .text(function(d){return(d.key);})
                            .style("fill", '#a65628');

                        svg.append("text")
                            .attr("y", height+15)
                            .attr("x", width/3 )
                            .text("Metabolites")
                            .style("font-size", "12px");
                            });
                
                    
            }else{
                d3.selectAll("svg").remove();
                        
                var svg = d3.select("#container3")
                    .selectAll("svg")
                    .data(groupclusters)
                    .enter()
                    .append("svg")
                    .attr("width", width + margin.left + margin.right)
                    .attr("height", height + margin.top + margin.bottom)
                    .append("g")
                    .attr("transform","translate(" + margin.left + "," + margin.top + ")");
           
                var x = d3.scaleBand()
                    .domain(mtbsnameslist)
                    .range([ 0, width ]);


                var y = d3.scaleLinear()
                    .domain([d3.min(expsdata, function(d) { return +d.clustcenters; }), d3.max(expsdata, function(d) { return +d.clustcenters; })])
                    .range([ height, 0 ]);
            
                var yAxis = d3.axisLeft(y).ticks(10).tickFormat(formatAbbreviation);
                
                var yZero = y(0);
                        svg.append('line')
                          .attr('x1', 0)
                          .attr('x2', width)
                          .attr('y1', yZero)
                          .attr('y2', yZero)
                          .style('stroke', '#000');        
    
            svg.append("g")
                    .attr("class", "y axis")
                    .call(yAxis);
            
   
                svg.append("text")
                    .attr("text-anchor", "start")
                    .attr("y", -5)
                    .attr("x", 0)
                    .text(function(d){return(d.key);})
                    .style("fill", '#a65628');

                svg.append("text")
                    .attr("y", height+15)
                    .attr("x", width/3 )
                    .text("Metabolites")
                    .style("font-size", "12px");
    

                 svg.append("path")
                    .attr("fill", "none")
                    .attr("stroke", '#a65628')
                    .attr("stroke-width", 1)
                    .attr("class", "line")
                    .attr("d", function(d){
                        return d3.line()
                      .x(function(d) {return x(d.mtbname); })
                      .y(function(d) {return y(+d.clustcenters); })(d.values);
                    });
            }

            

        }else if(clusteringlevel.value==="onrepls"){

            var mtbstable = document.getElementById("mtbstable");
            var replitable = document.getElementById("replitable");
            var replidata=[];
            var replitablelengthrows =replitable.rows.length;
            var mtbsnameslist=[];
            var k=0;
            var l=0;
            for(k=0;k<replitablelengthrows;k++){
                var replitablelengthcells =replitable.rows[k].cells.length;
                for(l=1;l<replitablelengthcells;l++){

                    replidata.push({"clustindex":replitable.rows[k].cells[0].innerHTML,"mtbname":mtbstable.rows[l-1].cells[0].innerHTML,"clustcenters":replitable.rows[k].cells[l].innerHTML});
                    mtbsnameslist.push(mtbstable.rows[l-1].cells[0].innerHTML);

                }
            }

            var formatNumber = d3.format(".1f"),
                    formatTrillion = function(x) { return formatNumber(x / 1e12) + "T"; },
                    formatBillion = function(x) { return formatNumber(x / 1e9) + "B"; },
                    formatMillion = function(x) { return formatNumber(x / 1e6) + "M"; },
                    formatThousand = function(x) { return formatNumber(x / 1e3) + "k"; },
                    formatHundred = function(x) { return x; };


                function formatAbbreviation(x) {
                  var v = Math.abs(x);
                  return (v >= .9995e12 ? formatTrillion
                      :v >= .9995e9 ? formatBillion
                      : v >= .9995e6 ? formatMillion
                      : v >= .9995e3 ? formatThousand
                      : formatHundred)(x);
                }


            // group the data: I want to draw one line per group
            var groupclusters = d3.nest() // nest function allows to group the calculation per level of a factor
                .key(function(d) { return d.clustindex; })
                .entries(replidata);

            var margin = {top: 30, right: 0, bottom: 30, left: 50},
                width = 290 - margin.left - margin.right,
                height = 240 - margin.top - margin.bottom;

            if(rescale==="yes"){
                d3.selectAll("svg").remove();
                var svg = d3.select("#container3")
                        .selectAll("svg")
                        .data(groupclusters)
                        .enter()
                        .append("svg")
                        .attr("width", width + margin.left + margin.right)
                        .attr("height", height + margin.top + margin.bottom)
                        .append("g")
                        .attr("transform","translate(" + margin.left + "," + margin.top + ")")
                        .each(function(d, i){
                        var eachclust = d.values;
                        var svg = d3.select(this);
                        var yMax = d3.max(eachclust, function(d) { return +d.clustcenters; });
                        var yMin = d3.min(eachclust, function(d) { return +d.clustcenters; });
                        
                        var y = d3.scaleLinear().domain([yMin, yMax]).range([height, 0]);
                        
                        svg.append("g")
                                .attr("class", "y axis")
                                .call(d3.axisLeft(y).ticks(10,"s").tickFormat(formatAbbreviation));
                        
                        var x = d3.scaleBand()
                            .domain(mtbsnameslist)
                            .range([ 0, width ]);

                        var yZero = y(0);
                                svg.append('line')
                                  .attr('x1', 0)
                                  .attr('x2', width)
                                  .attr('y1', yZero)
                                  .attr('y2', yZero)
                                  .style('stroke', '#000');
                    
                        svg.append("path")
                                .attr("fill", "none")
                                .attr("stroke", '#a65628')
                                .attr("stroke-width", 1)
                                .attr("class", "line")
                                .attr("d", function(d){
                                  return d3.line()
                                .x(function(d) { return x(d.mtbname); })
                                .y(function(d) { return y(+d.clustcenters); })(d.values);
                                });

                        svg.append("text")
                            .attr("text-anchor", "start")
                            .attr("y", -5)
                            .attr("x", 0)
                            .text(function(d){return(d.key);})
                            .style("fill", '#a65628');

                        svg.append("text")
                            .attr("y", height+15)
                            .attr("x", width/3 )
                            .text("Metabolites")
                            .style("font-size", "12px");
                            });
                
                
            }else{
                d3.selectAll("svg").remove();
                // Create the svg canvas in the "graph" div
                var svg = d3.select("#container3")
                        .selectAll("svg")
                        .data(groupclusters)
                        .enter()
                        .append("svg")
                        .attr("width", width + margin.left + margin.right)
                        .attr("height", height + margin.top + margin.bottom)
                        .append("g")
                        .attr("transform","translate(" + margin.left + "," + margin.top + ")");//it was margin.left+30

                var x = d3.scaleBand()
                    .domain(mtbsnameslist)
                    .range([ 0, width ]);

                var y = d3.scaleLinear()
                    .domain([d3.min(replidata, function(d) { return +d.clustcenters; }), d3.max(replidata, function(d) { return +d.clustcenters; })])
                    .range([ height, 0 ]);

                svg.append("g")
                    .attr("class", "y axis")
                    .call(d3.axisLeft(y).ticks(10,"s").tickFormat(formatAbbreviation));
                    
                    var yZero = y(0);
                        svg.append('line')
                          .attr('x1', 0)
                          .attr('x2', width)
                          .attr('y1', yZero)
                          .attr('y2', yZero)
                          .style('stroke', '#000');
                  
                svg.append("text")
                    .attr("text-anchor", "start")
                    .attr("y", -5)
                    .attr("x", 0)
                    .text(function(d){return(d.key);})
                    .style("fill", '#a65628');

                svg.append("text")
                    .attr("y", height+15)
                    .attr("x", width/3 )
                    .text("Metabolites")
                    .style("font-size", "12px");

                 svg.append("path")
                    .attr("fill", "none")
                    .attr("stroke", '#a65628')
                    .attr("stroke-width", 1)
                    .attr("class", "line")
                    .attr("d", function(d){
                      return d3.line()
                    .x(function(d) { return x(d.mtbname); })
                    .y(function(d) { return y(+d.clustcenters); })(d.values);
                    }); 
                }
        }else if(clusteringlevel.value==="onmtbs"){
            var mtbstable = document.getElementById("mtbstable");
            var expstable = document.getElementById("expstable");
            var expnameslist=[];
            var mtbstablelengthrows =mtbstable.rows.length;

            var mtbsdata=[];
            var k=0;
            var l=0;
            for(k=0;k<mtbstablelengthrows;k++){
                var mtbstablelengthcells =mtbstable.rows[k].cells.length;
                for(l=1;l<mtbstablelengthcells;l++){
                    mtbsdata.push({"clustindex":mtbstable.rows[k].cells[0].innerHTML,"expname":expstable.rows[l-1].cells[0].innerHTML,"clustcenters":mtbstable.rows[k].cells[l].innerHTML});
                    expnameslist.push(expstable.rows[l-1].cells[0].innerHTML);

                }
            }
            var formatNumber = d3.format(".1f"),
                    formatTrillion = function(x) { return formatNumber(x / 1e12) + "T"; },
                    formatBillion = function(x) { return formatNumber(x / 1e9) + "B"; },
                    formatMillion = function(x) { return formatNumber(x / 1e6) + "M"; },
                    formatThousand = function(x) { return formatNumber(x / 1e3) + "k"; },
                    formatHundred = function(x) { return x; };


                function formatAbbreviation(x) {
                  var v = Math.abs(x);
                  return (v >= .9995e12 ? formatTrillion
                      :v >= .9995e9 ? formatBillion
                      : v >= .9995e6 ? formatMillion
                      : v >= .9995e3 ? formatThousand
                      : formatHundred)(x);
                }
            
            // group the data: I want to draw one line per group
            var groupclusters = d3.nest() // nest function allows to group the calculation per level of a factor
                .key(function(d) { return d.clustindex; })
                .entries(mtbsdata);

            var margin = {top: 30, right: 0, bottom: 30, left: 50},
                width = 290 - margin.left - margin.right,
                height = 240 - margin.top - margin.bottom;

            if(rescale==="yes"){
                d3.selectAll("svg").remove();
                var svg = d3.select("#container3")
                        .selectAll("svg")
                        .data(groupclusters)
                        .enter()
                        .append("svg")
                        .attr("width", width + margin.left + margin.right)
                        .attr("height", height + margin.top + margin.bottom)
                        .append("g")
                        .attr("transform","translate(" + margin.left + "," + margin.top + ")")
                .each(function(d, i){
                        var eachclust = d.values;
                        var svg = d3.select(this);
                        var yMax = d3.max(eachclust, function(d) { return +d.clustcenters; });
                        var yMin = d3.min(eachclust, function(d) { return +d.clustcenters; });
                        var y = d3.scaleLinear().domain([yMin, yMax]).range([height, 0]);
                        
                        svg.append("g")
                                .attr("class", "y axis")
                                .call(d3.axisLeft(y).ticks(10).tickFormat(formatAbbreviation));
                        
                        var x = d3.scaleBand()
                            .domain(expnameslist)
                            .range([ 0, width ]);

                        var yZero = y(0);
                        svg.append('line')
                          .attr('x1', 0)
                          .attr('x2', width)
                          .attr('y1', yZero)
                          .attr('y2', yZero)
                          .style('stroke', '#000');
                        
                        svg.append("text")
                            .attr("text-anchor", "start")
                            .attr("y", -5)
                            .attr("x", 0)
                            .text(function(d){return(d.key);})
                            .style("fill", '#a65628');

                        
                        svg.append("text")
                            .attr("y", height+15)
                            .attr("x", width/3 )
                            .text("Experiments")
                            .style("font-size", "12px");
                    
                    svg.append("path")
                                .attr("fill", "none")
                                .attr("stroke", '#a65628')
                                .attr("stroke-width", 1)
                                .attr("class", "line")
                                .attr("d", function(d){
                                  return d3.line()
                                .x(function(d) { return x(d.expname); })
                                .y(function(d) { return y(+d.clustcenters); })(d.values);
                                });
                        
                    });
                
                }else{
                    
                d3.selectAll("svg").remove();
                // Create the svg canvas in the "graph" div
                var svg = d3.select("#container3")
                        .selectAll("svg")
                        .data(groupclusters)
                        .enter()
                        .append("svg")
                        .attr("width", width + margin.left + margin.right)
                        .attr("height", height + margin.top + margin.bottom)
                        .append("g")
                        .attr("transform","translate(" + margin.left + "," + margin.top + ")");


                var x = d3.scaleBand()
                    .domain(expnameslist)
                    .range([ 0, width ]);

                var y = d3.scaleLinear()
                    .domain([d3.min(mtbsdata, function(d) { return +d.clustcenters; }), d3.max(mtbsdata, function(d) { return +d.clustcenters; })])
                    .range([ height, 0 ]);

                svg.append("g")
                    .attr("class", "y axis")
                    .call(d3.axisLeft(y).ticks(10).tickFormat(formatAbbreviation));
            
            var yZero = y(0);
                        svg.append('line')
                          .attr('x1', 0)
                          .attr('x2', width)
                          .attr('y1', yZero)
                          .attr('y2', yZero)
                          .style('stroke', '#000');
                  
                svg.append("text")
                    .attr("text-anchor", "start")
                    .attr("y", -5)
                    .attr("x", 0)
                    .text(function(d){return(d.key);})
                    .style("fill", '#a65628');

                
                svg.append("text")
                        .attr("y", height+15)
                        .attr("x", width/3 )
                        .text("Experiments")
                        .style("font-size", "12px");  
                

                svg.append("path")
                    .attr("fill", "none")
                    .attr("stroke", '#a65628')
                    .attr("stroke-width", 1)
                    .attr("class", "line")
                    .attr("d", function(d){
                      return d3.line()
                    .x(function(d) { return x(d.expname); })
                    .y(function(d) { return y(+d.clustcenters); })(d.values);
                    }); 
            }
        }
    }
    
    clustering("no");
    $('#rescaling').change(function () {
        if ($(this).prop("checked")) { // checked
            clustering("yes");
        }else{ // not checked
            clustering("no");
        }
    });
    
});


