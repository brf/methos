/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


$(document).ready(function() {
    $("#password, #confirmpassword").on('keyup', function () {
        if($('#password').val().length!=0 || $('#confirmpassword').val().length!=0){
            if ($('#password').val() === $('#confirmpassword').val()) {
              $('#message').html('Matching').css('color', 'green');
            } else {
                $('#message').html('Not Matching').css('color', 'red');
            }
        }else{
            $('#message').hide();
        }
    });
    
    if($('#invalidtoken').val() === ''){
        $('#resetbutton').prop('disabled', true);
    }
});

var code;
function createCaptcha() {
    //clear the contents of captcha div first 
    document.getElementById('captcha').innerHTML = "";
    var charsArray =
    "0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ@!#$%^&*";
    var lengthOtp = 6;
    var captcha = [];
    for (var i = 0; i < lengthOtp; i++) {
      //below code will not allow Repetition of Characters
      var index = Math.floor(Math.random() * charsArray.length + 1); //get the next character from the array
      if (captcha.indexOf(charsArray[index]) == -1)
        captcha.push(charsArray[index]);
      else i--;
    }
    var canv = document.createElement("canvas");
    canv.id = "captcha";
    canv.width = 140;
    canv.height = 35;
   
    var ctx = canv.getContext("2d");
    ctx.font = "italic 25px Georgia";
    ctx.strokeStyle = "black";
    ctx.moveTo(5, canv.height/2);
    ctx.lineTo(canv.width-5, canv.height/2);
    ctx.stroke();
    ctx.textBaseline = "middle";

    ctx.strokeText(captcha.join(""), (canv.width - 100)/2, canv.height/2);
    //storing captcha so that can validate you can save it somewhere else according to your specific requirements
    code = captcha.join("");
    document.getElementById("captcha").appendChild(canv); // adds the canvas to the body element
}
    
function validateCaptcha() {
    if (document.getElementById("captchabox").value == code) {
        return (true);
    }else{
        document.getElementById('errorcap').innerHTML = "Invalid captcha!";
        
        return (false);
    }
}
function validatematchingpsw(){
    var text = document.getElementById("message").innerHTML;
    
    if(text == 'Not Matching'){
        return (false);
    }else{
        return (true);
    }
}

function validateform (){
   if(!validatematchingpsw() && validateCaptcha()){
       document.getElementById('errorcap').innerHTML = "";
       document.getElementById('captchabox').value = '';
       event.preventDefault();
       createCaptcha();
   }else if(!validatematchingpsw() && !validateCaptcha()){
       event.preventDefault();
       document.getElementById('errorcap').innerHTML = "Invalid captcha!";
       createCaptcha();
   }else if(validatematchingpsw() && !validateCaptcha()){
       event.preventDefault();
       document.getElementById('errorcap').innerHTML = "Invalid captcha!";
       createCaptcha();
   }
}