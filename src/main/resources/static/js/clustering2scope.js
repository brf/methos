/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


$(document).ready(function() {
    $('#protable tr:gt(0)').click(function() {
        ele = $(this).find('td input:checkbox')[0];
        ele.checked = ! ele.checked;
      });
      $('input:checkbox').click(function(e){
        e.stopPropagation();
      });
     
    var missval = document.getElementById("missval").value;
    var clusteringlevel = document.getElementById("clusteringlevel").value;
    if(clusteringlevel==="onexps" && missval==="omit"){
    
        var table = $('#protable').on( 'draw.dt', function () {
                $("#msgcontainer").attr("id","msgcontainerafter");
                $("#loadercontainer").css("display","none");
              } ).DataTable( {
            
            responsive: true,
            bLengthChange: false,
            pageLength: 10,
            "language": {
                "info": "Showing _START_ to _END_ of _TOTAL_ common metabolites that are present at least once in each of the selected experiments"
            }
        });
    }else if(clusteringlevel==="onexps" && missval!=="omit"){
        var table = $('#protable').on( 'draw.dt', function () {
                $("#msgcontainer").attr("id","msgcontainerafter");
                $("#loadercontainer").css("display","none");
              } ).DataTable( {
            responsive: true,
            bLengthChange: false,
            pageLength: 10,
            "language": {
                "info": "Showing _START_ to _END_ of _TOTAL_ metabolites of selected experiments"
            }
        });
    }else if(clusteringlevel==="onrepls" && missval==="omit"){
        var table = $('#protable').on( 'draw.dt', function () {
                $("#msgcontainer").attr("id","msgcontainerafter");
                $("#loadercontainer").css("display","none");
              } ).DataTable( {
            responsive: true,
            bLengthChange: false,
            pageLength: 10,
            "language": {
                "info": "Showing _START_ to _END_ of _TOTAL_ common metabolites that are present at least once in each replicate of the selected experiments"
            }
        });
    }else if(clusteringlevel==="onrepls" && missval!=="omit"){
        var table = $('#protable').on( 'draw.dt', function () {
                $("#msgcontainer").attr("id","msgcontainerafter");
                $("#loadercontainer").css("display","none");
              } ).DataTable( {
            responsive: true,
            bLengthChange: false,
            pageLength: 10,
            "language": {
                "info": "Showing _START_ to _END_ of _TOTAL_ metabolites of the selected experiments"
            }
        });
    }else if(clusteringlevel==="onmtbs" && missval==="omit"){
        var table = $('#protable').on( 'draw.dt', function () {
                $("#msgcontainer").attr("id","msgcontainerafter");
                $("#loadercontainer").css("display","none");
              } ).DataTable( {
            responsive: true,
            bLengthChange: false,
            pageLength: 10,
            "language": {
                "info": "Showing _START_ to _END_ of _TOTAL_ common metabolites of the selected experiments"
            }
        });
    }else if(clusteringlevel==="onmtbs" && missval!=="omit"){
        var table = $('#protable').on( 'draw.dt', function () {
                $("#msgcontainer").attr("id","msgcontainerafter");
                $("#loadercontainer").css("display","none");
              } ).DataTable( {
            responsive: true,
            bLengthChange: false,
            pageLength: 10,
            "language": {
                "info": "Showing _START_ to _END_ of _TOTAL_ metabolites of the selected experiments"
            }
        });
    }
    
    $('#myform').on('submit', function(){
        var $form = $(this);
        // Iterate over all checkboxes in the table
        table.$('input[type="checkbox"]').each(function(){
           // If checkbox doesn't exist in DOM
           if(!$.contains(document, this)){
              // If checkbox is checked
              if(this.checked){
                 // Create a hidden element 
                 $form.append(
                    $('<input>')
                       .attr('type', 'hidden')
                       .attr('name', this.name)
                       .val(this.value)
                 );
              }
           } 
        });
    });
    
    $('#selectall').change(function(){
        var rows = table.rows({ 'search': 'applied' }).nodes();
        $( rows ).find(':checkbox').prop('checked', $(this).is(':checked'));
    });
    
    
    
    $("#disbtn").click(function(){
        var boxes = table.cells( ).nodes();
        var chboxes = $( boxes ).find(':checkbox');
        var k = 0;
        for(var i = 0; i < chboxes.length; i++){
            if(chboxes[i].checked===true){
                k++;
            }
        }
        
        if(k<2){
            alert("Select at least two metabolites for analysis.");
            return (false);
        }else{
            $(document).on('click', ".update",function (){
                    $("#analysisspinner").show();
                    document.getElementById("disbtn").disabled=true;
                    $("#disbtn").removeClass("setbtn");
                    $("#myform").submit();
               });
            return (true);
        }
    });
    
} );