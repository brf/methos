/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


$(document).ready(function() {
    
    var table = $('#protable').on( 'draw.dt', function () {
                $("#msgcontainer").attr("id","msgcontainerafter");
                $("#loadercontainer").css("display","none");
              } )
        .DataTable( {
        responsive: true,
        bLengthChange: false,
        pageLength: 10,
        "columnDefs": [ {
            "targets": 0,
            "orderable": false
          } ],
        order: [[ 1, 'asc' ]],
        "language": {
            "info": "Showing _START_ to _END_ of _TOTAL_ experiments"
      }
    } );
    
    $('#protable tbody').on('click', 'tr', function () {
        var tr = $(this).closest('tr');
        var row = table.row( tr );

        if ( row.child.isShown() ) {
            row.child.hide();
            tr.removeClass('shown');
            $(this).closest('tr').find(".openpng").show();
            $(this).closest('tr').find(".closepng").hide();
        }
        else {
            $("#protable td[colspan=5]").remove();
            $("#protable td[colspan=4]").remove();
            $(".openpng").show();
            $(".closepng").hide();
            // Open this row
            row.child( format(row.data()) ).show();
            tr.addClass('shown');
            $(this).closest('tr').find(".openpng").hide();
            $(this).closest('tr').find(".closepng").show();
            
        }
    } );
    
} );

function format ( d ) {
    // `d` is the original data object for the row
    var i;
    var k;
    var list1='';
    var list2='';
    for(i=1;i<d[5].length-1;i++){
        list1 = list1 + d[5][i];
    }
    for(k=0;k<d[4].length;k++){
        list2 = list2 + d[4][k];
    }
    return '<table  cellpadding="0" cellspacing="0" border="0" class="yolo">'+
            '<tr>'+
                    '<td style="font-size:small;padding:0;"><b>Uploaded on:</b><br/><b>Replicates:</b> </td><td style="font-size:small;">'+ list2 +'<br/>'+ list1 +' </td>'+
                '</tr>'+
            '</table>';
}
