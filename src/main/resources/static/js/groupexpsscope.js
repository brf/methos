/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

$(document).ready(function() {
    document.getElementById("meanval").checked=true;
    document.getElementById("omit").checked=true;
    document.getElementById("normval").value="no";
    document.getElementById("ionboth").checked=true;
    
    var table=$('#protable').on( 'draw.dt', function () {
                $("#msgcontainer").attr("id","msgcontainerafter");
                $("#loadercontainer").css("display","none");
              } ).DataTable( {
        responsive: true,
        bLengthChange: false,
        pageLength: 10,
        "language": {
            "info": "Showing _START_ to _END_ of _TOTAL_ experiments"
      }
    } );
    
    $("#protable").on("click", "tr:gt(0) .gr1", function(e) {
        ele = $(this).find('input:checkbox')[0];
        ele.checked = ! ele.checked;
        if($(".group1").is(':checked')){
             $(this).closest("tr").find('.group2').prop('checked', false);
         }
        e.stopPropagation();
    });
    
    $("#protable").on("click", "tr:gt(0) .gr2", function(e) {
        ele = $(this).find('input:checkbox')[0];
        ele.checked = ! ele.checked;
        if($(".group2").is(':checked')){
             $(this).closest("tr").find('.group1').prop('checked', false);
        }
        e.stopPropagation();
    });
 
    $("#protable").on("click","tr:gt(0) td .group1", function(e) {
        if($(this).is(':checked')){
             $(this).closest("tr").find('.group2').prop('checked', false);
         }
         e.stopPropagation();
    });
    
    $("#protable").on("click","tr:gt(0) td .group2", function(e) {
        if($(this).is(':checked')){
             $(this).closest("tr").find('.group1').prop('checked', false);
         }
         e.stopPropagation();
    });
      
      
      
    $('#selectall1').change(function(){
        
        var rows = table.rows({ 'search': 'applied' }).nodes();
        $( rows ).find('input[name=experimentidgroup1]:checkbox').prop('checked', $(this).is(':checked'));
        if($(this).is(':checked')){
            $( rows ).find('input[name=experimentidgroup2]:checkbox').prop('checked', false);
            //$("#selectall2").prop('checked',false);
        }
    });
    $('#selectall2').change(function(){
        
        var rows = table.rows({ 'search': 'applied' }).nodes();
        $( rows ).find('input[name=experimentidgroup2]:checkbox').prop('checked', $(this).is(':checked'));
        if($(this).is(':checked')){
            $( rows ).find('input[name=experimentidgroup1]:checkbox').prop('checked', false);
        }
    });
    
    $('#myform').on('submit', function(){
        var $form = $(this);
        // Iterate over all checkboxes in the table
        table.$('input[type="checkbox"]').each(function(){
           // If checkbox doesn't exist in DOM
           if(!$.contains(document, this)){
              // If checkbox is checked
              if(this.checked){
                 // Create a hidden element 
                 $form.append(
                    $('<input>')
                       .attr('type', 'hidden')
                       .attr('name', this.name)
                       .val(this.value)
                 );
              }
           } 
        });
    });
        
    
    $("#disbtn").click(function(){
        var boxes = table.cells( ).nodes();
        
        var chkboxes1=$( boxes ).find('input[name=experimentidgroup1]:checkbox');
        var nrgroup1 = 0;
        for(var i = 0; i < chkboxes1.length; i++){
            if(chkboxes1[i].checked===true){
                nrgroup1++;
            }
        }
        var chkboxes2=$( boxes ).find('input[name=experimentidgroup2]:checkbox');
        var nrgroup2 = 0;        
        for(var i = 0; i < chkboxes2.length; i++){
            if(chkboxes2[i].checked===true){
                nrgroup2++;
            }
        }

        if(nrgroup1<1 || nrgroup2<1 || nrgroup1+nrgroup2<3){
            alert("Please select at least three experiments in total. Every group must be assigned with experiments.");
            return (false);
        }else{
            $(document).on('click', ".update",function (){
                    $('#analysisspinner').show();
                    document.getElementById("disbtn").disabled=true;
                    $("#disbtn").removeClass("setbtn");
                    $("#myform").submit();
                });
            return (true);
        }
        
    });
    
    
    $('#normalizationgroup').change(function () {
        if ($("#normalizationgroup").prop("checked")) { // checked
            $("#normalizationexp").prop("checked",false);
            document.getElementById("normval").value="normvalgroup";
        }else{ // not checked
            $("#normalizationgroup").prop("checked",false);
            document.getElementById("normval").value="no";
        }
    });
    $('#normalizationexp').change(function () {
        if ($("#normalizationexp").prop("checked")) { // checked
            $("#normalizationgroup").prop("checked",false);
            document.getElementById("normval").value="normvalexp";
        }else{ // not checked
            $("#normalizationexp").prop("checked",false);
            document.getElementById("normval").value="no";
        }
    });
    
} );