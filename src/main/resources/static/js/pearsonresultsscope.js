/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

$(document).ready(function() {

    var tablemissexp = $('#protablemissexp').on( 'draw.dt', function () {
                $("#msgcontainer1").attr("id","msgcontainerafter1");
                $("#loadercontainer1").css("display","none");
              } ).DataTable( {
        responsive: true,
        bLengthChange: false,
        columnDefs: [ {
            orderable: false,
            targets:   0
        } ],
        pageLength: 10,
        "language": {
            "info": "Showing _START_ to _END_ of _TOTAL_ metabolites"
        }
        
    });
    
    var tablemissrepl = $('#protablemissrepl').on( 'draw.dt', function () {
                $("#msgcontainer2").attr("id","msgcontainerafter2");
                $("#loadercontainer2").css("display","none");
              } ).DataTable( {
        responsive: true,
        bLengthChange: false,
        columnDefs: [ {
            orderable: false,
            targets:   0
        } ],
        pageLength: 10,
        "language": {
            "info": "Showing _START_ to _END_ of _TOTAL_ metabolites"
        }
        
    });
    
    var allorsome =document.getElementById("allorsome");
    if(allorsome.value==="onallexprepl"){
        //1st heatmap
        var expsonall =document.getElementById("expsonall");
        if(expsonall !== null){
        var expsonallsize = expsonall.rows.length;
        var expelem =[];
        var l=0;var k=0;
        var dataexps ={"exps":[]};
        for(l=1;l<expsonallsize;l++){
            expelem.push(expsonall.rows[0].cells[l].innerHTML);
            for(k=1;k<expsonallsize;k++){
                dataexps["exps"].push({"x_exps":expsonall.rows[0].cells[l].innerHTML,"y_exps":expsonall.rows[0].cells[k].innerHTML,"corrvalexps":expsonall.rows[l].cells[k].innerHTML});
            }
        }
        var newdata1=dataexps["exps"];
        var margin1 = {top:30,right:30,bottom:20,left:20};
        var width1 = 720-margin1.right-margin1.left;
        var height1=720-margin1.top-margin1.bottom;
        var color=['#4575b4','#4776b5','#4a78b6','#4c79b6','#4e7ab7','#517bb8','#537db9','#557eb9','#577fba','#5980bb',
                   '#5b82bc','#5e83bc','#6084bd','#6286be','#6487bf','#6688bf','#688ac0','#6a8bc1','#6c8cc2','#6e8ec2',
                   '#708fc3','#7290c4','#7491c5','#7593c5','#7794c6','#7995c7','#7b97c8','#7d98c8','#7f99c9','#819bca',
                   '#839ccb','#859ecb','#869fcc','#88a0cd','#8aa2ce','#8ca3ce','#8ea4cf','#90a6d0','#91a7d1','#93a8d1',
                   '#95aad2','#97abd3','#99add4','#9baed4','#9cafd5','#9eb1d6','#a0b2d7','#a2b3d7','#a4b5d8','#a5b6d9',
                   '#a7b8da','#a9b9da','#abbadb','#adbcdc','#aebddd','#b0bfde','#b2c0de','#b4c1df','#b5c3e0','#b7c4e1',
                   '#b9c6e1','#bbc7e2','#bdc8e3','#becae4','#c0cbe4','#c2cde5','#c4cee6','#c5d0e7','#c7d1e7','#c9d2e8',
                   '#cbd4e9','#cdd5ea','#ced7ea','#d0d8eb','#d2daec','#d4dbed','#d5dced','#d7deee','#d9dfef','#dbe1f0',
                   '#dce2f0','#dee4f1','#e0e5f2','#e2e7f3','#e4e8f3','#e5eaf4','#e7ebf5','#e9edf6','#ebeef6','#eceff7',
                   '#eef1f8','#f0f2f9','#f2f4f9','#f3f5fa','#f5f7fb','#f7f8fc','#f9fafc','#fbfbfd','#fcfdfe','#fefeff',
                   '#fffefe','#fffcfb','#fffaf9','#fff8f7','#fff6f4','#fff4f2','#fff2f0','#fff0ed','#ffeeeb','#ffede8',
                   '#ffebe6','#ffe9e4','#ffe7e1','#ffe5df','#ffe3dd','#ffe1da','#ffdfd8','#ffddd6','#ffdbd3','#ffd9d1',
                   '#ffd7cf','#ffd5cd','#ffd3ca','#ffd1c8','#ffd0c6','#ffcec3','#ffccc1','#ffcabf','#ffc8bd','#ffc6ba',
                   '#ffc4b8','#fec2b6','#fec0b3','#febeb1','#febcaf','#fdbaad','#fdb8ab','#fdb6a8','#fdb4a6','#fcb3a4',
                   '#fcb1a2','#fcaf9f','#fbad9d','#fbab9b','#faa999','#faa797','#faa594','#f9a392','#f9a190','#f89f8e',
                   '#f89d8c','#f89b8a','#f79987','#f79785','#f69583','#f69381','#f5917f','#f58f7d','#f48d7a','#f48b78',
                   '#f38976','#f38774','#f28572','#f18370','#f1816e','#f07f6c','#f07d6a','#ef7b68','#ee7965','#ee7763',
                   '#ed7561','#ed735f','#ec715d','#eb6f5b','#eb6d59','#ea6b57','#e96955','#e86753','#e86451','#e7624f',
                   '#e6604d','#e65e4b','#e55c49','#e45947','#e35745','#e35543','#e25341','#e1503f','#e04e3d','#df4b3b',
                   '#df4939','#de4637','#dd4435','#dc4133','#db3f31','#da3c2f','#da392d','#d9362b','#d83329','#d73027'];

        var canvas1 = d3.select(".expsheatmap1")
            .attr("height", height1 + margin1.top + margin1.bottom)
            .attr("width", width1 + margin1.left + margin1.right+60);

        var group1 = canvas1.append("g")
                .attr("transform",function(){return "translate("+(margin1.right)+","+(margin1.top)+")";});
        var div1 = d3.select('.tooltip1');
        var xScale1 = d3.scaleBand().domain(expelem).range([0,width1]);
        var yScale1 = d3.scaleBand().domain(expelem).range([0,height1]);
        var colorScale = d3.scaleQuantize().domain([-1,1]).range(color);

        var barWidth1 = width1/(newdata1.length/expelem.length);
        var barHeight1 = height1/expelem.length;

        var xAxis1 = d3.axisTop(xScale1).tickSize(0).tickValues("");
        var yAxis1 = d3.axisLeft(yScale1).tickSize(0).tickValues("");
        group1.selectAll('g').data(newdata1).enter().append("g")
                .attr("transform", function(newdata1){
                            return "translate("+xScale1(newdata1.x_exps)+","+yScale1(newdata1.y_exps)+")";})
                .append("rect")
                .attr("width", barWidth1)
                .attr("height", barHeight1)
                .style("fill",function(newdata1){return colorScale(newdata1.corrvalexps);})
                        .on("mouseover",function(d1){
                            //make the tooltip to follow the mouse
                            div1.transition().duration(10)
                                    .style("opacity",0.8)
                                    .style("left",d3.event.pageX - 350 + "px")
                                    .style("top", d3.event.pageY - 250 + "px");
                            div1.html("<p>x: " + d1.x_exps + "</br>y: " + d1.y_exps + "</br>Value= " + parseFloat(d1.corrvalexps).toFixed(2) + "</p>" );
            }).on("mouseout",function(d1){
                div1.transition().duration(100).style("opacity",0);
            });
        group1.append("g")
                .attr("class", "xAxis1")
                .attr("transform","translate(0,0)")
                .call(xAxis1)
        .selectAll("text")
            .attr("transform", "rotate(-45)")
            .style("text-anchor", "start");

        group1.append("g")
            .attr("class","yAxis1")
            .attr("transform", "translate(0,0)")
            .call(yAxis1)
        .selectAll("text")
            .attr("transform", "rotate(45)")
            .style("text-anchor", "end");


        var countScale1 = d3.scaleLinear()
                .domain([-1,1])
                .range([0,width1]);

        //Calculate the variables for the temp gradient
        var numStops1 = 5;
        var countRange1 = countScale1.domain();
        countRange1[2] = countRange1[1] - countRange1[0];
        var countPoint1 = [];
        for(var k = 0; k < numStops1; k++) {
                countPoint1.push(k * countRange1[2]/(numStops1-1) + countRange1[0]);
        }
            //Create the gradient
        group1.append("defs")
            .append("linearGradient")
            .attr("id", "legend-pearson")
            .attr("x1", "0%").attr("y1", "100%")
            .attr("x2", "0%").attr("y2", "0%")
            .selectAll("stop") 
            .data(d3.range(numStops1))                
            .enter().append("stop")
            .attr("offset", function(d,i) { 
                    return countScale1( countPoint1[i] )/width1;
            })   
            .attr("stop-color", function(d,i) { 
                    return colorScale( countPoint1[i] ); 
            });

        var legendHeight1 = height1/3;
        //Color Legend container
        var legendgroup1 =group1.append("g")
                .attr("class", "legendWrapper")
                .attr("transform", "translate(" + (width1) + "," + (0) + ")");

        //Draw the Rectangle
        legendgroup1.append("rect")
                .attr("class", "legendRect")
                .attr("x", 30)
                .attr("y", 0)
                .attr("width", 15)
                .attr("height", legendHeight1)
                .style("fill", "url(#legend-pearson)");



        //Set scale for x-axis
        var xScaleleg1 = d3.scaleLinear()
                 .range([-legendHeight1/2, (legendHeight1/2)-1])
                 .domain([1,-1] );

        //Define x-axis
        var xAxisleg1 = d3.axisRight(xScaleleg1)
                  .ticks(5)
                  .scale(xScaleleg1);

        //Set up X axis
        legendgroup1.append("g")
                .attr("class", "axis")
                .attr("transform", "translate("+(45) +","+(legendHeight1/2)+")")
                .call(xAxisleg1);
        }
        //2nd heatmap
        var replonall =document.getElementById("replonall");
        if(replonall !== null){
        var replonallsize = replonall.rows.length;
        var replelem =[];
        var i=0;var p=0;
        var datarepls = {"repls":[]};
        for(i=1;i<replonallsize;i++){
           
            replelem.push(replonall.rows[0].cells[i].innerHTML);
            for(p=1;p<replonallsize;p++){
                datarepls["repls"].push({"x_repls":replonall.rows[0].cells[i].innerHTML,"y_repls":replonall.rows[0].cells[p].innerHTML,"corrvalrepls":replonall.rows[i].cells[p].innerHTML});
            }
        }
        var newdata2=datarepls["repls"];
        var margin2 = {top:30,right:30,bottom:20,left:20};
        var width2 = 720-margin2.right-margin2.left;
        var height2=720-margin2.top-margin2.bottom;
        var color2=['#4575b4','#4776b5','#4a78b6','#4c79b6','#4e7ab7','#517bb8','#537db9','#557eb9','#577fba','#5980bb',
                   '#5b82bc','#5e83bc','#6084bd','#6286be','#6487bf','#6688bf','#688ac0','#6a8bc1','#6c8cc2','#6e8ec2',
                   '#708fc3','#7290c4','#7491c5','#7593c5','#7794c6','#7995c7','#7b97c8','#7d98c8','#7f99c9','#819bca',
                   '#839ccb','#859ecb','#869fcc','#88a0cd','#8aa2ce','#8ca3ce','#8ea4cf','#90a6d0','#91a7d1','#93a8d1',
                   '#95aad2','#97abd3','#99add4','#9baed4','#9cafd5','#9eb1d6','#a0b2d7','#a2b3d7','#a4b5d8','#a5b6d9',
                   '#a7b8da','#a9b9da','#abbadb','#adbcdc','#aebddd','#b0bfde','#b2c0de','#b4c1df','#b5c3e0','#b7c4e1',
                   '#b9c6e1','#bbc7e2','#bdc8e3','#becae4','#c0cbe4','#c2cde5','#c4cee6','#c5d0e7','#c7d1e7','#c9d2e8',
                   '#cbd4e9','#cdd5ea','#ced7ea','#d0d8eb','#d2daec','#d4dbed','#d5dced','#d7deee','#d9dfef','#dbe1f0',
                   '#dce2f0','#dee4f1','#e0e5f2','#e2e7f3','#e4e8f3','#e5eaf4','#e7ebf5','#e9edf6','#ebeef6','#eceff7',
                   '#eef1f8','#f0f2f9','#f2f4f9','#f3f5fa','#f5f7fb','#f7f8fc','#f9fafc','#fbfbfd','#fcfdfe','#fefeff',
                   '#fffefe','#fffcfb','#fffaf9','#fff8f7','#fff6f4','#fff4f2','#fff2f0','#fff0ed','#ffeeeb','#ffede8',
                   '#ffebe6','#ffe9e4','#ffe7e1','#ffe5df','#ffe3dd','#ffe1da','#ffdfd8','#ffddd6','#ffdbd3','#ffd9d1',
                   '#ffd7cf','#ffd5cd','#ffd3ca','#ffd1c8','#ffd0c6','#ffcec3','#ffccc1','#ffcabf','#ffc8bd','#ffc6ba',
                   '#ffc4b8','#fec2b6','#fec0b3','#febeb1','#febcaf','#fdbaad','#fdb8ab','#fdb6a8','#fdb4a6','#fcb3a4',
                   '#fcb1a2','#fcaf9f','#fbad9d','#fbab9b','#faa999','#faa797','#faa594','#f9a392','#f9a190','#f89f8e',
                   '#f89d8c','#f89b8a','#f79987','#f79785','#f69583','#f69381','#f5917f','#f58f7d','#f48d7a','#f48b78',
                   '#f38976','#f38774','#f28572','#f18370','#f1816e','#f07f6c','#f07d6a','#ef7b68','#ee7965','#ee7763',
                   '#ed7561','#ed735f','#ec715d','#eb6f5b','#eb6d59','#ea6b57','#e96955','#e86753','#e86451','#e7624f',
                   '#e6604d','#e65e4b','#e55c49','#e45947','#e35745','#e35543','#e25341','#e1503f','#e04e3d','#df4b3b',
                   '#df4939','#de4637','#dd4435','#dc4133','#db3f31','#da3c2f','#da392d','#d9362b','#d83329','#d73027'];

        var canvas2 = d3.select(".replsheatmap2")
            .attr("height", height2 + margin2.top + margin2.bottom)
            .attr("width", width2 + margin2.left + margin2.right+60);

        var group2 = canvas2.append("g")
                .attr("transform",function(){return "translate("+(margin2.right)+","+(margin2.top)+")";});
        var div2 = d3.select('.tooltip2');
        var xScale2 = d3.scaleBand().domain(replelem).range([0,width2]);
        var yScale2 = d3.scaleBand().domain(replelem).range([0,height2]);
        var colorScale2 = d3.scaleQuantize().domain([-1,1]).range(color2);

        var barWidth2 = width2/(newdata2.length/replelem.length);
        var barHeight2 = height2/replelem.length;

        var xAxis2 = d3.axisTop(xScale2).tickSize(0).tickValues("");
        var yAxis2 = d3.axisLeft(yScale2).tickSize(0).tickValues("");
        group2.selectAll('g').data(newdata2).enter().append("g")
                .attr("transform", function(newdata2){
                            return "translate("+xScale2(newdata2.x_repls)+","+yScale2(newdata2.y_repls)+")";})
                .append("rect")
                .attr("width", barWidth2)
                .attr("height", barHeight2)
                .style("fill",function(newdata2){return colorScale2(newdata2.corrvalrepls);})
                        .on("mouseover",function(d2){
                            var x1=d2.x_repls.split("__")[0];
                            var x2=d2.x_repls.split("__")[1];
                            var y1=d2.y_repls.split("__")[0];
                            var y2=d2.y_repls.split("__")[1];
                            //make the tooltip to follow the mouse
                            div2.transition().duration(10)
                                    .style("opacity",0.8)
                                    .style("left",d3.event.pageX - 350 + "px")//+700
                                    .style("top", d3.event.pageY -1030  + "px");//+600
                            div2.html("<p>x: " + x1 +"->"+x2 +"</br>y: " + y1+"->" +y2+ "</br>Value= " + parseFloat(d2.corrvalrepls).toFixed(2) + "</p>" );
            }).on("mouseout",function(d2){
                div2.transition().duration(100).style("opacity",0);
            });
        group2.append("g")
                .attr("class", "xAxis1")
                .attr("transform","translate(0,0)")
                .call(xAxis2)
        .selectAll("text")
            .attr("transform", "rotate(-45)")
            .style("text-anchor", "start");

        group2.append("g")
            .attr("class","yAxis1")
            .attr("transform", "translate(0,0)")
            .call(yAxis2)
        .selectAll("text")
            .attr("transform", "rotate(45)")
            .style("text-anchor", "end");


        var countScale2 = d3.scaleLinear()
                .domain([-1,1])
                .range([0,width2]);

        //Calculate the variables for the temp gradient
        var numStops2 = 5;
        var countRange2 = countScale2.domain();
        countRange2[2] = countRange2[1] - countRange2[0];
        var countPoint2 = [];
        for(var k = 0; k < numStops2; k++) {
                countPoint2.push(k * countRange2[2]/(numStops2-1) + countRange2[0]);
        }
            //Create the gradient
        group2.append("defs")
            .append("linearGradient")
            .attr("id", "legend-pearson")
            .attr("x1", "0%").attr("y1", "100%")
            .attr("x2", "0%").attr("y2", "0%")
            .selectAll("stop") 
            .data(d3.range(numStops2))                
            .enter().append("stop")
            .attr("offset", function(d,i) { 
                    return countScale2( countPoint2[i] )/width2;
            })   
            .attr("stop-color", function(d,i) { 
                    return colorScale2( countPoint2[i] ); 
            });

        var legendHeight2 = height2/3;
        //Color Legend container
        var legendgroup2 =group2.append("g")
                .attr("class", "legendWrapper")
                .attr("transform", "translate(" + (width2) + "," + (0) + ")");

        //Draw the Rectangle
        legendgroup2.append("rect")
                .attr("class", "legendRect")
                .attr("x", 30)
                .attr("y", 0)
                .attr("width", 15)
                .attr("height", legendHeight2)
                .style("fill", "url(#legend-pearson)");

        //Set scale for x-axis
        var xScaleleg2 = d3.scaleLinear()
                 .range([-legendHeight2/2, (legendHeight2/2)-1])
                 .domain([1,-1] );

        //Define x-axis
        var xAxisleg2 = d3.axisRight(xScaleleg2)
                  .ticks(5)
                  .scale(xScaleleg2);

        //Set up X axis
        legendgroup2.append("g")
                .attr("class", "axis")
                .attr("transform", "translate("+(45) +","+(legendHeight2/2)+")")
                .call(xAxisleg2);
    }
        
    }else if(allorsome.value==="onselectedexp"){
        var expsonselected =document.getElementById("expsonselected");
        var expsonselectedsize = expsonselected.rows.length;
        var expelem =[];
        var l=0;var k=0;
        var dataexps ={"exps":[]};
        for(l=1;l<expsonselectedsize;l++){
            expelem.push(expsonselected.rows[0].cells[l].innerHTML);
            for(k=1;k<expsonselectedsize;k++){
                dataexps["exps"].push({"x_exps":expsonselected.rows[0].cells[l].innerHTML,"y_exps":expsonselected.rows[0].cells[k].innerHTML,"corrvalexps":expsonselected.rows[l].cells[k].innerHTML});
            }
        }
        var newdata1=dataexps["exps"];
        var margin1 = {top:30,right:30,bottom:20,left:20};
        var width1 = 720-margin1.right-margin1.left;
        var height1=720-margin1.top-margin1.bottom;
        var color=['#4575b4','#4776b5','#4a78b6','#4c79b6','#4e7ab7','#517bb8','#537db9','#557eb9','#577fba','#5980bb',
                   '#5b82bc','#5e83bc','#6084bd','#6286be','#6487bf','#6688bf','#688ac0','#6a8bc1','#6c8cc2','#6e8ec2',
                   '#708fc3','#7290c4','#7491c5','#7593c5','#7794c6','#7995c7','#7b97c8','#7d98c8','#7f99c9','#819bca',
                   '#839ccb','#859ecb','#869fcc','#88a0cd','#8aa2ce','#8ca3ce','#8ea4cf','#90a6d0','#91a7d1','#93a8d1',
                   '#95aad2','#97abd3','#99add4','#9baed4','#9cafd5','#9eb1d6','#a0b2d7','#a2b3d7','#a4b5d8','#a5b6d9',
                   '#a7b8da','#a9b9da','#abbadb','#adbcdc','#aebddd','#b0bfde','#b2c0de','#b4c1df','#b5c3e0','#b7c4e1',
                   '#b9c6e1','#bbc7e2','#bdc8e3','#becae4','#c0cbe4','#c2cde5','#c4cee6','#c5d0e7','#c7d1e7','#c9d2e8',
                   '#cbd4e9','#cdd5ea','#ced7ea','#d0d8eb','#d2daec','#d4dbed','#d5dced','#d7deee','#d9dfef','#dbe1f0',
                   '#dce2f0','#dee4f1','#e0e5f2','#e2e7f3','#e4e8f3','#e5eaf4','#e7ebf5','#e9edf6','#ebeef6','#eceff7',
                   '#eef1f8','#f0f2f9','#f2f4f9','#f3f5fa','#f5f7fb','#f7f8fc','#f9fafc','#fbfbfd','#fcfdfe','#fefeff',
                   '#fffefe','#fffcfb','#fffaf9','#fff8f7','#fff6f4','#fff4f2','#fff2f0','#fff0ed','#ffeeeb','#ffede8',
                   '#ffebe6','#ffe9e4','#ffe7e1','#ffe5df','#ffe3dd','#ffe1da','#ffdfd8','#ffddd6','#ffdbd3','#ffd9d1',
                   '#ffd7cf','#ffd5cd','#ffd3ca','#ffd1c8','#ffd0c6','#ffcec3','#ffccc1','#ffcabf','#ffc8bd','#ffc6ba',
                   '#ffc4b8','#fec2b6','#fec0b3','#febeb1','#febcaf','#fdbaad','#fdb8ab','#fdb6a8','#fdb4a6','#fcb3a4',
                   '#fcb1a2','#fcaf9f','#fbad9d','#fbab9b','#faa999','#faa797','#faa594','#f9a392','#f9a190','#f89f8e',
                   '#f89d8c','#f89b8a','#f79987','#f79785','#f69583','#f69381','#f5917f','#f58f7d','#f48d7a','#f48b78',
                   '#f38976','#f38774','#f28572','#f18370','#f1816e','#f07f6c','#f07d6a','#ef7b68','#ee7965','#ee7763',
                   '#ed7561','#ed735f','#ec715d','#eb6f5b','#eb6d59','#ea6b57','#e96955','#e86753','#e86451','#e7624f',
                   '#e6604d','#e65e4b','#e55c49','#e45947','#e35745','#e35543','#e25341','#e1503f','#e04e3d','#df4b3b',
                   '#df4939','#de4637','#dd4435','#dc4133','#db3f31','#da3c2f','#da392d','#d9362b','#d83329','#d73027'];

        var canvas1 = d3.select(".expsheatmap3")
            .attr("height", height1 + margin1.top + margin1.bottom)
            .attr("width", width1 + margin1.left + margin1.right+60);

        var group1 = canvas1.append("g")
                .attr("transform",function(){return "translate("+(margin1.right)+","+(margin1.top)+")";});
        var div1 = d3.select('.tooltip3');
        var xScale1 = d3.scaleBand().domain(expelem).range([0,width1]);
        var yScale1 = d3.scaleBand().domain(expelem).range([0,height1]);
        var colorScale = d3.scaleQuantize().domain([-1,1]).range(color);

        var barWidth1 = width1/(newdata1.length/expelem.length);
        var barHeight1 = height1/expelem.length;

        var xAxis1 = d3.axisTop(xScale1).tickSize(0).tickValues("");
        var yAxis1 = d3.axisLeft(yScale1).tickSize(0).tickValues("");
        group1.selectAll('g').data(newdata1).enter().append("g")
                .attr("transform", function(newdata1){
                            return "translate("+xScale1(newdata1.x_exps)+","+yScale1(newdata1.y_exps)+")";})
                .append("rect")
                .attr("width", barWidth1)
                .attr("height", barHeight1)
                .style("fill",function(newdata1){return colorScale(newdata1.corrvalexps);})
                        .on("mouseover",function(d1){
                            //make the tooltip to follow the mouse
                            div1.transition().duration(10)
                                    .style("opacity",0.8)
                                    .style("left",d3.event.pageX - 350 + "px")
                                    .style("top", d3.event.pageY - 250 + "px");
                            div1.html("<p>x: " + d1.x_exps + "</br>y: " + d1.y_exps + "</br>Value= " + parseFloat(d1.corrvalexps).toFixed(2) + "</p>" );
            }).on("mouseout",function(d1){
                div1.transition().duration(100).style("opacity",0);
            });
        group1.append("g")
                .attr("class", "xAxis1")
                .attr("transform","translate(0,0)")
                .call(xAxis1)
        .selectAll("text")
            .attr("transform", "rotate(-45)")
            .style("text-anchor", "start");

        group1.append("g")
            .attr("class","yAxis1")
            .attr("transform", "translate(0,0)")
            .call(yAxis1)
        .selectAll("text")
            .attr("transform", "rotate(45)")
            .style("text-anchor", "end");


        var countScale1 = d3.scaleLinear()
                .domain([-1,1])
                .range([0,width1]);

        //Calculate the variables for the temp gradient
        var numStops1 = 5;
        var countRange1 = countScale1.domain();
        countRange1[2] = countRange1[1] - countRange1[0];
        var countPoint1 = [];
        for(var k = 0; k < numStops1; k++) {
                countPoint1.push(k * countRange1[2]/(numStops1-1) + countRange1[0]);
        }
            //Create the gradient
        group1.append("defs")
            .append("linearGradient")
            .attr("id", "legend-pearson")
            .attr("x1", "0%").attr("y1", "100%")
            .attr("x2", "0%").attr("y2", "0%")
            .selectAll("stop") 
            .data(d3.range(numStops1))                
            .enter().append("stop")
            .attr("offset", function(d,i) { 
                    return countScale1( countPoint1[i] )/width1;
            })   
            .attr("stop-color", function(d,i) { 
                    return colorScale( countPoint1[i] ); 
            });

        var legendHeight1 = height1/3;
        //Color Legend container
        var legendgroup1 =group1.append("g")
                .attr("class", "legendWrapper")
                .attr("transform", "translate(" + (width1) + "," + (0) + ")");

        //Draw the Rectangle
        legendgroup1.append("rect")
                .attr("class", "legendRect")
                .attr("x", 30)
                .attr("y", 0)
                .attr("width", 15)
                .attr("height", legendHeight1)
                .style("fill", "url(#legend-pearson)");



        //Set scale for x-axis
        var xScaleleg1 = d3.scaleLinear()
                 .range([-legendHeight1/2, (legendHeight1/2)-1])
                 .domain([1,-1] );

        //Define x-axis
        var xAxisleg1 = d3.axisRight(xScaleleg1)
                  .ticks(5)
                  .scale(xScaleleg1);

        //Set up X axis
        legendgroup1.append("g")
                .attr("class", "axis")
                .attr("transform", "translate("+(45) +","+(legendHeight1/2)+")")
                .call(xAxisleg1);
    }else if(allorsome.value==="onselectedrepl"){
        var replonselected =document.getElementById("replonselected");
        var replonselectedsize = replonselected.rows.length;
        var replelem =[];
        var i=0;var p=0;
        var datarepls = {"repls":[]};
        for(i=1;i<replonselectedsize;i++){
            replelem.push(replonselected.rows[0].cells[i].innerHTML);
            for(p=1;p<replonselectedsize;p++){
                datarepls["repls"].push({"x_repls":replonselected.rows[0].cells[i].innerHTML,"y_repls":replonselected.rows[0].cells[p].innerHTML,"corrvalrepls":replonselected.rows[i].cells[p].innerHTML});
            }
        }
        var newdata2=datarepls["repls"];
        var margin2 = {top:30,right:30,bottom:20,left:20};
        var width2 = 720-margin2.right-margin2.left;
        var height2=720-margin2.top-margin2.bottom;
        var color2=['#4575b4','#4776b5','#4a78b6','#4c79b6','#4e7ab7','#517bb8','#537db9','#557eb9','#577fba','#5980bb',
                   '#5b82bc','#5e83bc','#6084bd','#6286be','#6487bf','#6688bf','#688ac0','#6a8bc1','#6c8cc2','#6e8ec2',
                   '#708fc3','#7290c4','#7491c5','#7593c5','#7794c6','#7995c7','#7b97c8','#7d98c8','#7f99c9','#819bca',
                   '#839ccb','#859ecb','#869fcc','#88a0cd','#8aa2ce','#8ca3ce','#8ea4cf','#90a6d0','#91a7d1','#93a8d1',
                   '#95aad2','#97abd3','#99add4','#9baed4','#9cafd5','#9eb1d6','#a0b2d7','#a2b3d7','#a4b5d8','#a5b6d9',
                   '#a7b8da','#a9b9da','#abbadb','#adbcdc','#aebddd','#b0bfde','#b2c0de','#b4c1df','#b5c3e0','#b7c4e1',
                   '#b9c6e1','#bbc7e2','#bdc8e3','#becae4','#c0cbe4','#c2cde5','#c4cee6','#c5d0e7','#c7d1e7','#c9d2e8',
                   '#cbd4e9','#cdd5ea','#ced7ea','#d0d8eb','#d2daec','#d4dbed','#d5dced','#d7deee','#d9dfef','#dbe1f0',
                   '#dce2f0','#dee4f1','#e0e5f2','#e2e7f3','#e4e8f3','#e5eaf4','#e7ebf5','#e9edf6','#ebeef6','#eceff7',
                   '#eef1f8','#f0f2f9','#f2f4f9','#f3f5fa','#f5f7fb','#f7f8fc','#f9fafc','#fbfbfd','#fcfdfe','#fefeff',
                   '#fffefe','#fffcfb','#fffaf9','#fff8f7','#fff6f4','#fff4f2','#fff2f0','#fff0ed','#ffeeeb','#ffede8',
                   '#ffebe6','#ffe9e4','#ffe7e1','#ffe5df','#ffe3dd','#ffe1da','#ffdfd8','#ffddd6','#ffdbd3','#ffd9d1',
                   '#ffd7cf','#ffd5cd','#ffd3ca','#ffd1c8','#ffd0c6','#ffcec3','#ffccc1','#ffcabf','#ffc8bd','#ffc6ba',
                   '#ffc4b8','#fec2b6','#fec0b3','#febeb1','#febcaf','#fdbaad','#fdb8ab','#fdb6a8','#fdb4a6','#fcb3a4',
                   '#fcb1a2','#fcaf9f','#fbad9d','#fbab9b','#faa999','#faa797','#faa594','#f9a392','#f9a190','#f89f8e',
                   '#f89d8c','#f89b8a','#f79987','#f79785','#f69583','#f69381','#f5917f','#f58f7d','#f48d7a','#f48b78',
                   '#f38976','#f38774','#f28572','#f18370','#f1816e','#f07f6c','#f07d6a','#ef7b68','#ee7965','#ee7763',
                   '#ed7561','#ed735f','#ec715d','#eb6f5b','#eb6d59','#ea6b57','#e96955','#e86753','#e86451','#e7624f',
                   '#e6604d','#e65e4b','#e55c49','#e45947','#e35745','#e35543','#e25341','#e1503f','#e04e3d','#df4b3b',
                   '#df4939','#de4637','#dd4435','#dc4133','#db3f31','#da3c2f','#da392d','#d9362b','#d83329','#d73027'];

        var canvas2 = d3.select(".replsheatmap4")
            .attr("height", height2 + margin2.top + margin2.bottom)
            .attr("width", width2 + margin2.left + margin2.right+60);

        var group2 = canvas2.append("g")
                .attr("transform",function(){return "translate("+(margin2.right)+","+(margin2.top)+")";});
        var div2 = d3.select('.tooltip4');
        var xScale2 = d3.scaleBand().domain(replelem).range([0,width2]);
        var yScale2 = d3.scaleBand().domain(replelem).range([0,height2]);
        var colorScale2 = d3.scaleQuantize().domain([-1,1]).range(color2);

        var barWidth2 = width2/(newdata2.length/replelem.length);
        var barHeight2 = height2/replelem.length;

        var xAxis2 = d3.axisTop(xScale2).tickSize(0).tickValues("");
        var yAxis2 = d3.axisLeft(yScale2).tickSize(0).tickValues("");
        group2.selectAll('g').data(newdata2).enter().append("g")
                .attr("transform", function(newdata2){
                            return "translate("+xScale2(newdata2.x_repls)+","+yScale2(newdata2.y_repls)+")";})
                .append("rect")
                .attr("width", barWidth2)
                .attr("height", barHeight2)
                .style("fill",function(newdata2){return colorScale2(newdata2.corrvalrepls);})
                        .on("mouseover",function(d2){
                            var x1=d2.x_repls.split("__")[0];
                            var x2=d2.x_repls.split("__")[1];
                            var y1=d2.y_repls.split("__")[0];
                            var y2=d2.y_repls.split("__")[1];
                            //make the tooltip to follow the mouse
                            div2.transition().duration(10)
                                    .style("opacity",0.8)
                                    .style("left",d3.event.pageX -350 + "px")
                                    .style("top", d3.event.pageY-250  + "px");
                            div2.html("<p>x: " + x1 +"->"+x2 +"</br>y: " + y1+"->" +y2+ "</br>Value= " + parseFloat(d2.corrvalrepls).toFixed(2) + "</p>" );
            }).on("mouseout",function(d2){
                div2.transition().duration(100).style("opacity",0);
            });
        group2.append("g")
                .attr("class", "xAxis1")
                .attr("transform","translate(0,0)")
                .call(xAxis2)
        .selectAll("text")
            .attr("transform", "rotate(-45)")
            .style("text-anchor", "start");

        group2.append("g")
            .attr("class","yAxis1")
            .attr("transform", "translate(0,0)")
            .call(yAxis2)
        .selectAll("text")
            .attr("transform", "rotate(45)")
            .style("text-anchor", "end");


        var countScale2 = d3.scaleLinear()
                .domain([-1,1])
                .range([0,width2]);

        //Calculate the variables for the temp gradient
        var numStops2 = 5;
        var countRange2 = countScale2.domain();
        countRange2[2] = countRange2[1] - countRange2[0];
        var countPoint2 = [];
        for(var k = 0; k < numStops2; k++) {
                countPoint2.push(k * countRange2[2]/(numStops2-1) + countRange2[0]);
        }
            //Create the gradient
        group2.append("defs")
            .append("linearGradient")
            .attr("id", "legend-pearson")
            .attr("x1", "0%").attr("y1", "100%")
            .attr("x2", "0%").attr("y2", "0%")
            .selectAll("stop") 
            .data(d3.range(numStops2))                
            .enter().append("stop")
            .attr("offset", function(d,i) { 
                    return countScale2( countPoint2[i] )/width2;
            })   
            .attr("stop-color", function(d,i) { 
                    return colorScale2( countPoint2[i] ); 
            });

        var legendHeight2 = height2/3;
        //Color Legend container
        var legendgroup2 =group2.append("g")
                .attr("class", "legendWrapper")
                .attr("transform", "translate(" + (width2) + "," + (0) + ")");

        //Draw the Rectangle
        legendgroup2.append("rect")
                .attr("class", "legendRect")
                .attr("x", 30)
                .attr("y", 0)
                .attr("width", 15)
                .attr("height", legendHeight2)
                .style("fill", "url(#legend-pearson)");



        //Set scale for x-axis
        var xScaleleg2 = d3.scaleLinear()
                 .range([-legendHeight2/2, (legendHeight2/2)-1])
                 .domain([1,-1] );

        //Define x-axis
        var xAxisleg2 = d3.axisRight(xScaleleg2)
                  .ticks(5)
                  .scale(xScaleleg2);

        //Set up X axis
        legendgroup2.append("g")
                .attr("class", "axis")
                .attr("transform", "translate("+(45) +","+(legendHeight2/2)+")")
                .call(xAxisleg2);
        
        
        
    }else if(allorsome.value==="onselectedmtb"){
        var metonselected =document.getElementById("metonselected");
        var metonselectedsize = metonselected.rows.length;
        var metelem =[];
        var l=0;var k=0;
        var datamet ={"met":[]};
        for(l=1;l<metonselectedsize;l++){
            metelem.push(metonselected.rows[0].cells[l].innerHTML);
            for(k=1;k<metonselectedsize;k++){
                datamet["met"].push({"x_met":metonselected.rows[0].cells[l].innerHTML,"y_met":metonselected.rows[0].cells[k].innerHTML,"corrvalmet":metonselected.rows[l].cells[k].innerHTML});
            }
        }
        var newdata1=datamet["met"];
        var margin1 = {top:30,right:30,bottom:20,left:20};
        var width1 = 770-margin1.right-margin1.left;
        var height1=770-margin1.top-margin1.bottom;
        var color=['#4575b4','#4776b5','#4a78b6','#4c79b6','#4e7ab7','#517bb8','#537db9','#557eb9','#577fba','#5980bb',
                   '#5b82bc','#5e83bc','#6084bd','#6286be','#6487bf','#6688bf','#688ac0','#6a8bc1','#6c8cc2','#6e8ec2',
                   '#708fc3','#7290c4','#7491c5','#7593c5','#7794c6','#7995c7','#7b97c8','#7d98c8','#7f99c9','#819bca',
                   '#839ccb','#859ecb','#869fcc','#88a0cd','#8aa2ce','#8ca3ce','#8ea4cf','#90a6d0','#91a7d1','#93a8d1',
                   '#95aad2','#97abd3','#99add4','#9baed4','#9cafd5','#9eb1d6','#a0b2d7','#a2b3d7','#a4b5d8','#a5b6d9',
                   '#a7b8da','#a9b9da','#abbadb','#adbcdc','#aebddd','#b0bfde','#b2c0de','#b4c1df','#b5c3e0','#b7c4e1',
                   '#b9c6e1','#bbc7e2','#bdc8e3','#becae4','#c0cbe4','#c2cde5','#c4cee6','#c5d0e7','#c7d1e7','#c9d2e8',
                   '#cbd4e9','#cdd5ea','#ced7ea','#d0d8eb','#d2daec','#d4dbed','#d5dced','#d7deee','#d9dfef','#dbe1f0',
                   '#dce2f0','#dee4f1','#e0e5f2','#e2e7f3','#e4e8f3','#e5eaf4','#e7ebf5','#e9edf6','#ebeef6','#eceff7',
                   '#eef1f8','#f0f2f9','#f2f4f9','#f3f5fa','#f5f7fb','#f7f8fc','#f9fafc','#fbfbfd','#fcfdfe','#fefeff',
                   '#fffefe','#fffcfb','#fffaf9','#fff8f7','#fff6f4','#fff4f2','#fff2f0','#fff0ed','#ffeeeb','#ffede8',
                   '#ffebe6','#ffe9e4','#ffe7e1','#ffe5df','#ffe3dd','#ffe1da','#ffdfd8','#ffddd6','#ffdbd3','#ffd9d1',
                   '#ffd7cf','#ffd5cd','#ffd3ca','#ffd1c8','#ffd0c6','#ffcec3','#ffccc1','#ffcabf','#ffc8bd','#ffc6ba',
                   '#ffc4b8','#fec2b6','#fec0b3','#febeb1','#febcaf','#fdbaad','#fdb8ab','#fdb6a8','#fdb4a6','#fcb3a4',
                   '#fcb1a2','#fcaf9f','#fbad9d','#fbab9b','#faa999','#faa797','#faa594','#f9a392','#f9a190','#f89f8e',
                   '#f89d8c','#f89b8a','#f79987','#f79785','#f69583','#f69381','#f5917f','#f58f7d','#f48d7a','#f48b78',
                   '#f38976','#f38774','#f28572','#f18370','#f1816e','#f07f6c','#f07d6a','#ef7b68','#ee7965','#ee7763',
                   '#ed7561','#ed735f','#ec715d','#eb6f5b','#eb6d59','#ea6b57','#e96955','#e86753','#e86451','#e7624f',
                   '#e6604d','#e65e4b','#e55c49','#e45947','#e35745','#e35543','#e25341','#e1503f','#e04e3d','#df4b3b',
                   '#df4939','#de4637','#dd4435','#dc4133','#db3f31','#da3c2f','#da392d','#d9362b','#d83329','#d73027'];

        var canvas1 = d3.select(".metheatmap5")
            .attr("height", height1 + margin1.top + margin1.bottom)
            .attr("width", width1 + margin1.left + margin1.right+60);

        var group1 = canvas1.append("g")
                .attr("transform",function(){return "translate("+(margin1.right)+","+(margin1.top)+")";});
        var div1 = d3.select('.tooltip5');
        var xScale1 = d3.scaleBand().domain(metelem).range([0,width1]);
        var yScale1 = d3.scaleBand().domain(metelem).range([0,height1]);
        var colorScale = d3.scaleQuantize().domain([-1,1]).range(color);

        var barWidth1 = width1/(newdata1.length/metelem.length);
        var barHeight1 = height1/metelem.length;

        var xAxis1 = d3.axisTop(xScale1).tickSize(0).tickValues("");
        var yAxis1 = d3.axisLeft(yScale1).tickSize(0).tickValues("");
        group1.selectAll('g').data(newdata1).enter().append("g")
                .attr("transform", function(newdata1){
                            return "translate("+xScale1(newdata1.x_met)+","+yScale1(newdata1.y_met)+")";})
                .append("rect")
                .attr("width", barWidth1)
                .attr("height", barHeight1)
                .style("fill",function(newdata1){return colorScale(newdata1.corrvalmet);})
                        .on("mouseover",function(d1){
                            //make the tooltip to follow the mouse
                            div1.transition().duration(10)
                                    .style("opacity",0.8)
                                    .style("left",d3.event.pageX - 350 + "px")
                                    .style("top", d3.event.pageY - 250 + "px");
                            div1.html("<p>x: " + d1.x_met + "</br>y: " + d1.y_met + "</br>Value= " + parseFloat(d1.corrvalmet).toFixed(2) + "</p>" );
            }).on("mouseout",function(d1){
                div1.transition().duration(100).style("opacity",0);
            });
        group1.append("g")
                .attr("class", "xAxis1")
                .attr("transform","translate(0,0)")
                .call(xAxis1);

        group1.append("g")
            .attr("class","yAxis1")
            .attr("transform", "translate(0,0)")
            .call(yAxis1);

        var countScale1 = d3.scaleLinear()
                .domain([-1,1])
                .range([0,width1]);

        //Calculate the variables for the temp gradient
        var numStops1 = 5;
        var countRange1 = countScale1.domain();
        countRange1[2] = countRange1[1] - countRange1[0];
        var countPoint1 = [];
        for(var k = 0; k < numStops1; k++) {
                countPoint1.push(k * countRange1[2]/(numStops1-1) + countRange1[0]);
        }
            //Create the gradient
        group1.append("defs")
            .append("linearGradient")
            .attr("id", "legend-pearson")
            .attr("x1", "0%").attr("y1", "100%")
            .attr("x2", "0%").attr("y2", "0%")
            .selectAll("stop") 
            .data(d3.range(numStops1))                
            .enter().append("stop")
            .attr("offset", function(d,i) { 
                    return countScale1( countPoint1[i] )/width1;
            })   
            .attr("stop-color", function(d,i) { 
                    return colorScale( countPoint1[i] ); 
            });

        var legendHeight1 = height1/3;
        //Color Legend container
        var legendgroup1 =group1.append("g")
                .attr("class", "legendWrapper")
                .attr("transform", "translate(" + (width1) + "," + (0) + ")");

        //Draw the Rectangle
        legendgroup1.append("rect")
                .attr("class", "legendRect")
                .attr("x", 30)
                .attr("y", 0)
                .attr("width", 15)
                .attr("height", legendHeight1)
                .style("fill", "url(#legend-pearson)");

        //Set scale for x-axis
        var xScaleleg1 = d3.scaleLinear()
                 .range([-legendHeight1/2, (legendHeight1/2)-1])
                 .domain([1,-1] );

        //Define x-axis
        var xAxisleg1 = d3.axisRight(xScaleleg1)
                  .ticks(5)
                  .scale(xScaleleg1);

        //Set up X axis
        legendgroup1.append("g")
                .attr("class", "axis")
                .attr("transform", "translate("+(45) +","+(legendHeight1/2)+")")
                .call(xAxisleg1);
}

});