/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

$(document).ready(function() {
    var table = $('#protable2').on( 'draw.dt', function () {
                $("#msgcontainer2").attr("id","msgcontainerafter2");
                $("#loadercontainer2").css("display","none");
              } ).DataTable( {
            responsive: true,
            bLengthChange: false,
            pageLength: 10,
            //order: [[ 1, 'asc' ]],
            "language": {
                "info": "Showing _START_ to _END_ of _TOTAL_ metabolites"
            }
        });
        
    var table=$('#protable1').on( 'draw.dt', function () {
                $("#msgcontainer1").attr("id","msgcontainerafter1");
                $("#loadercontainer1").css("display","none");
              } ).DataTable( {
        responsive: true,
        bLengthChange: true,
        pageLength: 10,
        columnDefs: [ {type: "num",targets: 1},{type: "num",targets: 2} ],
        "language": {
            "info": "Showing _START_ to _END_ of _TOTAL_ metabolites"
      }
    } );
    
    function download_csv(csv, filename) {
            var csvFile;
            var downloadLink;

            // CSV FILE
            csvFile = new Blob([csv], {type: "text/csv"});

            // Download link
            downloadLink = document.createElement("a");

            // File name
            downloadLink.download = filename;

            // We have to create a link to the file
            downloadLink.href = window.URL.createObjectURL(csvFile);

            // Make sure that the link is not displayed
            downloadLink.style.display = "none";

            // Add the link to your DOM
            document.body.appendChild(downloadLink);

            downloadLink.click();
        }

        function export_table_to_csv(html, filename) {
            var csv = [];
            var tempcsv=[];
            var rowshead = document.querySelectorAll("#protable1 >thead tr");
            var temprowsbody = $("#protable1").DataTable();
            var rowsbody =   temprowsbody.rows({ 'search': 'applied' }).nodes();

            for(var k=0; k<rowshead.length;k++){
                var rowh=[];
                var colsh = rowshead[k].querySelectorAll("th, td");
                for(var l=0; l<colsh.length;l++){
                    rowh.push(colsh[l].innerText);
                }
                tempcsv.push(rowh);
            }

            var temprowh=[];
            temprowh.push("Metabolite");
            temprowh.push("p-value");
            temprowh.push("adj. p-value");
            temprowh.push("f-value");
            temprowh.push("log2FC");
                
            csv.push(temprowh);
            for (var i = 0; i < rowsbody.length; i++) {
                var rowb = [];
                var colsb = rowsbody[i].querySelectorAll("th, td");

                for (var j = 0; j < colsb.length; j++){
                    rowb.push(colsb[j].innerText);
                }
                csv.push(rowb.join(","));		
            }
            // Download CSV
            download_csv(csv.join("\n"), filename);
        }

        document.querySelector('input[name="export"]').addEventListener("click", function () {
            var html = document.querySelector("table").outerHTML;
                export_table_to_csv(html, "anova.csv");
        });
    
} );

