/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

function boxplotimage(response,expidexpname,eachorgroup){
    
    var formatNumber = d3.format(".1f"),
                formatTrillion = function(x) { return formatNumber(x / 1e12) + "T"; },
                formatBillion = function(x) { return formatNumber(x / 1e9) + "B"; },
                formatMillion = function(x) { return formatNumber(x / 1e6) + "M"; },
                formatThousand = function(x) { return formatNumber(x / 1e3) + "k"; },
                formatHundred = function(x) { return x; };


            function formatAbbreviation(x) {
              var v = Math.abs(x);
              return (v >= .9995e12 ? formatTrillion
                  :v >= .9995e9 ? formatBillion
                  : v >= .9995e6 ? formatMillion
                  : v >= .9995e3 ? formatThousand
                  : formatHundred)(x);
            }
    var experimentlist=[];
    var intensitylist=[];
    var newdatabefore=[];
   
    if(eachorgroup.value==="eachval"){
        var repsize=response.length;
        var expidexpnamesize=expidexpname.length;
        var k=0;
        var l=0;
        for(k=0;k<repsize;k++){
            for(l=0;l<expidexpnamesize;l++){
                if(k<1){
                    experimentlist.push(expidexpname[l].cells[1].innerHTML);
                }
                if(response[k].experimentid===expidexpname[l].cells[0].innerHTML){
                    newdatabefore.push({"experiment":response[k].experimentid, "expname":expidexpname[l].cells[1].innerHTML,"intensity":response[k].intensity});
                }
            }
            intensitylist.push(response[k].intensity);
        }
        
    }else if(eachorgroup.value==="groupval2"){
        experimentlist.push("group1");
        experimentlist.push("group2");
        var repsize=response.length;
        var expidexpnamesize=2;
        var k=0;
        var l=0;
        for(k=0;k<repsize;k++){
            for(l=0;l<expidexpnamesize;l++){
                if(response[k].experimentid==="group1"){
                    newdatabefore.push({"experiment":response[k].experimentid, "expname":"group1","intensity":response[k].intensity});
                }else if(response[k].experimentid==="group2"){
                    newdatabefore.push({"experiment":response[k].experimentid, "expname":"group2","intensity":response[k].intensity});
                }
            }
            intensitylist.push(response[k].intensity);
        }
    }else if(eachorgroup.value==="groupval3"){
        experimentlist.push("group1");
        experimentlist.push("group2");
        experimentlist.push("group3");
        var repsize=response.length;
        var expidexpnamesize=3;
        var k=0;
        var l=0;
        for(k=0;k<repsize;k++){
            for(l=0;l<expidexpnamesize;l++){
                if(response[k].experimentid==="group1"){
                    newdatabefore.push({"experiment":response[k].experimentid, "expname":"group1","intensity":response[k].intensity});
                }else if(response[k].experimentid==="group2"){
                    newdatabefore.push({"experiment":response[k].experimentid, "expname":"group2","intensity":response[k].intensity});
                }else if(response[k].experimentid==="group3"){
                    newdatabefore.push({"experiment":response[k].experimentid, "expname":"group3","intensity":response[k].intensity});
                }
            }
            intensitylist.push(response[k].intensity);
        }
    }
    
    // set the dimensions and margins of the graph
    var margin = {top: 20, right: 0, bottom: 20, left: 50},
        width = 940 - margin.left - margin.right,
        height = 700 - margin.top - margin.bottom;

    // append the svg object to the body of the page
    var svg = d3.select("#container1")
      .append("svg")
        .attr("width", width + margin.left + margin.right)
        .attr("height", height + margin.top + margin.bottom)
      .append("g")
        .attr("transform",
              "translate(" + margin.left + "," + margin.top + ")");

    var newdata = newdatabefore.sort(function(a, b){ return d3.ascending(a.intensity, +b.intensity); });
    var internalpoints=[];
    var externalpoints=[];
    var sumstat = d3.nest() // nest function allows to group the calculation per level of a factor
        .key(function(d) { return d.expname;})
        .rollup(function(d) {
          q1 = d3.quantile(d.map(function(g) { return g.intensity;}),0.25);
          median = d3.quantile(d.map(function(g) { return g.intensity;}),0.5);
          q3 = d3.quantile(d.map(function(g) { return g.intensity;}),0.75);
          interQuantileRange = q3 - q1;
          tempmin = q1 - 1.5 * interQuantileRange; 
          tempmax = q3 + 1.5 * interQuantileRange;

          var minarray=d.map(function(g) { return +g.intensity;});
          
          function getClosestmin() {
                closest = null;
                for (var i in minarray) {
                   if (closest === null || (Math.abs(tempmin - closest) > Math.abs(minarray[i] - tempmin) && (minarray[i] - tempmin>0))) {
                      closest = minarray[i];
                   }
                }
                return closest;
             }
            function getClosestmax() {
               closest = null;
               for (var i in minarray) {
                  if (closest === null || (Math.abs(tempmax - closest) > Math.abs(minarray[i] - tempmax) && (tempmax - minarray[i]>0))) {
                    closest = minarray[i];
                  }
               }
               return closest;
            }
            min=getClosestmin();
            max=getClosestmax();
            function correctminmax(){
                if(q1<min ){
                    min=q1;
                }
                if(q3>max ){
                    max=q3;
                }
                if(minarray.length===2){
                    q1=min;
                    q3=max;
                    
                }
            }
           correctminmax();
           function internalexternalpo(){
               d.map(function(g) {
                   if(g.intensity<=max && g.intensity>=min){internalpoints.push({"experiment":g.experiment, "expname":g.expname,"intensity":g.intensity});}
                   else{externalpoints.push({"experiment":g.experiment, "expname":g.expname,"intensity":g.intensity});}
                   return g.experiment;
               });
               
           }
           internalexternalpo();
           if(minarray.length===2){
                min=median;
                max=median;
            }
          return({q1: q1, median: median, q3: q3, interQuantileRange: interQuantileRange, min: min, max: max});
        })
        .entries(newdata);

    // Show the X scale
      var x = d3.scaleBand()
        .range([ 0, width ])
        .domain(experimentlist)
        .paddingInner(1)
        .paddingOuter(.5);
      svg.append("g")
        .attr("transform", "translate(0," + height + ")")
        .call(d3.axisBottom(x));

      // Show the Y scale
      var y = d3.scaleLinear()
        .domain([0, d3.max(newdata, function(d) { return +d.intensity; })])
        .range([height, 0]);
      svg.append("g").call(d3.axisLeft(y).tickFormat(formatAbbreviation));

      // Show the main vertical line
      svg
        .selectAll("vertLines")
        .data(sumstat)
        .enter()
        .append("line")
          .attr("x1", function(d){return(x(d.key));})
          .attr("x2", function(d){return(x(d.key));})
          .attr("y1", function(d){return(y(d.value.min));})
          .attr("y2", function(d){return(y(d.value.max));})
          .attr("stroke", "black")
          .style("width", 40);
  
    // rectangle for the main box
      var boxWidth = 20;
      svg
        .selectAll("boxes")
        .data(sumstat)
        .enter()
        .append("rect")
        .attr("x", function(d){return(x(d.key)-boxWidth/2);})
        .attr("y", function(d){return(y(d.value.q3));})
        .attr("height", function(d){return(y(d.value.q1)-y(d.value.q3));})
        .attr("width", boxWidth )
        .attr("stroke", "black")
        .style("fill", "#B5D2FC");

      // Show the median
     svg
        .selectAll("medianLines")
        .data(sumstat)
        .enter()
        .append("line")
          .attr("x1", function(d){return(x(d.key)-boxWidth/2); })
          .attr("x2", function(d){return(x(d.key)+boxWidth/2);})
          .attr("y1", function(d){return(y(d.value.median));})
          .attr("y2", function(d){return(y(d.value.median));})
          .attr("stroke", "black")
          .style("width", 40);
  
  svg
        .selectAll("outlines")
        .data(sumstat)
        .enter()
        .append("line")
          .attr("x1", function(d){return(x(d.key)-boxWidth/2); })
          .attr("x2", function(d){return(x(d.key)+boxWidth/2);})
          .attr("y1", function(d){return(y(d.value.min));})
          .attr("y2", function(d){return(y(d.value.min));})
          .attr("stroke", "black")
          .style("width", 40);

     svg
        .selectAll("outlines")
        .data(sumstat)
        .enter()
        .append("line")
          .attr("x1", function(d){return(x(d.key)-boxWidth/2); })
          .attr("x2", function(d){return(x(d.key)+boxWidth/2);})
          .attr("y1", function(d){return(y(d.value.max));})
          .attr("y2", function(d){return(y(d.value.max));})
          .attr("stroke", "black")
          .style("width", 80);
  
    // Add individual points with jitter
    var jitterWidth = 50;
    var legend1svg = svg
      .selectAll("indPoints")
      .data(externalpoints)
      .enter()
      .append("circle")
        .attr("cx", function(d){return(x(d.expname) );})
        .attr("cy", function(d){return(y(d.intensity));})
        .attr("r", 4)
        .style("fill", "white")
        .attr("stroke", "black")
        .style('stroke-width', 0.9).attr("class","external");

    var legend2svg = svg
        .selectAll("indPoints2")
        .data(internalpoints)
        .enter()
        .append("circle")
        .attr("cx", function(d){return(x(d.expname) );})
        .attr("cy", function(d){return(y(d.intensity));})
        .attr("r", 4)
        .style("fill", "white")
        .attr("stroke", "black")
        .style('stroke-width', 0.9).style('opacity', 0).attr("class","internal");

    var legend1= d3.select("#container1")
            .append('div').style('width',940+'px').style('height',50+'px').style("align-items","center").style("display","flex").attr("class", "showchk")
            .append("label").attr("class","showmtblabel").style("cursor","pointer").style("align-items","center").style("display","flex")
            .append("input").style("cursor","pointer").attr("type", "checkbox").attr("class","shmtbs")
            .append("g")
            .attr("class", "legend1").style("cursor", "pointer")
            .attr("transform", function() { return "translate(0,0)"; });

    d3.selectAll(".showmtblabel").append("text").text("Show metabolites");

    var clicked1=false;

    d3.select(".shmtbs").on('click', function(){
        if(!clicked1){
            svg.selectAll('.internal').style('opacity', 0.8);
            clicked1=true;
        }else{
            svg.selectAll('.internal').style('opacity', 0);
            clicked1=false;
        }
    });
}
var currentradioid='';
$(document).ready(function() {
    
    $('#protable tbody tr').click(function(event) {
        if (event.target.type !== 'radio') {
          $(':radio', this).trigger('click');
        }
      });
      
    $('#protable').on('change',"input[name='description']",function(){
        currentradioid=$(this).attr('id');
      });
    if(!document.getElementById("rowflag").value){
        var eachorgroup = document.getElementById("eachorgroup");
        var transf = document.getElementById("transf");
        var ionmode = document.getElementById("ionmode");
    
    if(eachorgroup.value==="eachval"){
        var table2=$('#explist').on( 'draw.dt', function () {
                    $("#msgcontainer2").attr("id","msgcontainerafter2");
                    $("#loadercontainer2").css("display","none");
                  } ).DataTable({
            responsive: true,
            bLengthChange: false,
            pageLength: 8,
            "order": [[ 1, "asc" ]],
            "language": {
                "info": "Showing _START_ to _END_ of _TOTAL_ experiments"
            }
        });
    }else if(eachorgroup.value==="groupval2"){
        var table3=document.getElementById('explistgroup1');
        var table4=document.getElementById('explistgroup2');
    }else if(eachorgroup.value==="groupval3"){
        var table3=document.getElementById('explistgroup1');
        var table4=document.getElementById('explistgroup2');
        var table5=document.getElementById('explistgroup3');
    }
    
    $.fn.DataTable.ext.pager.numbers_length = 4;
    var table = $('#protable').on( 'draw.dt', function () {
                $("#msgcontainer1").attr("id","msgcontainerafter1");
                $("#loadercontainer1").css("display","none");
              } ).DataTable({
        responsive: true,
        bLengthChange: false,
        pageLength: 8,
        "language": {
            "info": "Showing _START_ to _END_ of _TOTAL_ metabolites of the selected experiments"
        },
        fnDrawCallback: function (oSettings) {
            $.each($("input[name='description']:checked"), function(){
                    if(currentradioid!==$(this).attr('id')){
                        $(this).prop('checked', false);
                    }
            });

        }
    });
    var firstmtb = document.getElementById("onemetabolite").value;
    var projectid = document.getElementById("projectid").value;
    
    var radios = table.cells( ).nodes();
    var tempexperimentrows;
    if(eachorgroup.value==="eachval"){
        tempexperimentrows=table2.rows().nodes();
        var experimentids=[];
        for(var i=0;i<tempexperimentrows.length;i++){
            experimentids.push(tempexperimentrows[i].cells[0].innerHTML);
        }
        var chboxes = $( radios ).find(':radio');
        for(var l = 0; l < chboxes.length; l++){
            if(chboxes[l].value===firstmtb){
                chboxes[l].checked=true;
                break;
            }
        }
        $.ajax({
            "url": "fetchboxplotsviewmtbs?experimentids="+experimentids+"&eachorgroup="+eachorgroup.value+"&transf="+transf.value+"&description="+encodeURIComponent(firstmtb)+"&projectid="+projectid+"&ionmode="+ionmode.value,//at first it had contextpath+"/
            type: 'GET',
            dataType: "json",
            "success": function(response){
                            $('#expintens').DataTable({
                                data: response ,
                                "columns": [
                                     { "data" : "experimentid" },
                                     { "data" : "intensity" }
                                 ]
                            });
                            boxplotimage(response.data,tempexperimentrows,eachorgroup);

                        }
        });
    }else if(eachorgroup.value==="groupval2"){
        var radios = table.cells( ).nodes();
        var chboxes = $( radios ).find(':radio');
        for(var l = 0; l < chboxes.length; l++){
            if(chboxes[l].value===firstmtb){
                chboxes[l].checked=true;
                break;
            }
        }
        var tmpexplistgroup1len=table3.rows.length;
        var experimentids1=[];
        for(var i=0;i<tmpexplistgroup1len;i++){
            experimentids1.push(table3.rows[i].cells[0].innerHTML);
        }
        var tmpexplistgroup2len=table4.rows.length;
        var experimentids2=[];
        for(var p=0;p<tmpexplistgroup2len;p++){
            experimentids2.push(table4.rows[p].cells[0].innerHTML);
        }
        $.ajax({
            "url": "fetchboxplotsviewmtbs?experimentids1="+experimentids1+"&experimentids2="+experimentids2+"&eachorgroup="+eachorgroup.value+"&transf="+transf.value+"&description="+encodeURIComponent(firstmtb)+"&projectid="+projectid+"&ionmode="+ionmode.value,//at first it had contextpath+"/
            type: 'GET',
            dataType: "json",
            "success": function(response){
                            $('#expintens').DataTable({
                                data: response ,
                                "columns": [
                                     { "data" : "experimentid" },
                                     { "data" : "intensity" }
                                 ]
                            });
                            boxplotimage(response.data,tempexperimentrows,eachorgroup);

                        }
        });
        
    }else if(eachorgroup.value==="groupval3"){
        var radios = table.cells( ).nodes();
        var chboxes = $( radios ).find(':radio');
        for(var l = 0; l < chboxes.length; l++){
            if(chboxes[l].value===firstmtb){
                chboxes[l].checked=true;
                break;
            }
        }
        var tmpexplistgroup1len=table3.rows.length;
        var experimentids1=[];
        for(var i=0;i<tmpexplistgroup1len;i++){
            experimentids1.push(table3.rows[i].cells[0].innerHTML);
        }
        var tmpexplistgroup2len=table4.rows.length;
        var experimentids2=[];
        for(var p=0;p<tmpexplistgroup2len;p++){
            experimentids2.push(table4.rows[p].cells[0].innerHTML);
        }
        var tmpexplistgroup3len=table5.rows.length;
        var experimentids3=[];
        for(var i=0;i<tmpexplistgroup3len;i++){
            experimentids3.push(table5.rows[i].cells[0].innerHTML);
        }
        $.ajax({
            "url": "fetchboxplotsviewmtbs?experimentids1="+experimentids1+"&experimentids2="+experimentids2+"&experimentids3="+experimentids3+"&transf="+transf.value+"&eachorgroup="+eachorgroup.value+"&description="+encodeURIComponent(firstmtb)+"&projectid="+projectid+"&ionmode="+ionmode.value,//at first it had contextpath+"/
            type: 'GET',
            dataType: "json",
            "success": function(response){
                            $('#expintens').DataTable({
                                data: response ,
                                "columns": [
                                     { "data" : "experimentid" },
                                     { "data" : "intensity" }
                                 ]
                            });
                            boxplotimage(response.data,tempexperimentrows,eachorgroup);
                        }
        });
        
    }
    
    
    $("#disbtn").click(function(){
        $(document).on('click', ".update",function (){
            $('#analysisspinner').show();
            document.getElementById("disbtn").disabled=true;
            $("#disbtn").removeClass("setbtn");
            d3.select("svg").remove();
            d3.select(".showchk").remove();
            $('#expintens').DataTable().destroy();        
        });        
        var checkeddescription=$('input[name=description]:checked').val();
        if(eachorgroup.value==="eachval"){
            $.ajax({
                "url": "fetchboxplotsviewmtbs?experimentids="+experimentids+"&description="+encodeURIComponent(checkeddescription)+"&projectid="+projectid+"&transf="+transf.value+"&eachorgroup="+eachorgroup.value+"&ionmode="+ionmode.value,//at first it had contextpath+"/
                type: 'GET',
                dataType: "json",
                "success":  function(response){
                                if(response.data===true){
                                   
                                    $("#refreshdiv").remove();
                                    $('.container').append('<div>'+
                                                        '<br/><br/><br/>'+
                                                        '<div class="alert alert-info text-center col-sm-12 centered" style="border-radius: 5px;padding: 15px;">There is an ongoing processing taking place right now and the cluster is busy.'+
                                                        '<br/>'+
                                                        'Please try again in a few minutes.'+
                                                        '</div>'+
                                                    '</div>');
                                }else{
                                    
                                    $('#expintens').DataTable({
                                        data: response ,
                                        "columns": [
                                             { "data" : "experimentid" },
                                             { "data" : "intensity" }
                                         ]
                                    });
                                    boxplotimage(response.data,tempexperimentrows,eachorgroup);
                                    $('#analysisspinner').hide();
                                    document.getElementById("disbtn").disabled=false;
                                    $("#disbtn").addClass("setbtn");
                                }
                            }
            });
        }else if(eachorgroup.value==="groupval2"){
            $.ajax({
                "url": "fetchboxplotsviewmtbs?experimentids1="+experimentids1+"&experimentids2="+experimentids2+"&transf="+transf.value+"&eachorgroup="+eachorgroup.value+"&description="+encodeURIComponent(checkeddescription)+"&projectid="+projectid+"&ionmode="+ionmode.value,//at first it had contextpath+"/
                type: 'GET',
                dataType: "json",
                "success":  function(response){
                                if(response.data===true){
                                    $("#refreshdiv").remove();
                                    $('.container').append('<div>'+
                                                        '<br/><br/><br/>'+
                                                        '<div class="alert alert-info text-center col-sm-12 centered" style="border-radius: 5px;padding: 15px;">There is an ongoing processing taking place right now and the cluster is busy.'+
                                                        '<br/>'+
                                                        'Please try again in a few minutes.'+
                                                        '</div>'+
                                                    '</div>');
                                }else{
                                    $('#expintens').DataTable({
                                        data: response ,
                                        "columns": [
                                             { "data" : "experimentid" },
                                             { "data" : "intensity" }
                                         ]
                                    });
                                    boxplotimage(response.data,tempexperimentrows,eachorgroup);
                                    $('#analysisspinner').hide();
                                    document.getElementById("disbtn").disabled=false;
                                    $("#disbtn").addClass("setbtn");
                                }
                            }
            });
        }else if(eachorgroup.value==="groupval3"){
            $.ajax({
                "url": "fetchboxplotsviewmtbs?experimentids1="+experimentids1+"&experimentids2="+experimentids2+"&experimentids3="+experimentids3+"&transf="+transf.value+"&eachorgroup="+eachorgroup.value+"&description="+encodeURIComponent(checkeddescription)+"&projectid="+projectid+"&ionmode="+ionmode.value,//at first it had contextpath+"/
                type: 'GET',
                dataType: "json",
                "success":  function(response){
                                if(response.data===true){
                                    $("#refreshdiv").remove();
                                    $('.container').append('<div>'+
                                                        '<br/><br/><br/>'+
                                                        '<div class="alert alert-info text-center col-sm-12 centered" style="border-radius: 5px;padding: 15px;">There is an ongoing processing taking place right now and the cluster is busy.'+
                                                        '<br/>'+
                                                        'Please try again in a few minutes.'+
                                                        '</div>'+
                                                    '</div>');
                                }else{
                                    $('#expintens').DataTable({
                                        data: response ,
                                        "columns": [
                                             { "data" : "experimentid" },
                                             { "data" : "intensity" }
                                         ]
                                    });
                                    boxplotimage(response.data,tempexperimentrows,eachorgroup);
                                    $('#analysisspinner').hide();
                                    document.getElementById("disbtn").disabled=false;
                                    $("#disbtn").addClass("setbtn");
                                }
                            }
            });
        }
            
        });
        return (true);
}else{
    return (false);
}
});
    
    
    