/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
$(document).ready(function() {
    var table = $('#protable').on( 'draw.dt', function () {
                $("#msgcontainer").attr("id","msgcontainerafter");
                $("#loadercontainer").css("display","none");
              } ).DataTable( {
            responsive: true,
            bLengthChange: false,
            pageLength: 10,
            "language": {
                "info": "Showing _START_ to _END_ of _TOTAL_ metabolites"
            }
        });
    //1st scatterplot
    var pcalevel = document.getElementById("pcalevel");
    if(pcalevel.value==="onexps"){
        var table1 = document.getElementById("pcaofexps");
        var tablelength1 =table1.rows.length;

        var data1={"triplicates":[]};
        var k=0;
        for(k=0;k<tablelength1;k++){
            var exp1 = table1.rows[k].cells[0].innerHTML.split("__");
            data1["triplicates"].push({"xel":table1.rows[k].cells[1].innerHTML,"yel":table1.rows[k].cells[2].innerHTML,"experiments":exp1[0]}); 
        }

        var newdata1=data1["triplicates"];
        var margin1 = {top: 20, right: 20, bottom: 20, left: 20};
        var outerWidth1 = 600,
            outerHeight1 = 600;
        var width1 = outerWidth1 - margin1.right - margin1.left;
        var height1 = outerHeight1 - margin1.top - margin1.bottom;

        var colorScale1 = d3.scaleOrdinal(d3.schemeCategory10);

        // create scale objects
        var xScale1 = d3.scaleLinear()
          .domain([-1, 1])
          .range([0, width1]);
        var yScale1 = d3.scaleLinear()
          .domain([-1, 1])
          .range([height1, 0]);


        var zoom1 = d3.zoom()
            .scaleExtent([.5, 10000])
            .extent([[0, 0], [width1, height1]])
            .on("zoom", zoomed1);

        var svg1 = d3.select('.container1').style('height', (height1 + margin1.top + margin1.bottom)+'px')
                .append('div').style('width', (width1 + margin1.left + margin1.right)+'px').style('height', (height1 + margin1.top + margin1.bottom)+'px').style('float','left')
                .append('svg')
                .attr('width', width1 + margin1.left + margin1.right)
                .attr('height', height1 + margin1.top + margin1.bottom)
                .style("background","#FFFFFF")
                .append('g')
                .attr("transform", "translate(0,0)").call(zoom1);

        // create axis objects
        var xAxis1 = d3.axisBottom(xScale1).tickSize(0);
        var yAxis1 = d3.axisLeft(yScale1).tickSize(0);

        // Draw Axis
        var gX1 = svg1.append('g')
          .attr('transform', 'translate(' + margin1.left + ',' + (margin1.top + height1/2) + ')')
          .call(xAxis1);  

        var gY1 = svg1.append('g')
          .attr('transform', 'translate(' + (margin1.left + width1/2) + ',' + margin1.top + ')')
          .call(yAxis1);

        var zoomBox1 = svg1.append('rect')
                    .attr('class', 'zoom1')
                    .attr('height', height1 + margin1.top + margin1.bottom)
                    .attr('width', width1 + margin1.left + margin1.right);

        svg1.append("text")
          .classed("label", true)
          .attr("x", width1+margin1.left)
          .attr("y", height1/2 +10)
          .style("text-anchor", "end")
          .text("PC1");

        svg1.append("text")
          .classed("label", true)
          .attr("x", width1/2 +60)
          .attr("y", margin1.top +10)
          .style("text-anchor", "end")
          .text("PC2");

        // Draw Datapoints
        var points_g1 = svg1.append("g")
          .attr('transform', 'translate(' + margin1.left + ',' + margin1.top + ')')
          .classed("points_g1", true);
 
   
   var controls=["controls"];
   var status1=["status1"];
   var status2=["status2"];
   var status3=["status3"];
   var status4=["status4"];
   var status5=["status5"];
   var status6=["status6"];
   var status7=["status7"];
   
        var points1 = points_g1.selectAll(".dot")
              .data(newdata1).enter().append("circle")
              .attr('r', 4)
              .attr('cx', function(d) {return xScale1(d.xel);})
              .attr('cy', function(d) {return yScale1(d.yel);})
              .style('fill', function(d){
                            if(status1.some(v => d.experiments.toLowerCase().includes(v))){
                                return "#FF6962";
                            }else if(controls.some(v => d.experiments.toLowerCase().includes(v))){
                                return "#77DD76";
                            }else if(status2.some(v => d.experiments.toLowerCase().includes(v))){
                                return "#CAB9E3";
                            }else if(status3.some(v => d.experiments.toLowerCase().includes(v))){
                                return "#FFDFD3";
                            }else if(status4.some(v => d.experiments.toLowerCase().includes(v))){
                                return "#A0CED9";
                            }else if(status5.some(v => d.experiments.toLowerCase().includes(v))){
                                return "#C36F31";
                            }else if(status6.some(v => d.experiments.toLowerCase().includes(v))){
                                return "#FB9E25";
                            }else if(status7.some(v => d.experiments.toLowerCase().includes(v))){
                                return "#AEAEAE";
                            }else{
                                return colorScale1(d.experiments);
                            }
              })
              .style('stroke', '#000')
              .style('stroke-width', 0.5)
              .on("mouseover",function(d){
                        tooltip1.transition().duration(10)
                                .style("opacity",0.8)
                                .style("left",(d3.event.pageX+20)+"px")
                                .style("top", (d3.event.pageY-35)+"px");
                        tooltip1.html("<p>Experiment: "+d.experiments+"</p>");
                  }).on("mouseout",function(d){
                      tooltip1.transition().duration(100).style("opacity",0);
                  });  

        var tooltip1 = d3.select("body")
                .append('div')
                .attr("id","tooltip1");

        function zoomed1() {
        // create new scale ojects based on event
            var new_xScale1 = d3.event.transform.rescaleX(xScale1);
            var new_yScale1 = d3.event.transform.rescaleY(yScale1);
        // update axes
            gX1.call(xAxis1.scale(new_xScale1));
            gY1.call(yAxis1.scale(new_yScale1));
            points1.data(newdata1)
             .attr('cx', function(d) {return new_xScale1(d.xel);})
             .attr('cy', function(d) {return new_yScale1(d.yel);});
        }

        $("#Reset1").click(() => {
            svg1.transition()
              .duration(750)
              .call(zoom1.transform, d3.zoomIdentity);
          });

        var legend1svg = d3.select(".container1")
                .append('div').style('width',225+'px').style('height',410+'px').style('overflow-y', 'scroll').style('overflow-x', 'hidden')
                .append('svg')
                .attr('width', 225)
                .attr('height', 20*newdata1.length)
                .style("background","#FFFFFF")
                .selectAll('.legend1')
                .data(colorScale1.domain())
              .enter().append("g")
                .attr("class", "legend1").style("cursor", "pointer")
                .attr("transform", function(d, i) { return "translate(0," + i * 20 + ")"; });

                var legendinfo = d3.select(".container1")
                .append('div').style('width',225+'px').style('height',190+'px').style('float','right')
                .append('svg').attr("id", "leginfo")
                .attr('width', 225)
                .attr('height', 190)
                .style("background","#FFFFFF");
                
                var legendinfosvg=d3.select("#leginfo");
                
                legendinfosvg.append("text").attr("x", 0).attr("y", 12).text("If keywords were used:");
                
                legendinfosvg.append("circle").attr("r", 8).attr("cx",10).attr("cy",26).style("fill","#77DD76");
                        legendinfosvg.append("text").attr("x", 25).attr("y", 30).text("controls");
                legendinfosvg.append("circle").attr("r", 8).attr("cx",10).attr("cy",47).style("fill","#FF6962");
                        legendinfosvg.append("text").attr("x", 25).attr("y", 51).text("status1");
                legendinfosvg.append("circle").attr("r", 8).attr("cx",10).attr("cy",68).style("fill","#CAB9E3");
                        legendinfosvg.append("text").attr("x", 25).attr("y", 72).text("status2");
                legendinfosvg.append("circle").attr("r", 8).attr("cx",10).attr("cy",89).style("fill","#FFDFD3");
                        legendinfosvg.append("text").attr("x", 25).attr("y", 93).text("status3");
                legendinfosvg.append("circle").attr("r", 8).attr("cx",10).attr("cy",110).style("fill","#A0CED9");
                        legendinfosvg.append("text").attr("x", 25).attr("y", 114).text("status4");
                        
                legendinfosvg.append("circle").attr("r", 8).attr("cx",10).attr("cy",131).style("fill","#C36F31");
                        legendinfosvg.append("text").attr("x", 25).attr("y", 135).text("status5");
                legendinfosvg.append("circle").attr("r", 8).attr("cx",10).attr("cy",152).style("fill","#FB9E25");
                        legendinfosvg.append("text").attr("x", 25).attr("y", 156).text("status6");
                legendinfosvg.append("circle").attr("r", 8).attr("cx",10).attr("cy",173).style("fill","#AEAEAE");
                        legendinfosvg.append("text").attr("x", 25).attr("y", 177).text("status7");

        legend1svg
            .append("circle")
            .attr("r", 5)
            .attr("cx",5)
            .attr("cy",10)
            .style("fill",colorScale1);

        function wrap1() {
            var self = d3.select(this),
                textLength = self.node().getComputedTextLength(),
                text = self.text();
            while (textLength > (200) && text.length > 0) {
                text = text.slice(0, -1);
                self.text(text + '...');
                textLength = self.node().getComputedTextLength();
            }
        }

        legend1svg.append("text")
            .attr("x", 15)
            .attr("y",15)
            .style("font-size", "small")
            .text(function(d) { return d; }).each(wrap1)
            .on("mouseover",function(d){
                                tooltip11.transition().duration(10)
                                        .style("opacity",0.8)
                                        .style("left",(d3.event.pageX)+"px")
                                        .style("top", (d3.event.pageY)+"px");
                                tooltip11.html("<p>"+d+"</p>");
                          }).on("mouseout",function(d){
                              tooltip11.transition().duration(100).style("opacity",0);
                          });
        var tooltip11 = d3.select("body")
                .append('div')
                .attr("id","tooltip11");

        var clicked1=false;

        legend1svg.on('click', function(type){
            if(!clicked1){
                svg1.selectAll('circle')
                        .style('opacity', 0.05)
                        .filter(function(d){
                                return d["experiments"]==type;
                        })
                        .style('opacity', 1);
                clicked1=true;
            }
            else{
                    svg1.selectAll('circle')
                            .style('opacity', 1);
                    clicked1=false;
            }
        });
    }else if(pcalevel.value==="onrepls"){
        //2nd scatterplot
        var table2 = document.getElementById("pcaofrepls");
        var tablelength2 =table2.rows.length;
        var table1 = document.getElementById("pcaofexps");
        var tablelength1 =table1.rows.length;

        var data2={"triplicates":[]};                   
        var i=0;
        var l=0;
        for(i=0;i<tablelength2;i++){
            var repl = table2.rows[i].cells[0].innerHTML.split("__");
            for(var l=0;l<tablelength1;l++){
                var exp1 = table1.rows[l].cells[0].innerHTML.split("__");
                if(repl[0]===exp1[1]){
                    repl[0]=exp1[0];
                }
            }
            data2["triplicates"].push({"xel":table2.rows[i].cells[1].innerHTML,"yel":table2.rows[i].cells[2].innerHTML,"replicates":repl[1],"experiments":repl[0]});    
        }

        var newdata2=data2["triplicates"];
        var margin2 = {top: 20, right: 20, bottom: 20, left: 20};
        var outerWidth2 = 600,
            outerHeight2 = 600;
        var width2 = outerWidth2 - margin2.right - margin2.left;
        var height2 = outerHeight2 - margin2.top - margin2.bottom;

        var colorScale2 = d3.scaleOrdinal(d3.schemeCategory10);

        // create scale objects
        var xScale2 = d3.scaleLinear()
          .domain([-1, 1])
          .range([0, width2]);
        var yScale2 = d3.scaleLinear()
          .domain([-1, 1])
          .range([height2, 0]);

        var zoom2 = d3.zoom()
            .scaleExtent([.5, 10000])
            .extent([[0, 0], [width2, height2]])
            .on("zoom", zoomed2);

        var svg2 = d3.select('.container2').style('height', (height2 + margin2.top + margin2.bottom)+'px')
                .append('div').style('width', width2 + margin2.left + margin2.right+'px').style('height', height2 + margin2.top + margin2.bottom+'px').style('float','left')
                .append('svg')
                .attr('width', width2 + margin2.left + margin2.right)
                .attr('height', height2 + margin2.top + margin2.bottom)
                .style("background","#FFFFFF")
                .append('g')
                .attr("transform", "translate(0,0)").call(zoom2);

        // create axis objects
        var xAxis2 = d3.axisBottom(xScale2).tickSize(0);
        var yAxis2 = d3.axisLeft(yScale2).tickSize(0);

        // Draw Axis
        var gX2 = svg2.append('g')
          .attr('transform', 'translate(' + margin2.left + ',' + (margin2.top + height2/2) + ')')
          .call(xAxis2);  

        var gY2 = svg2.append('g')
          .attr('transform', 'translate(' + (margin2.left + width2/2) + ',' + margin2.top + ')')
          .call(yAxis2);

        var zoomBox2 = svg2.append('rect')
                    .attr('class', 'zoom2')
                    .attr('height', height2 + margin2.top + margin2.bottom)
                    .attr('width', width2 + margin2.left + margin2.right);

        svg2.append("text")
          .classed("label", true)
          .attr("x", width2+margin2.left)
          .attr("y", height2/2 +10)
          .style("text-anchor", "end")
          .text("PC1");

        svg2.append("text")
          .classed("label", true)
          .attr("x", width2/2 +60)
          .attr("y", margin2.top +10)
          .style("text-anchor", "end")
          .text("PC2");

        // Draw Datapoints
        var points_g2 = svg2.append("g")
          .attr('transform', 'translate(' + margin2.left + ',' + margin2.top + ')')
          .classed("points_g2", true);
  
   var controls=["controls"];
   var status1=["status1"];
   var status2=["status2"];
   var status3=["status3"];
   var status4=["status4"];
   var status5=["status5"];
   var status6=["status6"];
   var status7=["status7"];
   
      var points2 = points_g2.selectAll(".dot")
              .data(newdata2).enter().append("circle")
              .attr('r', 4)
              .attr('cx', function(d) {return xScale2(d.xel);})
              .attr('cy', function(d) {return yScale2(d.yel);})
              .style('fill', function(d){
                           if(status1.some(v => d.experiments.toLowerCase().includes(v))){
                                return "#FF6962";
                            }else if(controls.some(v => d.experiments.toLowerCase().includes(v))){
                                return "#77DD76";
                            }else if(status2.some(v => d.experiments.toLowerCase().includes(v))){
                                return "#CAB9E3";
                            }else if(status3.some(v => d.experiments.toLowerCase().includes(v))){
                                return "#FFDFD3";
                            }else if(status4.some(v => d.experiments.toLowerCase().includes(v))){
                                return "#A0CED9";
                            }else if(status5.some(v => d.experiments.toLowerCase().includes(v))){
                                return "#C36F31";
                            }else if(status6.some(v => d.experiments.toLowerCase().includes(v))){
                                return "#FB9E25";
                            }else if(status7.some(v => d.experiments.toLowerCase().includes(v))){
                                return "#AEAEAE";
                            }else{
                                return colorScale1(d.experiments);
                            }
                      }
                      )
              .style('stroke', '#000')
              .style('stroke-width', 0.5)
              .on("mouseover",function(d){
                        tooltip2.transition().duration(10)
                                .style("opacity",0.8)
                                .style("left",(d3.event.pageX+20)+"px")
                                .style("top", (d3.event.pageY-35)+"px");
                        tooltip2.html("<p>Replicate: "+d.replicates+"</p>");
                  }).on("mouseout",function(d){
                      tooltip2.transition().duration(100).style("opacity",0);
                  });  

        var tooltip2 = d3.select("body")
                .append('div')
                .attr("id","tooltip2");

        function zoomed2() {
        // create new scale ojects based on event
            var new_xScale2 = d3.event.transform.rescaleX(xScale2);
            var new_yScale2 = d3.event.transform.rescaleY(yScale2);
        // update axes
            gX2.call(xAxis2.scale(new_xScale2));
            gY2.call(yAxis2.scale(new_yScale2));
            points2.data(newdata2)
             .attr('cx', function(d) {return new_xScale2(d.xel);})
             .attr('cy', function(d) {return new_yScale2(d.yel);});
        }

        $("#Reset2").click(() => {
            svg2.transition()
              .duration(750)
              .call(zoom2.transform, d3.zoomIdentity);
          });

        var legend2svg = d3.select(".container2")
                .append('div').style('width',225+'px').style('height',410+'px').style('overflow-y', 'scroll').style('overflow-x', 'hidden')
                .append('svg')
                .attr('width', 225)
                .attr('height', 20*newdata2.length)
                .style("background","#FFFFFF")
                .selectAll('.legend2')
                .data(colorScale2.domain())
              .enter().append("g")
                .attr("class", "legend2").style("cursor", "pointer")
                .attr("transform", function(d, i) { return "translate(0," + i * 20 + ")"; });

                var legendinfo = d3.select(".container1")
                .append('div').style('width',225+'px').style('height',190+'px').style('float','right')
                .append('svg').attr("id", "leginfo")
                .attr('width', 225)
                .attr('height', 190)
                .style("background","#FFFFFF");
                
                var legendinfosvg=d3.select("#leginfo");
                
                legendinfosvg.append("text").attr("x", 0).attr("y", 12).text("If keywords were used:");
                legendinfosvg.append("circle").attr("r", 8).attr("cx",10).attr("cy",26).style("fill","#FF6962");
                        legendinfosvg.append("text").attr("x", 25).attr("y", 30).text("cases");
                legendinfosvg.append("circle").attr("r", 8).attr("cx",10).attr("cy",47).style("fill","#77DD76");
                        legendinfosvg.append("text").attr("x", 25).attr("y", 51).text("controls");
                legendinfosvg.append("circle").attr("r", 8).attr("cx",10).attr("cy",68).style("fill","#CAB9E3");
                        legendinfosvg.append("text").attr("x", 25).attr("y", 72).text("status1");
                legendinfosvg.append("circle").attr("r", 8).attr("cx",10).attr("cy",89).style("fill","#FFDFD3");
                        legendinfosvg.append("text").attr("x", 25).attr("y", 93).text("status2");
                legendinfosvg.append("circle").attr("r", 8).attr("cx",10).attr("cy",110).style("fill","#A0CED9");
                        legendinfosvg.append("text").attr("x", 25).attr("y", 114).text("status3");
                        
                legendinfosvg.append("circle").attr("r", 8).attr("cx",10).attr("cy",131).style("fill","#C36F31");
                        legendinfosvg.append("text").attr("x", 25).attr("y", 135).text("status5");
                legendinfosvg.append("circle").attr("r", 8).attr("cx",10).attr("cy",152).style("fill","#FB9E25");
                        legendinfosvg.append("text").attr("x", 25).attr("y", 156).text("status6");
                legendinfosvg.append("circle").attr("r", 8).attr("cx",10).attr("cy",173).style("fill","#AEAEAE");
                        legendinfosvg.append("text").attr("x", 25).attr("y", 177).text("status7");

        legend2svg
            .append("circle")
            .attr("r", 5)
            .attr("cx",5)
            .attr("cy",10)
            .style("fill",colorScale2);

        function wrap2() {
            var self = d3.select(this),
                textLength = self.node().getComputedTextLength(),
                text = self.text();
            while (textLength > (200) && text.length > 0) {
                text = text.slice(0, -1);
                self.text(text + '...');
                textLength = self.node().getComputedTextLength();
            }
        }

        legend2svg.append("text")
            .attr("x", 15)
            .attr("y",15)
            .style("font-size", "small").append('tspan')
            .text(function(d) { return d; }).each(wrap2)
            .on("mouseover",function(d){
                                tooltip22.transition().duration(10)
                                        .style("opacity",0.8)
                                        .style("left",(d3.event.pageX)+"px")
                                        .style("top", (d3.event.pageY)+"px");
                                tooltip22.html("<p>"+d+"</p>");
                          }).on("mouseout",function(d){
                              tooltip22.transition().duration(100).style("opacity",0);
                          });
        var tooltip22 = d3.select("body")
                .append('div')
                .attr("id","tooltip22");

        var clicked2=false;

        legend2svg.on('click', function(type){
            if(!clicked2){
                svg2.selectAll('circle')
                        .style('opacity', 0.05)
                        .filter(function(d){
                                return d["experiments"]==type;
                        })
                        .style('opacity', 1);
                clicked2=true;
            }
            else{
                svg2.selectAll('circle')
                        .style('opacity', 1);
                clicked2=false;
            }
        });
    }else if(pcalevel.value==="onmtbs"){
        //3rd scatterplot
        var table3 = document.getElementById("pcaofmtbs");
        var tablelength3 =table3.rows.length;

        var data3={"triplicates":[]};
        var k=0;
        for(k=0;k<tablelength3;k++){
            data3["triplicates"].push({"xel":table3.rows[k].cells[1].innerHTML,"yel":table3.rows[k].cells[2].innerHTML,"mtbs":table3.rows[k].cells[0].innerHTML});    
        }

        var newdata3=data3["triplicates"];
        
        var margin3 = {top: 20, right: 20, bottom: 20, left: 20};
        var outerWidth3 = 600,
            outerHeight3 = 600;
        var width3 = outerWidth3 - margin3.right - margin3.left;
        var height3 = outerHeight3 - margin3.top - margin3.bottom;

        var colorScale3 = d3.scaleOrdinal(d3.schemeCategory10);

        // create scale objects
        var xScale3 = d3.scaleLinear()
          .domain([-1, 1])
          .range([0, width3]);
        var yScale3 = d3.scaleLinear()
          .domain([-1, 1])
          .range([height3, 0]);

        var zoom3 = d3.zoom()
            .scaleExtent([.5, 10000])
            .extent([[0, 0], [width3, height3]])
            .on("zoom", zoomed3);

        var svg3 = d3.select('.container3').style('height', (height3 + margin3.top + margin3.bottom)+'px')
                .append('div').style('width', width3 + margin3.left + margin3.right+'px').style('height', height3 + margin3.top + margin3.bottom+'px').style('float','left')
                .append('svg')
                .attr('width', width3 + margin3.left + margin3.right)
                .attr('height', height3 + margin3.top + margin3.bottom)
                .style("background","#FFFFFF")
                .append('g')
                .attr("transform", "translate(0,0)").call(zoom3);

        // create axis objects
        var xAxis3 = d3.axisBottom(xScale3).tickSize(0);
        var yAxis3 = d3.axisLeft(yScale3).tickSize(0);

        // Draw Axis
        var gX3 = svg3.append('g')
          .attr('transform', 'translate(' + margin3.left + ',' + (margin3.top + height3/2) + ')')
          .call(xAxis3);  

        var gY3 = svg3.append('g')
          .attr('transform', 'translate(' + (margin3.left + width3/2) + ',' + margin3.top + ')')
          .call(yAxis3);

        var zoomBox3 = svg3.append('rect')
                    .attr('class', 'zoom3')
                    .attr('height', height3 + margin3.top + margin3.bottom)
                    .attr('width', width3 + margin3.left + margin3.right);

        svg3.append("text")
          .classed("label", true)
          .attr("x", width3+margin3.left)
          .attr("y", height3/2 +10)
          .style("text-anchor", "end")
          .text("PC1");

        svg3.append("text")
          .classed("label", true)
          .attr("x", width3/2 +60)
          .attr("y", margin3.top +10)
          .style("text-anchor", "end")
          .text("PC2");

        // Draw Datapoints
        var points_g3 = svg3.append("g")
          .attr('transform', 'translate(' + margin3.left + ',' + margin3.top + ')')
          .classed("points_g3", true);

        var points3 = points_g3.selectAll(".dot")
              .data(newdata3).enter().append("circle")
              .attr('r', 4)
              .attr('cx', function(d) {return xScale3(d.xel);})
              .attr('cy', function(d) {return yScale3(d.yel);})
              .style('fill', function(d){return colorScale3(d.mtbs);})
              .style('stroke', '#000')
              .style('stroke-width', 0.5)
              .on("mouseover",function(d){
                        tooltip3.transition().duration(10)
                                .style("opacity",0.8)
                                .style("left",(d3.event.pageX+20)+"px")
                                .style("top", (d3.event.pageY-35)+"px");
                        tooltip3.html("<p>Metabolite: "+d.mtbs+"</p>");
                  }).on("mouseout",function(d){
                      tooltip3.transition().duration(100).style("opacity",0);
                  });  

        var tooltip3 = d3.select("body")
                .append('div')
                .attr("id","tooltip3");

        function zoomed3() {
        // create new scale ojects based on event
            var new_xScale3 = d3.event.transform.rescaleX(xScale3);
            var new_yScale3 = d3.event.transform.rescaleY(yScale3);
        // update axes
            gX3.call(xAxis3.scale(new_xScale3));
            gY3.call(yAxis3.scale(new_yScale3));
            points3.data(newdata3)
             .attr('cx', function(d) {return new_xScale3(d.xel);})
             .attr('cy', function(d) {return new_yScale3(d.yel);});
        }

        $("#Reset3").click(() => {
            svg3.transition()
              .duration(750)
              .call(zoom3.transform, d3.zoomIdentity);
          });

        var legend3svg = d3.select(".container3")
                .append('div').style('width',225+'px').style('height',600+'px').style('overflow-y', 'scroll').style('overflow-x', 'hidden')
                .append('svg')
                .attr('width', 225)
                .attr('height', 20*newdata3.length)
                .style("background","#FFFFFF")
                .selectAll('.legend3')
                .data(colorScale3.domain())
              .enter().append("g")
                .attr("class", "legend3")
                .style("cursor", "pointer")
                .attr("transform", function(d, i) { return "translate(0," + i * 20 + ")"; });

        legend3svg
            .append("circle")
            .attr("r", 5)
            .attr("cx",5)
            .attr("cy",10)
            .style("fill",colorScale3);

        function wrap3() {
            var self = d3.select(this),
                textLength = self.node().getComputedTextLength(),
                text = self.text();
            while (textLength > (200) && text.length > 0) {
                text = text.slice(0, -1);
                self.text(text + '...');
                textLength = self.node().getComputedTextLength();
            }
        }

        legend3svg.append("text")
            .attr("x", 15)
            .attr("y",15)
            .style("font-size", "small")
            .text(function(d) { return d; }).each(wrap3)
            .on("mouseover",function(d){
                                tooltip33.transition().duration(10)
                                        .style("opacity",0.8)
                                        .style("left",(d3.event.pageX)+"px")
                                        .style("top", (d3.event.pageY)+"px");
                                tooltip33.html("<p>"+d+"</p>");
                          }).on("mouseout",function(d){
                              tooltip33.transition().duration(100).style("opacity",0);
                          });
        var tooltip33 = d3.select("body")
                .append('div')
                .attr("id","tooltip33");

        var clicked3=false;

        legend3svg.on('click', function(type){
            if(!clicked3){
                svg3.selectAll('circle')
                        .style('opacity', 0.05)
                        .filter(function(d){
                                return d["mtbs"]==type;
                        })
                        .style('opacity', 1);
                clicked3=true;
            }
            else{
                svg3.selectAll('circle')
                        .style('opacity', 1);
                clicked3=false;
            }
        });      
    }
});