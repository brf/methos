/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

$(".form_datetime").datepicker(today());

function today(){
    var d = new Date();
    var curr_date = d.getDate();
    var curr_month = d.getMonth() + 1;
    var curr_year = d.getFullYear();
    

    if (curr_month < 10) {curr_month = "0" + curr_month;}
    if (curr_date < 10) {curr_date = "0" + curr_date;}
    

    var currentDate = curr_date + "-" + curr_month + "-" + curr_year;
    $('.form_datetime').val(currentDate);
}
