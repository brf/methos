/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

$(document).ready(function() {
    
    const togglePassword = document.querySelector('#togglePassword');
    const password = document.querySelector('#password');
 
  togglePassword.addEventListener('click', function (e) {
    // toggle the type attribute
    const type = password.getAttribute('type') === 'password' ? 'text' : 'password';
    password.setAttribute('type', type);
    // toggle the eye slash icon
    this.classList.toggle('fa-eye-slash');
    });

});
var code;
    function createCaptcha() {
      //clear the contents of captcha div first 
      document.getElementById('captcha').innerHTML = "";
      var charsArray =
      "0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ@!#$%^&*";
      var lengthOtp = 6;
      var captcha = [];
      for (var i = 0; i < lengthOtp; i++) {
        //below code will not allow Repetition of Characters
        var index = Math.floor(Math.random() * charsArray.length + 1); //get the next character from the array
        if (captcha.indexOf(charsArray[index]) == -1)
          captcha.push(charsArray[index]);
        else i--;
      }
      var canv = document.createElement("canvas");
      canv.id = "captcha";
      canv.width = 140;
      canv.height = 35;
      var ctx = canv.getContext("2d");
      ctx.font = "italic 25px Georgia";
      ctx.strokeStyle = "black";
      ctx.moveTo(5, canv.height/2);
      ctx.lineTo(canv.width-5, canv.height/2);
      ctx.stroke();
      ctx.textBaseline = "middle";

      ctx.strokeText(captcha.join(""), (canv.width - 100)/2, canv.height/2);
      //storing captcha so that can validate you can save it somewhere else according to your specific requirements
      code = captcha.join("");
      document.getElementById("captcha").appendChild(canv); // adds the canvas to the body element
    }
    
    function validateCaptcha() {
        if (document.getElementById("captchabox").value == code) {
            return (true);
        }else{
            event.preventDefault();
            document.getElementById('errorcap').innerHTML = "Invalid captcha!";
            createCaptcha();
            return (false);
        }
      }