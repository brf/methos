/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

document.getElementById("Default").checked=true;

$(document).ready(function() {
    $('#protable tr:gt(0)').click(function() {
        ele = $(this).find('td input:checkbox')[0];
        ele.checked = ! ele.checked;
      });
      $('input:checkbox').click(function(e){
        e.stopPropagation();
      });
      
    var table = $('#protable').on( 'draw.dt', function () {
                $("#msgcontainer").attr("id","msgcontainerafter");
                $("#loadercontainer").css("display","none");
              } ).DataTable( {
        responsive: true,
        bLengthChange: false,
        columnDefs: [ {
            orderable: false,
            targets:   0
        } ],
        pageLength: 10,
        order: [[ 1, 'asc' ]],
        "language": {
            "info": "Showing _START_ to _END_ of _TOTAL_ experiments"
        }
        
        
    });
    
    $('#myform').on('submit', function(){
        var $form = $(this);
        // Iterate over all checkboxes in the table
        table.$('input[type="checkbox"]').each(function(){
           // If checkbox doesn't exist in DOM
           if(!$.contains(document, this)){
              // If checkbox is checked
              if(this.checked){
                 // Create a hidden element 
                 $form.append(
                    $('<input>')
                       .attr('type', 'hidden')
                       .attr('name', this.name)
                       .val(this.value)
                 );
              }
           } 
        });
    });
    
    $('#selectall').change(function(){
        var rows = table.rows({ 'search': 'applied' }).nodes();
        $( rows ).find(':checkbox').prop('checked', $(this).is(':checked'));
    });
    
} );

function expchecked(){
    var boxes = document.getElementsByName("experimentid");
    var k = 0;
    for(var i = 0; i < boxes.length; i++){
        if(boxes[i].checked===true){
            k+=1;
        }
    }
    if(k<1){
        alert("Select at least one experiment for processing.");
        return (false);
    }else{
        $('#analysisspinner').show();
        $("#disbtn").removeClass("setbtn");
        document.getElementById("disbtn").disabled=true;
        $("#myform").submit();
        $(".group").prop("disabled",true);
        return (true);
    }
}