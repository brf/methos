/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
var ongoingprocess = document.getElementById("ifongoingprocessing").value;
if(ongoingprocess==="false"){
document.getElementById("onall").checked=true;
document.getElementById("kmeans").checked=true;
document.getElementById("explvl").checked=true;
document.getElementById("omit").checked=true;
document.getElementById("ionboth").checked=true;
document.getElementById("normval").value="no";
document.getElementById('initialval').style.height="25px";
}
$(document).ready(function() {
      $('#protable tr:gt(0)').click(function(ele) {
        ele = $(this).find('td input:checkbox')[0];
        ele.checked = ! ele.checked;
      });
      $('input:checkbox').click(function(e){
        e.stopPropagation();
      });
     
    var table = $('#protable').on( 'draw.dt', function () {
                $("#msgcontainer").attr("id","msgcontainerafter");
                $("#loadercontainer").css("display","none");
              } ).DataTable( {
        responsive: true,
        bLengthChange: false,
        columnDefs: [ {
            orderable: false,
            targets:   0
        } ],
        pageLength: 10,
        "language": {
            "info": "Showing _START_ to _END_ of _TOTAL_ experiments"
        }
        
    });

    $('#myform').on('submit', function(){
        var $form = $(this);
        // Iterate over all checkboxes in the table
        table.$('input[type="checkbox"]').each(function(){
           // If checkbox doesn't exist in DOM
           if(!$.contains(document, this)){
              // If checkbox is checked
              if(this.checked){
                 // Create a hidden element 
                 $form.append(
                    $('<input>')
                       .attr('type', 'hidden')
                       .attr('name', this.name)
                       .val(this.value)
                 );
              }
           } 
        });
    });
    
    $(".group2").click(function(){
        if($("#repllvl").is(":checked") && $("#omit").is(":checked")){
            $(".grouprad").css("opacity","0.5");
            $(".group5").css("pointer-events","none");
            document.getElementById("ionboth").checked=true;            
        }else{
            $(".grouprad").css("opacity","1");
            $(".group5").css("pointer-events","auto");
        }
    });
    $(".group3").click(function(){
        if($("#repllvl").is(":checked") && $("#omit").is(":checked")){
            $(".grouprad").css("opacity","0.5");
            $(".group5").css("pointer-events","none");
            document.getElementById("ionboth").checked=true;
        }else{
            $(".grouprad").css("opacity","1");
            $(".group5").css("pointer-events","auto");
        }
    });
    
    $('#selectall').change(function(){
        var rows = table.rows({ 'search': 'applied' }).nodes();
        $( rows ).find(':checkbox').prop('checked', $(this).is(':checked'));
    });
    
    $("#disbtn").click(function(){
        var onall = document.getElementById("onall");
        var onselected = document.getElementById("onselected");
        
        var boxes = table.cells( ).nodes();
        var chboxes = $( boxes ).find(':checkbox');
        var k = 0;
        for(var i = 0; i < chboxes.length; i++){
            if(chboxes[i].checked===true){
                k++;
            }
        }
        if(onall.checked===true && k>1){
            $('#spinner1').show();
            document.getElementById("disbtn").disabled=true;
            $("#disbtn").removeClass("setbtn");
            $("#myform").submit();
            return (true);
        }else if(onselected.checked===true && k>1){
            $('#spinner2').show();
            document.getElementById("disbtn").disabled=true;
            $("#disbtn").removeClass("setbtn");
            $("#myform").submit();
            return (true);
        } else if(k<2){
            alert("Please select at least two experiments for analysis.");
            return (false);
        }
    });
    
    $('#normalizationmean').change(function () {
        if ($("#normalizationmean").prop("checked")) { // checked
            $("#normalizationorig").prop("checked",false);
            $("#normalizationmeanmtb").prop("checked",false);
            document.getElementById("normval").value="normvalmean";
        }else{ // not checked
            $("#normalizationmean").prop("checked",false);
            document.getElementById("normval").value="no";
        }
    });
    $('#normalizationorig').change(function () {
        if ($("#normalizationorig").prop("checked")) { // checked
            $("#normalizationmean").prop("checked",false);
            $("#normalizationmeanmtb").prop("checked",false);
            document.getElementById("normval").value="normvalorig";
        }else{ // not checked
            $("#normalizationorig").prop("checked",false);
            document.getElementById("normval").value="no";
        }
    });
    $('#normalizationmeanmtb').change(function () {
        if ($("#normalizationmeanmtb").prop("checked")) { // checked
            $("#normalizationorig").prop("checked",false);
            $("#normalizationmean").prop("checked",false);
            document.getElementById("normval").value="normvalmeanmtb";
        }else{ // not checked
            $("#normalizationmeanmtb").prop("checked",false);
            document.getElementById("normval").value="no";
        }
    });
    
} );
