/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


// file upload
$(document).on('change', '.file-upload', function(){
    
  var replnum=0;
  var replnames=[];
  for(k=0;k<this.files.length;k++){
      replnum++;
      replnames.push(this.files.item(k).name);
  }
  var regex=/.mzML$|.mzXML$|.mzData$|.mzml$|.mzxml$|.mzdata$/;
  var firstexten = replnames[0].match(regex);
  var sameextnum=0;
  for(l=0;l<replnames.length;l++){
    var nextexten=replnames[l].match(regex);
    if(firstexten[0]===nextexten[0]){
        sameextnum++;
    }
  }
  if(sameextnum===replnames.length){
    $(this).prev('label').text(replnum + " replicates selected");
    $(this).next('.replnum').val(replnum);
    $(this).nextAll('.replnames').val(replnames);
  }else{
    
    alert("Set of replicates must have the same extension!");
  }
});

// Dynamically add-on fields
$(function() {
    
    // Remove button click
    $(document).on(
        'click',
        '[data-role="dynamic-fields"] > .form-inline [data-role="remove"]',
        function(e) { 
            e.preventDefault();
            if(x>1){
                $(this).closest('.form-inline').remove();
                x--;
            }else if(x<2){
                $(this).find('.form-inline:not(:child)').remove();
            }
        }
    );
    var x=1;  
    // Add button click
    $(document).on(
        'click',
        '[data-role="dynamic-fields"] > .form-inline [data-role="add"]',
        function(e) {
            e.preventDefault();
            var valproid = $(".proid").val();        
            $('.allfields').append('<div class="form-inline form-row" style="padding-top:10px;padding-bottom:10px">'+
                                    '<input type="hidden" id="projectexperimentlist'+x+'.projectExperimentKey.projectid" name="projectexperimentlist['+x+'].projectExperimentKey.projectid" value = "'+valproid+'"/>'+
                                    '<input type="hidden" class="form_datetime" value="" id="projectexperimentlist'+x+'.expdate" name="projectexperimentlist['+x+'].expdate"/>'+
                                    '<input type="hidden" value="" id="projectexperimentlist'+x+'.projectExperimentKey.experimentid" name="projectexperimentlist['+x+'].projectExperimentKey.experimentid"/>'+
                                    '<input type="text" class="form-control col-sm-4" value="" id="projectexperimentlist'+x+'.expname" name="projectexperimentlist['+x+'].expname" placeholder="Name of the experiment" required=""/>'+             
                                    '<div class="col-sm-6 wrap-input-container">'+
                                        '<label class="custom-file-upload form-control">'+
                                            'Replicates<small> <i>(.mzML, .mzXML, .mzData)</i></small>'+
                                        '</label>'+
                                        '<input class="file-upload" name="file" type="file"  multiple="" required="" accept=".mzML, .mzXML, .mzData, .mzml, .mzxml, .mzdata"/>'+
                                        '<input class="replnum" value="" id="projectexperimentlist'+x+'.replicatesnum" name="projectexperimentlist['+x+'].replicatesnum" type="hidden"/>'+
                                        '<input class="replnames" value="" id="projectexperimentlist'+x+'.replicatesnames" name="projectexperimentlist['+x+'].replicatesnames" type="hidden"/>'+
                                    '</div>'+
                                    '<div class="col-sm-2">'+
                                        '<div class="row ">'+
                                        '<div class="col-sm-4" style="display: flex;justify-content: center;padding: 0px" >'+
                                            '<button class="btn btn-danger" data-role="remove">'+
                                                '<i class="fas fa-minus"></i>'+
                                            '</button>'+
                                        '</div>'+
                                        '<div class="col-sm-3" style="display: flex;justify-content: center;padding: 0px">'+
                                            '<button class="btn btn-success" data-role="add">'+
                                                '<i class="fas fa-plus"></i>'+
                                            '</button>'+
                                        '</div>'+
                                        '</div>'+
                                    '</div>'+
                                '</div>');
           
            x++;
        }
    );
});

$(document).on(
        'click', ".update",
function (){
    var d = new Date();
    var curr_date = d.getDate();
    var curr_month = d.getMonth() + 1;
    var curr_year = d.getFullYear();
    if (curr_month < 10) {curr_month = "0" + curr_month;}
    if (curr_date < 10) {curr_date = "0" + curr_date;}
    var currentDate = curr_date + "-" + curr_month + "-" + curr_year;
    $('.form_datetime').val(currentDate);
    $('#spinner').show();
    $('.alert').hide();
    document.getElementById("disbtn").disabled=true;
});
$('input').tooltip();

