/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


$(document).ready(function() {
    var projectid = document.getElementById("projectid").value;
    var experimentid=document.getElementById("experimentid").value;
    var contextpath =document.getElementById("contextpath").value;
    var columns = [
            { "data": "filename"},
            { "data": "groupid"},
             { "data": "description" },
             { "data": "adduct"},
             { "data": "mz" },
             { "data": "rt" },
             { "data": "intensity" },
             { "data": "identifier" },
             { "data": "chemical_formula" }
         ];
         
    $('#protable').on( 'draw.dt', function () {
                $("#msgcontainer").attr("id","msgcontainerafter");
                $("#loadercontainer").css("display","none");
              } ).DataTable( {
        "processing": true,
        "ajax": {
            url: contextpath+"/fetchdataview?experimentid="+experimentid+"&projectid="+projectid,
            dataSrc: function (response) {
                  if(response.data===true){
                      $("#refreshdiv").remove();
                      $('.container').append('<div>'+
                                          '<br/><br/><br/>'+
                                          '<div class="alert alert-info text-center col-sm-12 centered" style="border-radius: 5px;padding: 15px;">There is an ongoing processing taking place right now and the cluster is busy.'+
                                          '<br/>'+
                                          'Please try again in a few minutes.'+
                                          '</div>'+
                                      '</div>');
                  }
                  return response.data;
              }
        },
        "columns": columns,
        responsive: true,
        bLengthChange: true,
        columnDefs: [ {
            orderable: false,
            targets:   0
        } ],
        pageLength: 10,
        "language": {
            "info": "Showing _START_ to _END_ of _TOTAL_ metabolites",
            "loadingRecords": "&nbsp;",
            "processing": "Loading..."
      }
    } );
    
} );