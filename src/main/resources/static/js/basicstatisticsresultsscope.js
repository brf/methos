/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
$(document).ready(function() {
    var rowflag = document.getElementById("rowflag").value;
    if(rowflag==0){
        var contextpath =document.getElementById("contextpath").value;
        var tempexperiment=document.getElementById("explist"); //console.log(tempexperiment);
        var tempmetlist=document.getElementById("metlist");
        var projectid=document.getElementById("projectid").value;
        var experimentids=[];
        var metlist=[];
        for(var i=0;i<tempexperiment.rows.length;i++){
            experimentids.push(tempexperiment.rows[i].cells[0].innerHTML);
        }
        for(var i=0;i<tempmetlist.rows.length;i++){
            metlist.push(tempmetlist.rows[i].cells[0].innerHTML);
        }
        
        $('#spinner1').show();
        $('#export').attr('disabled','disabled');
        var allorcommon = document.getElementById("allorcommon").value;
        var alloreach = document.getElementById("alloreach").value;
        var ionmode = document.getElementById("ionmode").value;
        
        if(alloreach==="eachexp"){
        $.ajax({
            "url": contextpath+"/fetchbasicstatistics?experimentids="+experimentids+"&allorcommon="+allorcommon+"&projectid="+projectid+"&ionmode="+ionmode,
            type: 'GET',
            dataType: "json",
            "success": function(response){
                            if(response.data===true){
                                
                                $("#refreshdiv").remove();
                                $('.container').append('<div>'+
                                                    '<br/><br/><br/>'+
                                                    '<div class="alert alert-info text-center col-sm-12 centered" style="border-radius: 5px;padding: 15px;">There is an ongoing processing taking place right now and the cluster is busy.'+
                                                    '<br/>'+
                                                    'Please try again in a few minutes.'+
                                                    '</div>'+
                                                '</div>');
                            }else{
                                
                                var tempcolumns=[];
                                var repsize=response.data.length;
                                var expidexpnamesize = tempexperiment.rows.length;
                                var k=0;var l=0;

                                for(k=0;k<repsize;k++){
                                    for(l=0;l<expidexpnamesize;l++){
                                        if(response.data[k].experimentid===tempexperiment.rows[l].cells[0].innerHTML){
                                            response.data[k].experimentid=tempexperiment.rows[l].cells[1].innerHTML;
                                            if(tempcolumns.length===0 || (k>0 && response.data[k].experimentid!==response.data[k-1].experimentid)){ 
                                                tempcolumns.push(response.data[k].experimentid);
                                            }
                                        }

                                    }

                                }
                                function unEntity(str){
                                    return str.replace(/&amp;/g, "&");
                                 }
                                for(var i=0;i<tempcolumns.length;i++){
                                    for(var k=0;k<metlist.length;k++){
                                        var exists=0;
                                        for(var l=0;l<repsize;l++){
                                            if(response.data[l].experimentid===tempcolumns[i] && response.data[l].description===unEntity(metlist[k])){
                                                exists++;
                                                break;
                                            }
                                        }
                                        if(exists===0){
                                            response.data.push({"experimentid":tempcolumns[i],"description":unEntity(metlist[k]),"mean":"null","std":"null"});
                                        }
                                    }
                                }

                                function compare(a, b) {
                                    const firstexp = a.experimentid;
                                    const secondexp = b.experimentid;
                                    const firstdesc = a.description;
                                    const seconddesc = b.description;

                                    if (firstexp > secondexp) {
                                      return 1;
                                    } else if (firstexp < secondexp) {
                                      return -1;
                                    }
                                    if (firstdesc > seconddesc) {
                                      return 1;
                                    } else if (firstdesc < seconddesc) {
                                      return -1;
                                    }
                                    return 0;
                                  }
                                  tempcolumns.sort();
                                  response.data.sort(compare);
                                $('<thead>').append(
                                    $('<tr>').append(
                                        $('<th rowspan="2"/>').text("Metabolites")
                                    )
                                ).appendTo('#bstable');
                                $.each(tempcolumns, function(i,val){
                                    $('<th colspan="2" style="text-align: center"/>').text(val).appendTo('#bstable >thead tr');
                                });
                                $('<tr>').appendTo('#bstable >thead');
                                $.each(tempcolumns, function(i,val){
                                    $('<th >').text("Mean").appendTo('#bstable >thead tr:last');
                                    $('<th >').text("Std").appendTo('#bstable >thead tr:last'); 
                                 });
                                 
                                var l=0;
                                var desclength = response.data.length/tempcolumns.length;
                                for(var i=0;i<desclength;i++){
                                    $('<tr class="'+i+'">').append(
                                        $('<th>').text(response.data[i].description),
                                        $('<td style="background-color:#E5EEFC">').text(response.data[i].mean),
                                        $('<td style="background-color:#F8E5E4">').text(response.data[i].std) 
                                    ).appendTo('#bstable');

                                    for(var k=0;k<tempcolumns.length-1;k++){
                                        $('<td style="background-color:#E5EEFC">').text(response.data[l+desclength+desclength*k].mean).appendTo('#bstable .'+i); 
                                        $('<td style="background-color:#F8E5E4">').text(response.data[l+desclength+desclength*k].std).appendTo('#bstable .'+i); 
                                    }
                                    l++;
                                }
                                $('#bstable').DataTable({
                                    bLengthChange: false,
                                    pageLength: 10,
                                    "language": {
                                        "info": "Showing _START_ to _END_ of _TOTAL_ metabolites"
                                    }
                                });
                                $('#spinner1').remove();
                                $("#export").removeAttr("disabled");
                            
                                
                            }
                        }
        });
        }else{
            
            var table = $('#bstable').on( 'draw.dt', function () {
                $("#msgcontainer1").attr("id","msgcontainerafter1");
                $("#loadercontainer1").css("display","none");
              } ).DataTable({
                bLengthChange: true,
                pageLength: 10,
                "language": {
                    "info": "Showing _START_ to _END_ of _TOTAL_ metabolites"
                }
            });
            $("#export").removeAttr("disabled");
        }

        function download_csv(csv, filename) {
            var csvFile;
            var downloadLink;

            // CSV FILE
            csvFile = new Blob([csv], {type: "text/csv"});

            // Download link
            downloadLink = document.createElement("a");

            // File name
            downloadLink.download = filename;

            // We have to create a link to the file
            downloadLink.href = window.URL.createObjectURL(csvFile);

            // Make sure that the link is not displayed
            downloadLink.style.display = "none";

            // Add the link to your DOM
            document.body.appendChild(downloadLink);

            downloadLink.click();
        }

        function export_table_to_csv(html, filename) {
            var csv = [];
            var tempcsv=[];
            var rowshead = document.querySelectorAll("#bstable >thead tr");
            var temprowsbody = $("#bstable").DataTable();
            var rowsbody =   temprowsbody.rows({ 'search': 'applied' }).nodes();

            for(var k=0; k<rowshead.length;k++){
                var rowh=[];
                var colsh = rowshead[k].querySelectorAll("th, td");
                for(var l=0; l<colsh.length;l++){
                    rowh.push(colsh[l].innerText);
                }
                tempcsv.push(rowh);
            }
            var firstrow = tempcsv[0];

            var temprowh=[];
            temprowh.push("Metabolite");
            if(alloreach==="eachexp"){
                for(var f=1;f<firstrow.length;f++){
                    temprowh.push(firstrow[f]+"_mean");
                    temprowh.push(firstrow[f]+"_std");
                }
            }else{
                temprowh.push("Mean");
                temprowh.push("Std");
            }
            csv.push(temprowh);
            for (var i = 0; i < rowsbody.length; i++) {
                var rowb = [];
                var colsb = rowsbody[i].querySelectorAll("th, td");

                for (var j = 0; j < colsb.length; j++){
                    rowb.push(colsb[j].innerText);
                }
                csv.push(rowb.join(","));		
            }
            // Download CSV
            download_csv(csv.join("\n"), filename);
        }

        document.querySelector('input[name="export"]').addEventListener("click", function () {
            var html = document.querySelector("table").outerHTML;
                export_table_to_csv(html, "meanstd.csv");
        });

        return (true);
    }else{
        return (false);
    }
    
});