/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


$(document).ready(function() {
    $('#protable tr:gt(0)').click(function() {
        ele = $(this).find('td input:checkbox')[0];
        ele.checked = ! ele.checked;
      });
      $('input:checkbox').click(function(e){
        e.stopPropagation();
      });
    
    var allorsome = document.getElementById("allorsome");
    if(allorsome.value==="onselectedexp"){
        var table = $('#protable').on( 'draw.dt', function () {
                $("#msgcontainer").attr("id","msgcontainerafter");
                $("#loadercontainer").css("display","none");
              } ).DataTable( {
            responsive: true,
            bLengthChange: false,
            pageLength: 10,
            "language": {
                "info": "Showing _START_ to _END_ of _TOTAL_ common metabolites of selected experiments that are present at least once in each of the selected experiments"
            }

        });
    }else if(allorsome.value==="onselectedrepl"){
        var table = $('#protable').on( 'draw.dt', function () {
                $("#msgcontainer").attr("id","msgcontainerafter");
                $("#loadercontainer").css("display","none");
              } ).DataTable( {
            responsive: true,
            bLengthChange: false,
            pageLength: 10,
            "language": {
                "info": "Showing _START_ to _END_ of _TOTAL_ common metabolites that are present at least once in each replicate of the selected experiments"
            }

        });
    }else if(allorsome.value==="onselectedmtb"){
        var table = $('#protable').on( 'draw.dt', function () {
                $("#msgcontainer").attr("id","msgcontainerafter");
                $("#loadercontainer").css("display","none");
              } ).DataTable( {
            responsive: true,
            bLengthChange: false,
            pageLength: 10,
            "language": {
                "info": "Showing _START_ to _END_ of _TOTAL_ common metabolites of selected experiments that are present at least once in each of the selected experiments"
            }

        });
    }
    
    $('#myform').on('submit', function(){
        var $form = $(this);
        // Iterate over all checkboxes in the table
        table.$('input[type="checkbox"]').each(function(){
           // If checkbox doesn't exist in DOM
           if(!$.contains(document, this)){
              // If checkbox is checked
              if(this.checked){
                 // Create a hidden element 
                 $form.append(
                    $('<input>')
                       .attr('type', 'hidden')
                       .attr('name', this.name)
                       .val(this.value)
                 );
              }
           } 
        });
    });
    
    $('#selectall').change(function(){
        var rows = table.rows({ 'search': 'applied' }).nodes();
        $( rows ).find(':checkbox').prop('checked', $(this).is(':checked'));
    });
    
    
    
    $("#disbtn").click(function(){
        var boxes = table.cells( ).nodes();
        var chboxes = $( boxes ).find(':checkbox');
        var k = 0;
        for(var i = 0; i < chboxes.length; i++){
            if(chboxes[i].checked===true){
                k++;
            }
        }
        if((allorsome.value==="onselectedmtb") && (k<2 || k>120)){
            alert("For representation reasons please select from 2 to 120 metabolites for analysis.");
            return (false);
        }else if((allorsome.value==="onselectedrepl" || allorsome.value==="onselectedexp") && k<2){
            alert("Please select at least 2 metabolites for analysis.");
            return (false);
        }else{
            $(document).on('click', ".update",function (){
                    $('#analysisspinner').show();
                    document.getElementById("disbtn").disabled=true;
                    $("#disbtn").removeClass("setbtn");
                    $("#myform").submit();
                });
            return (true);
        }
    });
    
} );



