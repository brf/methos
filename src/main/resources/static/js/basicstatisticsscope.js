/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
var ongoingprocess = document.getElementById("ifongoingprocessing").value;
if(ongoingprocess==="false"){
    document.getElementById("oncommonmtbs").checked=true;
    document.getElementById("allexps").checked=true;
    document.getElementById("ionboth").checked=true;
}
$(document).ready(function() {
      $('#protable tr:gt(0)').click(function(ele) {
        ele = $(this).find('td input:checkbox')[0];
        ele.checked = ! ele.checked;
      });
      $('input:checkbox').click(function(e){
        e.stopPropagation();
      });
     
    var table = $('#protable').on( 'draw.dt', function () {
                $("#msgcontainer").attr("id","msgcontainerafter");
                $("#loadercontainer").css("display","none");
              } ).DataTable( {

        responsive: true,
        bLengthChange: false,
        columnDefs: [ {
            orderable: false,
            targets:   0
        } ],
        pageLength: 10,
        "language": {
            "info": "Showing _START_ to _END_ of _TOTAL_ experiments"
        }
        
    });


    
    
    $('#myform').on('submit', function(){
        var $form = $(this);
        // Iterate over all checkboxes in the table
        table.$('input[type="checkbox"]').each(function(){
           // If checkbox doesn't exist in DOM
           if(!$.contains(document, this)){
              // If checkbox is checked
              if(this.checked){
                 // Create a hidden element 
                 $form.append(
                    $('<input>')
                       .attr('type', 'hidden')
                       .attr('name', this.name)
                       .val(this.value)
                 );
              }
           } 
        });
    });
    $('#selectall').change(function(){
        var rows = table.rows({ 'search': 'applied' }).nodes();
        $( rows ).find(':checkbox').prop('checked', $(this).is(':checked'));
    });
    
    
    $("#disbtn").click(function(){
        
        
        var boxes = table.cells( ).nodes();
        var chboxes = $( boxes ).find(':checkbox');
        var k = 0;
        for(var i = 0; i < chboxes.length; i++){
            if(chboxes[i].checked===true){
                k++;
            }
        }
        if($("#allexps").is(":checked")){
            if(k>0){
                $('#spinner1').show();
                document.getElementById("disbtn").disabled=true;
                $("#disbtn").removeClass("setbtn");
                $("#myform").submit();
                return (true);
            }else{
                alert("Please select at least 1 experiment for analysis.");
                return (false);
            }
        }else{
            if(k>0 && k<11){
                $('#spinner1').show();
                document.getElementById("disbtn").disabled=true;
                $("#disbtn").removeClass("setbtn");
                $("#myform").submit();
                return (true);
            }else{
                alert("Please select from 1 to 10 experiments for analysis.");
                return (false);
            }
        }
        
    });
    
} );
